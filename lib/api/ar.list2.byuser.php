<?php
session_start();

include_once('../../config.php');
include_once('../funcjax.php');

if(empty($_SESSION['authenticated'])){
	// deny action

}else{
	$year = date('Y');
	$month = date('m');
	$last_day_of_month = date('t');
	$start_date = $year.'-'.$month.'-01';
	$end_date = $year.'-'.$month.'-'.$last_day_of_month;

	/*
	`fid`, `client_id`, `intrate`, `debit`, `credit`, `balance`, `extra`, `status`, `loan_type`, `voucher_id`, `term`, `amount`, `date_added`, `loan_date`, `processedby`, `cancelledby`, `addedby`, `date_approved`
	
	`client_id`, `name`, `middle_name`, `last_name`, `address`, `birthdate`, `civil_status`, `contact_num`, `sss_num`, `company`

	*/

	// $sql = "select 
	// 	`finance`.`fid`, `finance`.`client_id`, `finance`.`intrate`, `finance`.`debit`, `finance`.`credit`, `finance`.`balance`, `finance`.`extra`, `finance`.`status`, `finance`.`loan_type`, `finance`.`voucher_id`, `finance`.`term`, `finance`.`amount`, `finance`.`date_added`, `finance`.`loan_date`, `finance`.`processedby`, `finance`.`cancelledby`, `finance`.`addedby`, `finance`.`date_approved`, `client_data`.`name`, `client_data`.`middle_name`, `client_data`.`last_name`
	// from `finance` INNER JOIN `client_data` ON `finance`.`client_id` = `client_data`.`client_id` where `finance`.`loan_date` between '".$start_date." 00:00:00' and '".$end_date." 23:59:00'   order by `loan_date` desc ";

	// $sql = "select * from `finance` where `loan_date` between '".$start_date." 00:00:00' and '".$end_date." 23:59:00'   order by `loan_date` desc ";
	$s_start_date = $_POST['start_date'];
	$s_end_date = $_POST['end_date'];
	$loan_type = $_POST['loan_type'];

	if(!empty($s_start_date) && !empty($s_end_date)){

		if(empty($loan_type) || $loan_type == '-' || $loan_type == 'all'){
			$sql = "select 
				`finance`.`fid`, `finance`.`client_id`, `finance`.`intrate`, `finance`.`debit`, `finance`.`credit`, `finance`.`balance`, `finance`.`extra`, `finance`.`status`, `finance`.`loan_type`, `finance`.`voucher_id`, `finance`.`term`, `finance`.`amount`, `finance`.`date_added`, `finance`.`loan_date`, `finance`.`processedby`, `finance`.`cancelledby`, `finance`.`addedby`, `finance`.`date_approved`, `client_data`.`name`, `client_data`.`middle_name`, `client_data`.`last_name`, `finance`.`inProcFee`, `finance`.`inpFindersFee`, `finance`.`inpNotarial`, `finance`.`inpMisc`, `finance`.`inpInsurance`, `finance`.`inpOthers`, `finance`.`inpAdjustment`, `finance`.`inpBonus`
			from `finance` INNER JOIN `client_data` ON `finance`.`client_id` = `client_data`.`client_id` AND (`date_approved` BETWEEN '".$s_start_date." 00:00:00' and '".$s_end_date." 23:59:00' ) AND `finance`.`status` = 'approved' AND `finance`.`addedby` = '".$_SESSION['uuid']."' order by `finance`.`fid` desc ";
		}else{
			$sql = "select 
				`finance`.`fid`, `finance`.`client_id`, `finance`.`intrate`, `finance`.`debit`, `finance`.`credit`, `finance`.`balance`, `finance`.`extra`, `finance`.`status`, `finance`.`loan_type`, `finance`.`voucher_id`, `finance`.`term`, `finance`.`amount`, `finance`.`date_added`, `finance`.`loan_date`, `finance`.`processedby`, `finance`.`cancelledby`, `finance`.`addedby`, `finance`.`date_approved`, `client_data`.`name`, `client_data`.`middle_name`, `client_data`.`last_name`, `finance`.`inProcFee`, `finance`.`inpFindersFee`, `finance`.`inpNotarial`, `finance`.`inpMisc`, `finance`.`inpInsurance`, `finance`.`inpOthers`, `finance`.`inpAdjustment`, `finance`.`inpBonus`
			from `finance` INNER JOIN `client_data` ON `finance`.`client_id` = `client_data`.`client_id` AND (`date_approved` BETWEEN '".$s_start_date." 00:00:00' and '".$s_end_date." 23:59:00' ) AND `loan_type` = '".$loan_type."'  AND `finance`.`status` = 'approved' AND `finance`.`addedby` = '".$_SESSION['uuid']."' order by `finance`.`fid` desc ";
		}

	}else{
		$sql = "select 
			`finance`.`fid`, `finance`.`client_id`, `finance`.`intrate`, `finance`.`debit`, `finance`.`credit`, `finance`.`balance`, `finance`.`extra`, `finance`.`status`, `finance`.`loan_type`, `finance`.`voucher_id`, `finance`.`term`, `finance`.`amount`, `finance`.`date_added`, `finance`.`loan_date`, `finance`.`processedby`, `finance`.`cancelledby`, `finance`.`addedby`, `finance`.`date_approved`, `client_data`.`name`, `client_data`.`middle_name`, `client_data`.`last_name`, `finance`.`inProcFee`, `finance`.`inpFindersFee`, `finance`.`inpNotarial`, `finance`.`inpMisc`, `finance`.`inpInsurance`, `finance`.`inpOthers`, `finance`.`inpAdjustment`, `finance`.`inpBonus`
		from `finance` INNER JOIN `client_data` ON `finance`.`client_id` = `client_data`.`client_id`  AND `finance`.`status` = 'approved' AND `finance`.`addedby` = '".$_SESSION['uuid']."' order by `finance`.`fid` desc ";
	}
	// echo $sql;
	$rs = $conn->dbquery($sql);
	echo $rs;


}


?>