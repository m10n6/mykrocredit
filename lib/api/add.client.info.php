<?php
// function for adding clients
session_start();
include_once('../../config.php');
include_once('../funcjax.php');

if(empty($_SESSION['authenticated'])){
	// deny action

}else{
	// action function
	// client_data
	// - client_id
	// - name
	// - address
	// - birthdate
	// - civil_status
	// - contact_num
	// - sss_num

	// client_meta
	// - c_metaid
	// - meta 
	// - value
	// - extra

	$cid = $_POST['cid'];

	$txtName = $_POST['txtName'];
	$txtmname = $_POST['txtmname'];
	$txtlname = $_POST['txtlname'];
	$txtCivil = $_POST['txtCivil'];
	$txtDBirth = $_POST['txtDBirth'];
	$txtAddress = $_POST['txtAddress'];
	$txtContact = $_POST['txtContact'];
	$txtSSS = $_POST['txtSSS'];
	$txtCompany = $_POST['txtCompany'];
	$txtCompanyAddress = $_POST['txtCompanyAddress'];
	$txtS_name = $_POST['txtS_name'];
	$txtS_contact = $_POST['txtS_contact'];
	$txtG_name = $_POST['txtG_name'];
	$txtG_address = $_POST['txtG_address'];
	$txtG_contactnum = $_POST['txtG_contactnum'];
	$txtG_SSS = $_POST['txtG_SSS'];
	$txtCard1 = $_POST['txtCard1'];
	$txtPin1 = $_POST['txtPin1'];
	$txtCard2 = $_POST['txtCard2'];
	$txtPin2 = $_POST['txtPin2'];
	$txtCard3 = $_POST['txtCard3'];
	$txtPin3 = $_POST['txtPin3'];
	$txtCreditLimit = $_POST['txtCreditLimit'];
	$txtP_name = $_POST['txtP_name'];
	$txtP_address = $_POST['txtP_address'];
	$txtClientType = $_POST['txtClientType'];

	$txtPaymentSched = $_POST['txtPaymentSched'];

	$txtAdditionalBalance = $_POST['txtAdditionalBalance'];

	$txtInterestSchedStart = $_POST['txtInterestSchedStart'];

	$txtActualPaymentAmount = $_POST['txtActualPaymentAmount'];

	$txtBeginningBal = $_POST['txtBeginningBal'];

	$txtBegBalDate = $_POST['txtBegBalDate'];

	$txtBegBalIntRate = $_POST['txtBegBalIntRate'];

	$txtBegBalTypeRate = $_POST['txtBegBalTypeRate'];

	$txtIsInActive = $_POST['txtIsInActive'];

	$txtPaymentSelect = $_POST['txtPaymentSelect'];
	$txtInterestSchedStart1 = $_POST['txtInterestSchedStart1'];
	$txtInterestSchedStart1a = $_POST['txtInterestSchedStart1a'];
	$txtInterestSchedStart2 = $_POST['txtInterestSchedStart2'];
	$txtInterestSchedStart3 = $_POST['txtInterestSchedStart3'];
	$txtInterestSchedStart4 = $_POST['txtInterestSchedStart4'];


	if(empty($cid)){
		$sql = "select * from `client_data` 
			where 
			`company` = '".$txtCompany."' and 
			`name` = '".$txtName."' and 
			`middle_name` = '".$txtmname."' and
			`last_name` = '".$txtlname."' ";
		$rs = $conn->dbquery($sql);
		if($rs !== 'false'){
			echo "exists!";
		}else{

			$sql = "insert into `client_data` set 
				`name` = '".$txtName."', 
				`middle_name` = '".$txtmname."', 
				`last_name` = '".$txtlname."', 
				`address` = '".$txtAddress."', 
				`birthdate`  = '".$txtDBirth."', 
				`civil_status` = '".$txtCivil."', 
				`contact_num` = '".$txtContact."', 
				`sss_num` = '".$txtSSS."',
				`company` = '".$txtCompany."'
			";

			$new_user_id = $conn->dbquery($sql);

			// echo $new_user_id;

			// $meta_fields = [
			// 	"txtCompanyAddress",
			// 	"txtS_name",
			// 	"txtS_contact",
			// 	"txtG_name",
			// 	"txtG_address",
			// 	"txtG_contactnum",
			// 	"txtG_SSS",
			// 	"txtCard1",
			// 	"txtPin1",
			// 	"txtCard2",
			// 	"txtPin2",
			// 	"txtCard3",
			// 	"txtPin3",
			// 	"txtCreditLimit",
			// 	"txtP_name",
			// 	"txtP_address",
			// 	"txtClientType"
			// ];
			// foreach ($meta_fields as $key => $value) {
			// 	# code...
			// }
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtCompany', `value` = '".$txtCompany."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtCompanyAddress', `value` = '".$txtCompanyAddress."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtS_name', `value` = '".$txtS_name."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtS_contact', `value` = '".$txtS_contact."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtG_name', `value` = '".$txtG_name."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtG_address', `value` = '".$txtG_address."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtG_contactnum', `value` = '".$txtG_contactnum."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtG_SSS', `value` = '".$txtG_SSS."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtCard1', `value` = '".$txtCard1."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtPin1', `value` = '".$txtPin1."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtCard2', `value` = '".$txtCard2."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtPin2', `value` = '".$txtPin2."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtCard3', `value` = '".$txtCard3."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtPin3', `value` = '".$txtPin3."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtCreditLimit', `value` = '".$txtCreditLimit."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtP_name', `value` = '".$txtP_name."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtP_address', `value` = '".$txtP_address."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtClientType', `value` = '".$txtClientType."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtPaymentSched', `value` = '".$txtPaymentSched."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtAdditionalBalance', `value` = '".$txtAdditionalBalance."', `extra` = ''");

			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtInterestSchedStart', `value` = '".$txtInterestSchedStart."', `extra` = ''");


			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtActualPaymentAmount', `value` = '".$txtActualPaymentAmount."', `extra` = ''");
			
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtBeginningBal', `value` = '".$txtBeginningBal."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtBegBalIntRate', `value` = '".$txtBegBalIntRate."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtBegBalTypeRate', `value` = '".$txtBegBalTypeRate."', `extra` = ''");

			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtBegBalDate', `value` = '".$txtBegBalDate."', `extra` = ''");
			
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtIsInActive', `value` = '".$txtIsInActive."', `extra` = ''");

			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtPaymentSelect', `value` = '".$txtPaymentSelect."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtInterestSchedStart1', `value` = '".$txtInterestSchedStart1."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtInterestSchedStart1a', `value` = '".$txtInterestSchedStart1a."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtInterestSchedStart2', `value` = '".$txtInterestSchedStart2."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtInterestSchedStart3', `value` = '".$txtInterestSchedStart3."', `extra` = ''");
			$conn->dbquery("insert into `client_meta` set `client_id` = '".$new_user_id."', `meta` = 'txtInterestSchedStart4', `value` = '".$txtInterestSchedStart4."', `extra` = ''");
			
			
			
			
			
			if(!empty($txtBeginningBal)){

				$extra = '{ "guarantor": "", "TypeRate" : "'.$txtBegBalTypeRate.'" }' ;
				$insert = "insert into `finance` set 
						`client_id` = '".$new_user_id."', 
						`intrate` = '', 
						`debit` = '0', 
						`credit` = '".$txtBeginningBal."', 
						`balance` = '".$txtBeginningBal."', 
						`extra` = '".$extra."',  
						`status` = 'approved',
						`loan_type` = 'beg_bal', 
						`voucher_id` = '', 
						`term` = '', 
						`amount` = '".$txtBeginningBal."', 
						`date_added` = '".date('Y-m-d H:i:s')."' , 
						`loan_date` = '".date('Y-m-d H:i:s')."',
						`processedby` = '',  
						`cancelledby` = '',  
						`addedby` = '".$_SESSION['uuid']."', 
						`date_approved` = '".date('Y-m-d 00:00:00', strtotime($txtBegBalDate))."'
				";

				$conn->dbquery($insert);				
			}



			echo 'success';
		

		}

	}else{
		//update
			$sql = "update `client_data` set 
				`name` = '".$txtName."', 
				`middle_name` = '".$txtmname."', 
				`last_name` = '".$txtlname."', 
				`address` = '".$txtAddress."', 
				`birthdate`  = '".$txtDBirth."', 
				`civil_status` = '".$txtCivil."', 
				`contact_num` = '".$txtContact."', 
				`sss_num` = '".$txtSSS."',
				`company` = '".$txtCompany."'
				where
				`client_id` = '".$cid."'
			";

			$conn->dbquery($sql);

			// echo $new_user_id;

			// $meta_fields = [
			// 	"txtCompanyAddress",
			// 	"txtS_name",
			// 	"txtS_contact",
			// 	"txtG_name",
			// 	"txtG_address",
			// 	"txtG_contactnum",
			// 	"txtG_SSS",
			// 	"txtCard1",
			// 	"txtPin1",
			// 	"txtCard2",
			// 	"txtPin2",
			// 	"txtCard3",
			// 	"txtPin3",
			// 	"txtCreditLimit",
			// 	"txtP_name",
			// 	"txtP_address",
			// 	"txtClientType"
			// ];
			// foreach ($meta_fields as $key => $value) {
			// 	# code...
			// }

			$conn->dbquery("update `client_meta` set `value` = '".$txtCompany."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtCompany'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtCompanyAddress."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtCompanyAddress'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtS_name."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtS_name'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtS_contact."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtS_contact'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtG_name."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtG_name'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtG_address."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtG_address'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtG_contactnum."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtG_contactnum'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtG_SSS."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtG_SSS'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtCard1."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtCard1'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtPin1."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtPin1'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtCard2."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtCard2'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtPin2."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtPin2'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtCard3."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtCard3'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtPin3."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtPin3'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtCreditLimit."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtCreditLimit'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtP_name."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtP_name'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtP_address."', `extra` = '' where `client_id` = '".$cid."' and `meta` = 'txtP_address'");
			$conn->dbquery("update `client_meta` set `value` = '".$txtClientType."', `extra` = '' where `client_id` = '".$cid."'  and `meta` = 'txtClientType'");


			//PAYMENT SCHEDULE
			$sql1 = "select * from `client_meta` where `meta` = 'txtPaymentSched' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtPaymentSched', `value` = '".$txtPaymentSched."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtPaymentSched."', `extra` = '' where `client_id` = '".$cid."'  and `meta` = 'txtPaymentSched'");
			}

			//ADDITIONAL BALANCE
			$sql1 = "select * from `client_meta` where `meta` = 'txtAdditionalBalance' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtAdditionalBalance', `value` = '".$txtAdditionalBalance."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtAdditionalBalance."', `extra` = '' where `client_id` = '".$cid."'  and `meta` = 'txtAdditionalBalance'");
			}


			//PAYMENT START
			$sql1 = "select * from `client_meta` where `meta` = 'txtInterestSchedStart' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtInterestSchedStart', `value` = '".$txtInterestSchedStart."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtInterestSchedStart."', `extra` = '' where `client_id` = '".$cid."'  and `meta` = 'txtInterestSchedStart'");
			}
			
			

			$sql1 = "select * from `client_meta` where `meta` = 'txtActualPaymentAmount' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtActualPaymentAmount', `value` = '".$txtActualPaymentAmount."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtActualPaymentAmount."', `extra` = '' where `client_id` = '".$cid."'  and `meta` = 'txtActualPaymentAmount'");
			}
			
			$sql1 = "select * from `client_meta` where `meta` = 'txtBeginningBal' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtBeginningBal', `value` = '".$txtBeginningBal."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtBeginningBal."', `extra` = '' where `client_id` = '".$cid."'  and `meta` = 'txtBeginningBal'");
			}


			$sql1 = "select * from `client_meta` where `meta` = 'txtBegBalIntRate' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtBegBalIntRate', `value` = '".$txtBegBalIntRate."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtBegBalIntRate."', `extra` = '' where `client_id` = '".$cid."'  and `meta` = 'txtBegBalIntRate'");
			}
			
			$sql1 = "select * from `client_meta` where `meta` = 'txtBegBalTypeRate' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtBegBalTypeRate', `value` = '".$txtBegBalTypeRate."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtBegBalTypeRate."', `extra` = '' where `client_id` = '".$cid."'  and `meta` = 'txtBegBalTypeRate'");
			}
			


			$sql1 = "select * from `client_meta` where `meta` = 'txtBegBalDate' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtBegBalDate', `value` = '".$txtBegBalDate."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtBegBalDate."', `extra` = '' where `client_id` = '".$cid."'  and `meta` = 'txtBegBalDate'");
			}



			if(!empty($txtBeginningBal)){
				$sql1 = "select * from `finance` where `loan_type` = 'beg_bal' and `client_id` = '".$cid."' ";
				$rs1 = $conn->dbquery($sql1);
				// $rs1 = json_decode($rs1);
				if($rs1 == 'false'){
					// $extra = '{ "guarantor": "", "TypeRate" : "diminishing" }' ;
					$extra = '{ "guarantor": "", "TypeRate" : "'.$txtBegBalTypeRate.'" }' ;
					
					$insert = "insert into `finance` set 
							`client_id` = '".$cid."', 
							`intrate` = '".$txtBegBalIntRate."', 
							`debit` = '0', 
							`credit` = '".$txtBeginningBal."', 
							`balance` = '".$txtBeginningBal."', 
							`extra` = '".$extra."',  
							`status` = 'approved',
							`loan_type` = 'beg_bal', 
							`voucher_id` = '', 
							`term` = '', 
							`amount` = '".$txtBeginningBal."', 
							`date_added` = '".date('Y-m-d H:i:s')."' , 
							`loan_date` = '".date('Y-m-d H:i:s')."',
							`processedby` = '',  
							`cancelledby` = '',  
							`addedby` = '".$_SESSION['uuid']."', 
							`date_approved` = '".date('Y-m-d 00:00:00', strtotime($txtBegBalDate))."'
					";

					$conn->dbquery($insert);
				}else{
					$conn->dbquery("update `finance` set `credit` = '".$txtBeginningBal."',`balance` = '".$txtBeginningBal."',`intrate` = '".$txtBegBalIntRate."', `amount` = '".$txtBeginningBal."', `date_approved` = '".date('Y-m-d 00:00:00', strtotime($txtBegBalDate))."' where `client_id` = '".$cid."' and `loan_type` = 'beg_bal'");
				}
			}


			$sql1 = "select * from `client_meta` where `meta` = 'txtIsInActive' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtIsInActive', `value` = '".$txtIsInActive."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtIsInActive."' where `client_id` = '".$cid."'  and `meta` = 'txtIsInActive'");
			}
			

			// txtPaymentSelect
			$sql1 = "select * from `client_meta` where `meta` = 'txtPaymentSelect' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtPaymentSelect', `value` = '".$txtPaymentSelect."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtPaymentSelect."' where `client_id` = '".$cid."'  and `meta` = 'txtPaymentSelect'");
			}

			// txtInterestSchedStart1
			$sql1 = "select * from `client_meta` where `meta` = 'txtInterestSchedStart1' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtInterestSchedStart1', `value` = '".$txtInterestSchedStart1."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtInterestSchedStart1."' where `client_id` = '".$cid."'  and `meta` = 'txtInterestSchedStart1'");
			}

			// txtInterestSchedStart1a
			$sql1 = "select * from `client_meta` where `meta` = 'txtInterestSchedStart1a' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtInterestSchedStart1a', `value` = '".$txtInterestSchedStart1a."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtInterestSchedStart1a."' where `client_id` = '".$cid."'  and `meta` = 'txtInterestSchedStart1a'");
			}

			// txtInterestSchedStart2
			$sql1 = "select * from `client_meta` where `meta` = 'txtInterestSchedStart2' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtInterestSchedStart2', `value` = '".$txtInterestSchedStart2."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtInterestSchedStart2."' where `client_id` = '".$cid."'  and `meta` = 'txtInterestSchedStart2'");
			}

			// txtInterestSchedStart3
			$sql1 = "select * from `client_meta` where `meta` = 'txtInterestSchedStart3' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtInterestSchedStart3', `value` = '".$txtInterestSchedStart3."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtInterestSchedStart3."' where `client_id` = '".$cid."'  and `meta` = 'txtInterestSchedStart3'");
			}

			// txtInterestSchedStart4
			$sql1 = "select * from `client_meta` where `meta` = 'txtInterestSchedStart4' and `client_id` = '".$cid."' ";
			$rs1 = $conn->dbquery($sql1);
			// $rs1 = json_decode($rs1);
			if($rs1 == 'false'){
				$conn->dbquery("insert into `client_meta` set `client_id` = '".$cid."', `meta` = 'txtInterestSchedStart4', `value` = '".$txtInterestSchedStart4."', `extra` = ''");
			}else{
				$conn->dbquery("update `client_meta` set `value` = '".$txtInterestSchedStart4."' where `client_id` = '".$cid."'  and `meta` = 'txtInterestSchedStart4'");
			}

			echo 'save';
	}




}