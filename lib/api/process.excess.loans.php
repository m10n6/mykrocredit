<?php
// function for adding clients
session_start();
include_once('../../config.php');
include_once('../funcjax.php');

if(empty($_SESSION['authenticated'])){
	// deny action
	$result['message'] = 'Error: Session Expired!';
	echo json_encode($result);
}else{
	$inpHiddenUID = $_POST['inpHiddenUID'];
	$userID = $_POST['userID'];
	$selTypeLoans = $_POST['selTypeLoans'];
	$inpLoanAmount = $_POST['inpLoanAmount'];
	$selIntRate = $_POST['selIntRate'];
	$selTypeRate = $_POST['selTypeRate'];
	$inpTerm = $_POST['inpTerm'];
	$inpGuarantor = $_POST['inpGuarantor'];
	$inpBonus = $_POST['inpBonus'];
	$inpDOL = $_POST['inpDOL2'];


	// `client_id`, `intrate`, `debit`, `credit`, `balance`, `extra`, `status`, `loan_type`, `voucher_id`, `term`, `amount`, `date_added`, `processedby`, `cancelledby`, `addedby`, `date_approved`

	// txtCreditLimit
	$sql = "select * from `client_meta` where `meta` = 'txtCreditLimit' and `client_id` = '".$inpHiddenUID."'";
	$res = $conn->dbquery($sql);
	$res = json_decode($res);
	$res = json_decode($res->data[0]);
	$creditLimit = $res->value;

	$sql1 = "select * from `client_meta` where `meta` = 'txtAdditionalBalance' and `client_id` = '".$inpHiddenUID."'";
	$res1 = $conn->dbquery($sql1);
	$res1 = json_decode($res1);
	$res1 = json_decode($res1->data[0]);
	$txtAdditionalBalance = $res1->value;

	if($inpLoanAmount == 0){
		$result['message'] = 'Error: Amount is Zero (0) or Empty!';
		echo json_encode($result);
		exit();
	}


	$sql_get_last_rates = "select * from `finance` where `client_id` = '".$inpHiddenUID."' and `status` = 'approved' order by `date_approved`, `fid` desc";
	$rs_get_lr = $conn->dbquery($sql_get_last_rates);
	$rs_get_lr = json_decode($rs_get_lr);
	$rs_get_lr = json_decode($rs_get_lr->data[0]);
	$selIntRate = $rs_get_lr->intrate;
	if(empty($selIntRate)){
		$selIntRate = 0;
	}
	
	$ext_rs_get_lr = json_decode($rs_get_lr->extra);
	$selTypeRate = $ext_rs_get_lr->TypeRate;
	if(empty($selTypeRate)){
		$selTypeRate = 'diminishing';
	}

	$inpTerm = 0;

	$currBalance = str_replace(',', '', getClientBalance($inpHiddenUID));
	$bal_less_creditlim = $creditLimit - $currBalance;

	$isActive = getIsInActive($inpHiddenUID);
	$extra = '{ "guarantor": "'.$inpGuarantor.'", "TypeRate" : "'.$selTypeRate.'" }' ;
	$balance = $creditLimit - $inpLoanAmount;


	$insert = "insert into `finance` set 
			`client_id` = '".$inpHiddenUID."', 
			`intrate` = '".$selIntRate."', 
			`debit` = '0', 
			`credit` = '".$inpLoanAmount."', 
			`balance` = '".$balance."', 
			`extra` = '".$extra."',  
			`status` = 'pending',
			`loan_type` = '".$selTypeLoans."', 
			`voucher_id` = '', 
			`term` = '".$inpTerm."', 
			`amount` = '".$inpLoanAmount."', 
			`date_added` = '".date('Y-m-d H:i:s')."' , 
			`loan_date` = '".date('Y-m-d 00:00:00', strtotime($inpDOL))."',
			`processedby` = '',  
			`cancelledby` = '',  
			`addedby` = '".$userID."', 
			`date_approved` = '".date('Y-m-d 00:00:00', strtotime($inpDOL))."'
	";
	$conn->dbquery($insert);

	$result['message'] = 'success';

	echo json_encode($result);
}
?>