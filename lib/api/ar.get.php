<?php
session_start();

include_once('../../config.php');
include_once('../funcjax.php');

if(empty($_SESSION['authenticated'])){
	// deny action

}else{
	$year = date('Y');
	$month = date('m');
	$last_day_of_month = date('t');
	$start_date = $year.'-'.$month.'-01';
	$end_date = $year.'-'.$month.'-'.$last_day_of_month;

	$fid = $_POST['fid'];

	/*
	`fid`, `client_id`, `intrate`, `debit`, `credit`, `balance`, `extra`, `status`, `loan_type`, `voucher_id`, `term`, `amount`, `date_added`, `loan_date`, `processedby`, `cancelledby`, `addedby`, `date_approved`
	
	`client_id`, `name`, `middle_name`, `last_name`, `address`, `birthdate`, `civil_status`, `contact_num`, `sss_num`, `company`

	*/

	// $sql = "select 
	// 	`finance`.`fid`, `finance`.`client_id`, `finance`.`intrate`, `finance`.`debit`, `finance`.`credit`, `finance`.`balance`, `finance`.`extra`, `finance`.`status`, `finance`.`loan_type`, `finance`.`voucher_id`, `finance`.`term`, `finance`.`amount`, `finance`.`date_added`, `finance`.`loan_date`, `finance`.`processedby`, `finance`.`cancelledby`, `finance`.`addedby`, `finance`.`date_approved`, `client_data`.`name`, `client_data`.`middle_name`, `client_data`.`last_name`
	// from `finance` INNER JOIN `client_data` ON `finance`.`client_id` = `client_data`.`client_id` where `finance`.`loan_date` between '".$start_date." 00:00:00' and '".$end_date." 23:59:00'   order by `loan_date` desc ";

	// $sql = "select * from `finance` where `loan_date` between '".$start_date." 00:00:00' and '".$end_date." 23:59:00'   order by `loan_date` desc ";

	$get_vc_from_fid = "select `voucher_id` from `finance` where `fid` = '".$fid."'";
	$res_vc = $conn->dbquery($get_vc_from_fid);
	$res_vc = json_decode($res_vc);
	$res_vc = json_decode($res_vc->data[0]);
	$existing_voucher_id = $res_vc->voucher_id;

	if(empty($existing_voucher_id)){
		//get prefix
		$sqlprefix = "select * from `settings` where `meta` = 'vprefix' ";
		$res = $conn->dbquery($sqlprefix);
		$res = json_decode($res);
		$res = json_decode($res->data[0]);
		$vprefix = $res->value;

		$sqldigit = "select * from `settings` where `meta` = 'vstart' ";
		$resd = $conn->dbquery($sqldigit);
		$resd = json_decode($resd);
		$resd = json_decode($resd->data[0]);
		$vstart = $resd->value; //digit

		//update 
		// `vid`, `prefix`, `number`, `date_added`

		$getLatest = "select * from `voucher` where `prefix` = '".$vprefix."' order by `vid` desc  ";
		$res1 = $conn->dbquery($getLatest);
		$res1 = json_decode($res1);
		$res1 = json_decode($res1->data[0]);

		$vnum = $res1->code + 1;

		$insert = "insert into `voucher` set `code` = LPAD(".$vnum.", ".$vstart.", 0), `prefix` = '".$vprefix."', `is_used` = '0', `date_added` = '".date('Y-m-d H:i:s')."' ";
		$insert_id = $conn->dbquery($insert);

	// str_pad($value, 8, '0', STR_PAD_LEFT);
		// $newCode = $vprefix.'-'.str_pad($vnum, 5, '0', STR_PAD_LEFT);
		$newCode = $vprefix.''.str_pad($vnum, $vstart, '0', STR_PAD_LEFT);

		$update_finance = "update `finance` set `voucher_id` = '".$newCode."' where `fid` = '".$fid."'";
		$uprs = $conn->dbquery($update_finance);

	}

	$sql = "select 
		`finance`.`fid`, `finance`.`client_id`, `finance`.`intrate`, `finance`.`debit`, `finance`.`credit`, `finance`.`balance`, `finance`.`extra`, `finance`.`status`, `finance`.`loan_type`, `finance`.`voucher_id`, `finance`.`term`, `finance`.`amount`, `finance`.`date_added`, `finance`.`loan_date`, `finance`.`processedby`, `finance`.`cancelledby`, `finance`.`addedby`, `finance`.`date_approved`, `client_data`.`name`, `client_data`.`middle_name`, `client_data`.`last_name`, `client_data`.`company`, `finance`.`inProcFee`, `finance`.`inpFindersFee`, `finance`.`inpNotarial`, `finance`.`inpMisc`, `finance`.`inpInsurance`, `finance`.`inpOthers`,`finance`.`inpAdjustment`, `finance`.`inpBonus`
	from `finance` INNER JOIN `client_data` ON `finance`.`client_id` = `client_data`.`client_id` where `finance`.`fid` = '".$fid."' order by `loan_date` desc ";


	// echo $sql;
	$rs = $conn->dbquery($sql);


	echo $rs;
}


?>