
 <div class="row">
 	<div class="col-md-12">

 		<!-- <button class="btn btn-primary" type="button" id="btnAddNewJV"><i class="fa fa-plus"></i> New Entry </button> -->
 		<h2>No Payment</h2>

 		<hr class="hidden-print"/>

 		<div class="row hidden-print">
 			<div class="col-md-6">
 				<div class="col-md-4">
 					<div class="form-group">
 						<label>Date From:</label>
 						<input type="text" id="frmInpDstart" class="form-control">
 					</div>
 				</div>
 				<div class="col-md-4">
 					<div class="form-group">
 						<label>Date To:</label>
 						<input type="text" id="frmInpDend" class="form-control">
 					</div>
 				</div>
 				<div class="col-md-4">
 					<div class="spacer25"></div>
 					<button type="button" class="btn btn-success" id="btnLoadRange" data-id="<?php echo secure_get('cid'); ?>" ><i class="fa fa-refresh"></i> Load</button>
 				</div>
 			</div>
 			<div class="col-md-6">
 				<div class="spacer25"></div>
 				<button type="button" class="btn btn-success" id="btnReloadPage" data-id="<?php echo secure_get('cid'); ?>" ><i class="fa fa-refresh"></i> Reload Payments</button>
 				<a class="btn btn-warning pull-right  hidden-print" id="btnPrintMe" ><i class="fa fa-print"></i> Print </a>
 			</div>
 		</div>
 		<hr class="hidden-print"/>

 		<table class="table table-striped" id="tblBalanceSheet">
 			<thead>
                <tr>
                    <th>Client #</th>
                    <th>Name</th>
                    <th>Company</th>
                    <th>Amount</th>
                    <th>Paid Amount</th>

                    <!-- <th class = 'hidden-print'>Action</th> -->
                </tr>
 			</thead>
 			<!-- <tfoot>
 				<tr>
 					<td>Account Code</td>
 					<td>Account Name</td>
 					<td>Category</td>
 					<td>Action</td>
 				</tr>
 			</tfoot> -->
 			<tbody>
<!--  				<tr>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 				</tr> -->
  				<?php
 					$axn = secure_get('axn');
 					if(empty($axn)){
 						$date_start = date('Y').'-'.date('m').'-01 00:00:00';
 						$date_end = date('Y').'-'.date('m').'-'.date('t').' 23:59:59';
 						$sql_query = "select * from `finance` where `loan_type` like 'payment%' and  (`voucher_id` = '000000' or `voucher_id` = '0000000000') and 
 						(`date_approved` between '".$date_start." 00:00:00'  and '".$date_end." 23:59:59' )   order by `date_approved` asc";
 					}else{
  						$date_start = secure_get('dstart');
  						$date_end = secure_get('dend');
 						$sql_query = "select * from `finance` where `loan_type` like 'payment%' and  (`voucher_id` = '000000' or `voucher_id` = '0000000000')  and 
 						(`date_approved` between '".$date_start." 00:00:00'  and '".$date_end." 23:59:59' ) order by `date_approved` asc";
 					}

 					// $sql = "select * from `finance` where `loan_type` like 'payment%' and (`voucher_id` = '000000' or `voucher_id` = '0000000000') order by `date_approved` desc";
 					$res = $conn->dbquery($sql_query);
 					// print_r($res);
 					if($res !== 'false'){
						$res = json_decode($res);
						foreach ($res->data as $key) {
							# code...
							$nres = json_decode($key);
							$should_pay_amount = getValueByMeta($nres->client_id, 'txtActualPaymentAmount');
							$amount_paid = $nres->amount;

							$is_active = getIsInActive($nres->client_id);
							if(!empty($is_active) && $is_active == 'on'){
								$is_active = false;
							}else{
								$is_active = true;
							}

							if($amount_paid == 0 && $is_active == true){
								$totalAmount_should_pay_amount += $should_pay_amount;
								$totalAmount += $nres->amount;
								echo '
									<tr>
										<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
										<td>'.getClientName($nres->client_id, true ).'</td>
										<td>'.getClientCompany($nres->client_id).'</td>
					 					<td>'.number_format($should_pay_amount, 2).'</td>
					 					<td>'.number_format($amount_paid, 2).'</td>
									</tr>
								';									
							}

						}
 					}else{
 						echo '
			 				<tr>
			 					<td colspan="5" align="center">No Record found!</td>
			 				</tr>
 						';	
 					}
 				?>
 				<tfoot>
	 				<tr>
	 					<td></td>
	 					<td></td>
	 					<td style="font-weight: bold;">Total Amount: </td>
	 					<td style="font-weight: bold;"><?php echo number_format($totalAmount_should_pay_amount, 2); ?></td>
	 					<td style="font-weight: bold;"><?php echo number_format($totalAmount, 2); ?></td>
	 				</tr>
 				</foot>
 			</tbody>
 		</table>
 	
 	</div>

 </div>


<!-- Modal -->
<div class="modal fade" id="myJVForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Amount</h4>
      </div>
      <div class="modal-body">
          <form id="frmJV">
          	<input type="hidden" class="cl_jvnid" value="" />
          	<div class="row" id="jv_set0">
          		<div class="col-md-4">
          			<div class="col-md-6">
		              <div class="form-group">
		                  <label class="control-label">Account Code</label>
		                  <input type="text" id="jv_account_code" name="jv_account_code[]" value="" class="form-control cl_jv_account_code" />   
		                  <input type="hidden" class="cl_jv_account_id" value="" />
		              </div>
	              	</div>
	              	<div class="col-md-6">
		              <div class="form-group">
		                  <label class="control-label">Account Title</label>
		                  <input type="text" id="jv_account_title" name="jv_account_title[]" value="" class="form-control cl_jv_account_title" />   
		              </div>
	              	</div>
              	</div>

              	<div class="col-md-4">
              		<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Client Name</label>
							<input type="text" id="jv_client_name" name="jv_client_name[]" value="" class="form-control cl_jv_client_name" />   
							<input type="hidden" id="jv_client_id" name="jv_client_id[]" value="" class="cl_jv_client_id" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						  <label class="control-label">Debit</label>
						  <input type="text" id="jv_debit" name="jv_debit[]" value="" class="cl_jv_debit form-control" />   
						  <!-- <input type="hidden" id="h_id" value="" /> -->
						</div>
					</div>
              	</div>

          		<div class="col-md-4">
	              	<div class="col-md-6">
		              <div class="form-group">
		                  <label class="control-label">Credit</label>
		                  <input type="text" id="jv_credit" name="jv_credit[]" value="" class="cl_jv_credit form-control" />   
		              </div>
	              	</div>

	              	<div class="col-md-6">
		              <div class="form-group">
		                  <label class="control-label">Date Entry</label>
		                  <input type="text" id="jv_date" name="jv_date[]" value="" class="cl_jv_datetrans form-control" />   
		              </div>
	              	</div>
              	</div>
            </div>
          </form>

          	<div class="row">
          		<div class="col-md-4">
          			<div class="col-md-12">
		              <div class="form-group">
		                  <!-- <label class="control-label">Notes</label> -->
		                  <input type="text" id="jv_notes" name="jv_notes" value="" class="form-control" placeholder="Notes"/>   
		              </div>
	              	</div>
              	</div>

              	<div class="col-md-4">
              		<div class="col-md-6 text-right">
              			<strong>TOTAL:</strong>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						  <!-- <label class="control-label">Debit</label> -->
						  <input type="text" id="jv_debitTotal" name="jv_debitTotal" value="" class="form-control" readonly/>   
						  <!-- <input type="hidden" id="h_id" value="" /> -->
						</div>
					</div>
              	</div>

          		<div class="col-md-4">
	              	<div class="col-md-6">
		              <div class="form-group">
		                  <!-- <label class="control-label">Credit</label> -->
		                  <input type="text" id="jv_creditTotal" name="jv_creditTotal" value="" class="form-control" readonly/>   
		              </div>
	              	</div>

	              	<div class="col-md-6">
		              <!-- <div class="form-group">
		                  <label class="control-label">Date Entry</label>
		                  <input type="text" id="jv_date" name="jv_date[]" value="" class="form-control" />   
		              </div> -->
	              	</div>
              	</div>
            </div>

      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success pull-left" id="btnAddAccounts" style="margin-top: 0px;"><i class="fa fa-plus"></i> Add</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnSaveAmntUp" style="margin-top: 0px;"><i class="fa fa-save"></i> Save</button>
      </div>
    </div>
  </div>
</div>
<style>
@media print {
	.col-md-12 {
		width: 100% !important;
	}
	#tblPaymentSheet {
		width: 100% !important;
	}
}
</style>
<script>



$(document).ready(function(){
        $('#frmInpDstart').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_1",
          showDropdowns: true,
          "opens": "center",
          "drops": "down",
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#frmInpDend').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_1",
          showDropdowns: true,
          "opens": "center",
          "drops": "down",
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });


        $('#btnLoadRange').click(function(){
            var dstart = $('#frmInpDstart').val();
            var dend_ = $('#frmInpDend').val();
            var cid = $(this).data('id');
            window.location = 'dashboard.php?page=no_payment&axn=load&dstart=' + dstart + '&dend=' + dend_;
        });

        $('#btnReloadPage').click(function(){
            var cid = $(this).data('id');
            window.location = 'dashboard.php?page=no_payment';
        });

        $('#btnPrintMe').click(function(){
            window.print();
        });    


	// $('#tblBalanceSheet').dataTable({
 //        aLengthMenu: [
 //          [10, 25, 50, 100, -1],
 //          [10, 25, 50, 100, "All"]
 //        ],
 //        bSort: false
 //    });

    $('.cl_jv_datetrans').daterangepicker({
      singleDatePicker: true,
      calender_style: "picker_1",
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
    });

	$('#btnAddNewJV').click(function(){
		$('#myJVForm .modal-title').html('New Journal Voucher');
		$('#myJVForm').modal('show');
	});

	var jv_set_ctr = 1;
	$('#btnAddAccounts').click(function(){
		var add_jv_form = '\
		          	<div class="row" id="jv_set'+ jv_set_ctr +'">\
		          		<div class="col-md-4">\
		          			<div class="col-md-6">\
				              <div class="form-group">\
				                  <label class="control-label hideme">Account Code</label>\
				                  <input type="text" id="jv_account_code" name="jv_account_code[]" value="" class="cl_jv_account_code form-control" placeholder="Account Code" />   \
				                  <input type="hidden" class="cl_jv_account_id" value="" />\
				              </div>\
			              	</div>\
			              	<div class="col-md-6">\
				              <div class="form-group">\
				                  <label class="control-label hideme">Account Title</label>\
				                  <input type="text" id="jv_account_title" name="jv_account_title[]" value="" class="cl_jv_account_title form-control" placeholder="Account Title"/>   \
				              </div>\
			              	</div>\
		              	</div>\
		\
		              	<div class="col-md-4">\
		              		<div class="col-md-6">\
								<div class="form-group">\
									<label class="control-label hideme">Client Name</label>\
									<input type="text" id="jv_client_name" name="jv_client_name[]" value="" class="form-control cl_jv_client_name" placeholder="Client Name" />   \
									<input type="hidden" id="jv_client_id" name="jv_client_id[]" value="" class="cl_jv_client_id"/>\
								</div>\
							</div>\
							<div class="col-md-6">\
								<div class="form-group">\
								  <label class="control-label hideme">Debit</label>\
								  <input type="text" id="jv_debit" name="jv_debit[]" value="" class="cl_jv_debit form-control" placeholder="Debit" />   \
								  <!-- <input type="hidden" id="h_id" value="" /> -->\
								</div>\
							</div>\
		              	</div>\
		\
		          		<div class="col-md-4">\
			              	<div class="col-md-6">\
				              <div class="form-group">\
				                  <label class="control-label hideme">Credit</label>\
				                  <input type="text" id="jv_credit" name="jv_credit[]" value="" class="cl_jv_credit form-control" placeholder="Credit" />   \
				              </div>\
			              	</div>\
		\
			              	<div class="col-md-6">\
				              <div class="form-group">\
				                  <label class="control-label hideme">Date Entry</label>\
				                  <input type="text" id="jv_date" name="jv_date[]" value="" class="cl_jv_datetrans form-control" placeholder="Date Entry" />   \
				              </div>\
			              	</div>\
		              	</div>\
		            </div>\
		'; 


		$('#frmJV').append(add_jv_form);

	    $('.cl_jv_datetrans').daterangepicker({
	      singleDatePicker: true,
	      calender_style: "picker_1",
            locale: {
                format: 'YYYY-MM-DD'
            }
	    }, function(start, end, label) {
	      console.log(start.toISOString(), end.toISOString(), label);
	    });

	    var div = '#jv_set'+ jv_set_ctr ;
	    $('.cl_jv_account_code', div).autocomplete({
	        minChars : 2,
	        serviceUrl: 'lib/api/get.ch_account.code.php',
	        onSelect: function (suggestion) {
	            // alert('You selected: ' + suggestion.value + ', ' + suggestion.data + ', ID =' + suggestion.id);
	            // $('#divClientSearch').html('');
	            // $('#inpClientID').val(suggestion.id);
	            // $.ajax({
	            //     type : 'post',
	            //     url : 'lib/api/get.clientbalance.php',
	            //     data : {
	            //         'cid' : suggestion.id
	            //     },success: function(result){
	            //          $('#inpBalance').val(result);
	            //     }
	            // })
	         	// var closestTitle = $(this).parent().parent().parent().parent().parent().find('.cl_jv_account_title');
	         	var closestTitle = $('.cl_jv_account_title', div);
	         	closestTitle.val(suggestion.data);

	         	var closestACID = $('.cl_jv_account_id', div);
	         	closestACID.val(suggestion.id);
	        },
	        onHint: function(){
	            // $('#divClientSearch').html('');
	        },
	        onSearchStart : function(params){
	            // $('#divClientSearch').html('<div class="alert alert-info"><i class="fa fa-pulse fa-spinner"></i> Searching Client from Database...</div>');
	         	// var closestTitle = $(this).parent().parent().parent().parent().parent().find('.cl_jv_account_title');
	         	// console.log(jv_set_ctr);
	         	var closestTitle = $('.cl_jv_account_title', div);
	         	closestTitle.val('Searching Account Title...');
	        }
	    });

	    $('.cl_jv_client_name', div).autocomplete({
	        minChars : 3,
	        serviceUrl: 'lib/api/get.clientdata.php',
	        onSelect: function (suggestion) {
	            // alert('You selected: ' + suggestion.value + ', ' + suggestion.data + ', ID =' + suggestion.id);
	            // $('#divClientSearch').html('');
	            // $('#inpClientID').val(suggestion.id);
	            // $.ajax({
	            //     type : 'post',
	            //     url : 'lib/api/get.clientbalance.php',
	            //     data : {
	            //         'cid' : suggestion.id
	            //     },success: function(result){
	            //          $('#inpBalance').val(result);
	            //     }
	            // })
	         	// var closestTitle = $(this).parent().parent().parent().parent().parent().find('.cl_jv_account_title');
	         	var closestName = $('.cl_jv_client_name', div);
	         	closestName.val(suggestion.data);
	         	var closestId = $('.cl_jv_client_id', div);
	         	closestId.val(suggestion.id);
	        },
	        onHint: function(){
	            // $('#divClientSearch').html('');
	        },
	        onSearchStart : function(params){
	            // $('#divClientSearch').html('<div class="alert alert-info"><i class="fa fa-pulse fa-spinner"></i> Searching Client from Database...</div>');
	         	// var closestTitle = $(this).parent().parent().parent().parent().parent().find('.cl_jv_account_title');
	         	// console.log(jv_set_ctr);
	         	// var closestName = $('.cl_jv_client_name', div);
	         	// closestName.val('Searching Account Title...');
	        }
	    });

		// $('.jv_set').on('focus', '.cl_jv_account_code', function(){
		// 	var $this = $(this);
		// 	$(this).autocomplete({
		//         minChars : 2,
		//         serviceUrl: 'lib/api/get.ch_account.code.php',
		//         onSelect: function (suggestion) {
		//             // alert('You selected: ' + suggestion.value + ', ' + suggestion.data + ', ID =' + suggestion.id);
		//             // $('#divClientSearch').html('');
		//             // $('#inpClientID').val(suggestion.id);
		//             // $.ajax({
		//             //     type : 'post',
		//             //     url : 'lib/api/get.clientbalance.php',
		//             //     data : {
		//             //         'cid' : suggestion.id
		//             //     },success: function(result){
		//             //          $('#inpBalance').val(result);
		//             //     }
		//             // })
		//          	var closestTitle = $this.parent().parent().parent().parent().parent().find('.cl_jv_account_title');
		//          	closestTitle.val(suggestion.data);
		//         },
		//         onHint: function(){
		//             // $('#divClientSearch').html('');
		//         },
		//         onSearchStart : function(params){
		//             // $('#divClientSearch').html('<div class="alert alert-info"><i class="fa fa-pulse fa-spinner"></i> Searching Client from Database...</div>');
		//          	var closestTitle = $this.parent().parent().parent().parent().parent().find('.cl_jv_account_title');
		// 	        closestTitle.val('Searching Account Title...');
		//         }
		//     });
		// });
		jv_set_ctr++;

		// $('.cl_jv_debit', '#frmJV').blur(function(){
		// 	var sum = 0;
		// 	$(this).each(function(x){
		// 		sum += Number($(this).val());
		// 	});
		// 	$('#jv_debitTotal').val(sum);
		// });
	});

	$('#frmJV').on('blur', '.cl_jv_debit', function(){
		var sum = 0;
		$('.cl_jv_debit', '#frmJV').each(function(x){
			sum += Number($(this).val());
		});
		$('#jv_debitTotal').val(sum.toFixed(2));
	});

	$('#frmJV').on('blur', '.cl_jv_credit', function(){
		var sum = 0;
		$('.cl_jv_credit', '#frmJV').each(function(x){
			sum += Number($(this).val());
		});
		$('#jv_creditTotal').val(sum.toFixed(2));
	});
	// $('.jv_set .cl_jv_account_code').keyup(function(){
	// 	console.log($(this).val());
	// 	var closestTitle = $(this).parent().parent().parent().parent().parent().children('.cl_jv_account_title');//.
	// 	closestTitle.val($(this).val());
	// 	console.log(closestTitle);
	// });

	// $('.jv_set').on('focus', '.cl_jv_account_code', function(){
	// 	var $this = $(this);
	// 	$(this).autocomplete({
	//         minChars : 2,
	//         serviceUrl: 'lib/api/get.ch_account.code.php',
	//         onSelect: function (suggestion) {
	//             // alert('You selected: ' + suggestion.value + ', ' + suggestion.data + ', ID =' + suggestion.id);
	//             // $('#divClientSearch').html('');
	//             // $('#inpClientID').val(suggestion.id);
	//             // $.ajax({
	//             //     type : 'post',
	//             //     url : 'lib/api/get.clientbalance.php',
	//             //     data : {
	//             //         'cid' : suggestion.id
	//             //     },success: function(result){
	//             //          $('#inpBalance').val(result);
	//             //     }
	//             // })
	//          	var closestTitle = $this.parent().parent().parent().parent().parent().find('.cl_jv_account_title');
	//          	closestTitle.val(suggestion.data);
	//         },
	//         onHint: function(){
	//             // $('#divClientSearch').html('');
	//         },
	//         onSearchStart : function(params){
	//             // $('#divClientSearch').html('<div class="alert alert-info"><i class="fa fa-pulse fa-spinner"></i> Searching Client from Database...</div>');
	//          	var closestTitle = $this.parent().parent().parent().parent().parent().find('.cl_jv_account_title');
	// 	        closestTitle.val('Searching Account Title...');
	//         }
	//     });
	// });

    $('#jv_set0 .cl_jv_account_code').autocomplete({
        minChars : 2,
        serviceUrl: 'lib/api/get.ch_account.code.php',
        onSelect: function (suggestion) {
            // alert('You selected: ' + suggestion.value + ', ' + suggestion.data + ', ID =' + suggestion.id);
            // $('#divClientSearch').html('');
            // $('#inpClientID').val(suggestion.id);
            // $.ajax({
            //     type : 'post',
            //     url : 'lib/api/get.clientbalance.php',
            //     data : {
            //         'cid' : suggestion.id
            //     },success: function(result){
            //          $('#inpBalance').val(result);
            //     }
            // })
         	// var closestTitle = $(this).parent().parent().parent().parent().parent().find('.cl_jv_account_title');
         	var closestTitle = $('#jv_set0 .cl_jv_account_title');
         	closestTitle.val(suggestion.data);

         	var closestACID = $('#jv_set0 .cl_jv_account_id');
         	closestACID.val(suggestion.id);
        },
        onHint: function(){
            // $('#divClientSearch').html('');
        },
        onSearchStart : function(params){
            // $('#divClientSearch').html('<div class="alert alert-info"><i class="fa fa-pulse fa-spinner"></i> Searching Client from Database...</div>');
         	// var closestTitle = $(this).parent().parent().parent().parent().parent().find('.cl_jv_account_title');
         	var closestTitle = $('#jv_set0 .cl_jv_account_title');
	        closestTitle.val('Searching Account Title...');
        }
    });

    $('#jv_set0 .cl_jv_client_name').autocomplete({
        minChars : 3,
        serviceUrl: 'lib/api/get.clientdata.php',
        onSelect: function (suggestion) {
            // alert('You selected: ' + suggestion.value + ', ' + suggestion.data + ', ID =' + suggestion.id);
            // $('#divClientSearch').html('');
            // $('#inpClientID').val(suggestion.id);
            // $.ajax({
            //     type : 'post',
            //     url : 'lib/api/get.clientbalance.php',
            //     data : {
            //         'cid' : suggestion.id
            //     },success: function(result){
            //          $('#inpBalance').val(result);
            //     }
            // })
         	// var closestTitle = $(this).parent().parent().parent().parent().parent().find('.cl_jv_account_title');
         	var closestName = $('#jv_set0 .cl_jv_client_name');
         	closestName.val(suggestion.data);
         	var closestId = $('#jv_set0 .cl_jv_client_id');
         	closestId.val(suggestion.id);
        },
        onHint: function(){
            // $('#divClientSearch').html('');
        },
        onSearchStart : function(params){
            // $('#divClientSearch').html('<div class="alert alert-info"><i class="fa fa-pulse fa-spinner"></i> Searching Client from Database...</div>');
         	// var closestTitle = $(this).parent().parent().parent().parent().parent().find('.cl_jv_account_title');
         	// console.log(jv_set_ctr);
         	// var closestName = $('#jv_set0 .cl_jv_client_name');
         	// closestName.val('Searching Account Title...');
        }
    });


	$('#btnSaveAmntUp').click(function(){
		// alert('asdasd');
		// alert('Journal Voucher Saved!');
		var acc_code = Array();
		var acc_cname = Array();
		var acc_caid = Array();
		var client_id = Array();
		var client_name = Array();

		var debits = Array();
		var credits = Array();
		var date_trans = Array();

		var errctr = 0;
		var err_msg = '';

		$('.cl_jv_account_code').each(function(x){
			acc_code[x] = $(this).val();
			if(acc_code[x] == ''){
				errctr++;
				err_msg += 'Please Account Code!\n';
				return false;
			}
		});

		$('.cl_jv_account_id').each(function(x){
			acc_caid[x] = $(this).val();
			if(acc_caid[x] == ''){
				errctr++;
				err_msg += 'Please Enter again Account Code, Retrieving ID Error!\n';
				return false;
			}
		});

		$('.cl_jv_account_title').each(function(x){
			acc_cname[x] = $(this).val();
			if(acc_cname[x] == ''){
				errctr++;
				err_msg += 'Please Enter again Account Code, Retrieving Account Name Error!\n';
				return false;
			}
		});
	
		$('.cl_jv_client_id').each(function(x){
			client_id[x] = $(this).val();
			if(client_id[x] == ''){
				errctr++;
				err_msg += 'Please Enter again Client Name, Retrieving ID Error!\n';
				return false;
			}
		});

		$('.cl_jv_client_name').each(function(x){
			client_name[x] = $(this).val();
			if(client_name[x] == ''){
				errctr++;
				err_msg += 'Please Enter Client Name!\n';
				return false;
			}
		});

		$('.cl_jv_debit').each(function(x){
			debits[x] = $(this).val();
			if(debits[x] == ''){
				errctr++;
				err_msg += 'Please Enter Debit Value!\n';
				return false;
			}
		});

		$('.cl_jv_credit').each(function(x){
			credits[x] = $(this).val();
			if(credits[x] == ''){
				errctr++;
				err_msg += 'Please Enter Credit Value!\n';
				return false;
			}
		});
	
		$('.cl_jv_datetrans').each(function(x){
			date_trans[x] = $(this).val();
			if(date_trans[x] == ''){
				errctr++;
				err_msg += 'Please Enter Date Entry!\n';
				return false;
			}
		});

		var notes = $('#jv_notes').val();

		// console.log(acc_code);
		// console.log(acc_cname);
		if(errctr == 0){
			$.ajax({
				type : 'post',
				url  : 'lib/api/jv.save.php',
				data : {
					'acc_caid' : acc_caid,
					// 'acc_cname' : acc_cname,
					'client_id' : client_id,
					'debits' : debits,
					'credits' : credits,
					'notes' : notes,
					'date_trans' : date_trans
				},
				beforeSend: function(){
					$('#btnSaveAmntUp').attr('disabled', true);
					$('#btnSaveAmntUp').html('<i class="fa fa-cog fa-spinner"></i>Saving...');
				},
				success: function(result){
					console.log(result);
					$('#btnSaveAmntUp').removeAttr('disabled');
					$('#btnSaveAmntUp').html('<i class="fa fa-save"></i> Save');
					window.location.reload();
				},
				error: function(err, errmsg){
					$('#btnSaveAmntUp').removeAttr('disabled');
					$('#btnSaveAmntUp').html('<i class="fa fa-save"></i> Save');
					alert('Error: Please contact developer!');
				}
			});			
		}else{
			alert(err_msg);
			return false;
		}

	});
});
</script>

<style>
.modal-lg {
	width: 1200px;
}

.hideme {
	display: none !important; 
}

.show-calendar {
	z-index: 99999;
}

.prev.available > .icon {
	background-image: none !important;
}

.next.available > .icon {
	background-image: none !important;
}
</style>