    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/js/moment/moment.min.js"></script>
    <script src="vendors/js/datepicker/daterangepicker.js"></script>

    <!-- bootstrap progress js -->
    <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>

    <!-- iCheck -->
    <script src="vendors/iCheck/icheck.min.js"></script>

    <!-- Datatables-->
    <script src="includes/js/datatables/jquery.dataTables.min.js"></script>
    <script src="includes/js/datatables/dataTables.bootstrap.js"></script>
    <script src="includes/js/datatables/dataTables.buttons.min.js"></script>
    <script src="includes/js/datatables/buttons.bootstrap.min.js"></script>
    <script src="includes/js/datatables/jszip.min.js"></script>
    <script src="includes/js/datatables/pdfmake.min.js"></script>
    <script src="includes/js/datatables/vfs_fonts.js"></script>
    <script src="includes/js/datatables/buttons.html5.min.js"></script>
    <script src="includes/js/datatables/buttons.print.min.js"></script>
    <script src="includes/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="includes/js/datatables/dataTables.keyTable.min.js"></script>
    <script src="includes/js/datatables/dataTables.responsive.min.js"></script>
    <script src="includes/js/datatables/responsive.bootstrap.min.js"></script>
    <script src="includes/js/datatables/dataTables.scroller.min.js"></script>

    <!-- Growl -->
    <script src="includes/js/jquery.growl/javascripts/jquery.growl.js"></script>
    <link href="includes/js/jquery.growl/stylesheets/jquery.growl.css" rel="stylesheet" type="text/css">

    <!-- autocomplete -->
    <script src="includes/js/autocomplete/jquery.autocomplete.js"></script>

    <script src="includes/js/custom.js"></script>
  <script>
    $(window).load(function() {

   
    });

    // String.prototype.capitalize = function() {
    //     return this.charAt(0).toUpperCase() + this.slice(1);
    // }

    // function toTitleCase(str){
    //     return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    // }

    var dt_table, dt_table1;
    var checkTimer;
    var errctr = 0;

        /*=============================================================*/
      function deleteClientProfile(cid){
         var q = confirm('Are you sure you want to delete Client?');
         //var cid = $(this).data('value');
         if(q){
            $.ajax({
                type: 'post',
                url : 'lib/api/client.delete.php',
                data : {
                    'cid' : cid
                },
                beforeSend : function(){
                    $('body').addClass('mykroloading');
                },
                success: function(){
                    $('body').removeClass('mykroloading');
                    dt_table.fnDestroy();
                    dt_table1.fnDestroy();
                    plot_clientlist_table();
                    growlme('Client has been deleted!', '','notice');
                }
            });
         }

      }

      function editClientProfile(cid){
            // alert('asdasd')
            // alert(cid)

            //var cid = $(this).data('value');
            // alert(cid);
            $.ajax({
                type: 'post',
                url : 'lib/api/client.getdata.php',
                data : {
                    'cid' : cid
                },
                dataType : 'json',
                error: function(err,errmsg){
                    console.log(errmsg)
                },
                beforeSend: function(){
                    $('body').addClass('mykroloading');
                },
                success : function(result){
                    console.log(result)
                    var cdata = JSON.parse(result.cdata);
                    console.log(cdata);
                    var cmeta = result.cmeta;
                    console.log(result.cmeta);
                    $('#txtCID').val(cdata.client_id);
                    $('#txtName').val(cdata.name);
                    $('#txtmname').val(cdata.middle_name);
                    $('#txtlname').val(cdata.last_name);
                    $('#txtCivil').val(cdata.civil_status);
                    $('#txtDBirth').val(cdata.birthdate);
                    $('#txtAddress').val(cdata.address);
                    $('#txtContact').val(cdata.contact_num);
                    $('#txtSSS').val(cdata.sss_num);
                    $('#txtCompany').val(cmeta.txtCompany);
                    $('#txtCompanyAddress').val(cmeta.txtCompanyAddress);
                    $('#txtS_name').val(cmeta.txtS_name);
                    $('#txtS_contact').val(cmeta.txtS_contact);
                    $('#txtG_name').val(cmeta.txtG_name);
                    $('#txtG_address').val(cmeta.txtG_address);
                    $('#txtG_contactnum').val(cmeta.txtG_contactnum);
                    $('#txtG_SSS').val(cmeta.txtG_SSS);
                    $('#txtCard1').val(cmeta.txtCard1);
                    $('#txtPin1').val(cmeta.txtPin1);
                    $('#txtCard2').val(cmeta.txtCard2);
                    $('#txtPin2').val(cmeta.txtPin2);
                    $('#txtCard3').val(cmeta.txtCard3);
                    $('#txtPin3').val(cmeta.txtPin3);
                    $('#txtCreditLimit').val(cmeta.txtCreditLimit);
                    $('#txtP_name').val(cmeta.txtP_name);
                    $('#txtP_address').val(cmeta.txtP_address);
                    $('#txtClientType').val(cmeta.txtClientType);

                    $('#spnCurrentBal').html(cmeta.currBalance);
                    $('#txtPaymentSched').val(cmeta.txtPaymentSched);

                    $('#txtAdditionalBalance').val(cmeta.txtAdditionalBalance);

                    $('#txtInterestSchedStart').val(cmeta.txtInterestSchedStart);

                    $('#spnTotalLimit').html(cmeta.totalCreditLimit);

                    // var loanables = Number(cmeta.totalCreditLimit) - Number(cmeta.currBalance);
                    $('#spnLoanable').html(cmeta.loanableAmount);

                    $('#txtActualPaymentAmount').val(cmeta.txtActualPaymentAmount);

                    $('#txtBeginningBal').val(cmeta.txtBeginningBal);
                    $('#txtBegBalIntRate').val(cmeta.txtBegBalIntRate);

                    $('#txtBegBalTypeRate').val(cmeta.txtBegBalTypeRate);

                    $('#txtBegBalDate').val(cmeta.txtBegBalDate);

                    $('#txtIsInActive').val(cmeta.txtIsInActive);
                    if(cmeta.txtIsInActive == 'on'){
                        $('#chkIsInActive').iCheck('check');
                    }else{
                        $('#chkIsInActive').iCheck('uncheck');
                        $('#txtIsInActive').val('off');
                    }

                    $('#div_once_month').hide();
                    $('#div_twice_month').hide();
                    $('#div_weekly').hide();

                    $('#txtPaymentSelect').val(cmeta.txtPaymentSelect);
                    // setTimeout(function(){
                    if(cmeta.txtInterestSchedStart1 !== null){
                        $('#txtInterestSchedStart1').val(cmeta.txtInterestSchedStart1);    
                    }else{
                        $('#txtInterestSchedStart1').val('-');
                    }
                    
                    if(cmeta.txtInterestSchedStart1a !== null){
                        $('#txtInterestSchedStart1a').val(cmeta.txtInterestSchedStart1a);    
                    }else{
                        $('#txtInterestSchedStart1a').val('-');
                    }

                    if(cmeta.txtInterestSchedStart2 !== null){
                        $('#txtInterestSchedStart2').val(cmeta.txtInterestSchedStart2);    
                    }else{
                        $('#txtInterestSchedStart2').val('-');
                    }

                    if(cmeta.txtInterestSchedStart3 !== null){
                        $('#txtInterestSchedStart3').val(cmeta.txtInterestSchedStart3);    
                    }else{
                        $('#txtInterestSchedStart3').val('-');
                    }

                    if(cmeta.txtInterestSchedStart4 !== null){
                        $('#txtInterestSchedStart4').val(cmeta.txtInterestSchedStart4);    
                    }else{
                        $('#txtInterestSchedStart4').val('-');
                    }
                    // },100)

                    console.log(cmeta.txtInterestSchedStart1);
                    if(cmeta.txtPaymentSelect !== null){
                        switch(cmeta.txtPaymentSelect){
                            case '-':
                                // $('#div_once_month').hide();
                                // $('#div_twice_month').hide();
                                // $('#div_weekly').hide();
                                $('#div_psched').hide();
                                $('#div_psched #div_psched1 #txtInterestSchedStart1').hide();
                                $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                                $('#div_psched #div_psched2').hide();
                                $('#div_psched #div_psched3').hide();
                                $('#div_psched #div_psched4').hide();
                            break;
                            case '1': //ONCE A MONTH
                                $('#div_psched').show();
                                $('#div_psched #div_psched1 #txtInterestSchedStart1').show();
                                $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                                $('#div_psched #div_psched2').hide();
                                $('#div_psched #div_psched3').hide();
                                $('#div_psched #div_psched4').hide();
                                // $('#div_once_month').show();
                                // $('#div_twice_month').hide();
                                // $('#div_weekly').hide();
                            break;
                            case '2': // TWICE A MONTH
                                // $('#div_once_month').hide();
                                // $('#div_twice_month').show();
                                // $('#div_weekly').hide();
                                $('#div_psched').show();
                                $('#div_psched #div_psched1 #txtInterestSchedStart1').show();
                                $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                                $('#div_psched #div_psched2').show();
                                $('#div_psched #div_psched3').hide();
                                $('#div_psched #div_psched4').hide();
                            break;
                            case '3': // WEEKLY
                                // $('#div_once_month').hide();
                                // $('#div_twice_month').hide();
                                // $('#div_weekly').show();
                                $('#div_psched').show();
                                $('#div_psched #div_psched1 #txtInterestSchedStart1').show();
                                $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                                $('#div_psched #div_psched2').show();
                                $('#div_psched #div_psched3').show();
                                $('#div_psched #div_psched4').show();
                            break;
                            default:
                                $('#div_psched').hide();
                                $('#div_psched #div_psched1 #txtInterestSchedStart1').hide();
                                $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                                $('#div_psched #div_psched2').hide();
                                $('#div_psched #div_psched3').hide();
                                $('#div_psched #div_psched4').hide();
                                // $('#div_once_month').hide();
                                // $('#div_twice_month').hide();
                                // $('#div_weekly').hide();
                            break;
                        }
                    }else{
                        $('#txtPaymentSelect').val('-');
                        $('#txtInterestSchedStart1').val('-');
                        $('#txtInterestSchedStart1a').val('-');
                        $('#txtInterestSchedStart2').val('-');
                        $('#txtInterestSchedStart3').val('-');
                        $('#txtInterestSchedStart4').val('-');
                        $('#div_psched').hide();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1').hide();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                        $('#div_psched #div_psched2').hide();
                    }

                    


                    $('#modClient .modal-title').html('Edit Profile');
                    $('#modClient').modal('show');
                    $('body').removeClass('mykroloading');
                }
            });
            

      }

      

      function processClientLoans(dataID){
        errctr = 0;

        //var dataID = $(this).data('value');
        $('#inpHiddenUID').val(dataID);
        $('#frmLoans1').show();
        $('#frmLoans2').hide();
        $('#frmExcess').hide();
        $('#dispClientName').html('');
        $.ajax({
            type : 'post',
            url  : 'lib/api/checkuser.newloans.php',
            dataType: 'json',
            data : {
                'uid' : $('#inpHiddenUID').val()
            },
            beforeSend: function(){
                $('body').addClass('mykroloading');
            },
            success: function(result){
                $('body').removeClass('mykroloading');
                console.log(result);
                var selhtml;
                if(!result){
                    selhtml = '\
                            <option value="new_loans">New Loan</option>\
                            <option value="cash_advance">Cash Advance</option>\
                            <option value="special_account">Special Account</option>\
                            <option value="additional_loan">Additional Loan</option>\
                            <option value="renewal">Renewal</option>\
                            <option value="excess">Excess</option>\
                        ';
                }else{
                    selhtml = '\
                            <option value="cash_advance">Cash Advance</option>\
                            <option value="special_account">Special Account</option>\
                            <option value="additional_loan">Additional Loan</option>\
                            <option value="renewal">Renewal</option>\
                            <option value="excess">Excess</option>\
                        ';
                }
                $('#selTypeLoans').html(selhtml);
                $('#modLoans').modal('show');        
            }
        });
        
      }
        /*=============================================================*/



    function loadClientList(callback){
        $.ajax({
            type: 'post',
            // url : 'lib/api/client.list.php',
            url : 'lib/api/client.list.active.php',
            dataType: 'json',
            data : {

            },
            beforeSend: function() {
                var html = '\
                <tr>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                </tr>\
                ';
                $('#tblClientList tbody').html(html);
            },
            success : function(x) {
                // alert(x);
                if(callback){
                    callback(x);
                }
            },
            error: function(){
                alert('Please contact developer!');
            }
        });
    }

    function loadClientListInActive(callback){
        $.ajax({
            type: 'post',
            // url : 'lib/api/client.list.php',
            url : 'lib/api/client.list.inactive.php',
            dataType: 'json',
            data : {

            },
            beforeSend: function() {
                var html = '\
                <tr>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                </tr>\
                ';
                $('#tblClientListInActive tbody').html(html);
            },
            success : function(x) {
                // alert(x);
                if(callback){
                    callback(x);
                }
            },
            error: function(){
                alert('Please contact developer!');
            }
        });
    }


    function plot_clientlist_table(){
      
      loadClientList(function(result){
        // console.log(result.data);
            var html = '';
            if(result){
                var len = result.data.length;
                // console.log(len);
                
                for(var i = 0; i < len; i++){
                    var obj = JSON.parse(result.data[i]);
                    // var bal = '<?php echo getClientBalance('+ obj.client_id +'); ?>';
                    if(obj.is_active == '' || obj.is_active == 'off' || obj.is_active == null){

                        html += '\
                            <tr>\
                                <td>'+ obj.client_id +'<!--<input type="checkbox" class="" data-value="'+ obj.client_id +'" id="cls_check">--></td>\
                                <td class="td_name"><strong>'+ obj.last_name +', '+obj.name + ' ' + obj.middle_name +'.</strong><br>\
                                <div class="" style="padding-top: 10px;">\
                                <?php if($_SESSION['ulevel'] != '3'):  ?>
                                <a href="#" class="btn btn-primary btn-xs cls_procar" data-value="'+ obj.client_id +'" onclick="processClientLoans(\''+ obj.client_id +'\'); return false;" >Process Loans</a>\
                                <?php endif; ?>
                                <a href="dashboard.php?page=client_payment&cid='+ obj.client_id +'" class="btn btn-success btn-xs cls_payment" data-value="'+ obj.client_id +'" >Payment</a>\
                                <a href="dashboard.php?page=viewdata&cid='+ obj.client_id +'" class="btn btn-warning btn-xs cls_viewdata" data-value="'+ obj.client_id +'" >View Data</a>\
                                <a href="#" class="btn btn-info btn-xs cls_editprofile" data-value="'+ obj.client_id +'"  onclick="editClientProfile(\''+ obj.client_id +'\'); return false;" >Edit Profile</a>\
                                <a href="#" class="btn btn-danger btn-xs cls_deleprofile" data-value="'+ obj.client_id +'"  onclick="deleteClientProfile(\''+ obj.client_id +'\'); return false;" >Delete</a>\
                                </div>\
                                </td>\
                                <td>' + obj.contact_num +'</td>\
                                <td>' + obj.address +'</td>\
                                <td>' + obj.company +'</td>\
                                <td><!--<button class="btn btn-success" data-cid="'+ obj.client_id +'"><i class="fa fa-bar-chart"></i> View Balance</button>--></td>\
                            </tr>\
                        ';                    
                    }



                }
                
                // var obj = JSON.parse(result)            
            }else{

                html = '\
                    <tr>\
                        <td></td>\
                        <td class="td_name"></td>\
                        <td></td>\
                        <td></td>\
                        <td></td>\
                        <td></td>\
                    </tr>\
                ';
            }
            $('#tblClientList tbody').html(html);
            dt_table = $('#tblClientList').dataTable({
                aLengthMenu: [
                  [10, 25, 50, 100, -1],
                  [10, 25, 50, 100, "All"]
                ]
            });
      });    

      loadClientListInActive(function(result){
        // console.log(result.data);
            var html = '';
            if(result){
                var len = result.data.length;
                // console.log(len);
                
                for(var i = 0; i < len; i++){
                    var obj = JSON.parse(result.data[i]);
                    // var bal = '<?php echo getClientBalance('+ obj.client_id +'); ?>';
                    if(obj.is_active == 'on'){
                        // html += '\
                        //     <tr>\
                        //         <td>'+ obj.client_id +'<!--<input type="checkbox" class="" data-value="'+ obj.client_id +'" id="cls_check">--></td>\
                        //         <td class="td_name"><strong>'+ obj.last_name +', '+obj.name + ' ' + obj.middle_name +'.</strong><br>\
                        //         <div class=""><a href="#" class="cls_procar" data-value="'+ obj.client_id +'" onclick="processClientLoans(\''+ obj.client_id +'\'); return false;" >Process Loans</a> | \
                        //         <a href="dashboard.php?page=client_payment&cid='+ obj.client_id +'" class="cls_payment" data-value="'+ obj.client_id +'" >Payment</a> | \
                        //         <a href="dashboard.php?page=viewdata&cid='+ obj.client_id +'" class="cls_viewdata" data-value="'+ obj.client_id +'" >View Data</a> | \
                        //         <br/><a href="#" class="cls_editprofile" data-value="'+ obj.client_id +'" onclick="editClientProfile(\''+ obj.client_id +'\'); return false;" >Edit Profile</a> | \
                        //         <a href="#" class="cls_deleprofile" data-value="'+ obj.client_id +'" onclick="deleteClientProfile(\''+ obj.client_id +'\'); return false;" >Delete</a>\
                        //         </div>\
                        //         </td>\
                        //         <td>' + obj.contact_num +'</td>\
                        //         <td>' + obj.address +'</td>\
                        //         <td>' + obj.company +'</td>\
                        //         <td><!--<button class="btn btn-success" data-cid="'+ obj.client_id +'"><i class="fa fa-bar-chart"></i> View Balance</button>--></td>\
                        //     </tr>\
                        // ';       

                        html += '\
                            <tr>\
                                <td>'+ obj.client_id +'<!--<input type="checkbox" class="" data-value="'+ obj.client_id +'" id="cls_check">--></td>\
                                <td class="td_name"><strong>'+ obj.last_name +', '+obj.name + ' ' + obj.middle_name +'.</strong><br>\
                                <div class="" style="padding-top: 10px;">\
                                <?php if($_SESSION['ulevel'] != '3'):  ?>
                                <a href="#" class="btn btn-primary btn-xs cls_procar" data-value="'+ obj.client_id +'" onclick="processClientLoans(\''+ obj.client_id +'\'); return false;" >Process Loans</a>\
                                <?php endif; ?>
                                <a href="dashboard.php?page=client_payment&cid='+ obj.client_id +'" class="btn btn-success btn-xs cls_payment" data-value="'+ obj.client_id +'" >Payment</a>\
                                <a href="dashboard.php?page=viewdata&cid='+ obj.client_id +'" class="btn btn-warning btn-xs cls_viewdata" data-value="'+ obj.client_id +'" >View Data</a>\
                                <a href="#" class="btn btn-info btn-xs cls_editprofile" data-value="'+ obj.client_id +'"  onclick="editClientProfile(\''+ obj.client_id +'\'); return false;" >Edit Profile</a>\
                                <a href="#" class="btn btn-danger btn-xs cls_deleprofile" data-value="'+ obj.client_id +'"  onclick="deleteClientProfile(\''+ obj.client_id +'\'); return false;" >Delete</a>\
                                </div>\
                                </td>\
                                <td>' + obj.contact_num +'</td>\
                                <td>' + obj.address +'</td>\
                                <td>' + obj.company +'</td>\
                                <td><!--<button class="btn btn-success" data-cid="'+ obj.client_id +'"><i class="fa fa-bar-chart"></i> View Balance</button>--></td>\
                            </tr>\
                        '; 
                    }

                }
                
                // var obj = JSON.parse(result)            
            }else{

                html = '\
                    <tr>\
                        <td></td>\
                        <td class="td_name"></td>\
                        <td></td>\
                        <td></td>\
                        <td></td>\
                        <td></td>\
                    </tr>\
                ';
            }
            $('#tblClientListInActive tbody').html(html);
            dt_table1 = $('#tblClientListInActive').dataTable({
                aLengthMenu: [
                  [10, 25, 50, 100, -1],
                  [10, 25, 50, 100, "All"]
                ]
            });
      });  
    }

    function unhideLoansFields(){
        $('#inpLoanAmount').parent().removeClass('hideme');
        $('#selIntRate').parent().removeClass('hideme');
        $('#selTypeRate').parent().removeClass('hideme');
        $('#inpTerm').parent().removeClass('hideme');
        $('#inpGuarantor').parent().removeClass('hideme');
        $('#inpBonus').parent().removeClass('hideme');
        $('#inpDOL').parent().removeClass('hideme');
    }

    $(document).ready(function(){
        
      plot_clientlist_table();




      $('#btnAddNew').click(function(){
        
        $('#txtCID').val('');
        $('#txtName').val('');
        $('#txtmname').val('');
        $('#txtlname').val('');
        $('#txtCivil').val('');
        // $('#txtDBirth').val('');
        $('#txtAddress').val('');
        $('#txtContact').val('');
        $('#txtSSS').val('');
        $('#txtCompany').val('');
        $('#txtCompanyAddress').val('');
        $('#txtS_name').val('');
        $('#txtS_contact').val('');
        $('#txtG_name').val('');
        $('#txtG_address').val('');
        $('#txtG_contactnum').val('');
        $('#txtG_SSS').val('');
        $('#txtCard1').val('');
        $('#txtPin1').val('');
        $('#txtCard2').val('');
        $('#txtPin2').val('');
        $('#txtCard3').val('');
        $('#txtPin3').val('');
        $('#txtCreditLimit').val('');
        $('#txtP_name').val('');
        $('#txtP_address').val('');
        // $('#txtClientType').val('');

        $('#spnCurrentBal').html('');
        // $('#txtPaymentSched').val();

        $('#txtAdditionalBalance').val('');

        // $('#txtInterestSchedStart').val('');

        $('#spnTotalLimit').html('');

        $('#spnLoanable').html('');

        $('#txtActualPaymentAmount').val('');

        $('#txtBeginningBal').val('');
        $('#chkIsInActive').iCheck('uncheck');
        $('#txtIsInActive').val('off');
        // $('#txtBegBalDate').val('');
        $('#modClient .modal-title').html('Add New Client');
        $('#modClient').modal('show');
      });

        $('input').on('ifChecked', function(event){
          // alert($(this).val()); // alert value
          $('#txtIsInActive').val('on');
        });

        $('input').on('ifUnchecked', function(event){
          // alert($(this).val()); // alert value
          $('#txtIsInActive').val('off');
        });
        

      $('#btnSaveClient').click(function(){


        var cid = $('#txtCID').val();

        var txtName = $('#txtName').val();
        var txtmname = $('#txtmname').val();
        var txtlname = $('#txtlname').val();
        var txtCivil = $('#txtCivil').val();
        var txtDBirth = $('#txtDBirth').val();
        var txtAddress = $('#txtAddress').val();
        var txtContact = $('#txtContact').val();
        var txtSSS = $('#txtSSS').val();
        var txtCompany = $('#txtCompany').val();
        var txtCompanyAddress = $('#txtCompanyAddress').val();
        var txtS_name = $('#txtS_name').val();
        var txtS_contact = $('#txtS_contact').val();
        var txtG_name = $('#txtG_name').val();
        var txtG_address = $('#txtG_address').val();
        var txtG_contactnum = $('#txtG_contactnum').val();
        var txtG_SSS = $('#txtG_SSS').val();
        var txtCard1 = $('#txtCard1').val();
        var txtPin1 = $('#txtPin1').val();
        var txtCard2 = $('#txtCard2').val();
        var txtPin2 = $('#txtPin2').val();
        var txtCard3 = $('#txtCard3').val();
        var txtPin3 = $('#txtPin3').val();
        var txtCreditLimit = $('#txtCreditLimit').val();
        var txtP_name = $('#txtP_name').val();
        var txtP_address = $('#txtP_address').val();
        var txtClientType = $('#txtClientType').val();

        var txtPaymentSched = $('#txtPaymentSched').val();

        var txtAdditionalBalance = $('#txtAdditionalBalance').val();
        var txtInterestSchedStart = $('#txtInterestSchedStart').val();

        var txtActualPaymentAmount = $('#txtActualPaymentAmount').val();

        var txtBeginningBal = $('#txtBeginningBal').val();

        var txtBegBalDate = $('#txtBegBalDate').val();
        var txtBegBalIntRate = $('#txtBegBalIntRate').val();
        var txtIsInActive = $('#txtIsInActive').val();

        var txtBegBalTypeRate = $('#txtBegBalTypeRate').val();

        var txtPaymentSelect = $('#txtPaymentSelect').val();
        var txtInterestSchedStart1 = $('#txtInterestSchedStart1').val();
        var txtInterestSchedStart1a = $('#txtInterestSchedStart1a').val();
        var txtInterestSchedStart2 = $('#txtInterestSchedStart2').val();
        var txtInterestSchedStart3 = $('#txtInterestSchedStart3').val();
        var txtInterestSchedStart4 = $('#txtInterestSchedStart4').val();
        // alert(txtInterestSchedStart);
        //&& txtmname != ''
        if(txtName != ''  && txtlname != '' && txtDBirth != '' && txtCreditLimit != '' && txtPaymentSched != '' && txtActualPaymentAmount != ''){
            $.ajax({
                type: 'post',
                url : 'lib/api/add.client.info.php',
                data : {
                    "txtName": txtName,
                    "txtmname": txtmname,
                    "txtlname": txtlname,
                    "txtCivil": txtCivil,
                    "txtDBirth": txtDBirth,
                    "txtAddress": txtAddress,
                    "txtContact": txtContact,
                    "txtSSS": txtSSS,
                    "txtCompany": txtCompany,
                    "txtCompanyAddress": txtCompanyAddress,
                    "txtS_name": txtS_name,
                    "txtS_contact": txtS_contact,
                    "txtG_name": txtG_name,
                    "txtG_address": txtG_address,
                    "txtG_contactnum": txtG_contactnum,
                    "txtG_SSS": txtG_SSS,
                    "txtCard1": txtCard1,
                    "txtPin1": txtPin1,
                    "txtCard2": txtCard2,
                    "txtPin2": txtPin2,
                    "txtCard3": txtCard3,
                    "txtPin3": txtPin3,
                    "txtCreditLimit": txtCreditLimit,
                    "txtP_name": txtP_name,
                    "txtP_address": txtP_address,
                    "txtClientType": txtClientType,
                    "txtPaymentSched": txtPaymentSched,
                    "txtAdditionalBalance" : txtAdditionalBalance,
                    "txtInterestSchedStart" : txtInterestSchedStart,
                    "txtActualPaymentAmount" : txtActualPaymentAmount,
                    "txtBeginningBal" : txtBeginningBal,
                    "txtBegBalIntRate" : txtBegBalIntRate,
                    "txtBegBalTypeRate" : txtBegBalTypeRate,
                    "txtBegBalDate" : txtBegBalDate,
                    "txtIsInActive" : txtIsInActive,
                    "txtPaymentSelect" : txtPaymentSelect,
                    "txtInterestSchedStart1" : txtInterestSchedStart1,
                    "txtInterestSchedStart1a" : txtInterestSchedStart1a,
                    "txtInterestSchedStart2" : txtInterestSchedStart2,
                    "txtInterestSchedStart3" : txtInterestSchedStart3,
                    "txtInterestSchedStart4" : txtInterestSchedStart4,
                    "cid": cid
                },
                beforeSend: function() {
                    $('#btnSaveClient').html(' <i class="fa fa-gear fa-spinner"></i> Saving data.... ');
                    $('body').addClass('mykroloading');
                },
                success : function(x) {
                    console.log(x);
                    $('body').removeClass('mykroloading');
                    if(x == 'success'){
                        //reload list                        
                        document.getElementById('frmPersonDetails').reset();
                        console.log(dt_table);
                        dt_table.fnDestroy();
                        dt_table1.fnDestroy();
                        setTimeout(function(){
                            plot_clientlist_table();
                        });
                        $('#modClient').modal('hide');
                        growlme('New Client Added!', '','notice');
                    }else if(x == 'save'){
                        document.getElementById('frmPersonDetails').reset();
                        console.log(dt_table);
                        dt_table.fnDestroy();
                        dt_table1.fnDestroy();
                        setTimeout(function(){
                            plot_clientlist_table();
                        });
                        $('#modClient').modal('hide');
                        growlme('Client Info Saved!', '','notice');

                    }else{
                        alert('Client is already on the list!');
                    }
                    $('#btnSaveClient').html(' <i class="fa fa-save"></i> SAVE ');
                },
                error: function(){
                    alert('Please contact developer!');
                }
            });            

        }else{
            alert('Plese enter complete name, Birthdate and Credit Limit!\nCredit Limit is on OTHERS tab.\nPlease enter value Payment Amount.\nPlease choose Date of when the Interest starts.');
        }
      });


        //loans
        $('#btnSelectLoan').click(function(){
            $('#frmLoans1').hide(function(){
                $('#frmLoans2').show();
                var v = $('#selTypeLoans').val();
                $('#modLoans .modal-title').html(toTitleCase(v.replace('_', ' ')));
                
                var uid = $('#inpHiddenUID').val();
                $.ajax({
                    type : 'post',
                    url  : 'lib/api/getusername.php',
                    dataType: 'json',
                    data : {
                        "uid" : uid
                    },beforeSend: function(x){

                    },
                    success: function(result){
                        console.log(result);
                        var obj = JSON.parse(result.data[0]);
                        $('#dispClientName').html('<strong> Client Name : </strong>' +  obj.name + ' ' + obj.middle_name + ' ' + obj.last_name);
                    }
                });


                //hide other fields
                // input fields
                /*
                    inpLoanAmount
                    selIntRate
                    selTypeRate
                    inpTerm
                    inpGuarantor
                    inpBonus
                    inpDOL

                    Available Fields

                    Cash Advance
                    - Client Name
                    - Amount
                    - Interest Rate
                    - Type of Rate
                    - Date of Loan

                    Special Account
                    - Client Name
                    - Amount
                    - Interest Rate
                    - Type of Rate
                    - Term
                    - Date of Loan


                    Renewal 
                    - Client Name
                    - Amount
                    - Interest Rate
                    - Type of Rate
                    - Term
                    - Bonuses
                    - Date of Loan

                    Excess
                    - Amount to Refund

                    Additional Loan
                    - Client Name
                    - Amount
                    - Interest Rate
                    - Type of Rate
                    - Term
                    - Date of Laon
                */
                switch(v){
                    
                    case 'new_loans' :
                        $('#frmExcess').hide();
                    break;

                    case 'cash_advance' :
                        $('#inpTerm').parent().addClass('hideme');
                        $('#inpGuarantor').parent().addClass('hideme');
                        $('#inpBonus').parent().addClass('hideme');
                        $('#frmExcess').hide();
                    break;

                    case 'special_account' :
                        $('#frmExcess').hide();
                        $('#inpGuarantor').parent().addClass('hideme');
                        $('#inpBonus').parent().addClass('hideme');
                    break;

                    case 'additional_loan' :
                        $('#frmExcess').hide();
                        $('#inpGuarantor').parent().addClass('hideme');
                        $('#inpBonus').parent().addClass('hideme');
                    break;

                    case 'renewal' :
                        $('#frmExcess').hide();
                        $('#inpGuarantor').parent().addClass('hideme');
                    break;
                    
                    case 'excess' :
                        $('#frmExcess').show();
                        $('#frmLoans2').hide();
                    break;
                }


                checkTimer = setInterval(function(){
                            console.log('timer');
                            var inpLoanAmount = $('#inpLoanAmount').val();
                            // var selIntRate = $('#selIntRate').val();
                            var selTypeRate = $('#selTypeRate').val();
                            var inpTerm = $('#inpTerm').val();
                            var inpGuarantor = $('#inpGuarantor').val();
                            var inpBonus = $('#inpBonus').val();
                            var inpDOL = $('#inpDOL').val();

                            // var errctr = 0;

                            if(inpLoanAmount == ''){
                                $('#inpLoanAmount').addClass("error-input");
                                // errctr++;
                            }else{
                                $('#inpLoanAmount').removeClass("error-input");
                                // errctr--;
                            }

                            if(selTypeRate == '-'){
                                $('#selTypeRate').addClass("error-input");
                                // errctr++;
                            }else{
                                $('#selTypeRate').removeClass("error-input");
                                // errctr--;
                            }

                            if(inpTerm == ''){
                                $('#inpTerm').addClass("error-input");
                                // errctr++;
                            }else{
                                $('#inpTerm').removeClass("error-input");
                                // errctr--;
                            }

                            if(inpGuarantor == ''){
                                $('#inpGuarantor').addClass("error-input");
                                // errctr++;
                            }else{
                                $('#inpGuarantor').removeClass("error-input");
                                // errctr--;
                            }

                            if(inpBonus == ''){
                                $('#inpBonus').addClass("error-input");
                                // errctr++;
                            }else{
                                $('#inpBonus').removeClass("error-input");
                                // errctr--;
                            }

                            if(inpDOL == ''){
                                $('#inpDOL').addClass("error-input");
                                // errctr++;
                            }else{
                                $('#inpDOL').removeClass("error-input");
                                // errctr--;
                            }


                            // if(errctr > 0){
                            //     $('#btnProcessLoans').attr('disabled', 'disabled');
                            // }else{
                            //     $('#btnProcessLoans').removeAttr('disabled');
                            // }


                            console.log('errctr ' + errctr );
                        }, 2000);

                    // $('#btnProcessLoans').attr('disabled', 'disabled');
                    $('#btnProcessLoans').removeAttr('disabled');
            });
        });

        $('#btnBackSelectLoan').click(function(){
            $('#frmLoans2').hide(function(){
                $('#frmLoans1').show();
                $('#modLoans .modal-title').html('Loans');
                unhideLoansFields();
                $('#btnProcessLoans').attr('disabled', 'disabled');
                clearInterval(checkTimer);

            });
        });

        $('#btnBackSelectLoan2').click(function(){
            $('#frmExcess').hide(function(){
                $('#frmLoans1').show();
                $('#modLoans .modal-title').html('Loans');
                unhideLoansFields();
                $('#btnProcessLoans').attr('disabled', 'disabled');
                clearInterval(checkTimer);
            });
        });

        $('#btnProcessLoans').click(function(){
            // alert('Process Loans...');
            clearInterval(checkTimer);
            var inpHiddenUID = $('#inpHiddenUID').val();
            var selTypeLoans = $('#selTypeLoans').val();
            var inpLoanAmount = $('#inpLoanAmount').val();
            var selIntRate = $('#selIntRate').val();
            var selTypeRate = $('#selTypeRate').val();
            var inpTerm = $('#inpTerm').val();
            var inpGuarantor = $('#inpGuarantor').val();
            var inpBonus = $('#inpBonus').val();
            var inpDOL = $('#inpDOL').val();

            if(selTypeLoans != 'excess'){
                $.ajax({
                    type : 'post',
                    url  : 'lib/api/process.loans.php',
                    dataType : 'json',
                    data : {
                        "inpHiddenUID" : inpHiddenUID,
                        "selTypeLoans" : selTypeLoans,
                        "inpLoanAmount" : inpLoanAmount,
                        "selIntRate" : selIntRate,
                        "selTypeRate" : selTypeRate,
                        "inpTerm" : inpTerm,
                        "inpGuarantor" : inpGuarantor,
                        "inpBonus" : inpBonus,
                        "inpDOL" : inpDOL,
                        "userID" : <?php echo $_SESSION['userID']; ?>
                    },
                    beforeSend : function (){
                        // alert('asdasd');
                    },
                    success : function(result){
                        console.log(result);
                        if(result.message == 'success'){
                            // growlme('Loan is being processed...\nThe page will reload!', '','notice');
                            // location.reload();
                            var pr = '<div class="alert alert-warning"><h3>Loan has been processed and needs approval!</h3>\
                                <p>Go to <a href="dashboard.php?page=voucher_list">Voucher</a> to approve. (Only accessible to Admin)</p>\
                            </div>';
                            $('#modNotifications .modal-body').html(pr);
                            $('#modLoans').modal('hide');
                            $('#modNotifications').modal('show');
                        }else{
                            // alert(result.message);
                            $('#modNotifications .modal-body').html('<div class="alert alert-danger"><h2>'+ result.message +'</h2></div>');
                            $('#modNotifications').modal('show');
                            growlme(''+ result.message,'','error');
                        }
                    },
                    error: function(err, errmsg){
                        alert('Error');
                        console.log(err)
                        console.log(errmsg)
                    }
                });                
            }else{
                var refund_amount = $('#inpRefundAmount').val();
                var inpDOL2 = $('#inpDOL2').val();
                if(refund_amount != ''){
                    $.ajax({
                        type : 'post',
                        url  : 'lib/api/process.excess.loans.php',
                        dataType : 'json',
                        data : {
                            "inpHiddenUID" : inpHiddenUID,
                            "selTypeLoans" : selTypeLoans,
                            "inpLoanAmount" : refund_amount,
                            "selIntRate" : "",
                            "selTypeRate" : "",
                            "inpTerm" : "",
                            "inpGuarantor" : "",
                            "inpBonus" : "",
                            "inpDOL2" : inpDOL2,
                            "userID" : <?php echo $_SESSION['userID']; ?>
                        },
                        beforeSend : function (){
                            // alert('asdasd');
                        },
                        success : function(result){
                            console.log(result);
                            if(result.message == 'success'){
                                // growlme('Loan is being processed...\nThe page will reload!', '','notice');
                                // location.reload();
                                var pr = '<div class="alert alert-warning"><h3>Loan has been processed and needs approval!</h3>\
                                    <p>Go to <a href="dashboard.php?page=voucher_list">Voucher</a> to approve. (Only accessible to Admin)</p>\
                                </div>';
                                $('#modNotifications .modal-body').html(pr);
                                $('#modLoans').modal('hide');
                                $('#modNotifications').modal('show');
                            }else{
                                // alert(result.message);
                                $('#modNotifications .modal-body').html('<div class="alert alert-danger"><h2>'+ result.message +'</h2></div>');
                                $('#modNotifications').modal('show');
                                growlme(''+ result.message,'','error');
                            }
                        },
                        error: function(err, errmsg){
                            alert('Error');
                            console.log(err)
                            console.log(errmsg)
                        }
                    });                      
                }else{
                    $('#modNotifications .modal-body').html('<div class="alert alert-danger"><h2>Please enter Refund amount!</h2></div>');
                    $('#modNotifications').modal('show');
                }

            }

            // selTypeLoans
            // inpLoanAmount
            // selIntRate
            // selTypeRate
            // inpTerm
            // inpGuarantor
            // inpBonus
            // inpDOL
        });

        
        // growlme('Loan is being processed...\nThe page will reload!', 'notice');

          //on close modal
          $('#modLoans').on('hidden.bs.modal', function () {
              // do something…
              clearInterval(checkTimer);
          })

            // var availableTags = [
            //   "ActionScript",
            //   "AppleScript",
            //   "Asp",
            //   "BASIC",
            //   "C",
            //   "C++",
            //   "Clojure",
            //   "COBOL",
            //   "ColdFusion",
            //   "Erlang",
            //   "Fortran",
            //   "Groovy",
            //   "Haskell",
            //   "Java",
            //   "JavaScript",
            //   "Lisp",
            //   "Perl",
            //   "PHP",
            //   "Python",
            //   "Ruby",
            //   "Scala",
            //   "Scheme"
            // ];
            // $( "#txtCompany" ).autocomplete({
            //   source: availableTags
            // });
    //client_data
            $('#txtCompany').autocomplete({
                serviceUrl: 'lib/api/get.companies.php',
                onSelect: function (suggestion) {
                    // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                }
            });


            $('#inpDOL2').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1",
              showDropdowns: true,
              "opens": "center",
              "drops": "up",
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#txtDBirth').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1",
              "minDate": "1930/01/01",
              showDropdowns: true,
              "opens": "center",
              "drops": "up",
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#inpDOL').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1",
              showDropdowns: true,
              "opens": "center",
              "drops": "up",
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#txtInterestSchedStart').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1",
              showDropdowns: true,
              "opens": "center",
              "drops": "down",
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#txtBegBalDate').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1",
              showDropdowns: true,
              "opens": "center",
              "drops": "down",
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });


            var malert = '\
                    <div class="alert alert-success" role="alert">\
                      <p style="font-size: 14px">\
                        You can now update Client\'s Payment Schedule by selecting the updating Client\'s Profile by Edit Profile >> Others tab (see screenshot <a href="https://screencast.com/t/UmIkBzkD6HfS" target="_blank">CLICK HERE</a>).<br>Payment Schedule can be <b>Once a Month</b>,\
                        <b>Twice a Month</b> and <b>Weekly</b>.\ You can accurately get the actual NO PAYMENT records of the clients.\
                      </p>\
                    </div>';

            malert += '\
                    <div class="alert alert-warning" role="alert">\
                      <p style="font-size: 14px">\
                      In case calculations of Interest (on View Data) is incorrect. Please double check Client\'s \"Other\" data if it\'s complete or correct.<br>\
                      Errors on calculations may cause of missing \"Type Of Rate\" or \"Interest Rate\", etc. Please double by \"Edit Profile\".<br>\
                      Thank you very much! :)<br>This message will disappear in <span id="message_timer">60 second(s)</span>.<br>\
                      </p>\
                    </div>';
            $('#alert_message').html(malert);

            var timer_cnt = 60;
            setInterval(function(){
                console.log(timer_cnt);
                $('#message_timer').html(timer_cnt + ' second(s)');
                if(timer_cnt == 0){
                    $('#alert_message').html('');    
                }
                timer_cnt--;
            }, 1000);

            // setTimeout(function(){
            //     $('#alert_message').html('');
            // }, 60000); //message will disappear in 1minute


            $('#txtPaymentSelect').change(function(){
                var x = $(this).val();
                switch(x){
                    case '-':
                        // $('#div_once_month').hide();
                        // $('#div_twice_month').hide();
                        // $('#div_weekly').hide();
                        $('#div_psched').hide();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1').hide();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                        $('#div_psched #div_psched2').hide();
                        $('#div_psched #div_psched3').hide();
                        $('#div_psched #div_psched4').hide();
                    break;
                    case '1': //ONCE A MONTH
                        $('#div_psched').show();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1').show();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                        $('#div_psched #div_psched2').hide();
                        $('#div_psched #div_psched3').hide();
                        $('#div_psched #div_psched4').hide();
                        // $('#div_once_month').show();
                        // $('#div_twice_month').hide();
                        // $('#div_weekly').hide();
                    break;
                    case '2': // TWICE A MONTH
                        // $('#div_once_month').hide();
                        // $('#div_twice_month').show();
                        // $('#div_weekly').hide();
                        $('#div_psched').show();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1').show();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                        $('#div_psched #div_psched2').show();
                        $('#div_psched #div_psched3').hide();
                        $('#div_psched #div_psched4').hide();
                    break;
                    case '3': // WEEKLY
                        // $('#div_once_month').hide();
                        // $('#div_twice_month').hide();
                        // $('#div_weekly').show();
                        $('#div_psched').show();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1').show();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                        $('#div_psched #div_psched2').show();
                        $('#div_psched #div_psched3').show();
                        $('#div_psched #div_psched4').show();
                    break;
                    default:
                        $('#div_psched').hide();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1').hide();
                        $('#div_psched #div_psched1 #txtInterestSchedStart1a').hide();
                        $('#div_psched #div_psched2').hide();
                        $('#div_psched #div_psched3').hide();
                        $('#div_psched #div_psched4').hide();
                        // $('#div_once_month').hide();
                        // $('#div_twice_month').hide();
                        // $('#div_weekly').hide();
                    break;
                }
            });

    });
  </script>
  <style>
  .error-input {
    border: 1px solid red !important;
  }

  .td_name {
    width: 30%;
  }

  .td_name > div {
    display: none;
  }

  .td_name:hover {
    cursor: pointer;
  }

  .td_name:hover > div{
    display: block;
  }

  .td_name:hover a {
    cursor: pointer;
  }

  .hideme { display: none !important; }

  #frmLoans2 { display: none; }
  </style>