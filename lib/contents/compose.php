<div class="row">
  <div class="col-md-6">
    <h2>Message</h2>
    <form>
        <div class="form-group">
            <label class="form-label">To</label>
            <input type="text" id="remail" class="form-control" />
        </div>
        <div class="form-group">
            <label class="form-label">Subject</label>
            <input type="text" id="rsubject" class="form-control" />
        </div>
        <div class="form-group">
            <label class="form-label">Message</label>
            <textarea class="form-control" id="rmessage"></textarea>
        </div>
        <div class="form-group">
            <button type="button" id="btnSendRep" class="btn btn-primary"><i class="fa fa-send"></i> Send</button>
        </div>
    </form>
  </div>
</div>

<script>
$('#btnSendRep').click(function(){
      var remail = $('#remail').val();
      var rsubj = $('#rsubject').val();
      var rmsg = $('#rmessage').val();
    $.ajax({
      type: 'post',
      url : 'api/api.php',
      data: {
        action: 'sendMail',
        'email': remail,
        'subj':  rsubj,
        'msg' : rmsg
      },
      success : function(xhr){
          alert(xhr);
      },
      beforeSend: function(xhr){
      }
    });
});
</script>
