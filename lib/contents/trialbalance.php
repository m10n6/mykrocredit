
<?php
	// echo getBegBalInActiveTotal().'=InActive';
?>
 <div class="row">
 	<div class="col-md-12">
<!-- 		<div class="alert alert-danger">
		  <strong>Sorry! Problem Generating Trial Balance...</strong>
		</div> -->
 		<!-- <button class="btn btn-primary" type="button" id="btnAddNew"><i class="fa fa-plus"></i> Add New Account </button> -->
 		<div class="row">
	 		<div class="col-md-6">
	 			<div class="col-md-6">
	 				<div class="form-group">
	 					<label>
	 						Month :
	 					</label>
	 					<select class="form-control" id="selTBMonth">
	 						<option value="01" <?php echo (!empty($_GET['month']) && $_GET['month']=='01')? 'selected': ''; ?> >January</option>
	 						<option value="02" <?php echo (!empty($_GET['month']) && $_GET['month']=='02')? 'selected': ''; ?> >February</option>
	 						<option value="03" <?php echo (!empty($_GET['month']) && $_GET['month']=='03')? 'selected': ''; ?> >March</option>
	 						<option value="04" <?php echo (!empty($_GET['month']) && $_GET['month']=='04')? 'selected': ''; ?> >April</option>
	 						<option value="05" <?php echo (!empty($_GET['month']) && $_GET['month']=='05')? 'selected': ''; ?> >May</option>
	 						<option value="06" <?php echo (!empty($_GET['month']) && $_GET['month']=='06')? 'selected': ''; ?> >June</option>
	 						<option value="07" <?php echo (!empty($_GET['month']) && $_GET['month']=='07')? 'selected': ''; ?> >July</option>
	 						<option value="08" <?php echo (!empty($_GET['month']) && $_GET['month']=='08')? 'selected': ''; ?> >August</option>
	 						<option value="09" <?php echo (!empty($_GET['month']) && $_GET['month']=='09')? 'selected': ''; ?> >September</option>
	 						<option value="10" <?php echo (!empty($_GET['month']) && $_GET['month']=='10')? 'selected': ''; ?> >October</option>
	 						<option value="11" <?php echo (!empty($_GET['month']) && $_GET['month']=='11')? 'selected': ''; ?> >November</option>
	 						<option value="12" <?php echo (!empty($_GET['month']) && $_GET['month']=='12')? 'selected': ''; ?> >December</option>
	 					</select>
	 				</div>
	 			</div>

	 			<div class="col-md-6">
	 				<div class="form-group">
	 					<label>
	 						Year :
	 					</label>
	 					<select class="form-control"  id="selTBYear">
	 						<?php
	 							$year = date('Y');
	 							for($i=0; $i<=10; $i++){

	 								$this_year = $year + $i;
	 								$sel = '';
	 								if(!empty($_GET['year']) && $_GET['year'] == $this_year){
	 									$sel = 'selected';
	 								}

	 								echo '<option value="'.$this_year.'" '.$sel.' >'.$this_year.'</option>';
	 							}
	 						?>
	 					</select>
	 				</div>
	 			</div>
	 		</div>
 		</div>
 		<div class="row">
	 		<div class="col-md-6">
	 			<div class="col-md-6">
	 				<button class="btn btn-primary" type="button" id="btnLoadTB"> <i class="fa fa-download"></i> Load Trial Balance </button>
	 			</div>
	 		</div>
	 	</div>


 		<hr/>

 		<div class="row">
	 		<div class="col-md-6">
	 			<div class="col-md-6">
	 				<label>Select Month Year to Save:</label>
	 				<select class="form-control" id="selMonthYear">
						<?php
							$year = date('Y');
							$current_month = date('F');
							$current_year = $year;

							for($i=0; $i<=10; $i++){
								for($i1=1; $i1<=12; $i1++){
									$disp_year = ($year + $i);
									$disp_month = date('F', mktime(0,0,0,$i1,1,$disp_year));
									$selected = '';
									if($disp_year == $current_year && $current_month == $disp_month){
										$selected = 'selected';
									}
									echo '<option value="'.str_pad($i1, 2, '0', STR_PAD_LEFT).'-'.$disp_year.'" '.$selected.'>'.$disp_month.' '.$disp_year.'</option>';	
								}
								
							}
						?>
	 				</select>
	 			</div>
	 			<div class="col-md-6">
	 				<div class="spacer25"></div>
	 				<button class="btn btn-success" type="button" id="btnSaveTB"> <i class="fa fa-save"></i> Save Trial Balance </button>
	 			</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="col-md-6">

	 			</div>
	 			<div class="col-md-6">
	 				<div class="spacer25"></div>
	 				<button class="btn btn-primary pull-right" type="button" id="btnReLoadTB"> <i class="fa fa-refresh"></i> Reload Trial Balance </button>
	 			</div>
	 		</div>
	 	</div>

 		<hr/>

 		<table class="table table-striped" id="tblBalanceSheet">
 			<thead>
 				<tr>
 					<th rowspan="2" style="width: 15%;">Account Code</th>
 					<th rowspan="2">Account Name</th>
 					<th colspan="2">PREVIOUS BALANCE</th>
 					<th colspan="2">TRANSACTIONS</th>
 					<th colspan="2">UP-TO-DATE BALANCE</th>
 				</tr>
 				<tr>
 					<!-- <th>Account Code</th> -->
 					<th>DEBIT</th>
 					<th>CREDIT</th>
 					<th>DEBIT</th>
 					<th>CREDIT</th>
 					<th>DEBIT</th>
 					<th>CREDIT</th>
 				</tr>
 			</thead>

 			<tbody>
 				<!-- <tr>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 				</tr> -->
  				<?php
  					$action = secure_get('action');

  					if($action == 'load'){
							/*=============================== START: LOAD SAVED TRIAL BALANCE =====================================*/
	  						//trial balance is empty
							$sql = "select * from `chart_accounts` order by `code` asc";
							$res = $conn->dbquery($sql);
							// print_r($res);
							if($res !== 'false'){
								$res = json_decode($res);
								$ctr = 0;
								foreach ($res->data as $key) {
									# code...
									$month_year = $_GET['month'].'-'.$_GET['year'];

									$nres = json_decode($key);



									//condition here and calculation
									switch($nres->code){
										case '100-001':

											$jvcaid = getChartAccountIDByCode('100-001');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');

										break;

										case '101-001':
											$jvcaid = getChartAccountIDByCode('101-001');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');

										break;

										case '101-001-01':
											$jvcaid = getChartAccountIDByCode('101-001-01');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');

										break;

										case '101-001-02':
											$jvcaid = getChartAccountIDByCode('101-001-02');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');

										break;

										case '101-001-03' :
											//employee
											$jvcaid = getChartAccountIDByCode('101-001-03');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');

										break;

										case '101-001-05' : 
											//special accounts
											$jvcaid = getChartAccountIDByCode('101-001-05');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');

										break;

										case '101-001-06' : 
											//excess
											$jvcaid = getChartAccountIDByCode('101-001-06');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;

										case '400-003-01':
											$jvcaid = getChartAccountIDByCode('400-003-01');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;

										case '400-003-02':
											$jvcaid = getChartAccountIDByCode('400-003-02');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;

										case '400-003-03':
											$jvcaid = getChartAccountIDByCode('400-003-03');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;

										case '400-003-04':
											$jvcaid = getChartAccountIDByCode('400-003-04');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;

										//105-000 (DEBIT)

										case '400-005' :  //(DEBIT)
											$jvcaid = getChartAccountIDByCode('400-005');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break; 


										case '400-005' :  //(DEBIT)
											$jvcaid = getChartAccountIDByCode('400-005');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break; 



										case '101-001-009': //BOnuses
											$jvcaid = getChartAccountIDByCode('101-001-009');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;


										case '101-001-008':

											$jvcaid = getChartAccountIDByCode('101-001-008');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '102-000':
											$jvcaid = getChartAccountIDByCode('102-000');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '102-001':
											$jvcaid = getChartAccountIDByCode('102-001');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '102-002':
											$jvcaid = getChartAccountIDByCode('102-002');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '102-003':
											$jvcaid = getChartAccountIDByCode('102-003');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '102-004':
											$jvcaid = getChartAccountIDByCode('102-004');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '103-000':
											$jvcaid = getChartAccountIDByCode('103-000');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '104-000':
											$jvcaid = getChartAccountIDByCode('104-000');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '105-000':
											$jvcaid = getChartAccountIDByCode('105-000');


											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '106-000':
											$jvcaid = getChartAccountIDByCode('106-000');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-001':
											$jvcaid = getChartAccountIDByCode('200-001');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-002':
											$jvcaid = getChartAccountIDByCode('200-002');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-003':
											$jvcaid = getChartAccountIDByCode('200-003');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-004':
											$jvcaid = getChartAccountIDByCode('200-004');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-005':
											$jvcaid = getChartAccountIDByCode('200-005');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-006':
											$jvcaid = getChartAccountIDByCode('200-006');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-007':
											$jvcaid = getChartAccountIDByCode('200-007');


											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;

										case '200-008':
											$jvcaid = getChartAccountIDByCode('200-008');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-009':
											$jvcaid = getChartAccountIDByCode('200-009');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-010':
											$jvcaid = getChartAccountIDByCode('200-010');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-011':
											$jvcaid = getChartAccountIDByCode('200-011');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-012':
											$jvcaid = getChartAccountIDByCode('200-012');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '200-013':
											$jvcaid = getChartAccountIDByCode('200-013');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-000':
											$jvcaid = getChartAccountIDByCode('500-000');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-001':
											$jvcaid = getChartAccountIDByCode('500-001');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-002':
											$jvcaid = getChartAccountIDByCode('500-002');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-003':
											$jvcaid = getChartAccountIDByCode('500-003');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-004':
											$jvcaid = getChartAccountIDByCode('500-004');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-005':
											$jvcaid = getChartAccountIDByCode('500-005');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-006':
											$jvcaid = getChartAccountIDByCode('500-006');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-007':
											$jvcaid = getChartAccountIDByCode('500-007');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-008':
											$jvcaid = getChartAccountIDByCode('500-008');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-009':
											$jvcaid = getChartAccountIDByCode('500-009');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-010':
											$jvcaid = getChartAccountIDByCode('500-010');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-011':
											$jvcaid = getChartAccountIDByCode('500-011');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-012':
											$jvcaid = getChartAccountIDByCode('500-012');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-013':
											$jvcaid = getChartAccountIDByCode('500-013');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-014':
											$jvcaid = getChartAccountIDByCode('500-014');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-015':
											$jvcaid = getChartAccountIDByCode('500-015');
											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;
										case '500-016':
											$jvcaid = getChartAccountIDByCode('500-016');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;

										case '400-002':
											$jvcaid = getChartAccountIDByCode('400-002');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;


										case '100-004':
											$jvcaid = getChartAccountIDByCode('100-004');

											$prevDebit = getActualTBAmount($jvcaid, $month_year, 'prev_debit');
											$prevCredit = getActualTBAmount($jvcaid, $month_year, 'prev_credit');
											$transDebit = getActualTBAmount($jvcaid, $month_year, 'trans_debit');
											$transCredit = getActualTBAmount($jvcaid, $month_year, 'trans_credit');
											$upDebit = getActualTBAmount($jvcaid, $month_year, 'ud_debit');
											$upCredit = getActualTBAmount($jvcaid, $month_year, 'ud_credit');
										break;


										default:
											$totalDebit = 0;
											$totalCredit = 0;
										break; 
									}

									//formula for trialbalance
									// $nres->code !== '102-000' &&
									if($nres->code !== '101-001' &&
									   $nres->code !== '101-001-04' && $nres->code !== '400-003'):

										$overAllDebit += str_replace(',','',$transDebit);
										$overAllCredit += str_replace(',','',$transCredit);

										echo '
											<tr>
												<td>'.$nres->code.'
													<input type="hidden" id="tb_hidden_coa'.$ctr.'" value="'.$nres->caid.'" >
												</td>
												<td>'.$nres->name.'</td>
												<td><input type="text" data-id="'.$ctr.'" id="cl_debit_'.$ctr.'" class="form-control cl_debit" value="'.number_format(str_replace(',','',$prevDebit),2).'"  style="width: 200px;"  readonly/></td>
												<td><input type="text" data-id="'.$ctr.'" id="cl_credit_'.$ctr.'" class="form-control cl_credit" value="'.number_format(str_replace(',','',$prevCredit),2).'"  style="width: 200px;" readonly/>
													<input type="hidden" id="td_hidden_'.$ctr.'" value="'.number_format(str_replace(',','',$transDebit), 2).'" >
													<input type="hidden" id="tc_hidden_'.$ctr.'" value="'.number_format(str_replace(',','',$transCredit), 2).'" >
												</td>
												<td id="trans_deb_'.$ctr.'">'.number_format(str_replace(',','',$transDebit), 2).'</td>
												<td id="trans_cre_'.$ctr.'">'.number_format(str_replace(',','',$transCredit), 2).'</td>
												<td id="bal_deb_'.$ctr.'">'.number_format(str_replace(',','',$upDebit), 2).'</td>
												<td id="bal_cre_'.$ctr.'">'.number_format(str_replace(',','',$upCredit), 2).'</td>
											</tr>
										';	
										$totalDebit = 0;
										$totalCredit = 0;

										$transDebit = 0;
										$transCredit = 0;

										$prevDebit = 0;
										$prevCredit = 0;

										$upDebit = 0;
										$upCredit = 0;
										$ctr++;
									endif;
								}
							}else{
								echo '
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								';	
							}
							/*=============================== END: LOAD SAVED TRIAL BALANCE  =====================================*/
  					}else{

	  					// START TRIAL BALANCE
	  					$sqlM = "select * from `trial_balance` order by `month_year` desc";
	  					$resM = $conn->dbquery($sqlM);
	  					if($resM !== 'false'){
	  						$resM = json_decode($resM);
	  						foreach ($resM->data as $rmTB) {
	  							# code...
	  							$tb = json_decode($rmTB);
	  							$last_month_year = $tb->month_year;
	  						}
	  						// echo $last_month_year;
	  						/*=============================== START: TRIAL BALANCE BY MONTH =====================================*/
	  						//trial balance is empty
							$sql = "select * from `chart_accounts` order by `code` asc";
							$res = $conn->dbquery($sql);
							// print_r($res);
							if($res !== 'false'){
								$res = json_decode($res);
								$ctr = 0;
								foreach ($res->data as $key) {
									# code...
									//now year
									$now_year = date('Y');

									//get next month
									$temp_month_year = explode('-', $last_month_year);
									$next_month = $temp_month_year[0] + 1;
									if($next_month == 12){
										$next_month = '01';
										$now_year += 1; 
									}else{
										$next_month = str_pad($next_month, 2, '0', STR_PAD_LEFT);	
									}

									$nres = json_decode($key);

									$newLoans = getLoanTypeTotalByDate('new_loans',$next_month, $now_year);
									$cashAdvance = getLoanTypeTotalByDate('cash_advance',$next_month, $now_year);
									$renewal = getLoanTypeTotalByDate('renewal',$next_month, $now_year);

									$employee = getLoanTypeTotalByDate('employee',$next_month, $now_year);
									$specialAccounts = getLoanTypeTotalByDate('special_accounts',$next_month, $now_year);
									$excess = getLoanTypeTotalByDate('excess',$next_month, $now_year);

									$procFeeTotal = getProcFeeTotalByDate($next_month, $now_year);
									$notFeeTotal = getNotFeeTotalByDate($next_month, $now_year);
									$miscFeeTotal = getMiscFeeTotalByDate($next_month, $now_year);
									$finderFeeTotal = getFindersFeeTotalByDate($next_month, $now_year);
									$bonusesTotal = getBonusesTotalByDate($next_month, $now_year);

									//condition here and calculation
									switch($nres->code){
										case '100-001':

											$jvcaid = getChartAccountIDByCode('100-001');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											// $totalDebit = getLoanTypeTotal('new_loans') + getLoanTypeTotal('cash_advance') + getLoanTypeTotal('renewal');
											$totalCredit = (($newLoans + $cashAdvance + $renewal + $excess) - ($procFeeTotal + $notFeeTotal + $miscFeeTotal + $finderFeeTotal)) + $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										case '101-001':
											$jvcaid = getChartAccountIDByCode('101-001');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = ($newLoans + $cashAdvance + $renewal) + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										case '101-001-01':
											$jvcaid = getChartAccountIDByCode('101-001-01');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $newLoans;
											// $totalDebit = $totalDebit + getLoanTypeTotal('cash_advance');
											$totalDebit = ($totalDebit + $renewal) + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '101-001-02':
											$jvcaid = getChartAccountIDByCode('101-001-02');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');
											
											$totalDebit = $cashAdvance + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										case '101-001-03' :
											//employee
											$jvcaid = getChartAccountIDByCode('101-001-03');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $employee + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										case '101-001-05' : 
											//special accounts
											$jvcaid = getChartAccountIDByCode('101-001-05');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $specialAccounts + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										case '101-001-06' : 
											//excess
											$jvcaid = getChartAccountIDByCode('101-001-06');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $excess + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										case '400-003-01':
											$jvcaid = getChartAccountIDByCode('400-003-01');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $procFeeTotal + $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										case '400-003-02':
											$jvcaid = getChartAccountIDByCode('400-003-02');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $notFeeTotal + $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										case '400-003-03':
											$jvcaid = getChartAccountIDByCode('400-003-03');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $miscFeeTotal + $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										case '400-003-04':
											$jvcaid = getChartAccountIDByCode('400-003-04');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $finderFeeTotal + $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										//105-000 (DEBIT)

										case '400-005' :  //(DEBIT)
											$jvcaid = getChartAccountIDByCode('400-005');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break; 



										case '101-001-009': //BOnuses
											$jvcaid = getChartAccountIDByCode('101-001-009');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $bonusesTotal + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;


										case '101-001-008':

											$jvcaid = getChartAccountIDByCode('101-001-008');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '102-000':
											$jvcaid = getChartAccountIDByCode('102-000');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '102-001':
											$jvcaid = getChartAccountIDByCode('102-001');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '102-002':
											$jvcaid = getChartAccountIDByCode('102-002');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '102-003':
											$jvcaid = getChartAccountIDByCode('102-003');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '102-004':
											$jvcaid = getChartAccountIDByCode('102-004');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '103-000':
											$jvcaid = getChartAccountIDByCode('103-000');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '104-000':
											$jvcaid = getChartAccountIDByCode('104-000');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '105-000':
											$jvcaid = getChartAccountIDByCode('105-000');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '106-000':
											$jvcaid = getChartAccountIDByCode('106-000');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-001':
											$jvcaid = getChartAccountIDByCode('200-001');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-002':
											$jvcaid = getChartAccountIDByCode('200-002');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-003':
											$jvcaid = getChartAccountIDByCode('200-003');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-004':
											$jvcaid = getChartAccountIDByCode('200-004');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-005':
											$jvcaid = getChartAccountIDByCode('200-005');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-006':
											$jvcaid = getChartAccountIDByCode('200-006');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-007':
											$jvcaid = getChartAccountIDByCode('200-007');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										case '200-008':
											$jvcaid = getChartAccountIDByCode('200-008');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-009':
											$jvcaid = getChartAccountIDByCode('200-009');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-010':
											$jvcaid = getChartAccountIDByCode('200-010');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-011':
											$jvcaid = getChartAccountIDByCode('200-011');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-012':
											$jvcaid = getChartAccountIDByCode('200-012');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '200-013':
											$jvcaid = getChartAccountIDByCode('200-013');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-000':
											$jvcaid = getChartAccountIDByCode('500-000');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-001':
											$jvcaid = getChartAccountIDByCode('500-001');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-002':
											$jvcaid = getChartAccountIDByCode('500-002');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-003':
											$jvcaid = getChartAccountIDByCode('500-003');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-004':
											$jvcaid = getChartAccountIDByCode('500-004');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-005':
											$jvcaid = getChartAccountIDByCode('500-005');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-006':
											$jvcaid = getChartAccountIDByCode('500-006');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-007':
											$jvcaid = getChartAccountIDByCode('500-007');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-008':
											$jvcaid = getChartAccountIDByCode('500-008');

											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-009':
											$jvcaid = getChartAccountIDByCode('500-009');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-010':
											$jvcaid = getChartAccountIDByCode('500-010');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-011':
											$jvcaid = getChartAccountIDByCode('500-011');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-012':
											$jvcaid = getChartAccountIDByCode('500-012');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-013':
											$jvcaid = getChartAccountIDByCode('500-013');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-014':
											$jvcaid = getChartAccountIDByCode('500-014');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-015':
											$jvcaid = getChartAccountIDByCode('500-015');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;
										case '500-016':
											$jvcaid = getChartAccountIDByCode('500-016');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;


										case '400-002':
											$jvcaid = getChartAccountIDByCode('400-002');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;


										case '100-004':
											$jvcaid = getChartAccountIDByCode('100-004');


											$jvAmountDebit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'debit');
											$jvAmountCredit = getGetJVTotalAmountDateEntry($jvcaid, $next_month, $year, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											$prevDebit =  getTBPreviousAmount($jvcaid, $last_month_year, 'debit');
											$prevCredit = getTBPreviousAmount($jvcaid, $last_month_year, 'credit');
										break;

										default:
											$totalDebit = 0;
											$totalCredit = 0;
										break; 
									}

									//formula for trialbalance
									//$nres->code !== '102-000' &&
									if($nres->code !== '101-001' && 
									   $nres->code !== '101-001-04' && $nres->code !== '400-003'):

										$overAllDebit += $totalDebit;
										$overAllCredit += $totalCredit;

										echo '
											<tr>
												<td>'.$nres->code.'
													<input type="hidden" id="tb_hidden_coa'.$ctr.'" value="'.$nres->caid.'" >
												</td>
												<td>'.$nres->name.'</td>
												<td><input type="text" data-id="'.$ctr.'" id="cl_debit_'.$ctr.'" class="form-control cl_debit" value="'.number_format(str_replace(',','',$prevDebit),2).'"  style="width: 200px;"  readonly/></td>
												<td><input type="text" data-id="'.$ctr.'" id="cl_credit_'.$ctr.'" class="form-control cl_credit" value="'.number_format(str_replace(',','',$prevCredit),2).'"  style="width: 200px;" readonly/>
													<input type="hidden" id="td_hidden_'.$ctr.'" value="'.number_format($totalDebit, 2).'" >
													<input type="hidden" id="tc_hidden_'.$ctr.'" value="'.number_format($totalCredit, 2).'" >
												</td>
												<td id="trans_deb_'.$ctr.'">'.number_format($totalDebit, 2).'</td>
												<td id="trans_cre_'.$ctr.'">'.number_format($totalCredit, 2).'</td>
												<td id="bal_deb_'.$ctr.'">0.00</td>
												<td id="bal_cre_'.$ctr.'">0.00</td>
											</tr>
										';	
										$totalDebit = 0;
										$totalCredit = 0;

										$jvAmountDebit = 0;
										$jvAmountCredit = 0;

										$prevDebit = 0;
										$prevCredit = 0;
										$ctr++;
									endif;
								}
							}else{
								echo '
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								';	
							}
							/*=============================== END: TRIAL BALANCE BY MONTH  =====================================*/
	  					}else{
	  						/*=============================== START: EMPTY TRIAL BALANCE  =====================================*/
	  						//trial balance is empty
							$sql = "select * from `chart_accounts` order by `code` asc";
							$res = $conn->dbquery($sql);
							// print_r($res);
							if($res !== 'false'){
								$res = json_decode($res);
								$ctr = 0;
								foreach ($res->data as $key) {
									# code...
									$nres = json_decode($key);

									$newLoans = getLoanTypeTotal('new_loans');
									$cashAdvance = getLoanTypeTotal('cash_advance');
									$renewal = getLoanTypeTotal('renewal');

									$employee = getLoanTypeTotal('employee');
									$specialAccounts = getLoanTypeTotal('special_accounts');
									$excess = getLoanTypeTotal('excess');

									$beginningBal = getLoanTypeTotal('beg_bal');

									$procFeeTotal = getProcFeeTotal();
									$notFeeTotal = getNotFeeTotal();
									$miscFeeTotal = getMiscFeeTotal();
									$finderFeeTotal = getFindersFeeTotal();
									$bonusesTotal = getBonusesTotal();

									
									//condition here and calculation
									switch($nres->code){
										case '100-001':

											$jvcaid = getChartAccountIDByCode('100-001');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											// $totalDebit = getLoanTypeTotal('new_loans') + getLoanTypeTotal('cash_advance') + getLoanTypeTotal('renewal');
											$totalCredit = (($newLoans + $cashAdvance + $renewal + $excess) - ($procFeeTotal + $notFeeTotal + $miscFeeTotal + $finderFeeTotal)) + $jvAmountCredit;

											// $StartCredit = $beginningBal; //Should be
											$StartCredit = 0;
										break;

										case '101-001':
											$jvcaid = getChartAccountIDByCode('101-001');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = ($newLoans + $cashAdvance + $renewal) + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;

										case '101-001-01':
											$jvcaid = getChartAccountIDByCode('101-001-01');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $newLoans;
											// $totalDebit = $totalDebit + getLoanTypeTotal('cash_advance');
											$totalDebit = ($totalDebit + $renewal) + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;

											
											$StartDebit = $beginningBal;
										break;
										case '101-001-02':
											$jvcaid = getChartAccountIDByCode('101-001-02');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');
											
											$totalDebit = $cashAdvance + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;

										case '101-001-03' :
											//employee
											$jvcaid = getChartAccountIDByCode('101-001-03');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $employee + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;

										case '101-001-05' : 
											//special accounts
											$jvcaid = getChartAccountIDByCode('101-001-05');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $specialAccounts + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;

										case '101-001-06' : 
											//excess
											$jvcaid = getChartAccountIDByCode('101-001-06');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $excess + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;

										case '400-003-01':
											$jvcaid = getChartAccountIDByCode('400-003-01');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $procFeeTotal + $jvAmountCredit;
										break;

										case '400-003-02':
											$jvcaid = getChartAccountIDByCode('400-003-02');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $notFeeTotal + $jvAmountCredit;
										break;

										case '400-003-03':
											$jvcaid = getChartAccountIDByCode('400-003-03');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $miscFeeTotal + $jvAmountCredit;
										break;

										case '400-003-04':
											$jvcaid = getChartAccountIDByCode('400-003-04');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $finderFeeTotal + $jvAmountCredit;
										break;

										//105-000 (DEBIT)

										case '400-005' :  //(DEBIT)
											$jvcaid = getChartAccountIDByCode('400-005');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break; 

										case '101-001-009': //BOnuses
											$jvcaid = getChartAccountIDByCode('101-001-009');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $bonusesTotal + $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;


										case '101-001-008':
											$jvcaid = getChartAccountIDByCode('101-001-008');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											
											$totalDebit = $jvAmountDebit ;

											$totalCredit = $jvAmountCredit;

											$inactive_total = getBegBalInActiveTotal();
											$StartDebit = $inactive_total;
										break;
										case '102-000':
											$jvcaid = getChartAccountIDByCode('102-000');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '102-001':
											$jvcaid = getChartAccountIDByCode('102-001');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '102-002':
											$jvcaid = getChartAccountIDByCode('102-002');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '102-003':
											$jvcaid = getChartAccountIDByCode('102-003');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '102-004':
											$jvcaid = getChartAccountIDByCode('102-004');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '103-000':
											$jvcaid = getChartAccountIDByCode('103-000');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '104-000':
											$jvcaid = getChartAccountIDByCode('104-000');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '105-000':
											$jvcaid = getChartAccountIDByCode('105-000');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '106-000':
											$jvcaid = getChartAccountIDByCode('106-000');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-001':
											$jvcaid = getChartAccountIDByCode('200-001');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-002':
											$jvcaid = getChartAccountIDByCode('200-002');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-003':
											$jvcaid = getChartAccountIDByCode('200-003');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-004':
											$jvcaid = getChartAccountIDByCode('200-004');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-005':
											$jvcaid = getChartAccountIDByCode('200-005');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-006':
											$jvcaid = getChartAccountIDByCode('200-006');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-007':
											$jvcaid = getChartAccountIDByCode('200-007');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;

										case '200-008':
											$jvcaid = getChartAccountIDByCode('200-008');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-009':
											$jvcaid = getChartAccountIDByCode('200-009');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-010':
											$jvcaid = getChartAccountIDByCode('200-010');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-011':
											$jvcaid = getChartAccountIDByCode('200-011');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-012':
											$jvcaid = getChartAccountIDByCode('200-012');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '200-013':
											$jvcaid = getChartAccountIDByCode('200-013');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-000':
											$jvcaid = getChartAccountIDByCode('500-000');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-001':
											$jvcaid = getChartAccountIDByCode('500-001');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-002':
											$jvcaid = getChartAccountIDByCode('500-002');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-003':
											$jvcaid = getChartAccountIDByCode('500-003');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-004':
											$jvcaid = getChartAccountIDByCode('500-004');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-005':
											$jvcaid = getChartAccountIDByCode('500-005');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-006':
											$jvcaid = getChartAccountIDByCode('500-006');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-007':
											$jvcaid = getChartAccountIDByCode('500-007');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-008':
											$jvcaid = getChartAccountIDByCode('500-008');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-009':
											$jvcaid = getChartAccountIDByCode('500-009');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-010':
											$jvcaid = getChartAccountIDByCode('500-010');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-011':
											$jvcaid = getChartAccountIDByCode('500-011');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-012':
											$jvcaid = getChartAccountIDByCode('500-012');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-013':
											$jvcaid = getChartAccountIDByCode('500-013');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-014':
											$jvcaid = getChartAccountIDByCode('500-014');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-015':
											$jvcaid = getChartAccountIDByCode('500-015');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										case '500-016':
											$jvcaid = getChartAccountIDByCode('500-016');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;
										
										case '400-002':
											$jvcaid = getChartAccountIDByCode('400-002');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;

										case '100-004':
											$jvcaid = getChartAccountIDByCode('100-004');

											$jvAmountDebit = getGetJVTotalAmountByCA($jvcaid, 'debit');
											$jvAmountCredit = getGetJVTotalAmountByCA($jvcaid, 'credit');

											$totalDebit = $jvAmountDebit;

											$totalCredit = $jvAmountCredit;
										break;


										default:
											$totalDebit = 0;
											$totalCredit = 0;
										break; 
									}

									//formula for trialbalance
									//$nres->code !== '102-000' &&
									if($nres->code !== '101-001' && 
									   $nres->code !== '101-001-04' && $nres->code !== '400-003'):

										$overAllDebit += $totalDebit;
										$overAllCredit += $totalCredit;

										echo '
											<tr>
												<td>'.$nres->code.'
													<input type="hidden" id="tb_hidden_coa'.$ctr.'" value="'.$nres->caid.'" >
												</td>
												<td>'.$nres->name.'</td>
												<td><input type="text" data-id="'.$ctr.'" id="cl_debit_'.$ctr.'" class="form-control cl_debit" value="'.number_format($StartDebit, 2).'"  style="width: 200px;"/></td>
												<td><input type="text" data-id="'.$ctr.'" id="cl_credit_'.$ctr.'" class="form-control cl_credit" value="'.number_format($StartCredit, 2).'"  style="width: 200px;"/>
													<input type="hidden" id="td_hidden_'.$ctr.'" value="'.number_format($totalDebit, 2).'" >
													<input type="hidden" id="tc_hidden_'.$ctr.'" value="'.number_format($totalCredit, 2).'" >
												</td>
												<td id="trans_deb_'.$ctr.'">'.number_format($totalDebit, 2).'</td>
												<td id="trans_cre_'.$ctr.'">'.number_format($totalCredit, 2).'</td>
												<td id="bal_deb_'.$ctr.'">0.00</td>
												<td id="bal_cre_'.$ctr.'">0.00</td>
											</tr>
										';	
										$totalDebit = 0;
										$totalCredit = 0;

										$jvAmountDebit = 0;
										$jvAmountCredit = 0;

										$StartDebit = 0;
										$StartCredit = 0;
										$ctr++;
									endif;
								}
							}else{
								echo '
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								';	
							}
							/*=============================== END: EMPTY TRIAL BALANCE  =====================================*/

	  					}
	  					// END TRIAL BALANCE

  					}
 				?>
 			</tbody>
 			<tfoot>
				<tr>
					<td style="font-weight: bold;"></td>
					<td style="font-weight: bold;">Grand Total :</td>
					<td style="font-weight: bold;" id="tbl_totalPrev_deb">0.00</td>
					<td style="font-weight: bold;" id="tbl_totalPrev_cre">0.00</td>
					<td style="font-weight: bold;" id="tbl_totalTrans_deb"><?php echo number_format($overAllDebit, 2); ?></td>
					<td style="font-weight: bold;" id="tbl_totalTrans_cre"><?php echo number_format($overAllCredit, 2); ?></td>
					<td style="font-weight: bold;" id="tbl_totalUpD_deb">0.00</td>
					<td style="font-weight: bold;" id="tbl_totalUpD_cre">0.00</td>
				</tr>
 			</tfoot>
 		</table>
 		<input type="hidden" id="grid_ctr" value="<?php echo $ctr-1; ?>" >
 	
 	</div>

 </div>

<script>
	function calculate_trial_balance(){
		var grid_ctr = $('#grid_ctr').val();
		// console.log(grid_ctr);
		// for(var i=0; i<=0; i++){
		for(var i=0; i<=grid_ctr; i++){
			var prev_deb = $('#cl_debit_'+i).val();
			// prev_deb = prev_deb.replace(',','');
			prev_deb = prev_deb.replace(/,/g, '');
			// console.log(prev_deb);
			var prev_cre = $('#cl_credit_'+i).val();
			// prev_cre = prev_cre.replace(',','');
			prev_cre = prev_cre.replace(/,/g, '');
			// var prev_ans = Number(prev_deb) - Number(prev_cre);

			// var trans_deb = $('#td_hidden_' + i).val().replace(',','');
			// var trans_cre = $('#tc_hidden_' + i).val().replace(',','');
			var trans_deb = $('#td_hidden_' + i).val().replace(/,/g,'');
			var trans_cre = $('#tc_hidden_' + i).val().replace(/,/g,'');
			var total_deb = Number(prev_deb) + Number(trans_deb);
			var total_cre = Number(prev_cre) + Number(trans_cre);

			var bal_ans = total_deb - total_cre;
			console.log(bal_ans);
			if(bal_ans > 0){
				$('#bal_deb_'+ i).html(numberWithCommas(bal_ans.toFixed(2)) + '');
				$('#bal_cre_'+ i).html('0.00');
			}else{
				$('#bal_deb_'+ i).html('0.00');
				$('#bal_cre_'+ i).html(numberWithCommas(Math.abs(bal_ans).toFixed(2)) + '');
			}

			// console.log(prev_ans);
			// if(prev_ans > 0){
			// 	var new_prev_ans = Number($('#td_hidden_' + i).val().replace(',','')) + Number(prev_ans);

			// 	$('#trans_deb_' + i).html(new_prev_ans + '');

			// 	// $('#trans_cre_' + i).html($('#tc_hidden_' + i).val() + '');
			// }else{
				

			// 	// $('#trans_deb_' + i).html($('#td_hidden_' + i).val() + '');

			// 	var new_prev_ans = Number($('#tc_hidden_' + i).val().replace(',','')) + Number(Math.abs(prev_ans));
			// 	$('#trans_cre_' + i).html( new_prev_ans + '');
			// }
		}

		var newprev_deb = '';
		var tnewprev_dev = 0;
		for(var i = 0; i<=grid_ctr; i++){
			newprev_deb = $('#cl_debit_' + i).val();
			// newprev_deb = newprev_deb.replace(',','');
			newprev_deb = newprev_deb.replace(/,/g, '');

			tnewprev_dev = Number(newprev_deb) + Number(tnewprev_dev);
		}

		var newprev_cre = '';
		var tnewprev_cre = 0;
		for(var i = 0; i<=grid_ctr; i++){
			newprev_cre = $('#cl_credit_' + i).val();
			// newprev_cre = newprev_cre.replace(',','');
			newprev_cre = newprev_cre.replace(/,/g, '');

			tnewprev_cre = Number(newprev_cre) + Number(tnewprev_cre);
		}

		var newUp_deb = '';
		var tnewUp_dev = 0;
		for(var i = 0; i<=grid_ctr; i++){
			newUp_deb = $('#bal_deb_' + i).html();
			// newUp_deb = newUp_deb.replace(',','');
			newUp_deb = newUp_deb.replace(/,/g, '');
			tnewUp_dev = Number(newUp_deb) + Number(tnewUp_dev);
		}

		var newUp_cre = '';
		var tnewUp_cre = 0;
		for(var i = 0; i<=grid_ctr; i++){
			newUp_cre = $('#bal_cre_' + i).html();
			// newUp_cre = newUp_cre.replace(',','');
			newUp_cre = newUp_cre.replace(/,/g, '');
			tnewUp_cre = Number(newUp_cre) + Number(tnewUp_cre);
		}

// trans_cre_

		$('#tbl_totalPrev_deb').html(numberWithCommas(tnewprev_dev.toFixed(2))+'');
		$('#tbl_totalPrev_cre').html(numberWithCommas(tnewprev_cre.toFixed(2))+'');
		// $('#tbl_totalTrans_deb').html();
		// $('#tbl_totalTrans_cre').html();
		$('#tbl_totalUpD_deb').html(numberWithCommas(tnewUp_dev.toFixed(2)) + '');
		$('#tbl_totalUpD_cre').html(numberWithCommas(tnewUp_cre.toFixed(2)) + '');
	}


	$(document).ready(function(){ // START OF DOCUMENT READY
		setTimeout(function(){
			calculate_trial_balance();
		}, 1000);

		$('.cl_debit').blur(function(){
			calculate_trial_balance();
			// $(this).val(numberWithCommas($(this).val()).toFixed(2));
			$(this).val(Number($(this).val()).toFixed(2));
		});
		$('.cl_credit').blur(function(){
			calculate_trial_balance();
			// $(this).val(numberWithCommas($(this).val()).toFixed(2));
			$(this).val(Number($(this).val()).toFixed(2));
		});

        $('.cl_debit').focus(function(){
            $(this).select();
        });

        $('.cl_credit').focus(function(){
            $(this).select();
        });

		$('#btnLoadTB').click(function(){
			// alert('asd');
			window.location = "dashboard.php?page=trial_balance&month=" + $('#selTBMonth').val() + "&year=" + $('#selTBYear').val() + "&action=load";
		});

		$('#btnReLoadTB').click(function(){
			// alert('asd');
			window.location = "dashboard.php?page=trial_balance";
		});

		$('#btnSaveTB').click(function(){

			var month_year = $('#selMonthYear').val();
			var grid_ctr = $('#grid_ctr').val();

			var caid = Array();
			for(var i = 0; i<=grid_ctr; i++){
				caid[i] = $('#tb_hidden_coa' + i).val();
			}

			var prev_deb = Array();
			for(var i = 0; i<=grid_ctr; i++){
				prev_deb[i] = $('#cl_debit_' + i).val();
			}

			var prev_cre = Array();
			for(var i = 0; i<=grid_ctr; i++){
				prev_cre[i] = $('#cl_credit_' + i).val();
			}

			var trans_deb = Array();
			for(var i = 0; i<=grid_ctr; i++){
				trans_deb[i] = $('#trans_deb_' + i).html();
			}

			var trans_cre = Array();
			for(var i = 0; i<=grid_ctr; i++){
				trans_cre[i] = $('#trans_cre_' + i).html();
			}

			var up_deb = Array();
			for(var i = 0; i<=grid_ctr; i++){
				up_deb[i] = $('#bal_deb_' + i).html();
			}

			var up_cre = Array();
			for(var i = 0; i<=grid_ctr; i++){
				up_cre[i] = $('#bal_cre_' + i).html();
			}

			$.ajax({
				type : 'post',
				url  : 'lib/api/trial_balance.db.checker.php',
				data : {
					'month_year':month_year,
				},
				beforeSend: function(){

				},
				success: function(result){
					console.log('RESULT : '+ result);
					if(result == '1'){
						var q = confirm('There\'s a record saved on : "'+ month_year +'" \nDo you want to update/overwrite the data?');
						if(q){
							$.ajax({
								type : 'post',
								url  : 'lib/api/trialbalance.save.php',
								data : {
									'month_year':month_year,
									'grid_ctr':grid_ctr,
									'caid':caid,
									'prev_deb':prev_deb,
									'prev_cre':prev_cre,
									'trans_deb':trans_deb,
									'trans_cre':trans_cre,
									'up_deb':up_deb,
									'up_cre':up_cre,
									'axn' : 'update'
								},
								beforeSend: function(){

								},
								success: function(result){
									alert(result);
									// window.location.reload();
								}
							});
						}
					}else{
						//save
						$.ajax({
							type : 'post',
							url  : 'lib/api/trialbalance.save.php',
							data : {
								'month_year':month_year,
								'grid_ctr':grid_ctr,
								'caid':caid,
								'prev_deb':prev_deb,
								'prev_cre':prev_cre,
								'trans_deb':trans_deb,
								'trans_cre':trans_cre,
								'up_deb':up_deb,
								'up_cre':up_cre,
								'axn' : 'insert'
							},
							beforeSend: function(){

							},
							success: function(result){
								alert(result);
								// window.location.reload();
							}
						});
					}
				}
			});

		});

	});// END OF DOCUMENT READY


</script>

<style>
.cl_debit, .cl_credit {
	width: 200px !important;
}
</style>