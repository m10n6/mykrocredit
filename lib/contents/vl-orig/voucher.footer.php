    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/js/moment/moment.min.js"></script>
    <script src="vendors/js/datepicker/daterangepicker.js"></script>

    <!-- bootstrap progress js -->
    <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>



    <!-- Datatables-->
    <script src="includes/js/datatables/jquery.dataTables.min.js"></script>
    <script src="includes/js/datatables/dataTables.bootstrap.js"></script>
    <script src="includes/js/datatables/dataTables.buttons.min.js"></script>
    <script src="includes/js/datatables/buttons.bootstrap.min.js"></script>
    <script src="includes/js/datatables/jszip.min.js"></script>
    <script src="includes/js/datatables/pdfmake.min.js"></script>
    <script src="includes/js/datatables/vfs_fonts.js"></script>
    <script src="includes/js/datatables/buttons.html5.min.js"></script>
    <script src="includes/js/datatables/buttons.print.min.js"></script>
    <script src="includes/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="includes/js/datatables/dataTables.keyTable.min.js"></script>
    <script src="includes/js/datatables/dataTables.responsive.min.js"></script>
    <script src="includes/js/datatables/responsive.bootstrap.min.js"></script>
    <script src="includes/js/datatables/dataTables.scroller.min.js"></script>

    <script src="includes/js/custom.js"></script>
  <script>
    $(window).load(function() {

   
    });

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    function toTitleCase(str){
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }

    var dt_table;

    function loadVoucherList(callback){
        $.ajax({
            type: 'post',
            url : 'lib/api/voucher.list.php',
            dataType: 'json',
            data : {

            },
            beforeSend: function() {
                var html = '\
                <tr>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                </tr>\
                ';
                $('#tblVoucherList tbody').html(html);
            },
            success : function(x) {
                // alert(x);
                if(callback){
                    callback(x);
                }
            },
            error: function(){
                alert('Please contact developer!');
            }
        });
    }


    function plot_voucherlist_table(){
      
      loadVoucherList(function(result){
        // console.log(result.data);
        var len = result.data.length;
        // console.log(len);
        var html = '';
        for(var i = 0; i < len; i++){
            var obj = JSON.parse(result.data[i]);
            html += '\
                <tr>\
                    <td class="td_name"><strong>'+ obj.last_name +', '+obj.name + ' ' + obj.middle_name +'</strong><br>\
                    <div class=""><a href="#" class="cls_procar" data-value="'+ obj.client_id +'" >Process Loans</a> | \
                    <a href="#" class="cls_payment" data-value="'+ obj.client_id +'" >Payment</a> | \
                    <a href="#" class="cls_viewdata" data-value="'+ obj.client_id +'" >View Data</a> | \
                    <a href="#" class="cls_editprofile" data-value="'+ obj.client_id +'" >Edit Profile</a>\
                    </div>\
                    </td>\
                    <td>' + obj.contact_num +'</td>\
                    <td>' + obj.address +'</td>\
                    <td>' + obj.company +'</td>\
                </tr>\
            ';
        }
        $('#tblVoucherList tbody').html(html);
        // var obj = JSON.parse(result)
        dt_table = $('#tblVoucherList').dataTable({
            aLengthMenu: [
              [10, 25, 50, 100, -1],
              [10, 25, 50, 100, "All"]
            ]
        });
      });        
    }

    $(document).ready(function(){
      // plot_voucherlist_table();

      $('#tblVoucherList').on('click', ".cls_procar", function(){
        var dataID = $(this).data('value');
        $('#modLoans').modal('show');
      });

    });
  </script>
  <style>
  .error-input {
    border: 1px solid red !important;
  }

  .td_name > div {
    display: none;
  }

  .td_name:hover {
    cursor: pointer;
  }

  .td_name:hover > div{
    display: block;
  }

  .td_name:hover a {
    cursor: pointer;
  }

  .hideme { display: none !important; }

  #frmLoans2 { display: none; }
  </style>