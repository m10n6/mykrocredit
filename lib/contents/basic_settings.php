<?php

    include_once('config.php');
    include_once('lib/funcjax.php');
    $res = $conn->dbquery("SELECT * FROM `settings` WHERE `meta` = 'from_email' " );
    $res = json_decode($res);
    $res = json_decode($res->data[0]);

    $vprefix = $conn->dbquery("SELECT * FROM `settings` WHERE `meta` = 'vprefix' " );
    $vprefix = json_decode($vprefix);
    $vprefix = json_decode($vprefix->data[0]);

    $vstart = $conn->dbquery("SELECT * FROM `settings` WHERE `meta` = 'vstart' " );
    $vstart = json_decode($vstart);
    $vstart = json_decode($vstart->data[0]);

    $orsprefix = $conn->dbquery("SELECT * FROM `settings` WHERE `meta` = 'orsprefix' " );
    $orsprefix = json_decode($orsprefix);
    $orsprefix = json_decode($orsprefix->data[0]);

    $orsdigit = $conn->dbquery("SELECT * FROM `settings` WHERE `meta` = 'orsdigit' " );
    $orsdigit = json_decode($orsdigit);
    $orsdigit = json_decode($orsdigit->data[0]);

?>
<div class="row">
  
  <div class="col-md-6">
    <h2>Basic Settings</h2>
    <form>
        <div class="form-group">
            <label class="form-label">Email </label>
            <input type="text" id="email" name="email" class="form-control" value="<?php echo $res->value; ?>"/>
            <small>(this is email will also receive message from Contact Us page and will be used as "From Email" for the messaging inside the system)</small>
        </div>
        <button type="button" class="btn btn-primary" id="btnSave"><i class="fa fa-save"></i> Save</button>
    </form>
    <hr/>
    <h2>Voucher Settings</h2>
    <form>
      <div class="row">
        <!-- <div class="col-md-12"> -->
          <div class="col-md-6">
            <div class="form-group">
                <label class="form-label">Voucher Prefix </label>
                <input type="text" id="vprefix" name="vprefix" class="form-control" value="<?php echo $vprefix->value; ?>"/>
                <small></small>
            </div>

            <button type="button" class="btn btn-primary" id="btnSaveVSettings"><i class="fa fa-save"></i> Save</button>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <label class="form-label">Voucher Digit</label>
                <input type="number" id="vstart" name="vstart" class="form-control" min="0" max="9" size="2" maxlenght="2" value="<?php echo $vstart->value; ?>"/>
                <small></small>
            </div>
            <div id="divLastVoucher"></div>
          </div>
          
        <!-- </div> -->
      </div>
    </form>

    <hr/>
    <h2>Payment Settings</h2>
    <form>
      <div class="row">
        <!-- <div class="col-md-12"> -->
          <div class="col-md-6">
            <div class="form-group">
                <label class="form-label">ORS Prefix </label>
                <input type="text" id="orsprefix" name="orsprefix" class="form-control" value="<?php echo $orsprefix->value; ?>"/>
                <small></small>
            </div>

            <button type="button" class="btn btn-primary" id="btnSaveORSSettings"><i class="fa fa-save"></i> Save</button>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <label class="form-label">ORS Digit</label>
                <input type="number" id="orsdigit" name="orsdigit" class="form-control" min="0" max="9" size="2" maxlenght="2" value="<?php echo $orsdigit->value; ?>"/>
                <small></small>
            </div>
            <div id="divLastORS"></div>
          </div>
          
        <!-- </div> -->
      </div>
    </form>

    <hr/>
    <h2>Added Fees</h2>
    <form>
      <div class="row">
          <div class="col-md-6">
            <div class="form-group">
                <label class="form-label">Processing Fee </label>
                <input type="text" id="fprocess" name="fprocess" class="form-control" value="0.00"/>
                <small></small>
            </div>
            <div class="form-group">
                <label class="form-label">Finders Fee </label>
                <input type="text" id="fprocess" name="fprocess" class="form-control" value="0.00"/>
                <small></small>
            </div>
            <div class="form-group">
                <label class="form-label">Miscelaneous Fee </label>
                <input type="text" id="fprocess" name="fprocess" class="form-control" value="0.00"/>
                <small></small>
            </div>
            <div class="form-group">
                <label class="form-label">Others </label>
                <input type="text" id="fprocess" name="fprocess" class="form-control" value="0.00"/>
                <small></small>
            </div>
<!--             <div class="form-group">
                <label class="form-label">Adjustment </label>
                <input type="text" id="fprocess" name="fprocess" class="form-control" value="0.00"/>
                <small></small>
            </div> -->

            <button type="button" class="btn btn-primary" id="btnSaveFSettings"><i class="fa fa-save"></i> Save</button>
          </div>
      </div>
    </form>    

  </div>

  <div class="col-md-6" style="display: none;">
    <h2>Accounting Settings</h2>
    <hr/>
    <form id="frmAS1">
      <div class="form-group">
          <label><strong>Regular</strong> Loans Percentage</label>
          <input type="text" class="form-control" id="txtRegular1" name="txtRegular[]">
          <input type="text" class="form-control" id="txtRegular2" name="txtRegular[]">
          <input type="text" class="form-control" id="txtRegular3" name="txtRegular[]">
          <button type="button" class="btn btn-primary" id="btnAddRegular"><i class="fa fa-plus"></i></button>
      </div>
    </form>

    <form id="frmAS2">
      <div class="form-group">
          <label><strong>Special</strong> Loans Percentage</label>
          <input type="text" class="form-control" id="txtSpecial1" name="txtSpecial[]">
          <input type="text" class="form-control" id="txtSpecial2" name="txtSpecial[]">
          <input type="text" class="form-control" id="txtSpecial3" name="txtSpecial[]">
          <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
      </div>
    </form>

    <form id="frmAS3">
      <div class="form-group">
          <label><strong>Employee</strong> Loans Percentage</label>
          <input type="text" class="form-control" id="txtEmployee1" name="txtEmployee[]">
          <input type="text" class="form-control" id="txtEmployee2" name="txtEmployee[]">
          <input type="text" class="form-control" id="txtEmployee3" name="txtEmployee[]">
          <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
      </div>
    </form>
  </div>


</div>


<script>
$(document).ready(function(){
  $('#btnSave').click(function(){
      var nemail = $('#email').val();

      if(nemail != ''){
          $.ajax({
            type: 'post',
            url : 'api/api.php',
            data : {
              action : 'saveSettings',
              'value' : nemail,
              'extra' : '',
              'meta'  : 'from_email'
            },
            success: function(xhr){
                console.log(xhr);
                alert(xhr);
                location.reload();
            }
          });
      }else{
          alert('Please fill in all fields!');
      }
  });

  $('#btnAddRegular').click(function(){
     var html = '<div class="divRegular" style="margin-bottom: 10px;">\
                  <button type="button" class="btn btn-danger btnRemoveRegular"><i class="fa fa-minus"></i></button>\
                  <input type="text" name="txtRegular[]" class="form-control">\
                </div>';
     $('#frmAS1').append(html);

    $('.btnRemoveRegular').bind("click", function(){
      $(this).parent().remove();
    });
  });


  $('#btnSaveVSettings').click(function(){
      $.ajax({
        type: 'post',
        url : 'lib/api/voucher.settings.php',
        data : {
          'vstart' : $('#vstart').val(),
          'vprefix' : $('#vprefix').val()
        },
        success: function(xhr){
            console.log(xhr);
            alert(xhr);
            // location.reload();
        }
      });
    
  });

  $('#btnSaveORSSettings').click(function(){
      $.ajax({
        type: 'post',
        url : 'lib/api/ors.settings.php',
        data : {
          'orsprefix' : $('#orsprefix').val(),
          'orsdigit' : $('#orsdigit').val()
        },
        success: function(xhr){
            console.log(xhr);
            alert(xhr);
            // location.reload();
        }
      });
  });

});
</script>