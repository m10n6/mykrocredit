<?php
	$sql = "select * from `users` where `user_id` = '".$_SESSION['uuid']."'";
	$rs  = $conn->dbquery($sql);

	if($rs !== 'false'){
		$rs = json_decode($rs);
		foreach ($rs->data as $r) {
			# code...
			$nr = json_decode($r);
			$name = $nr->name;
			$email = $nr->email;
			$uname = $nr->username;
		}
	}
?>

<div class="row">
	<div class="col-md-12">
		<h1>My Profile</h1>

		<div class="row">
			<div class="col-md-6">
				<h2>Personal Info</h2>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Name</label>
							<input type="text" id="fullname" class="form-control" value="<?php echo $name; ?>">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Email</label>
							<input type="text" id="email" class="form-control" value="<?php echo $email; ?>">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<button type="button" class="btn btn-primary" id="btnSavePersonal"> <i class="fa fa-save"></i> SAVE </button>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<h2>Login Info</h2>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Username</label>
							<input type="text" id="username" class="form-control" value="<?php echo $uname; ?>">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>New Password (Leave blank New Password to unchange)</label>
							<input type="password" id="newpass" class="form-control">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Confirm Password</label>
							<input type="password" id="conpass" class="form-control">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<button type="button" class="btn btn-primary" id="btnSaveLogin"> <i class="fa fa-save"></i> SAVE </button>
					</div>
				</div>
			</div>
		</div>


	</div>

	<div class="spacer20"></div>
</div>

<script type="text/javascript">
	function validateEmail(email) {
	  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return re.test(email);
	}

	$('#btnSavePersonal').click(function(){
		var fname = $('#fullname').val();
		var email = $('#email').val();

		if(fname != '' && email != '' && validateEmail(email)){
			$.ajax({
				type : 'post',
				url  : 'lib/api/save.myprofile.php',
				data : {
					uid : '<?php echo $_SESSION['uuid']; ?>',
					nm  : fname,
					em  : email,
					axn : 'savePersonal'
				},
				dataType: 'json',
				beforeSend : function(){
					$('#btnSavePersonal').html(' <i class="fa fa-gear fa-spin"></i> Saving... ');
				},
				success : function(res){
					if(res.message == 'success'){
						window.location.reload();
					}else{
						alert(res.message);
					}
					$('#btnSavePersonal').html(' <i class="fa fa-save"></i> SAVE ');
				}
			});
		}else{
			alert('Please enter Name and Email (make sure email format is valid)!');
		}
	});


	$('#btnSaveLogin').click(function(){

		var username = $('#username').val();
		var newpass = $('#newpass').val();
		var conpass = $('#conpass').val();

		if(username != ''){
			$.ajax({
				type : 'post',
				url  : 'lib/api/save.myprofile.php',
				data : {
					uid : '<?php echo $_SESSION['uuid']; ?>',
					un  : username,
					np  : newpass,
					cp  : conpass,
					axn : 'savelogin'
				},
				dataType: 'json',
				beforeSend : function(){
					$('#btnSaveLogin').html(' <i class="fa fa-gear fa-spin"></i> Saving... ');
				},
				success : function(res){
					console.log(res);
					if(res.message != 'Username is already in use by other user!'){
						alert(res.message);
						window.location.reload();
					}else{
						alert(res.message);
					}
					$('#btnSaveLogin').html(' <i class="fa fa-save"></i> SAVE ');
				}
			});
		}else{
			alert('Please enter Username!');
		}
	});



</script>