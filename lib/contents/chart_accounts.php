<?php if($_SESSION['ulevel'] == '1'){ ?>

 <div class="row">
 	<div class="col-md-12">

 		<button class="btn btn-primary" type="button" id="btnAddNew"><i class="fa fa-plus"></i> Add New Account </button>

 		<hr/>

 		<table class="table table-striped" id="tblChartList">
 			<thead>
 				<tr>
 					<th>Account Code</th>
 					<th>Account Name</th>
 					<th>Category</th>
 					<th>Action</th>
 				</tr>
 			</thead>
 			<tfoot>
 				<tr>
 					<td>Account Code</td>
 					<td>Account Name</td>
 					<td>Category</td>
 					<td>Action</td>
 				</tr>
 			</tfoot>
 			<tbody>
 				<tr>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 				</tr>

 			</tbody>
 		</table>
 	
 	</div>

 </div>


 <div class="modal fade" tabindex="-1" role="dialog" id="modAddNew">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <form id="frmAccnt">
        	<div class="col-md-12">
        		<div class="form-group">
        			<label>Account Code *</label>
        			<input type="text" id="txtAccCode" class="form-control" />
        			<input type="hidden" id="txtAccID" class="form-control" />
        		</div>

        		<div class="form-group">
        			<label>Account Name *</label>
        			<input type="text" id="txtAccName" class="form-control" />
        		</div>

        		<div class="form-group">
        			<label>Account Category <small>(ex. CASH, LOANS and other receivables, etc)</small></label>
        			<input type="text" id="txtAccCategory" class="form-control" >
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnSave" style="margin-bottom: 5px;"><i class="fa fa-save"></i> Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

var dt_table;

function loadChartList(callback){
    $.ajax({
        type: 'post',
        url : 'lib/api/chart_account.list.php',
        dataType: 'json',
        data : {

        },
        beforeSend: function() {
            var html = '\
            <tr>\
                <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
            </tr>\
            ';
            $('#tblChartList tbody').html(html);
        },
        success : function(x) {
            console.log(x);
            if(callback){
                callback(x);
            }
        },
        error: function(){
            alert('Please contact developer!');
        }
    });
}

function plot_chartlist_table(){
  
  loadChartList(function(result){
    console.log(result);
    
    // console.log(len);
    var html = '';
    if(result){
	    var len = result.data.length;
	    for(var i = 0; i < len; i++){
	        var obj = JSON.parse(result.data[i]);
	        html += '\
	            <tr>\
	                <td class="td_name">' + obj.code +'</td>\
	                <td>' + obj.name +'</td>\
	                <td>' + obj.category +'</td>\
	                <td>\
	                <button type="button" data-value="'+ obj.caid +'" class="btn btn-primary btnEdit"><i class="fa fa-pencil"></i></button>\
	                <button type="button" data-value="'+ obj.caid +'" class="btn btn-danger btnDelete"><i class="fa fa-trash"></i></button>\
	                </td>\
	            </tr>\
	        ';
	    }    	
    }else{
        html += '\
            <tr>\
                <td class="td_name"></td>\
                <td></td>\
                <td></td>\
                <td></td>\
            </tr>\
        ';
    }

    $('#tblChartList tbody').html(html);
    // var obj = JSON.parse(result)
    dt_table = $('#tblChartList').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ]
    });
  });        
}



$(document).ready(function(){

	plot_chartlist_table();

	$('#btnAddNew').click(function(){
		$('#txtAccID').val('');
		$('#modAddNew .modal-title').html('Add New Account');
		$('#modAddNew').modal('show');
	});

	$('#tblChartList').on('click', '.btnEdit', function(){
        var cid = $(this).data('value');
        $.ajax({
            type: 'post',
            url : 'lib/api/chart_account.get.php',
            data : {
                'cid' : cid
            },
            dataType : 'json',
            success : function(result){
            	console.log(result);
            	var data = JSON.parse(result.data);
				$('#txtAccCode').val(data.code);
				$('#txtAccID').val(data.caid);
				$('#txtAccName').val(data.name);
				$('#txtAccCategory').val(data.category);
				$('#modAddNew .modal-title').html('Edit Account');
				$('#modAddNew').modal('show');

            }
        });
	});

	$('#tblChartList').on('click', '.btnDelete', function(){
		var cid = $(this).data('value');
		var q = confirm('Are you sure you want to delete the Account?');
		if(q){
			$.ajax({
				type: 'post',
				url : 'lib/api/chart_account.delete.php',
				data : {
					'cid' : cid
				},
				success: function(result){
                    dt_table.fnDestroy();
                    plot_chartlist_table();
                    growlme('Account has been deleted!', '','notice');
				}
			});
		}
	});

	$('#btnSave').click(function(){
		var txtAccCode = $('#txtAccCode').val();
		var txtAccID = $('#txtAccID').val();
		var txtAccName = $('#txtAccName').val();
		var txtAccCategory = $('#txtAccCategory').val();

		if(txtAccCode == '' && txtAccName == ''){
			growlme('Please enter Account # and CDO ','','error');
		}else{
			$.ajax({
				type: 'post',
				url : 'lib/api/chart_account.add.php',
				data: {
					'cid' :txtAccID,
					'ccode' : txtAccCode,
					'cname' : txtAccName,
					'ccat'  : txtAccCategory,
				},
				dataTye: 'json',
				success : function(x){
                    if(x == 'success'){
                        //reload list                        
                        document.getElementById('frmAccnt').reset();
                        console.log(dt_table);
                        dt_table.fnDestroy();
                        setTimeout(function(){
                            plot_chartlist_table();
                        });
                        $('#modAddNew').modal('hide');
                        growlme('New Chart of Account Added!', '','notice');
                    }else if(x == 'save'){
                        document.getElementById('frmAccnt').reset();
                        console.log(dt_table);
                        dt_table.fnDestroy();
                        setTimeout(function(){
                            plot_chartlist_table();
                        });
                        $('#modAddNew').modal('hide');
                        growlme('New Chart of Account Saved!', '','notice');

                    }else{
                        alert('Chart of Account is already on the list!');
                    }
				}
			});
		}

	});
});

</script>

<!-- `caid`, `code`, `name`, `is_display`, `date_added` -->

<?php } else { ?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger">
            <h2>You're not allowed to view this page! Please contact administrator!</h2>
        </div>
    </div>
</div>
<?php } ?>