    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/js/moment/moment.min.js"></script>
    <script src="vendors/js/datepicker/daterangepicker.js"></script>

    <!-- bootstrap progress js -->
    <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>



    <!-- Datatables-->
    <script src="includes/js/datatables/jquery.dataTables.min.js"></script>
    <script src="includes/js/datatables/dataTables.bootstrap.js"></script>
    <script src="includes/js/datatables/dataTables.buttons.min.js"></script>
    <script src="includes/js/datatables/buttons.bootstrap.min.js"></script>
    <script src="includes/js/datatables/jszip.min.js"></script>
    <script src="includes/js/datatables/pdfmake.min.js"></script>
    <script src="includes/js/datatables/vfs_fonts.js"></script>
    <script src="includes/js/datatables/buttons.html5.min.js"></script>
    <script src="includes/js/datatables/buttons.print.min.js"></script>
    <script src="includes/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="includes/js/datatables/dataTables.keyTable.min.js"></script>
    <script src="includes/js/datatables/dataTables.responsive.min.js"></script>
    <script src="includes/js/datatables/responsive.bootstrap.min.js"></script>
    <script src="includes/js/datatables/dataTables.scroller.min.js"></script>

    
    <script src="includes/js/custom.js"></script>
    
  <script>

    /*=========================================================================================================================*/
    var th = ['', 'thousand', 'million', 'billion', 'trillion'];

    var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
    var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
    var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    function MoneytoWords(s) {
        s = s.toString();
        s = s.replace(/[\, ]/g, '');
        if (s != parseFloat(s)) return 'not a number';
        var x = s.indexOf('.');
        if (x == -1) x = s.length;
        if (x > 15) return 'too big';
        var n = s.split('');
        var str = '';
        var sk = 0;
        for (var i = 0; i < x; i++) {
            if ((x - i) % 3 == 2) {
                if (n[i] == '1') {
                    str += tn[Number(n[i + 1])] + ' ';
                    i++;
                    sk = 1;
                } else if (n[i] != 0) {
                    str += tw[n[i] - 2] + ' ';
                    sk = 1;
                }
            } else if (n[i] != 0) {
                str += dg[n[i]] + ' ';
                if ((x - i) % 3 == 0) str += 'hundred ';
                sk = 1;
            }
            if ((x - i) % 3 == 1) {
                if (sk) str += th[(x - i - 1) / 3] + ' ';
                sk = 0;
            }
        }
        if (x != s.length) {
            var y = s.length;
            if(dg[n[x+2]] > 0){
                str += 'and ';
                for (var i = x + 1; i < y; i++){
                    if(dg[n[i]] != 'zero'){
                        str += dg[n[i]] + ' ';        
                    }
                }                
            }

        }
        return str.replace(/\s+/g, ' ');
    }
    /*=========================================================================================================================*/


    function editARFunc(fid){
        alert('Edit function here!');
    }

    function deleteARFunc(fid){
        var q = confirm('Are you sure you want to Delete AR?');
        if(q){
            // alert(fid);
            // $.ajax({
            //     type : 'post',
            //     url  : 'lib/api/ar.delete.php',
            //     data : {
            //         ''
            //     }
            // });
        }
    }

    /* Formatting function for row details - modify as you need */
    function format ( d ) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; width: 100%;" class="tblChildRow">'+
            '<tr>'+
                '<td style="width: 100%;">\
                    <div class="pull-right">\
                        <button type="button" class="btn btn-success btnEditAR" data-fid="'+ d +'" onclick="editARFunc(\''+ d +'\')"><i class="fa fa-pencil"></i></button>\
                        <button type="button" class="btn btn-danger btnDeleteAR"  data-fid="'+ d +'"  onclick="deleteARFunc(\''+ d +'\')"><i class="fa fa-trash"></i></button>\
                    </div>\
                </td>'+
            '</tr>'+
        '</table>';
    }

    $(window).load(function() {

   
    });

    var dt_table;

    function loadVoucherList(callback){
        $.ajax({
            type: 'post',
            url : 'lib/api/ar.list.php',
            dataType: 'json',
            data : {

            },
            beforeSend: function() {
                var html = '\
                <tr>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                </tr>\
                ';
                $('#tblVoucherList tbody').html(html);
            },
            success : function(x) {
                // alert(x);
                console.log(x);
                if(callback){
                    callback(x);
                }
            },
            error: function(){
                alert('Please contact developer!');
            }
        });
    }



    function plot_voucherlist_table(){
      
      loadVoucherList(function(result){
        console.log(result.data);
        var html = '';
        if(result){

            var len = result.data.length;
            // console.log(len);
            
            for(var i = 0; i < len; i++){
                var obj = JSON.parse(result.data[i]);

                    // <tr>
                    //     <th>CV #</th>
                    //     <th>Name</th>
                    //     <th>Amount</th>
                    //     <th>Interest</th>
                    //     <th>Interest Type</th>
                    //     <th>Term</th>
                    //     <th>Loan Type</th>
                    //     <th>Date</th>
                    //     <th>Action</th>
                    // </tr>
                var obj2 = JSON.parse(obj.extra);
                // console.log(obj.extra);

                var ld = obj.loan_date;

                var axn;
                ld = ld.split(' ');
                if(obj.status != 'approved'){
                    axn = '\
                            <button type="button" class="btn btn-success btnApprove" data-value="'+ obj.fid +'" data-clientid="'+ obj.client_id +'" data-toggle="tooltip" data-placement="top" title="Approve Transacton" ><i class="fa fa-thumbs-o-up"></i></button>\
                            <button type="button" class="btn btn-danger btnDecline"  data-value="'+ obj.fid +'" data-clientid="'+ obj.client_id +'" data-toggle="tooltip" data-placement="top" title="Decline Transacton" ><i class="fa fa-thumbs-o-down"></i></button>\
                    ';
                }else{
                    axn = '\
                        <button type="button" class="btn btn-primary btnView" data-value="'+ obj.fid +'" data-clientid="'+ obj.client_id +'" data-toggle="tooltip" data-placement="top" title="View Transacton" ><i class="fa fa-eye"></i></button>\
                    ';

                    <?php if($_SESSION['ulevel'] != '3'):  ?>
                    axn += '\
                        <button type="button" class="btn btn-warning btnUpdate" data-value="'+ obj.fid +'" data-clientid="'+ obj.client_id +'" data-toggle="tooltip" data-placement="top" title="Update Transacton" ><i class="fa fa-pencil"></i></button>\
                    ';
                    <?php endif; ?>
                }

                if(obj.status == 'declined'){
                    is_declined = 'declined';
                    axn = 'DECLINED!';
                }else{
                    is_declined = '';
                }


                if( obj.loan_type.indexOf('payment') > -1 || obj.loan_type.indexOf('jv') > -1 || obj.loan_type.indexOf('beg_bal') > -1  ){

                }else{

                    html += '\
                        <tr class="'+ is_declined +'">\
                            <td>'+ obj.voucher_id +'</td>\
                            <td class="details-control" data-fid="'+ obj.fid +'">'+ obj.last_name +', '+ obj.name +' '+ obj.middle_name +'.</td>\
                            <td>Php '+ numberWithCommas(Number(obj.amount).toFixed(2)) +'</td>\
                            <td>'+ obj.intrate +'%</td>\
                            <td>'+ obj2.TypeRate.capitalize() +'</td>\
                            <td>'+ obj.term +'</td>\
                            <td>'+ toTitleCase(valueToWords(obj.loan_type)) +'</td>\
                            <td>'+ ld[0] +'</td>\
                            <td>'+ axn +'</td>\
                        </tr>\
                    ';
                }
                // html += '\
                //     <tr>\
                //         <td class="td_name"><strong>'+ obj.last_name +', '+obj.name + ' ' + obj.middle_name +'</strong><br>\
                //         <div class=""><a href="#" class="cls_procar" data-value="'+ obj.client_id +'" >Process Loans</a> | \
                //         <a href="#" class="cls_payment" data-value="'+ obj.client_id +'" >Payment</a> | \
                //         <a href="#" class="cls_viewdata" data-value="'+ obj.client_id +'" >View Data</a> | \
                //         <a href="#" class="cls_editprofile" data-value="'+ obj.client_id +'" >Edit Profile</a>\
                //         </div>\
                //         </td>\
                //         <td>' + obj.contact_num +'</td>\
                //         <td>' + obj.address +'</td>\
                //         <td>' + obj.company +'</td>\
                //     </tr>\
                // ';
            }

        }else{
            // html = '\
            //     <tr>\
            //         <td></td>\
            //         <td class="details-control"></td>\
            //         <td></td>\
            //         <td></td>\
            //         <td></td>\
            //         <td></td>\
            //         <td></td>\
            //         <td></td>\
            //         <td></td>\
            //     </tr>\
            // ';
        }

        $('#tblVoucherList tbody').html(html);
        // var obj = JSON.parse(result)
        dt_table = $('#tblVoucherList').DataTable({
            aLengthMenu: [
              [10, 25, 50, 100, -1],
              [10, 25, 50, 100, "All"]
            ],
            "bSort": false
        });
      });        
    }

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
        
        $('#btnConfirmVoucher').hide();
        $('#btnConfirmVoucherPrint').hide();
        $('#btnUpdateVoucher').hide();

      plot_voucherlist_table();

      $('#tblVoucherList').on('click', ".cls_procar", function(){
        var dataID = $(this).data('value');
        $('#modLoans').modal('show');
      });

        // $('.tblChildRow').on('click', '.btnEditAR', function(){
        //     alert('Edit');
        // });

        // $('.tblChildRow').on('click', '.btnDeleteAR', function(){
        //     alert('Delete'); 
        // });

       // Add event listener for opening and closing details
        $('#tblVoucherList tbody').on('click', 'td.details-control', function () {
            var fid = $(this).data('fid');

            var tr = $(this).closest('tr');
            var row = dt_table.row( tr );
     
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(fid) ).show();
                tr.addClass('shown');
            }
        } );

        $('#tblVoucherList tbody').on('click', '.btnView', function(e){
            e.preventDefault();
            var vid = $(this).data('value');
            var cvid = $(this).data('clientid');

            $('#btnConfirmVoucher').hide();
            $('#btnConfirmVoucherPrint').hide();
            $('#btnUpdateVoucher').hide();

            $('#menu_toggle').trigger('click');
            $('#divList').hide(function(){
                
                $.ajax({
                    type: 'post',
                    url : 'lib/api/ar.plain.get.php',
                    data : {
                        'fid' : vid
                    },
                    dataType : 'json',
                    success : function(res){
                        console.log(res);
                        var obj = JSON.parse(res.data);
                        // var obj = res[0];
                        console.log(obj);
                        $('#voucherFID').val(vid);
                        $('#voucherClientID').val(cvid);

                        $('#cl_name').html(obj.last_name + ', ' + obj.name + ' ' + obj.middle_name);
                        $('#cl_comp').html(obj.company);
                        $('#cl_cvnum').html(obj.voucher_id);
                        $('#cl_bank').html();
                        $('#cl_date').html();

                        var new_intrate = Number(obj.intrate) / 100;
                        var interest = Number(obj.amount).toFixed(2) * new_intrate * Number(obj.term); 

                        $('#txtpromNote').val( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                        $('#txtInterest').val( numberWithCommas( interest.toFixed(2) ) );
                        $('#txtInterestRate').val( obj.intrate + ' %' );
                        $('#txtTerms').val( obj.term );
                        $('#txtGross').val( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                        $('#txtNetProceeds').html( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                        $('#divPrint').show();
                        $('#inpTotalCharge').val();
                        $('#inpNetProceeds').val(Number(obj.amount).toFixed(2));
                        $('#inpOrigNetProceeds').val(Number(obj.amount).toFixed(2));


                        $('#inProcFee').val(Number(obj.inProcFee).toFixed(2));
                        $('#inpFindersFee').val(Number(obj.inpFindersFee).toFixed(2));
                        $('#inpNotarial').val(Number(obj.inpNotarial).toFixed(2));
                        $('#inpMisc').val(Number(obj.inpMisc).toFixed(2));
                        $('#inpInsurance').val(Number(obj.inpInsurance).toFixed(2));
                        $('#inpOthers').val(Number(obj.inpOthers).toFixed(2));
                        $('#inpAdjustment').val(Number(obj.inpAdjustment).toFixed(2));


                        $('#inpBonus').val(Number(obj.inpBonus).toFixed(2));
                        $('#cl_date').html(obj.date_approved);

                        setTimeout(function(){
                            calculateMe();
                        }, 1000);

                        $('#inProcFee').prop("disabled", true);
                        $('#inpFindersFee').prop("disabled", true);
                        $('#inpNotarial').prop("disabled", true);
                        $('#inpMisc').prop("disabled", true);
                        $('#inpInsurance').prop("disabled", true);
                        $('#inpOthers').prop("disabled", true);
                        $('#inpAdjustment').prop("disabled", true);
                        $('#inpBonus').prop("disabled", true);

                        // var date_now = moment().format('MM-DD-YYYY');
                        // $('#cl_date').html(date_now);
                        // inProcFee
                        // inpFindersFee
                        // inpMisc
                        // inpOthers
                        // inpAdjustment
                        // inpBonus
                        // txtTotalCharge
                    }
                });
            });

        });

        $('#tblVoucherList tbody').on('click', '.btnUpdate', function(e){
            e.preventDefault();
            var vid = $(this).data('value');
            var cvid = $(this).data('clientid');

            $('#btnConfirmVoucher').hide();
            $('#btnConfirmVoucherPrint').hide();
            $('#btnUpdateVoucher').show();

            $('#menu_toggle').trigger('click');
            $('#divList').hide(function(){
                
                $.ajax({
                    type: 'post',
                    url : 'lib/api/ar.plain.get.php',
                    data : {
                        'fid' : vid
                    },
                    dataType : 'json',
                    success : function(res){
                        console.log(res);
                        var obj = JSON.parse(res.data);
                        // var obj = res[0];
                        console.log(obj);
                        $('#voucherFID').val(vid);
                        $('#voucherClientID').val(cvid);

                        $('#cl_name').html(obj.last_name + ', ' + obj.name + ' ' + obj.middle_name + '.');
                        $('#cl_comp').html(obj.company);
                        $('#cl_cvnum').html(obj.voucher_id);
                        $('#cl_bank').html();
                        $('#cl_date').html();

                        var new_intrate = Number(obj.intrate) / 100;
                        var interest = Number(obj.amount).toFixed(2) * new_intrate * Number(obj.term); 

                        $('#txtpromNote').val( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                        $('#txtInterest').val( numberWithCommas( interest.toFixed(2) ) );
                        $('#txtInterestRate').val( obj.intrate + ' %' );
                        $('#txtTerms').val( obj.term );
                        $('#txtGross').val( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                        $('#txtNetProceeds').html( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                        $('#divPrint').show();
                        $('#inpTotalCharge').val();
                        $('#inpNetProceeds').val(Number(obj.amount).toFixed(2));
                        $('#inpOrigNetProceeds').val(Number(obj.amount).toFixed(2));


                        $('#inProcFee').val(Number(obj.inProcFee).toFixed(2));
                        $('#inpFindersFee').val(Number(obj.inpFindersFee).toFixed(2));
                        $('#inpNotarial').val(Number(obj.inpNotarial).toFixed(2));
                        $('#inpMisc').val(Number(obj.inpMisc).toFixed(2));
                        $('#inpInsurance').val(Number(obj.inpInsurance).toFixed(2));
                        $('#inpOthers').val(Number(obj.inpOthers).toFixed(2));
                        $('#inpAdjustment').val(Number(obj.inpAdjustment).toFixed(2));


                        $('#inpBonus').val(Number(obj.inpBonus).toFixed(2));
                        $('#cl_date').html(obj.date_approved);

                        setTimeout(function(){
                            calculateMe();
                        }, 1000);

                        $('#inProcFee').prop("disabled", false);
                        $('#inpFindersFee').prop("disabled", false);
                        $('#inpNotarial').prop("disabled", false);
                        $('#inpMisc').prop("disabled", false);
                        $('#inpInsurance').prop("disabled", false);
                        $('#inpOthers').prop("disabled", false);
                        $('#inpAdjustment').prop("disabled", false);
                        $('#inpBonus').prop("disabled", false);

                        // var date_now = moment().format('MM-DD-YYYY');
                        // $('#cl_date').html(date_now);
                        // inProcFee
                        // inpFindersFee
                        // inpMisc
                        // inpOthers
                        // inpAdjustment
                        // inpBonus
                        // txtTotalCharge
                    }
                });
            });

        });

        $('#tblVoucherList tbody').on('click', '.btnDecline', function(e){
            e.preventDefault();
            var vid = $(this).data('value');
            var cvid = $(this).data('clientid');

            var q = confirm('You are DECLINING an Account!!!\nAre you sure you want to DECLINE?');
            if(q){
                $.ajax({
                    type: 'post',
                    url : 'lib/api/decline.loan.php',
                    data : {
                        'fid' : vid,
                        'cid' : cvid,
                        'uuid' : <?php echo $_SESSION['uuid']; ?>
                    },
                    success: function(result){
                        alert('Loan has been declined!');
                        // location.reload();
                        dt_table.destroy();
                        
                        plot_voucherlist_table();
                    }
                });
            }            
        });


        $('#tblVoucherList tbody').on('click', '.btnApprove', function(e){
            e.preventDefault();
            $('#btnUpdateVoucher').hide();

            $('#inProcFee').prop("disabled", false);
            $('#inpFindersFee').prop("disabled", false);
            $('#inpNotarial').prop("disabled", false);
            $('#inpMisc').prop("disabled", false);
            $('#inpInsurance').prop("disabled", false);
            $('#inpOthers').prop("disabled", false);
            $('#inpAdjustment').prop("disabled", false);
            $('#inpBonus').prop("disabled", false);

            $('#inProcFee').val('0.00');
            $('#inpFindersFee').val('0.00');
            $('#inpNotarial').val('0.00');
            $('#inpMisc').val('0.00');
            $('#inpInsurance').val('0.00');
            $('#inpOthers').val('0.00');
            $('#inpAdjustment').val('0.00');
            $('#inpBonus').val('0.00');

            var vid = $(this).data('value');
            var cvid = $(this).data('clientid');

            var q = confirm('Are you sure you want to Approve?');
            if(q){

                $('#btnConfirmVoucher').show();
                $('#btnConfirmVoucherPrint').show();

                $('#menu_toggle').trigger('click');
                $('#divList').hide(function(){
                    
                    $.ajax({
                        type: 'post',
                        url : 'lib/api/ar.get.php',
                        data : {
                            'fid' : vid,
                            'cid' : cvid,
                            'uuid' : <?php echo $_SESSION['uuid']; ?>
                        },
                        dataType : 'json',
                        success : function(res){
                            console.log(res);
                            var obj = JSON.parse(res.data);
                            // var obj = res[0];
                            console.log(obj);
                            $('#voucherFID').val(vid);
                            $('#voucherClientID').val(cvid);

                            $('#cl_name').html(obj.last_name + ', ' + obj.name + ' ' + obj.middle_name + '.');
                            $('#cl_comp').html(obj.company);
                            $('#cl_cvnum').html(obj.voucher_id);
                            $('#cl_bank').html();
                            $('#cl_date').html();

                            var new_intrate = Number(obj.intrate) / 100;
                            var interest = Number(obj.amount).toFixed(2) * new_intrate * Number(obj.term); 

                            $('#txtpromNote').val( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                            $('#txtInterest').val( numberWithCommas( interest.toFixed(2) ) );
                            $('#txtInterestRate').val( obj.intrate + ' %' );
                            $('#txtTerms').val( obj.term );
                            $('#txtGross').val( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                            $('#txtNetProceeds').html( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                            $('#divPrint').show();
                            $('#inpTotalCharge').val();
                            $('#inpNetProceeds').val(Number(obj.amount).toFixed(2));
                            $('#inpOrigNetProceeds').val(Number(obj.amount).toFixed(2));

                            var date_now = moment().format('MM-DD-YYYY');
                            $('#cl_date').html(date_now);

                            // if(obj.loan_type == 'new_loans' || obj.loan_type == 'renewal'){
                            //     if(obj.amount > 100000 ){

                            //     }
                            // }

                            if(obj.loan_type == 'cash_advance'){
                                var c_new_intrate, n_interest;
                                if(obj.amount <= 4000 ){
                                    c_new_intrate = 3 / 100;
                                    n_interest = Number(obj.amount).toFixed(2) * Number(c_new_intrate);
                                }else if(obj.amount > 4000 ){
                                    c_new_intrate = 5 / 100;
                                    n_interest = Number(obj.amount).toFixed(2) * Number(c_new_intrate);
                                }
                                // $('#inProcFee').val(n_interest.toFixed(2));
                                $('#inpMisc').val(n_interest.toFixed(2));
                            }

                            setTimeout(function(){
                                calculateMe();
                            }, 1000);
                            // inProcFee
                            // inpFindersFee
                            // inpMisc
                            // inpOthers
                            // inpAdjustment
                            // inpBonus
                            // txtTotalCharge
                        }
                    });
                });            
            }
        });

        //UPDATE VOUCHER
        $('#btnUpdateVoucher').click(function(){
            var q = confirm('Are you sure you want to update?');
            if(q){
                var p_name = $('#cl_name').text();
                var p_cvnum = $('#cl_cvnum').text();
                var p_datenum = $('#cl_date').text();
                var p_loan = $('#txtpromNote').val();
                var p_procfee = $('#inProcFee').val();
                var p_misc = $('#inpMisc').val();
                var p_findersfee = $('#inpFindersFee').val();
                var p_insurance = $('#inpInsurance').val();
                var p_notarial = $('#inpNotarial').val();
                var p_others = $('#inpOthers').val();
                var p_bonus = $('#inpBonus').val();
                var p_adjustment = $('#inpAdjustment').val();
                var p_totalCharge = $('#txtTotalCharge').text();
                var p_netproceeds = $('#txtNetProceeds').text();

                $('#p_name1').html(p_name);
                $('#p_cvnum1').html(p_cvnum);
                $('#p_datenum1').html(p_datenum);
                $('#p_loan1').html(p_loan);
                $('#p_procfee1').html(p_procfee);
                $('#p_misc1').html(p_misc);
                $('#p_findersfee1').html(p_findersfee);
                $('#p_insurance1').html(p_insurance);
                $('#p_notarial1').html(p_notarial);
                $('#p_others1').html(p_others);
                $('#p_totalCharge1').html(p_totalCharge);
                $('#p_netproceeds1').html(p_netproceeds);

                $('#p_name2').html(p_name);
                $('#p_cvnum2').html(p_cvnum);
                $('#p_datenum2').html(p_datenum);
                $('#p_loan2').html(p_loan);
                $('#p_procfee2').html(p_procfee);
                $('#p_misc2').html(p_misc);
                $('#p_findersfee2').html(p_findersfee);
                $('#p_insurance2').html(p_insurance);
                $('#p_notarial2').html(p_notarial);
                $('#p_others2').html(p_others);
                $('#p_totalCharge2').html(p_totalCharge);
                $('#p_netproceeds2').html(p_netproceeds);

                $('#p_name3').html(p_name);
                $('#p_cvnum3').html(p_cvnum);
                $('#p_datenum3').html(p_datenum);
                $('#p_loan3').html(p_loan);
                $('#p_procfee3').html(p_procfee);
                $('#p_misc3').html(p_misc);
                $('#p_findersfee3').html(p_findersfee);
                $('#p_insurance3').html(p_insurance);
                $('#p_notarial3').html(p_notarial);
                $('#p_others3').html(p_others);
                $('#p_totalCharge3').html(p_totalCharge);
                $('#p_netproceeds3').html(p_netproceeds);

                $('#numToText').html(MoneytoWords(p_loan) + '');
                $('#p_loan_last').html(p_loan);

                var fid = $('#voucherFID').val();
                var cid = $('#voucherClientID').val();
                $.ajax({
                    type : 'post',
                    url  : 'lib/api/update.loan.approval.php',
                    data : {
                        'fid' : fid,
                        'cid' : cid,
                        'inProcFee' : p_procfee,
                        'inpFindersFee' : p_findersfee,
                        'inpNotarial' : p_notarial,
                        'inpMisc' : p_misc,
                        'inpInsurance' : p_insurance,
                        'inpOthers' : p_others,
                        'inpAdjustment' : p_adjustment,
                        'inpBonus' : p_bonus,
                        'uuid' : '<?php echo $_SESSION['uuid']; ?>'
                    },
                    success : function(result){
                        alert('Done update!');
                        if(callback){
                            callback();
                        }
                    }
                });
            }
        });


        $('#tblVoucherList tbody').on('click', '.btnUpdateVoucher', function(e){
            e.preventDefault();
            alert('Update VOUCHER is under repair!');

            // $('#inProcFee').prop("disabled", false);
            // $('#inpFindersFee').prop("disabled", false);
            // $('#inpNotarial').prop("disabled", false);
            // $('#inpMisc').prop("disabled", false);
            // $('#inpInsurance').prop("disabled", false);
            // $('#inpOthers').prop("disabled", false);
            // $('#inpAdjustment').prop("disabled", false);
            // $('#inpBonus').prop("disabled", false);

            // $('#inProcFee').val('0.00');
            // $('#inpFindersFee').val('0.00');
            // $('#inpNotarial').val('0.00');
            // $('#inpMisc').val('0.00');
            // $('#inpInsurance').val('0.00');
            // $('#inpOthers').val('0.00');
            // $('#inpAdjustment').val('0.00');
            // $('#inpBonus').val('0.00');

            var vid = $(this).data('value');
            var cvid = $(this).data('clientid');

            var q = confirm('Are you sure you want to Update?');
            if(q){

                // $('#btnConfirmVoucher').show();
                // $('#btnConfirmVoucherPrint').show();

                // $('#menu_toggle').trigger('click');
                // $('#divList').hide(function(){
                    
                //     $.ajax({
                //         type: 'post',
                //         url : 'lib/api/ar.get.php',
                //         data : {
                //             'fid' : vid,
                //             'cid' : cvid,
                //             'uuid' : <?php echo $_SESSION['uuid']; ?>
                //         },
                //         dataType : 'json',
                //         success : function(res){
                //             console.log(res);
                //             var obj = JSON.parse(res.data);
                //             // var obj = res[0];
                //             console.log(obj);
                //             $('#voucherFID').val(vid);
                //             $('#voucherClientID').val(cvid);

                //             $('#cl_name').html(obj.last_name + ', ' + obj.name + ' ' + obj.middle_name);
                //             $('#cl_comp').html(obj.company);
                //             $('#cl_cvnum').html(obj.voucher_id);
                //             $('#cl_bank').html();
                //             $('#cl_date').html();

                //             var new_intrate = Number(obj.intrate) / 100;
                //             var interest = Number(obj.amount).toFixed(2) * new_intrate * Number(obj.term); 

                //             $('#txtpromNote').val( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                //             $('#txtInterest').val( numberWithCommas( interest.toFixed(2) ) );
                //             $('#txtInterestRate').val( obj.intrate + ' %' );
                //             $('#txtTerms').val( obj.term );
                //             $('#txtGross').val( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                //             $('#txtNetProceeds').html( numberWithCommas( Number(obj.amount).toFixed(2) ) );
                //             $('#divPrint').show();
                //             $('#inpTotalCharge').val();
                //             $('#inpNetProceeds').val(Number(obj.amount).toFixed(2));
                //             $('#inpOrigNetProceeds').val(Number(obj.amount).toFixed(2));

                //             var date_now = moment().format('MM-DD-YYYY');
                //             $('#cl_date').html(date_now);

                //             // if(obj.loan_type == 'new_loans' || obj.loan_type == 'renewal'){
                //             //     if(obj.amount > 100000 ){

                //             //     }
                //             // }

                //             if(obj.loan_type == 'cash_advance'){
                //                 var c_new_intrate, n_interest;
                //                 if(obj.amount <= 4000 ){
                //                     c_new_intrate = 3 / 100;
                //                     n_interest = Number(obj.amount).toFixed(2) * Number(c_new_intrate);
                //                 }else if(obj.amount > 4000 ){
                //                     c_new_intrate = 5 / 100;
                //                     n_interest = Number(obj.amount).toFixed(2) * Number(c_new_intrate);
                //                 }
                //                 // $('#inProcFee').val(n_interest.toFixed(2));
                //                 $('#inpMisc').val(n_interest.toFixed(2));
                //             }

                //             setTimeout(function(){
                //                 calculateMe();
                //             }, 1000);
                //             // inProcFee
                //             // inpFindersFee
                //             // inpMisc
                //             // inpOthers
                //             // inpAdjustment
                //             // inpBonus
                //             // txtTotalCharge
                //         }
                //     });
                // });            
            }
        });
 
        


        $('#btnCancelApp').click(function(){
            $('#divPrint').hide(function(){
                dt_table.destroy();
                plot_voucherlist_table();

                $('#voucherFID').val('');
                $('#voucherClientID').val('');
                $('#divList').show();
                $('#menu_toggle').trigger('click');
            });
        });

        function calculateMe(){
            var inProcFee = $('#inProcFee').val();
            var inpFindersFee = $('#inpFindersFee').val();
            var inpNotarial = $('#inpNotarial').val();
            var inpMisc = $('#inpMisc').val();
            var inpInsurance = $('#inpInsurance').val();
            var inpOthers = $('#inpOthers').val();
            var inpAdjustment = $('#inpAdjustment').val();

            var inpBonus = $('#inpBonus').val();

            var txtTotalCharge = Number(inProcFee) + Number(inpFindersFee) + Number(inpNotarial) + Number(inpMisc) + Number(inpInsurance) + Number(inpOthers) + Number(inpAdjustment);
            $('#txtTotalCharge').html( numberWithCommas(txtTotalCharge.toFixed(2)) + '');

            setTimeout(function(){
                var inpNetProceeds = $('#inpNetProceeds').val();
                var inpTotalCharge = $('#inpTotalCharge').val();
                
                var totalNetProceeds = (Number(inpNetProceeds) + Number(inpBonus) ) - Number(txtTotalCharge);
                console.log(totalNetProceeds);
                $('#txtNetProceeds').html( numberWithCommas(totalNetProceeds.toFixed(2)) + '' );  
                $('#spAmount').html ( numberWithCommas(totalNetProceeds.toFixed(2)) + '' );
            },200);
        }


        $('#inProcFee').keyup(function(){
            calculateMe();
        });

        $('#inpFindersFee').keyup(function(){
            calculateMe();
        });

        $('#inpNotarial').keyup(function(){
            calculateMe();
        });

        $('#inpMisc').keyup(function(){
            calculateMe();
        });

        $('#inpInsurance').keyup(function(){
            calculateMe();
        });

        $('#inpOthers').keyup(function(){
            calculateMe();
        });

        $('#inpAdjustment').keyup(function(){
            calculateMe();
        });


        $('#inpBonus').keyup(function(){
            calculateMe();
            // var bonus = $('#inpBonus').val();
            // var netProc = $('#inpNetProceeds').val();
            // var netOrigProc = $('#inpOrigNetProceeds').val();

            // if(bonus > 0){
            //     var new_totalNetProceeds = Number(netProc) + Number(bonus);
            //     // $('#txtNetProceeds').html( numberWithCommas( new_totalNetProceeds.toFixed(2) ) + '' );
            //     // $('#inpNetProceeds').val(new_totalNetProceeds.toFixed(2));                
            // }else{
            //     var new_totalNetProceeds = Number(netOrigProc);
            //     // $('#txtNetProceeds').html( numberWithCommas( new_totalNetProceeds.toFixed(2) ) + '' );
            //     // $('#inpNetProceeds').val(new_totalNetProceeds.toFixed(2));                
            // }

            // $('#txtNetProceeds').html( numberWithCommas( new_totalNetProceeds.toFixed(2) ) + '' );
            // // $('#inpNetProceeds').val(new_totalNetProceeds.toFixed(2));  

            // // alert(new_totalNetProceeds);
            // console.log(new_totalNetProceeds)

            // // setTimeout(function(){
            // //     var inpNetProceeds = $('#inpNetProceeds').val();
            // //     var inpTotalCharge = $('#inpTotalCharge').val();
                
            // //     var totalNetProceeds = Number(inpNetProceeds) - Number(txtTotalCharge);
            // //     console.log(totalNetProceeds);
            // //     $('#txtNetProceeds').html( numberWithCommas(totalNetProceeds.toFixed(2)) + '' );     
            // // },200);
        });

        $('#inpBonus').focus(function(){
            var new_totalNetProceeds = Number($('#inpOrigNetProceeds').val());
            // $('#txtNetProceeds').html( numberWithCommas( new_totalNetProceeds.toFixed(2) ) + '' );
            $('#inpNetProceeds').val(new_totalNetProceeds.toFixed(2));                
        });

        // $('#inProcFee').blur(function(){
        //     $(this).val( numberWithCommas( Number( $( this ).val() ).toFixed(2) ) );
        // });

        // $('#inpFindersFee').blur(function(){
        //     $(this).val( numberWithCommas( Number( $( this ).val() ).toFixed(2) ) );
        // });

        // $('#inpMisc').blur(function(){
        //     $(this).val( numberWithCommas( Number( $( this ).val() ).toFixed(2) ) );
        // });

        // $('#inpOthers').blur(function(){
        //     $(this).val( numberWithCommas( Number( $( this ).val() ).toFixed(2) ) );
        // });

        // $('#inpAdjustment').blur(function(){
        //     $(this).val( numberWithCommas( Number( $( this ).val() ).toFixed(2) ) );
        // });

        $('.cfees').focus(function(){
            // $(this).val('');
            $(this).select();
        });

        // $('.cfees').blur(function(){
        //    var val = $(this).val(); 
        //    if(val == ''){
        //         $(this).val('0.00');
        //    }else{
        //         $(this).val( val.toFixed(2) + '');
        //    }
        // });
// inpBonus
// txtTotalCharge

        // 


        $('#inProcFee').blur(function(){
            calculateMe();
           var val = $(this).val(); 
           if(val == ''){
                $(this).val('0.00');
           }else{
                val = Number(val);
                $(this).val(val.toFixed(2) + '');
           }
        });

        $('#inpFindersFee').blur(function(){
            calculateMe();
           var val = $(this).val(); 
           if(val == ''){
                $(this).val('0.00');
           }else{
                val = Number(val);
                $(this).val(val.toFixed(2) + '');
           }
        });

        $('#inpNotarial').blur(function(){
            calculateMe();
           var val = $(this).val(); 
           if(val == ''){
                $(this).val('0.00');
           }else{
                val = Number(val);
                $(this).val(val.toFixed(2) + '');
           }
        });

        $('#inpMisc').blur(function(){
            calculateMe();
           var val = $(this).val(); 
           if(val == ''){
                $(this).val('0.00');
           }else{
                val = Number(val);
                $(this).val(val.toFixed(2) + '');
           }
        });

        $('#inpInsurance').blur(function(){
            calculateMe();
           var val = $(this).val(); 
           if(val == ''){
                $(this).val('0.00');
           }else{
                val = Number(val);
                $(this).val(val.toFixed(2) + '');
           }
        });

        $('#inpOthers').blur(function(){
            calculateMe();
           var val = $(this).val(); 
           if(val == ''){
                $(this).val('0.00');
           }else{
                val = Number(val);
                $(this).val(val.toFixed(2) + '');
           }
        });

        $('#inpAdjustment').blur(function(){
            calculateMe();
           var val = $(this).val(); 
           if(val == ''){
                $(this).val('0.00');
           }else{
                val = Number(val);
                $(this).val(val.toFixed(2) + '');
           }
        });

        $('#inpBonus').blur(function(){
            calculateMe();
           var val = $(this).val(); 
           if(val == ''){
                $(this).val('0.00');
           }else{
                val = Number(val);
                $(this).val(val.toFixed(2) + '');
           }
        });


        function confirmApproval(callback){
            var p_name = $('#cl_name').text();
            var p_cvnum = $('#cl_cvnum').text();
            var p_datenum = $('#cl_date').text();
            var p_loan = $('#txtpromNote').val();
            var p_procfee = $('#inProcFee').val();
            var p_misc = $('#inpMisc').val();
            var p_findersfee = $('#inpFindersFee').val();
            var p_insurance = $('#inpInsurance').val();
            var p_notarial = $('#inpNotarial').val();
            var p_others = $('#inpOthers').val();
            var p_bonus = $('#inpBonus').val();
            var p_adjustment = $('#inpAdjustment').val();
            var p_totalCharge = $('#txtTotalCharge').text();
            var p_netproceeds = $('#txtNetProceeds').text();

            $('#p_name1').html(p_name);
            $('#p_cvnum1').html(p_cvnum);
            $('#p_datenum1').html(p_datenum);
            $('#p_loan1').html(p_loan);
            $('#p_procfee1').html(p_procfee);
            $('#p_misc1').html(p_misc);
            $('#p_findersfee1').html(p_findersfee);
            $('#p_insurance1').html(p_insurance);
            $('#p_notarial1').html(p_notarial);
            $('#p_others1').html(p_others);
            $('#p_totalCharge1').html(p_totalCharge);
            $('#p_netproceeds1').html(p_netproceeds);

            $('#p_name2').html(p_name);
            $('#p_cvnum2').html(p_cvnum);
            $('#p_datenum2').html(p_datenum);
            $('#p_loan2').html(p_loan);
            $('#p_procfee2').html(p_procfee);
            $('#p_misc2').html(p_misc);
            $('#p_findersfee2').html(p_findersfee);
            $('#p_insurance2').html(p_insurance);
            $('#p_notarial2').html(p_notarial);
            $('#p_others2').html(p_others);
            $('#p_totalCharge2').html(p_totalCharge);
            $('#p_netproceeds2').html(p_netproceeds);

            $('#p_name3').html(p_name);
            $('#p_cvnum3').html(p_cvnum);
            $('#p_datenum3').html(p_datenum);
            $('#p_loan3').html(p_loan);
            $('#p_procfee3').html(p_procfee);
            $('#p_misc3').html(p_misc);
            $('#p_findersfee3').html(p_findersfee);
            $('#p_insurance3').html(p_insurance);
            $('#p_notarial3').html(p_notarial);
            $('#p_others3').html(p_others);
            $('#p_totalCharge3').html(p_totalCharge);
            $('#p_netproceeds3').html(p_netproceeds);

            $('#numToText').html(MoneytoWords(p_loan) + '');
            $('#p_loan_last').html(p_loan);

            var fid = $('#voucherFID').val();
            var cid = $('#voucherClientID').val();
            $.ajax({
                type : 'post',
                url  : 'lib/api/confirm.loan.approval.php',
                data : {
                    'fid' : fid,
                    'cid' : cid,
                    'inProcFee' : p_procfee,
                    'inpFindersFee' : p_findersfee,
                    'inpNotarial' : p_notarial,
                    'inpMisc' : p_misc,
                    'inpInsurance' : p_insurance,
                    'inpOthers' : p_others,
                    'inpAdjustment' : p_adjustment,
                    'inpBonus' : p_bonus,
                    'uuid' : '<?php echo $_SESSION['uuid']; ?>'
                },
                success : function(result){
                    if(callback){
                        callback();
                    }
                }
            });
        }

        // CONFIRM ONLY 
        $('#btnConfirmVoucher').click(function(){
            // var clone = $('#divPrint').html();
            // $('#divForPrint').html(clone);
            confirmApproval(function(){
                dt_table.destroy();
                plot_voucherlist_table();
                alert('Transaction has been approved!');
            });

        });

        // CONFIRM & PRINT
        $('#btnConfirmVoucherPrint').click(function(){
            // var clone = $('#divPrint').html();
            // $('#divForPrint').html(clone);
            confirmApproval(function(){
                dt_table.destroy();
                plot_voucherlist_table();
                alert('Transaction has been approved!\nClick OK to continue for printing...');
                setTimeout(function(){
                    printDiv('divForPrint');
                    // $('#divForPrint').print()
                }, 2000);                
            });

        });

        // PRINT ONLY
        $('#btnPrintOnly').click(function(){
            var p_name = $('#cl_name').text();
            var p_cvnum = $('#cl_cvnum').text();
            var p_datenum = $('#cl_date').text();
            var p_loan = $('#txtpromNote').val();
            var p_procfee = $('#inProcFee').val();
            var p_misc = $('#inpMisc').val();
            var p_findersfee = $('#inpFindersFee').val();
            var p_insurance = $('#inpInsurance').val();
            var p_notarial = $('#inpNotarial').val();
            var p_others = $('#inpOthers').val();
            var p_totalCharge = $('#txtTotalCharge').text();
            var p_netproceeds = $('#txtNetProceeds').text();

            $('#p_name1').html(p_name);
            $('#p_cvnum1').html(p_cvnum);
            $('#p_datenum1').html(p_datenum);
            $('#p_loan1').html(p_loan);
            $('#p_procfee1').html(p_procfee);
            $('#p_misc1').html(p_misc);
            $('#p_findersfee1').html(p_findersfee);
            $('#p_insurance1').html(p_insurance);
            $('#p_notarial1').html(p_notarial);
            $('#p_others1').html(p_others);
            $('#p_totalCharge1').html(p_totalCharge);
            $('#p_netproceeds1').html(p_netproceeds);

            $('#p_name2').html(p_name);
            $('#p_cvnum2').html(p_cvnum);
            $('#p_datenum2').html(p_datenum);
            $('#p_loan2').html(p_loan);
            $('#p_procfee2').html(p_procfee);
            $('#p_misc2').html(p_misc);
            $('#p_findersfee2').html(p_findersfee);
            $('#p_insurance2').html(p_insurance);
            $('#p_notarial2').html(p_notarial);
            $('#p_others2').html(p_others);
            $('#p_totalCharge2').html(p_totalCharge);
            $('#p_netproceeds2').html(p_netproceeds);

            $('#p_name3').html(p_name);
            $('#p_cvnum3').html(p_cvnum);
            $('#p_datenum3').html(p_datenum);
            $('#p_loan3').html(p_loan);
            $('#p_procfee3').html(p_procfee);
            $('#p_misc3').html(p_misc);
            $('#p_findersfee3').html(p_findersfee);
            $('#p_insurance3').html(p_insurance);
            $('#p_notarial3').html(p_notarial);
            $('#p_others3').html(p_others);
            $('#p_totalCharge3').html(p_totalCharge);
            $('#p_netproceeds3').html(p_netproceeds);

            $('#numToText').html(MoneytoWords(p_loan) + '');
            $('#p_loan_last').html(p_loan);
            setTimeout(function(){
                printDiv('divForPrint');
                // $('#divForPrint').print()
            }, 500);  
        });

        function loadVoucherListWithRange(start_date, end_date, loan_type = '',callback){
            $.ajax({
                type: 'post',
                url : 'lib/api/ar.list.php',
                dataType: 'json',
                data : {
                    'start_date' : start_date,
                    'end_date' : end_date,
                    'loan_type' : loan_type
                },
                beforeSend: function() {
                    var html = '\
                    <tr>\
                        <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                        <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                        <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                        <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                        <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                        <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                        <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                        <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                        <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    </tr>\
                    ';
                    $('#tblVoucherList tbody').html(html);
                },
                success : function(x) {
                    // alert(x);
                    console.log(x);
                    if(callback){
                        callback(x);
                    }
                },
                error: function(){
                    alert('Please contact developer!');
                }
            });
        }


        //LOAD RANGE
        $('#btnLoadVoucherRange').click(function(){
            var frmInpDstart = $('#frmInpDstart').val();
            var frmInpDend  = $('#frmInpDend').val();

            var selTypeLoans = $('#selTypeLoans').val();
            loadVoucherListWithRange(frmInpDstart, frmInpDend, selTypeLoans,function(result){
                dt_table.destroy();

                console.log(result.data);
                var html = '';
                if(result){

                    var len = result.data.length;
                    // console.log(len);
                    
                    for(var i = 0; i < len; i++){
                        var obj = JSON.parse(result.data[i]);

                            // <tr>
                            //     <th>CV #</th>
                            //     <th>Name</th>
                            //     <th>Amount</th>
                            //     <th>Interest</th>
                            //     <th>Interest Type</th>
                            //     <th>Term</th>
                            //     <th>Loan Type</th>
                            //     <th>Date</th>
                            //     <th>Action</th>
                            // </tr>
                        var obj2 = JSON.parse(obj.extra);
                        // console.log(obj.extra);

                        var ld = obj.loan_date;

                        var axn;
                        ld = ld.split(' ');
                        if(obj.status != 'approved'){
                            axn = '\
                                    <button type="button" class="btn btn-success btnApprove" data-value="'+ obj.fid +'" data-clientid="'+ obj.client_id +'" data-toggle="tooltip" data-placement="top" title="Approve Transacton" ><i class="fa fa-thumbs-o-up"></i></button>\
                                    <button type="button" class="btn btn-danger btnDecline"  data-value="'+ obj.fid +'" data-clientid="'+ obj.client_id +'" data-toggle="tooltip" data-placement="top" title="Decline Transacton" ><i class="fa fa-thumbs-o-down"></i></button>\
                            ';
                        }else{
                            axn = '\
                                <button type="button" class="btn btn-primary btnView" data-value="'+ obj.fid +'" data-clientid="'+ obj.client_id +'" data-toggle="tooltip" data-placement="top" title="View Transacton" ><i class="fa fa-eye"></i></button>\
                            ';
                        }

                        if(obj.status == 'declined'){
                            is_declined = 'declined';
                            axn = 'DECLINED!';
                        }else{
                            is_declined = '';
                        }

                        if( obj.loan_type.indexOf('payment') > -1 ){

                        }else{

                            html += '\
                                <tr class="'+ is_declined +'">\
                                    <td>'+ obj.voucher_id +'</td>\
                                    <td class="details-control" data-fid="'+ obj.fid +'">'+ obj.last_name +', '+ obj.name +' '+ obj.middle_name +'.</td>\
                                    <td>Php '+ numberWithCommas(Number(obj.amount).toFixed(2)) +'</td>\
                                    <td>'+ obj.intrate +'%</td>\
                                    <td>'+ obj2.TypeRate.capitalize() +'</td>\
                                    <td>'+ obj.term +'</td>\
                                    <td>'+ toTitleCase(valueToWords(obj.loan_type)) +'</td>\
                                    <td>'+ ld[0] +'</td>\
                                    <td>'+ axn +'</td>\
                                </tr>\
                            ';

                        }

                        // html += '\
                        //     <tr>\
                        //         <td class="td_name"><strong>'+ obj.last_name +', '+obj.name + ' ' + obj.middle_name +'</strong><br>\
                        //         <div class=""><a href="#" class="cls_procar" data-value="'+ obj.client_id +'" >Process Loans</a> | \
                        //         <a href="#" class="cls_payment" data-value="'+ obj.client_id +'" >Payment</a> | \
                        //         <a href="#" class="cls_viewdata" data-value="'+ obj.client_id +'" >View Data</a> | \
                        //         <a href="#" class="cls_editprofile" data-value="'+ obj.client_id +'" >Edit Profile</a>\
                        //         </div>\
                        //         </td>\
                        //         <td>' + obj.contact_num +'</td>\
                        //         <td>' + obj.address +'</td>\
                        //         <td>' + obj.company +'</td>\
                        //     </tr>\
                        // ';
                    }

                }else{
                    html = '\
                        <tr>\
                            <td></td>\
                            <td class="details-control"></td>\
                            <td></td>\
                            <td></td>\
                            <td></td>\
                            <td></td>\
                            <td></td>\
                            <td></td>\
                            <td></td>\
                        </tr>\
                    ';
                }

                $('#tblVoucherList tbody').html(html);
                // var obj = JSON.parse(result)
                dt_table = $('#tblVoucherList').DataTable({
                    aLengthMenu: [
                      [10, 25, 50, 100, -1],
                      [10, 25, 50, 100, "All"]
                    ],
                    "bSort": false
                });

            });
        });

        //CLEAR RANGE
        $('#btnClearVoucherRange').click(function(){
            $('#frmInpDstart').val('');
            $('#frmInpDend').val('');
            dt_table.destroy();
            plot_voucherlist_table();
        });

        $('#frmInpDstart').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_1",
          showDropdowns: true,
          "opens": "center",
          "drops": "down",
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#frmInpDend').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_1",
          showDropdowns: true,
          "opens": "center",
          "drops": "down",
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          // alert(start.format('YYYY-MM-DD') + '');
        });


    });
  </script>
  <style>
  .error-input {
    border: 1px solid red !important;
  }

  .td_name > div {
    display: none;
  }

  .td_name:hover {
    cursor: pointer;
  }

  .td_name:hover > div{
    display: block;
  }

  .td_name:hover a {
    cursor: pointer;
  }

  .hideme { display: none !important; }

  #frmLoans2 { display: none; }

  .details-control:hover {
    cursor: pointer;
  }

  #divPrint {
    display: none;
  }

  #numToText {
    text-transform: uppercase;
  }
  </style>