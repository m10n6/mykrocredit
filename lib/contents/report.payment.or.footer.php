    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/js/moment/moment.min.js"></script>
    <script src="vendors/js/datepicker/daterangepicker.js"></script>

    <!-- bootstrap progress js -->
    <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>

    <!-- Growl -->
    <script src="includes/js/jquery.growl/javascripts/jquery.growl.js"></script>
    <link href="includes/js/jquery.growl/stylesheets/jquery.growl.css" rel="stylesheet" type="text/css">

    <!-- autocomplete -->
    <!--<script src="includes/js/jquery-ui/jquery-ui.js"></script>-->
    <script src="includes/js/autocomplete/jquery.autocomplete.js"></script>

    <script src="includes/js/custom.js"></script>
    <script>
        $(function(){
            // $( "#inpClientName" ).autocomplete({
            //   source: "lib/api/get.clientdata.php",
            //   minLength: 2,
            //   select: function( event, ui ) {
            //     log( "Selected: " + ui.item.value + " aka " + ui.item.id );
            //   }
            // });
        });

        $(document).ready(function(){
            $('#btnAddNewPayment').click(function(){
                $('#modNewPayment').modal('show');
                $('#frmPaymentForm').hide(function(){
                    $('#frmPaymentSelection').show(); 
                });
            });

            $('#btnPaymentMethodProceed').click(function(){
                $('#frmPaymentSelection').hide(function(){
                    $('#frmPaymentForm').show(); 
                    var ptype = $('#selPaymentType').val();
                    if(ptype == 'orss_payment'){
                        $('#divOR-warning small').show();
                        $('#divOR-container label').html('ORS # :');
                        generateORS('inpORNum');
                    }else{
                        $('#divOR-warning small').hide();
                        $('#divOR-container label').html('OR # :');
                        $('#inpORNum').val('');
                    }
                });
            });

            $('#btnPaymentMethodBack').click(function(){
                $('#frmPaymentForm').hide(function(){
                    $('#frmPaymentSelection').show(); 
                });
            });

            $('#inpClientName').autocomplete({
                minChars : 3,
                serviceUrl: 'lib/api/get.clientdata.php',
                onSelect: function (suggestion) {
                    // alert('You selected: ' + suggestion.value + ', ' + suggestion.data + ', ID =' + suggestion.id);
                    $('#divClientSearch').html('');
                    $('#inpClientID').val(suggestion.id);
                    $.ajax({
                        type : 'post',
                        url : 'lib/api/get.clientbalance.php',
                        data : {
                            'cid' : suggestion.id
                        },success: function(result){
                             $('#inpBalance').val(result);
                        }
                    })
                },
                onHint: function(){
                    $('#divClientSearch').html('');
                },
                onSearchStart : function(params){
                    $('#divClientSearch').html('<div class="alert alert-info"><i class="fa fa-pulse fa-spinner"></i> Searching Client from Database...</div>');
                }
            });

            $('#modNewPayment').on('click', '#btnPayhMents', function(){
                // alert('asda');
                var inpORNum = $('#inpORNum').val();
                var inpClientID = $('#inpClientID').val();
                var inpBalance = $('#inpBalance').val();
                var inpPayAmnt = $('#inpPayAmnt').val();
                var selIntRate = $('#selIntRate').val();
                var selTypeRate = $('#selTypeRate').val();
                // var inpApplyInt = $('#inpApplyInt:checked').val();
                var inpApplyInt;
                if($('#inpApplyInt').prop('checked')){
                    inpApplyInt = 'on';
                }else{
                    inpApplyInt = '';
                }

                var inpPaymentDate = $('#inpPaymentDate').val();

                var selPaymentType = $('#selPaymentType').val();
                if(inpORNum !== ''){
                    $.ajax({
                        type : 'post',
                        url  : 'lib/api/payment.php',
                        data : {
                            'inpORNum' : inpORNum,
                            'inpClientID' : inpClientID,
                            'inpBalance' : inpBalance,
                            'inpPayAmnt' : inpPayAmnt,
                            'selIntRate' : selIntRate,
                            'selTypeRate' : selTypeRate,
                            'inpApplyInt' : inpApplyInt,
                            'inpPaymentDate' : inpPaymentDate,
                            'selPaymentType' : selPaymentType
                        },
                        dataType : 'json',
                        success : function(result){
                            console.log(result);
                            alert('Payment proccess ' + result.message + '!\nPage will reload in 2secs.');
                            $('#modNewPayment').modal('hide');
                            setTimeout(function(){
                                window.location.reload();
                            },2000);
                            
                        }
                    });
                }else{
                    alert('Please enter OR # and Client Name!');
                }

            });

            $('#modNewPayment').on('change', '#selPaymentType', function(){
                var ptype = $(this).val();
                var divOR = $('#divOR-container');

                if(ptype == 'orss_payment'){
                    $('#divOR-warning small').show();
                    $('#divOR-container label').html('ORS # :');
                    generateORS('inpORNum');
                }else{
                    $('#divOR-warning small').hide();
                    $('#divOR-container label').html('OR # :');
                    $('#inpORNum').val('');
                }
            });

            $('#inpPaymentDate').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1",
              showDropdowns: true,
              "opens": "center",
              "drops": "up",
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });

            
            $('#frmInpDstart').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1",
              showDropdowns: true,
              "opens": "center",
              "drops": "down",
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#frmInpDend').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1",
              showDropdowns: true,
              "opens": "center",
              "drops": "down",
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });


            $('#btnLoadRange').click(function(){
                var dstart = $('#frmInpDstart').val();
                var dend = $('#frmInpDend').val();
                window.location = 'dashboard.php?page=report_orpayments&axn=load&dstart=' + dstart + '&dend=' + dend;
            });

            $('#btnReloadPage').click(function(){
                var cid = $(this).data('id');
                window.location = 'dashboard.php?page=report_orpayments';
            });

            $('#btnPrintMe').click(function(){
                var dstart = $('#frmInpDstart').val();

                $('#print_display_date').html('<strong>DATE : </strong>' + dstart);

                // $('#forPrintTitle').html('<strong>PAYMENT TYPE : </strong>' + title);
                window.print();
                $('#print_display_date').html('');
            });            
        });
    </script>

  <style>
  .error-input {
    border: 1px solid red !important;
  }

  .td_name {
    width: 30%;
  }

  .td_name > div {
    display: none;
  }

  .td_name:hover {
    cursor: pointer;
  }

  .td_name:hover > div{
    display: block;
  }

  .td_name:hover a {
    cursor: pointer;
  }

  .hideme { display: none !important; }

  #frmLoans2 { display: none; }

  .show-calendar {
    z-index: 99999;
}

.prev.available > .icon {
    background-image: none !important;
}

.next.available > .icon {
    background-image: none !important;
}
  </style>