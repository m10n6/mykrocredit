

 <div class="row">
 	<div class="col-md-12">

 		<div class="row">
 			<div class="col-md-12">
 				<h2>MYKRO CREDIT SERVICES CORPORATION</h2>
 				<span>COLLECTION REPORT - ORS</span><br>
 				<span id="print_display_date"></span>
 			</div>
 		</div>

 		<hr class="hidden-print"/>

 		<div class="row hidden-print">
 			<div class="col-md-8">
 				<div class="col-md-4">
 					<div class="form-group">
 						<label>Date From:</label>
 						<input type="text" id="frmInpDstart" class="form-control" value="<?php echo (!empty(secure_get('dstart')))? secure_get('dstart') : ""; ?>">
 					</div>
 				</div>
 				<div class="col-md-4">
 					<div class="form-group">
 						<label>Date To:</label>
 						<input type="text" id="frmInpDend" class="form-control" value="<?php echo (!empty(secure_get('dend')))? secure_get('dend') : ""; ?>">
 					</div>
 				</div>
 				<div class="col-md-4">
 					<div class="spacer25"></div>
 					<button type="button" class="btn btn-success" id="btnLoadRange" data-id="<?php echo secure_get('cid'); ?>" ><i class="fa fa-refresh"></i> Load</button>
 				</div>
 			</div>
 			<div class="col-md-4">
 				<div class="spacer25"></div>
 				<a class="btn btn-warning pull-right  hidden-print" id="btnPrintMe" ><i class="fa fa-print"></i> Print </a>
 			</div>
 		</div>
 		<hr class="hidden-print"/>

 		<div id="forPrintTitle"></div>

 		<table class="table table-striped" id="tblPaymentSheet">
 			<thead>
		        <tr>
		            <!-- <th>Tracking ID</th> -->
		            <th>DATE</th>
		            <th>ORS#</th>
		            <th>NAME</th>
		            <th>GROSS</th>
		            <th>INTEREST</th>
		            <th>NET</th>
		            <!-- <th>Status</th> -->
		            <!-- <th class = 'hidden-print'>Action</th> -->
		        </tr>
 			</thead>
 			<!-- <tfoot>
 				<tr>
 					<td>Account Code</td>
 					<td>Account Name</td>
 					<td>Category</td>
 					<td>Action</td>
 				</tr>
 			</tfoot> -->
 			<tbody>
 				<?php
 					$axn = secure_get('axn');
 					if(empty($axn)){
 						// $sql_query = "select * from `finance` where (`date_approved` between '".date('Y-m-d')." 00:00:00' and '".date('Y-m-d')." 23:59:59')  and `loan_type` like 'payment_ors' and `voucher_id` != '0000000000' and `status` = 'approved' and `addedby` = '".$_SESSION['uuid']."' order by `date_approved` asc, `voucher_id` asc";
 						$sql_query = "select * from `finance` where (`date_approved` between '".date('Y-m-d')." 00:00:00' and '".date('Y-m-d')." 23:59:59')  and `loan_type` like 'payment_ors' and `voucher_id` != '0000000000' and `status` = 'approved' and `addedby` = '".$_SESSION['uuid']."' order by `voucher_id` asc";
 						if($_SESSION['ulevel'] == '1'){
 							$sql_query = "select * from `finance` where (`date_approved` between '".date('Y-m-d')." 00:00:00' and '".date('Y-m-d')." 23:59:59') and `loan_type` like 'payment_ors' and `voucher_id` != '0000000000' and `status` = 'approved' order by `date_approved` asc, `voucher_id` asc";
 						}
 					}else{
  						$date_start = secure_get('dstart');
  						$date_end = secure_get('dend');
  						// $sql_query = "select * from `finance` where (`date_approved` between  '".$date_start." 00:00:00' and  '".$date_end." 23:59:59') and `loan_type` like 'payment_ors' and `voucher_id` != '0000000000' and `status` = 'approved' and  `addedby` = '".$_SESSION['uuid']."' order by `date_approved` asc, `voucher_id` asc";
  						$sql_query = "select * from `finance` where (`date_approved` between  '".$date_start." 00:00:00' and  '".$date_end." 23:59:59') and `loan_type` like 'payment_ors' and `voucher_id` != '0000000000' and `status` = 'approved' and  `addedby` = '".$_SESSION['uuid']."' order by `voucher_id` asc";
  						if($_SESSION['ulevel'] == '1'){
  							$sql_query = "select * from `finance` where (`date_approved` between '".$date_start." 00:00:00' and  '".$date_end." 23:59:59') and `loan_type` like 'payment_ors' and `voucher_id` != '0000000000' and `status` = 'approved' order by `date_approved` asc, `voucher_id` asc";
  						}
  						// echo $sql_query;
 					}
 					$res = $conn->dbquery($sql_query);
 					// print_r($res);
 					if($res !== 'false'){
						$res = json_decode($res);
						foreach ($res->data as $key) {
							# code...
							$nres = json_decode($key);

							// $number_amount = floatval(str_replace(',', '.', str_replace('.', '', $nres->amount)));
							$totalAmount += $nres->amount;

							$amount = $nres->amount;
							// $charges = $nres->inProcFee + $nres->inpFindersFee + $nres->inpNotarial + $nres->inpMisc + $nres->inpInsurance + $nres->inpOthers + $nres->inpAdjustment;
							// $netProceeds = $amount  - $charges;

							// $totalCharges += $charges;
							// $totalNet += $netProceeds;

							$intIncome = 0;
							$netAmount = $amount - $intIncome;

							$totalIntIncome += $intIncome;
							$totalNet += $netAmount;
							// $loan_type_text = str_replace('_', ' ', $nres->loan_type);
		 					echo '
				 				<tr>
				 					<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
				 					<td>'.$nres->voucher_id.'</td>
				 					<td>'.getClientName($nres->client_id).'</td>
				 					<td>'.number_format($amount, 2).'</td>
				 					<td>'.number_format($intIncome, 2).'</td>
				 					<td>'.number_format($netAmount, 2).'</td>
				 				</tr>
		 					';
						}
					}else{
	 					echo '
			 				<tr>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 				</tr>
	 					';
					}
 				?>
<!--  				<tr>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 				</tr> -->
  				<?php
 					// $sql = "select * from `journal` order by `code` asc";
 					// $res = $conn->dbquery($sql);
 					// // print_r($res);
 					// if($res !== 'false'){
						// $res = json_decode($res);
						// foreach ($res->data as $key) {
						// 	# code...
						// 	$nres = json_decode($key);
						// 	echo '
						// 		<tr>
						// 			<td>'.$nres->code.'</td>
						// 			<td>'.$nres->name.'</td>
						// 			<td>0.00</td>
						// 			<td>0.00</td>
						// 		</tr>
						// 	';	
						// }
 					// }else{
 					// 	echo '
						// 	<tr>
						// 		<td></td>
						// 		<td></td>
						// 		<td></td>
						// 		<td></td>
						// 	</tr>
 					// 	';	
 					// }
 				?>
 				<!-- <tfoot> -->
	 				<tr>
	 					<td></td>
	 					<td></td>
	 					<td style="font-weight: bold;">Total: </td>
	 					<td style="font-weight: bold;">Php <?php echo number_format($totalAmount, 2); ?></td>
	 					<td style="font-weight: bold;">Php <?php echo number_format($totalIntIncome, 2); ?></td>
	 					<td style="font-weight: bold;">Php <?php echo number_format($totalNet, 2); ?></td>
	 				</tr>
 				<!-- </tfoot> -->
 			</tbody>
 		</table>
 		
 		<div class="row">
 			<div class="col-md-12">
 				<strong>PREPARED BY: </strong><?php echo $_SESSION['fname']; ?>
 			</div>
 		</div>

 	</div>

 </div>


<!-- Modal -->
<div class="modal fade" role="dialog" id="modNewPayment">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">PAYMENT</h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" id="inpClientID" value="">
        <form id="frmPaymentSelection">
        	<div class="row">
        		<div class="col-md-12">
		        	<div class="form-group">
		        		<label>Payment Type</label>
		        		<select id="selPaymentType" class="form-control">
		        			<option value="or_payment">Manual Entry(OR)</option>
		        			<option value="orss_payment">System Generated(ORS)</option>
		        		</select>
		        	</div>
		        	<button type="button" class="btn btn-primary" id="btnPaymentMethodProceed" >Proceed <i class="fa fa-arrow-right"></i></button>
	        	</div>
        	</div>
        </form>
        <form id="frmPaymentForm">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="row">
        				<div class="col-md-12" id="divOR-warning">
        					<small style="color: red">ORS # may change / not, after processing. To avoid double ORS #.</small>
        				</div>
        			</div>
        			<div class="row">
	        			<div class="col-md-6">
				        	<div class="form-group" id="divOR-container">
				        		<label>OR # :</label>
				        		<input type="text" id="inpORNum" class="form-control">
				        		<!-- <small style="color: red">ORS # may change / not, after processing. To avoid double ORS #.</small> -->
				        	</div>
				        	<div class="form-group">
				        		<label>Client :</label>
				        		<input type="text" id="inpClientName" class="form-control" value="<?php echo getClientName($cid); ?>">
				        		<!-- <input type="hidden" id="inpClientID" class="form-control"> -->
				        	</div>
				        	<div class="form-group">
				        		<label>Balance :</label>
				        		<!-- <input type="text" id="inpBalance" class="form-control" value="<?php echo number_format($new_balance, 2); ?>"> -->
				        		<input type="text" id="inpBalance" class="form-control" value="<?php echo getClientBalance($cid); ?>" readonly>
				        	</div>
				        	<div class="form-group">
				        		<label>Amount :</label>
				        		<input type="text" id="inpPayAmnt" class="form-control">
				        	</div>
			        	</div>

			        	<div class="col-md-6">
				        	<div class="form-group">
				        		<label>Interest Rate :</label>
			        			<select id="selIntRate" class="form-control">
			        				<option value="4">4%</option>
			        				<option value="3.5">3.5%</option>
									<option value="3">3%</option>
									<option value="2">2%</option>
									<option value="2.5">2.5%</option>
									<option value="1">1%</option>
			        			</select>
				        	</div>
			        		<div class="form-group">
			        			<label>Type of Rate</label>
			        			<select id="selTypeRate" class="form-control">
			        				<option value="-">-select type-</option>
			        				<option value="diminishing">Diminishing Rate</option>
									<option value="flat">Flat Rate</option>
			        			</select>
			        		</div>
				        	<div class="form-group hideme">
				        		<label>Apply Interest :</label>
				        		<input type="checkbox" id="inpApplyInt" class="form-control">
				        	</div>
				        	<div class="form-group">
				        		<label>Payment Date :</label>
				        		<input type="text" id="inpPaymentDate" class="form-control">
				        	</div>
			        	</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-md-12">
		        			<button type="button" class="btn btn-primary pull-left" id="btnPaymentMethodBack" ><i class="fa fa-arrow-left"></i> Back</button>
		        		</div>
		        	</div>
<!-- 		        	<div class="form-group" id="divOR-container">
		        		<label>OR # :</label>
		        		<input type="text" id="inpORNum" class="form-control">
		        		<small style="color: red">ORS # may change / not, after processing. To avoid double ORS #.</small>
		        	</div>
		        	<div class="form-group">
		        		<label>Client :</label>
		        		<input type="text" id="inpClientName" class="form-control" value="">
		        		<div id="divClientSearch"></div>
		        		<input type="hidden" id="inpClientID" class="form-control">
		        	</div>
		        	<div class="form-group">
		        		<label>Balance :</label>
		        		<input type="text" id="inpBalance" class="form-control" value="<?php echo number_format($new_balance, 2); ?>">
		        	</div>
		        	<div class="form-group">
		        		<label>Amount :</label>
		        		<input type="text" id="inpPayAmnt" class="form-control">
		        	</div>
		        	<div class="form-group">
		        		<label>Interest Rate :</label>
	        			<select id="selIntRate" class="form-control">
	        				<option value="4">4%</option>
	        				<option value="3.5">3.5%</option>
							<option value="3">3%</option>
							<option value="2">2%</option>
							<option value="2.5">2.5%</option>
							<option value="1">1%</option>
	        			</select>
		        	</div>
	        		<div class="form-group">
	        			<label>Type of Rate</label>
	        			<select id="selTypeRate" class="form-control">
	        				<option value="-">-select type-</option>
	        				<option value="diminishing">Diminishing Rate</option>
							<option value="flat">Flat Rate</option>
	        			</select>
	        		</div>
		        	<div class="form-group">
		        		<label>Apply Interest :</label>
		        		<input type="checkbox" id="inpApplyInt" class="form-control">
		        	</div>
		        	<div class="form-group">
		        		<label>Payment Date :</label>
		        		<input type="date" id="inpPaymentDate" class="form-control">
		        	</div>
		        	<button type="button" class="btn btn-primary" id="btnPaymentMethodBack" ><i class="fa fa-arrow-left"></i> Back</button> -->
	        	</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnPayhMents" style="margin-top: -5px;"><i class="fa fa-save"></i>Save Payment</button>
      </div>
    </div>

  </div>
</div>
<style>
@media print {
	.col-md-12 {
		width: 100% !important;
	}
	#tblPaymentSheet {
		width: 100% !important;
	}
}
</style>