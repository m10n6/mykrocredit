  <script src="includes/js/bootstrap.min.js"></script>

  <script src="includes/js/nprogress.js"></script>
  
  <!-- bootstrap progress js -->
  <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="includes/js/icheck/icheck.min.js"></script>

  <script src="includes/js/custom.js"></script>

<!-- Datatables -->
<!-- <script src="includes/js/datatables/js/jquery.dataTables.js"></script>
<script src="includes/js/datatables/tools/js/dataTables.tableTools.js"></script> -->

<!-- Datatables-->
<script src="includes/js/datatables/jquery.dataTables.min.js"></script>
<script src="includes/js/datatables/dataTables.bootstrap.js"></script>
<script src="includes/js/datatables/dataTables.buttons.min.js"></script>
<script src="includes/js/datatables/buttons.bootstrap.min.js"></script>
<script src="includes/js/datatables/jszip.min.js"></script>
<script src="includes/js/datatables/pdfmake.min.js"></script>
<script src="includes/js/datatables/vfs_fonts.js"></script>
<script src="includes/js/datatables/buttons.html5.min.js"></script>
<script src="includes/js/datatables/buttons.print.min.js"></script>
<script src="includes/js/datatables/dataTables.fixedHeader.min.js"></script>
<script src="includes/js/datatables/dataTables.keyTable.min.js"></script>
<script src="includes/js/datatables/dataTables.responsive.min.js"></script>
<script src="includes/js/datatables/responsive.bootstrap.min.js"></script>
<script src="includes/js/datatables/dataTables.scroller.min.js"></script>



  <script src="includes/js/moment/moment.min.js"></script>
  <script src="includes/js/calendar/fullcalendar.min.js"></script>
  <!-- pace -->
  <script src="includes/js/pace/pace.min.js"></script>

    <!-- Chart.js -->
    <script src="includes/js/chartjs/chart.min.js"></script>
  <script>
    $(window).load(function() {

      var date = new Date();
      var d = date.getDate();
      var m = date.getMonth();
      var y = date.getFullYear();
      var started;
      var categoryClass;

      var calendar = $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
          $('#fc_create').click();

          started = start;
          ended = end

          $(".antosubmit").on("click", function() {
            var title = $("#title").val();
            if (end) {
              ended = end
            }
            categoryClass = $("#event_type").val();

            if (title) {
              calendar.fullCalendar('renderEvent', {
                  title: title,
                  start: started,
                  end: end,
                  allDay: allDay
                },
                true // make the event "stick"
              );
            }
            $('#title').val('');
            calendar.fullCalendar('unselect');

            $('.antoclose').click();

            return false;
          });
        },
        eventClick: function(calEvent, jsEvent, view) {
          //alert(calEvent.title, jsEvent, view);

          $('#fc_edit').click();
          $('#title2').val(calEvent.title);
          categoryClass = $("#event_type").val();

          $(".antosubmit2").on("click", function() {
            calEvent.title = $("#title2").val();

            calendar.fullCalendar('updateEvent', calEvent);
            $('.antoclose2').click();
          });
          calendar.fullCalendar('unselect');
        },
        editable: true,
        events: [
        <?php
          include_once('config.php');
          include_once('lib/funcjax.php');
          $res = $conn->dbquery("SELECT * FROM `reservation` where `status` = 'confirmed' or `status` = 'paid' order by `created_date`" );
          $res = json_decode($res);
          $res = $res->data;

          if(count($res) > 0){
            
             foreach($res as $rs){
                $rs1 = json_decode($rs);
                $xt .= '{';
                $xt .= 'title:\'Event: '.$rs1->event.'\nDetails: '.$rs1->details.'\nPriest: '.translateName($rs1->assigned_to, $conn).'\nStatus: '.$rs1->status.'\',';
                $xt .= 'start: new Date("'.$rs1->start_date.'"),';
                $xt .= 'end: new Date("'.$rs1->end_date.'")';
                $xt .= '},';
             }
             echo rtrim($xt, ",");
          }
        ?>
        // {
        //   title: 'All Day Event',
        //   start: new Date(y, m, 1)
        // }, {
        //   title: 'Long Event',
        //   start: new Date(y, m, d - 5),
        //   end: new Date(y, m, d - 2)
        // }, {
        //   title: 'Meeting',
        //   start: new Date(y, m, d, 10, 30),
        //   allDay: false
        // }, {
        //   title: 'Lunch',
        //   start: new Date(y, m, d + 14, 12, 0),
        //   end: new Date(y, m, d, 14, 0),
        //   allDay: false
        // }, {
        //   title: 'Birthday Party',
        //   start: new Date(y, m, d + 1, 19, 0),
        //   end: new Date(y, m, d + 1, 22, 30),
        //   allDay: false
        // }, {
        //   title: 'Click for Google',
        //   start: new Date(y, m, 28),
        //   end: new Date(y, m, 29),
        //   url: 'http://google.com/'
        // }
        ]
      });
    });
  </script>
  <!-- /footer content -->

    <!-- flot js -->
  <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
  <script type="text/javascript" src="includes/js/flot/jquery.flot.js"></script>
  <script type="text/javascript" src="includes/js/flot/jquery.flot.pie.js"></script>
  <script type="text/javascript" src="includes/js/flot/jquery.flot.orderBars.js"></script>
  <script type="text/javascript" src="includes/js/flot/jquery.flot.time.min.js"></script>
  <script type="text/javascript" src="includes/js/flot/date.js"></script>
  <script type="text/javascript" src="includes/js/flot/jquery.flot.spline.js"></script>
  <script type="text/javascript" src="includes/js/flot/jquery.flot.stack.js"></script>
  <script type="text/javascript" src="includes/js/flot/curvedLines.js"></script>
  <script type="text/javascript" src="includes/js/flot/jquery.flot.resize.js"></script>
  <script>
    $(document).ready(function() {
      // [17, 74, 6, 39, 20, 85, 7]
      //[82, 23, 66, 9, 99, 6, 2]
      var data1 = [
           <?php

              $year_now = date('Y');
              $limit = date('t');
              $month_now = date('m');
              $xt0 = '';
              for($x = 1; $x <= $limit; $x++){
                  $sql = "SELECT * FROM `collections` where (`date_added` between '".date('Y-m-d', strtotime($year_now.'-'.$month_now.'-'.$x))." 00:00:00' and '".date('Y-m-d', strtotime($year_now.'-'.$month_now.'-'.$x))." 23:59:59')" ;
                  // echo $sql;
                  $res = $conn->dbquery($sql);
                  $res = json_decode($res);
                  $res = $res->data;
                  // echo $sql.'<br>';
                  if(count($res) > 0){
                     foreach($res as $rs){
                        $rs1 = json_decode($rs);

                        $val = $rs1->amount;
                        // if(empty($val)){
                        //   $val = 0;
                        // }
                        
                        // $xt .= '{';
                        // $xt .= 'title:\'Event: '.$rs1->event.'\nDetails: '.$rs1->details.'\nPriest: '.translateName($rs1->assigned_to, $conn).'\nStatus: '.$rs1->status.'\',';
                        // $xt .= 'start: new Date("'.$rs1->start_date.'"),';
                        // $xt .= 'end: new Date("'.$rs1->end_date.'")';
                        // $xt .= '},';
                     }
                     
                  }else{
                    $val = 0;
                  }
                  $xt0 .= '[gd('.$year_now.', '.$month_now.', '.$x.'),'.$val.'],';  
              }
              echo rtrim($xt0, ",");
            ?>

        // [gd(2012, 1, 1), 17],
        // [gd(2012, 1, 2), 74],
        // [gd(2012, 1, 3), 6],
        // [gd(2012, 1, 4), 39],
        // [gd(2012, 1, 5), 20],
        // [gd(2012, 1, 6), 85],
        // [gd(2012, 1, 7), 7]
      ];

      var data2 = [
        [gd(2012, 1, 1), 82],
        [gd(2012, 1, 2), 23],
        [gd(2012, 1, 3), 66],
        [gd(2012, 1, 4), 9],
        [gd(2012, 1, 5), 119],
        [gd(2012, 1, 6), 6],
        [gd(2012, 1, 7), 9]
      ];
      $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
        data1 //, data2
      ], {
        series: {
          lines: {
            show: false,
            fill: true
          },
          splines: {
            show: true,
            tension: 0.4,
            lineWidth: 1,
            fill: 0.4
          },
          points: {
            radius: 0,
            show: true
          },
          shadowSize: 2
        },
        grid: {
          verticalLines: true,
          hoverable: true,
          clickable: true,
          tickColor: "#d5d5d5",
          borderWidth: 1,
          color: '#fff'
        },
        colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
        xaxis: {
          tickColor: "rgba(51, 51, 51, 0.06)",
          mode: "time",
          tickSize: [1, "day"],
          //tickLength: 10,
          axisLabel: "Date",
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 12,
          axisLabelFontFamily: 'Verdana, Arial',
          axisLabelPadding: 10
            //mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
        },
        yaxis: {
          ticks: 8,
          tickColor: "rgba(51, 51, 51, 0.06)",
        },
        tooltip: true
      });

      function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime();
      }
    });


$(document).ready(function(){
<?php
  $axn = secure_get('axn');
?>
        // Bar chart
      var ctx = document.getElementById("mymarkBar");
      var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
          datasets: [{
            label: '<?php echo strtoupper($axn); ?>',
            backgroundColor: "#26B99A",
            data: [
            //51, 30, 40, 28, 92, 50, 45
            <?php
              $year_now = date('Y');
              $xt1 = '';
              for($x = 1; $x <= 12; $x++){
                  if($axn == 'wedding' || $axn == 'burial' || $axn == 'baptism' || $axn == 'blessing' || $axn == 'mass'  || $axn == 'others'){
                    $sql = "SELECT * FROM `reservation` where (`start_date` between '".date('Y-m-01', strtotime($year_now.'-'.$x.'-1'))." 00:00:00' and '".date('Y-m-t', strtotime($year_now.'-'.$x.'-1'))." 23:59:59') and `event` = '".$axn."' and `status` = 'approved'" ;
                  }
                  if($axn == 'collection'){
                    $sql = "SELECT * FROM `collections` where (`date_added` between '".date('Y-m-01', strtotime($year_now.'-'.$x.'-1'))." 00:00:00' and '".date('Y-m-t', strtotime($year_now.'-'.$x.'-1'))." 23:59:59')" ;
                  }
                  if($axn == 'payment'){
                    $sql = "SELECT * FROM `reservation` where (`start_date` between '".date('Y-m-01', strtotime($year_now.'-'.$x.'-1'))." 00:00:00' and '".date('Y-m-t', strtotime($year_now.'-'.$x.'-1'))." 23:59:59') and `status` = 'approved'" ;
                  }
                  // echo $sql."<br>";
                  $res = $conn->dbquery($sql);
                  $res = json_decode($res);
                  $res = $res->data;
                  // echo $sql.'<br>';
                  if(count($res) > 0){
                     foreach($res as $rs){
                        $rs1 = json_decode($rs);

                        $val += $rs1->amount;
                        // if(empty($val)){
                        //   $val = 0;
                        // }
                        
                        // $xt .= '{';
                        // $xt .= 'title:\'Event: '.$rs1->event.'\nDetails: '.$rs1->details.'\nPriest: '.translateName($rs1->assigned_to, $conn).'\nStatus: '.$rs1->status.'\',';
                        // $xt .= 'start: new Date("'.$rs1->start_date.'"),';
                        // $xt .= 'end: new Date("'.$rs1->end_date.'")';
                        // $xt .= '},';
                     }
                     
                  }else{
                    $val = 0;
                  }
                  $xt1 .= $val.',';  
              }
              echo rtrim($xt1, ",");
            ?>
            ]
          }]
        },

        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });

  $('#datatable1').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],

        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                'Php '+pageTotal +' ( Php '+ total +' total)'
            );
        },
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ]
  }); 
});

  </script>