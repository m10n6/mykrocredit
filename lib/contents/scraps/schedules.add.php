<?php
include_once('config.php');
include_once('lib/funcjax.php');
$res_id = secure_get('res_id');
$res_axn = "reservation_form_admin";
if(!empty($res_id)){
  $res_axn = "update_form";
  $res = $conn->dbquery("SELECT * FROM `reservation` WHERE `res_id` = '".$res_id."' " );
  $res = json_decode($res);
  $res = json_decode($res->data[0]);
  // echo $res->gospel; 

  $start_date_date = explode(" ", $res->start_date);
  $start_date = $start_date_date[0];
  $start_time = $start_date_date[1];
}
?>

<div class="row">

	<div class="col-md-12">

      <div class="col-md-5">
          <form>
              <input type="hidden" name="hresid" value="<?php echo $res_id; ?>">
              <input type="hidden" name="haxn" value="<?php echo $res_axn; ?>">
              <div class="form-group">
                  <label class="form-label">Name</label>
                  <input type="text" name="name" class="form-control" value="<?php echo $res->name; ?>" />
              </div>
              <div class="form-group">
                  <label class="form-label">Email</label>
                  <input type="text" name="email" class="form-control" value="<?php echo $res->email; ?>"/>
              </div>
              <div class="form-group">
                  <label class="form-label">Phone</label>
                  <input type="text" name="phone" class="form-control" value="<?php echo $res->phone; ?>" />
              </div>
              <div class="form-group">
                  <label class="form-label">Date</label>
                  <div class="form-group">
                      <div class='input-group date-picker' >
                          <input type='text' class="form-control" id='datetimepicker1' value="<?php echo $start_date; ?>"/>
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label">Time</label>
                  <select name="time" class="form-control" >
                      <?php
                          $start = 7;
                          for($i = 0; $i < 12; $i++){
                              $seltime = ($start + $i).':00:00';
                              // ltrim($start_time, "0");
                              if($start_time == $seltime){
                                $sel = "selected";
                              }else{
                                $sel = "";
                              }
                              echo '
                                  <option value="'.$seltime.'" '.$sel.' >'.$seltime.'</option>
                              ';
                          }
                      ?>
                  </select>
              </div>
              <div class="form-group">
                  <label class="control-label">Assigned Priest</label>
                  <select name="assto" class="form-control" >
                      <?php
                          $res1 = $conn->dbquery("SELECT * FROM `users` WHERE `ulevel` = '2'" );
                          $res1 = json_decode($res1);
                          $res1 = $res1->data;
                          echo '<option value="0">-Select-</option>';
                          foreach ($res1 as $usersdata) {
                              $json_usersdata = json_decode($usersdata);
                              if($res->assigned_to == $json_usersdata->user_id){
                                $ssel = 'selected';
                              }else{
                                $ssel = '';
                              }
                              echo '
                                <option value="'.$json_usersdata->user_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                              ';
                          }
                      ?>
                  </select>
              </div>
              <div class="form-group">
                  <label class="form-label">Event</label>
                  <select name="revent" class="form-control" >
                      <option value="mass" <?php echo ($res->event == "mass")? "selected": "" ; ?>>Mass</option>
                      <option value="wedding" <?php echo ($res->event == "wedding")? "selected": "" ; ?>>Wedding Mass</option>
                      <option value="burial" <?php echo ($res->event == "burial")? "selected": "" ; ?>>Burial Mass</option>
                      <option value="baptism" <?php echo ($res->event == "baptism")? "selected": "" ; ?>>Baptism</option>
                      <option value="blessing" <?php echo ($res->event == "blessing")? "selected": "" ; ?>>Blessing</option>
                      <option value="others" <?php echo ($res->event == "others")? "selected": "" ; ?>>Others</option>
                  </select>
              </div>
              
              <div class="form-group">
                  <label class="form-label">Venue</label>
                  <select name="venue" class="form-control" >
                      <option value="venue_1" <?php echo ($res->venue == "venue_1")? "selected": "" ; ?>>Venue #1</option>
                      <option value="venue_2" <?php echo ($res->venue == "venue_2")? "selected": "" ; ?>>Venue #2</option>
                      <option value="venue_3" <?php echo ($res->venue == "venue_3")? "selected": "" ; ?>>Venue #3</option>
                      <option value="venue_4" <?php echo ($res->venue == "venue_4")? "selected": "" ; ?>>Venue #4</option>
                      <option value="others" <?php echo ($res->venue == "others")? "selected": "" ; ?>>Others</option>
                  </select>
              </div>

              <div class="form-group">
                  <label class="control-label">Notes / Details</label>
                  <textarea id="notes" name="notes" class="form-control" ><?php echo $res->details; ?></textarea>
              </div>
              <?php if(empty($res_id)){ ?>
              <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="form-control" >
                      <option value="-" <?php echo ($res->status == "-")? "selected": "" ; ?> >-Select-</option>
                      <option value="new" <?php echo ($res->status == "new")? "selected": "" ; ?>>NEW</option>
                      <!-- <option value="confirmed" <?php echo ($res->status == "approved")? "selected": "" ; ?>>Approved</option> -->
                      <option value="approved" <?php echo ($res->status == "confirmed")? "selected": "" ; ?>>Approved</option>
                      <option value="pending" <?php echo ($res->status == "pending")? "selected": "" ; ?>>Pending</option>
                      <option value="cancelled" <?php echo ($res->status == "cancelled")? "selected": "" ; ?>>Cancelled</option>
                      <option value="paid" <?php echo ($res->status == "paid")? "selected": "" ; ?>>Paid</option>
                  </select>
              </div>

              
              <div class="form-group" id="divAmount">
                  <label class="form-label">Amount</label>
                  <input type="text" name="amount" class="form-control" value="<?php echo $res->amount; ?>" />
              </div>
              <?php } ?>
              <input type="button" class="btn btn-primary" name="save" value="SAVE" />
              <?php if(!empty($res_id)){ ?>
              <button onclick="pendingSched('<?php echo $res_id; ?>')" class="btn btn-warning"  type="button"><i class="fa fa-pause"></i> Pending</button>
              <button onclick="approveSched('<?php echo $res_id; ?>')" class="btn btn-success" type="button"><i class="fa fa-check"></i> Approved</button>
              <button onclick="cancelledSched('<?php echo $res_id; ?>')" class="btn btn-danger"  type="button"><i class="fa fa-trash"></i> Cancelled</button>
              <?php } ?>
              <a href="dashboard.php?page=schedules" class="btn btn-default" >CANCEL</a>
          </form>
      </div>

	</div>


</div>



<!-- Modal -->
<div class="modal fade" id="myPayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Payment Info</h4>
      </div>
      <div class="modal-body">
          <form>
              <div class="form-group">
                  <label class="control-label">Payment Type</label>
                  <select name="payment_type" class="form-control">
                      <option value="init_payment">Initial/Partial Payment</option>
                      <option value="full_payment">Full Payment</option>
                  </select>
                  <!-- <input type="hidden" id="hu_id" value="<?php echo $gid; ?>" />                   -->
              </div>
              <div class="form-group">
                  <label class="control-label">Amount</label>
                  <input type="text" id="amount_payed" value="" class="form-control" />   
                  <!-- <input type="hidden" id="hu_id" value="<?php echo $gid; ?>" />                   -->
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnSavePayment" style="margin-top: -5px;">Save</button>
      </div>
    </div>
  </div>
</div>

<!-- -->
<script type="text/javascript">
function pendingSched(){
  var rid = $('input[name=hresid]').val();
  var q = confirm('Are you sure you want to set it Pending?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'pending_reservation',
          res_id : rid
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Schedule is now pending!');
              // location.reload();
              window.location = 'dashboard.php?page=schedules';
            }
        } 
      });    
  }
}



function approveSched(id){
  $('#myPayment').modal('show');
}




function cancelledSched(id){
  var q = confirm('Are you sure you want to Cancelled?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'cancelled_reservation',
          res_id : id
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Schedule successfully cancelled!');
              // location.reload();
              window.location = 'dashboard.php?page=schedules';
            }else{
              alert(xhr);
            }
        } 
      });    
  }
}

$(document).ready(function(){

    $('#btnSavePayment').click(function(){
        var nn = $('input[name=name]').val();
        var ne = $('input[name=email]').val();
        var np = $('input[name=phone]').val();
        var nd = $('#datetimepicker1').val();
        var nt = $('select[name=time]').val();
        var at = $('select[name=assto]').val();
        var rid = $('input[name=hresid]').val();
        var axn = $('input[name=haxn]').val();
        var st = $('select[name=status]').val();
        var ev = $('select[name=revent]').val();
        var dt = $('#notes').val();
        var venue  = $('select[name=venue]').val();
        // var amt = $('input[name=amount]').val();
        // var pt = $('select[name=payment_type]').val();

        var am = $('#amount_payed').val();
        var rid = $('input[name=hresid]').val();
        var pt = $('select[name=payment_type]').val();
        // if(isNan(am)){
            $.ajax({
              type: 'post',
              url: 'api/api.php',
              data: {
                action: 'approved_reservation',
                res_id : rid,
                amount : am,
                ptype : pt,
                name  : nn,
                email : ne,
                phone : np,
                ndate : nd,
                ntime : nt,
                ins_by : <?php echo $_SESSION['uuid']; ?>,
                ass_to : at,
                status : st,
                details : dt,
                res_id : rid,
                event_ : ev,
                venue_ : venue,
                ax : axn
                //ptype  : pt,
                //amount_ : amt

              },
              beforeSend: function(xhr){

              },
              success: function(xhr){
                  console.log(xhr);
                  if(xhr == 'success'){
                    alert('Schedule successfully Approved!');

                    //open new tab here
                    var win = window.open('invoice/index.php?resid='+rid, '_blank');
                    if (win) {
                        //Browser has allowed it to be opened
                        win.focus();
                    } else {
                        window.location = 'invoice/index.php?resid='+rid;
                        //Browser has blocked it
                        alert('Please allow popups for this website');

                    }


                    // location.reload();
                    window.location = 'dashboard.php?page=schedules';


                  }else{
                    alert(xhr);
                  }
              } 
            });    
        // }
    });


    var xv = $('select[name=status]').val();
    if(xv == 'paid' || xv == 'approved'){
      $('#divAmount').show();
    }else{
      $('#divAmount').hide();
    }

    
    $('select[name=status]').change(function(){
      var v = $(this).val();
      if(v == 'paid' || v == 'approved'){
        $('#divAmount').show();
      }else{
        $('#divAmount').hide();
      }
    });



    $('input[name=save]').click(function(){
            var nn = $('input[name=name]').val();
            var ne = $('input[name=email]').val();
            var np = $('input[name=phone]').val();
            var nd = $('#datetimepicker1').val();
            var nt = $('select[name=time]').val();
            var at = $('select[name=assto]').val();
            var rid = $('input[name=hresid]').val();
            var axn = $('input[name=haxn]').val();
            var st = $('select[name=status]').val();
            var ev = $('select[name=revent]').val();
            var dt = $('#notes').val();
            var amt = $('input[name=amount]').val();
            var venue  = $('select[name=venue]').val();
            var pt = $('select[name=payment_type]').val();


            if(nn != '' && ne != '' && np != '' && nd != '' && nt != ''){

            $.ajax({
                type: 'post',
                url : 'api/api.php',
                data : {
                    action : 'reservation',
                    name  : nn,
                    email : ne,
                    phone : np,
                    ndate : nd,
                    ntime : nt,
                    ins_by : <?php echo $_SESSION['uuid']; ?>,
                    ass_to : at,
                    status : st,
                    details : dt,
                    res_id : rid,
                    event_ : ev,
                    venue_ : venue,
                    ptype  : pt,
                    ax : axn,
                    amount_ : amt
                },
                beforeSend: function(xhr){
                    $('input[name=submit]').val('Sending...');
                },
                success: function(xhr){
                    console.log(xhr);
                    if(xhr == 'success'){
                        $('input[name=submit]').val('Submit');
                        alert('Reservation Saved!');
                        $('input[name=name]').val('');
                        $('input[name=email]').val('');
                        $('input[name=phone]').val('');
                        $('#datetimepicker1').val('');
                        $('input[name=haxn]').val('');
                        $('select[name=status]').val('-');
                        $('#notes').val('');
                        $('select[name=assto]').val('0');
                        window.location = 'dashboard.php?page=schedules';
                    }else{
                      alert(xhr);
                       //alert('Something wrong with your saving... :(');
                    }
                    

                }
            });

          }else{
              alert('Please fill-in all fields!');
          }
    });

});
</script>