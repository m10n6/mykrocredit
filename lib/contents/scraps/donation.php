<div class="row">
  <div class="col-md-12">
    <button id="" onclick="PrintMe('datatable_donate')" class="btn btn-warning"><i class="fa fa-print fa-lg"></i> PRINT REPORT</button>
    <hr/>
          <table id="datatable_donate" class="table table-striped table-bordered">
            <thead>
              <tr>
                <!-- <th>ID</th> -->
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Confirmed</th>
                <th>Status</th>
                <th>Deposit Slip</th>
                <th>Date Donated</th>
                <th>New Amount</th>
                <th>Invalid Amount</th>
                <th>Valid Amount</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="tblAssets_donate">
              <?php
                include_once('config.php');
                include_once('lib/funcjax.php');
                $res = $conn->dbquery("SELECT * FROM `donation`" );
                $res = json_decode($res);
                $res = $res->data;
                if(count($res) > 0){
                  // print_r($res);
                  $total_amount = 0;
                  foreach ($res as $usersdata) {
                    # code...
                    /*
                    `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                    */
                    $json_usersdata = json_decode($usersdata);
                    // <td>'.$json_usersdata->don_id.'</td>
                    // <td>'.$json_usersdata->account_num.'</td>
                    $invalid_button_status = '';
                    $valid_button_status = '';

                    $valid_val = '0';
                    $invalid_val = '0';
                    $new_val = '0';


                    if($json_usersdata->status == 'invalid'){
                      $invalid_button_status = 'disabled';
                      $invalid_val = $json_usersdata->amount;
                    }

                    if($json_usersdata->status == 'valid'){
                      $valid_button_status = 'disabled';
                      $valid_val = $json_usersdata->amount;
                    }

                    if($json_usersdata->status == 'new'){
                      $new_val = $json_usersdata->amount;
                    }


                    $limg = '';
                    if(!empty($json_usersdata->fpath)){
                      $limg = '<a href="../'.$json_usersdata->fpath.'" target="_blank" class="btn btn-default"><i class="fa fa-picture-o"></i></a>';  
                    }

                    echo '
                      <tr>
                        
                        <td>'.$json_usersdata->name.'</td>
                        <td>'.$json_usersdata->email.'</td>
                        <td>'.$json_usersdata->phone.'</td>
                        <td>'.$json_usersdata->confirmed_date.'</td>
                        <td>'.$json_usersdata->status.'</td>
                        <td>'.$limg.'</td>
                        <td>'.$json_usersdata->date_added.'</td>
                        <td align="right">'.$new_val.'</td>
                        <td align="right">'.$invalid_val.'</td>
                        <td align="right">'.$valid_val.'</td>
                        <td style="white-space: nowrap;">
                        <button type="button" class="btn btn-success" onclick="validDonate('.$json_usersdata->don_id.');" '.$valid_button_status.'><i class="fa fa-trash"></i> Valid</button>
                        <button type="button" class="btn btn-danger" onclick="invalidDonate('.$json_usersdata->don_id.');" '.$invalid_button_status.'><i class="fa fa-trash"></i> Invalid</button>
                        
                        </td>
                      </tr>
                    ';
                    /*
                        <a href="#" class="btn btn-danger" onclick="delDonate('.$json_usersdata->don_id.');"><i class="fa fa-trash"></i> Valid</a>
                        <a href="#" class="btn btn-danger" onclick="delDonate('.$json_usersdata->don_id.');"><i class="fa fa-trash"></i> Invalid</a>
                    */
                    // <a href="#" class="btn btn-danger" onclick="delDonate('.$json_usersdata->don_id.');"><i class="fa fa-trash"></i> Remove</a>
                    $total_amount = $total_amount + $json_usersdata->amount;
                  }                  
                }else{
                  //<td></td>
                  //<td></td>
                    echo '
                      <tr>
                        <td></td>
                        <td></td>                       
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    ';
                }
              ?>
            </tbody>
              <tfoot>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right"><strong>Total : </strong></td>
                <td align="right" style="white-space: nowrap;"></td>
                <td></td>
              </tfoot>
          </table>
  </div>
</div>

<script>
function PrintMe(sel_id){
 $("#" + sel_id).printThis({
        debug: false,               //* show the iframe for debugging
        importCSS: false,            //* import page CSS
        importStyle: false,         //* import style tags
        printContainer: true,       //* grab outer container as well as the contents of the selector
        loadCSS: [/*"lupit/admin/includes/css/custom_print.css",*/"admin/includes/css/custom_print.css?v=<?php echo date('YmdHis'); ?>"],                //* path to additional css file - us an array [] for multiple
        pageTitle: "Donation Report",              //* add title to print page
        removeInline: false,        //* remove all inline styles from print elements
        printDelay: 1900,            //* variable print delay; depending on complexity a higher value may be necessary
        header: null,               //* prefix to html
        formValues: true            //* preserve input/form values
    });
}

function delDonate(gid){
  var q = confirm('Are you sure you want to delete?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'deleteDonate',
          did : gid
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Donation successfully deleted!');
              location.reload();
            }
        } 
      });    
  }
}

function invalidDonate(id){
  var q = confirm('Are you sure that this is Invalid?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'invalidDonate',
          did : id
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Donation is Invalid!');
              location.reload();
            }
        } 
      });    
  }
}

function validDonate(id){
  var q = confirm('Are you sure that this is Valid?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'validDonate',
          did : id
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Donation is Valid!');
              location.reload();
            }
        } 
      });    
  }
}

</script>