<script src="includes/js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="includes/js/icheck/icheck.min.js"></script>

<script src="includes/js/custom.js"></script>


<!-- Datatables -->
<!-- <script src="includes/js/datatables/js/jquery.dataTables.js"></script>
<script src="includes/js/datatables/tools/js/dataTables.tableTools.js"></script> -->

<!-- Datatables-->
<script src="includes/js/datatables/jquery.dataTables.min.js"></script>
<script src="includes/js/datatables/dataTables.bootstrap.js"></script>
<script src="includes/js/datatables/dataTables.buttons.min.js"></script>
<script src="includes/js/datatables/buttons.bootstrap.min.js"></script>
<script src="includes/js/datatables/jszip.min.js"></script>
<script src="includes/js/datatables/pdfmake.min.js"></script>
<script src="includes/js/datatables/vfs_fonts.js"></script>
<script src="includes/js/datatables/buttons.html5.min.js"></script>
<script src="includes/js/datatables/buttons.print.min.js"></script>
<script src="includes/js/datatables/dataTables.fixedHeader.min.js"></script>
<script src="includes/js/datatables/dataTables.keyTable.min.js"></script>
<script src="includes/js/datatables/dataTables.responsive.min.js"></script>
<script src="includes/js/datatables/responsive.bootstrap.min.js"></script>
<script src="includes/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script src="includes/js/pace/pace.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="includes/js/moment/moment.min.js"></script>
<script src="includes/js/datepicker/daterangepicker.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  // $('#datatable').datatable();
  var table_ = $('#datatable').dataTable({

        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],

        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 9 ).footer() ).html(
                'Php '+pageTotal +' ( Php '+ total +' total)'
            );
        },
        "order": [[ 0, "desc" ]]

  });

  table_.on( 'search.dt', function () {
    //$('#filterInfo').html( 'Currently applied global search: '+table.search() );
    console.log('tests');
  } );

	$(function () {
	    //$('#datetimepicker1').datetimepicker();
	    $('#datetimepicker1').daterangepicker({
	      singleDatePicker: true,
	      calender_style: "picker_4",
	      format: 'YYYY-MM-DD'
	    }, function(start, end, label) {
	      console.log(start.toISOString(), end.toISOString(), label);
	    });
	});
});

$(document).ready(function(){
  $('#datatable1').dataTable();
  $('#datatable2').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],

        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 3 ).footer() ).html(
                'Php '+pageTotal +' ( Php '+ total +' total)'
            );
        },
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ]
  });
  $('#datatable3').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],

        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                'Php '+pageTotal +' ( Php '+ total +' total)'
            );
        },
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ]
  });

  $('#datatable9').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],

        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                'Php '+pageTotal +' ( Php '+ total +' total)'
            );
        },
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ]
  });
  $('#datatable4').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],

        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                'Php '+pageTotal +' ( Php '+ total +' total)'
            );
        },
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ]
  });
  $('#datatable5').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ]
  });
  $('#datatable6').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ]
  });
  $('#datatable7').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ]
  });
  $('#datatable8').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ]
  });

});


function PrintMe(sel_id){
 $("#" + sel_id).printThis({
        debug: false,               //* show the iframe for debugging
        importCSS: false,            //* import page CSS
        importStyle: false,         //* import style tags
        printContainer: true,       //* grab outer container as well as the contents of the selector
        loadCSS: ["lupit/admin/includes/css/custom_print.css","admin/includes/css/custom_print.css"],                //* path to additional css file - us an array [] for multiple
        pageTitle: "Booking Report",              //* add title to print page
        removeInline: false,        //* remove all inline styles from print elements
        printDelay: 900,            //* variable print delay; depending on complexity a higher value may be necessary
        header: null,               //* prefix to html
        formValues: true            //* preserve input/form values
    });
}
</script>