<script src="includes/js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="includes/js/icheck/icheck.min.js"></script>

<script src="includes/js/custom.js"></script>


<!-- Datatables -->
<!-- <script src="includes/js/datatables/js/jquery.dataTables.js"></script>
<script src="includes/js/datatables/tools/js/dataTables.tableTools.js"></script> -->

<!-- Datatables-->
<script src="includes/js/datatables/jquery.dataTables.min.js"></script>
<script src="includes/js/datatables/dataTables.bootstrap.js"></script>
<script src="includes/js/datatables/dataTables.buttons.min.js"></script>
<script src="includes/js/datatables/buttons.bootstrap.min.js"></script>
<script src="includes/js/datatables/jszip.min.js"></script>
<script src="includes/js/datatables/pdfmake.min.js"></script>
<script src="includes/js/datatables/vfs_fonts.js"></script>
<script src="includes/js/datatables/buttons.html5.min.js"></script>
<script src="includes/js/datatables/buttons.print.min.js"></script>
<script src="includes/js/datatables/dataTables.fixedHeader.min.js"></script>
<script src="includes/js/datatables/dataTables.keyTable.min.js"></script>
<script src="includes/js/datatables/dataTables.responsive.min.js"></script>
<script src="includes/js/datatables/responsive.bootstrap.min.js"></script>
<script src="includes/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script src="includes/js/pace/pace.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  // $('#datatable').datatable();
  $('#datatable').dataTable();
});
</script>