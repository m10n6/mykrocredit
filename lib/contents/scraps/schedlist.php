<?php
include_once('config.php');
include_once('lib/funcjax.php');

$user_id = secure_get('uid');
$axn = secure_get('axn');

$ulevel = getUser_levelid($user_id, $conn);
?>
<div class="row">

	<div class="col-md-12">
    <h2>List View Schedule of <strong><?php echo translateName($user_id, $conn);  ?></strong></h2>

    <button id="" onclick="PrintMe('datatable5')" class="btn btn-warning"><i class="fa fa-print fa-lg"></i> PRINT REPORT</button>
    <hr/>

            <table id="datatable5" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Event</th>
                <th>Venue</th>
                <th>Status</th>
                <th>Date</th>
                <?php if(!empty($ulevel) && $ulevel != '2' && $_SESSION['ulevel'] == '1'){ ?>
                <th>Action</th>
                <?php } ?>
              </tr>
            </thead>
            <tbody id="tblAssets2">
              <?php
                include_once('config.php');
                include_once('lib/funcjax.php');
                $startdate = date("Y-m-d")." "."00:00:00";
                $enddate = date("Y-m-d")." "."23:59:59";
                if(!empty($ulevel) && $ulevel != '2'){
                  $res = $conn->dbquery("SELECT * FROM `schedule` where `ulevel` = '".$ulevel."' and `user_id` = '".$user_id."' order by `sched_id` desc");  
                }elseif(!empty($ulevel) && $ulevel == '2'){
                  $res = $conn->dbquery("SELECT * FROM `reservation` where `assigned_to` = '".$user_id."' order by `created_date`" );
                }
                


                $res = json_decode($res);
                $res = $res->data;
                if(count($res) > 0){
                  // print_r($res);

                  $total_ = 0;
                  foreach ($res as $data) {
                    # code...
                    /*
                    `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                    */
                    $json_data = json_decode($data);
                    if(!empty($ulevel) && $ulevel != '2'){
                      $id = $json_data->sched_id;
                      $sched = $json_data->start_date;
                      $status = '-';
                      // $event  = 'Mass';
                      $event  = $json_data->title;
                      // $venue  = 'Church';
                      $venue  = $json_data->desc;
                      if(!empty($ulevel) && $ulevel != '2'  && $_SESSION['ulevel'] == '1'){
                      $bt = '<td><button type="button" onclick="deleteSched(\''.$id.'\');" class="btn btn-danger"><i class="fa fa-trash"></i> Remove Schedule</button></td>';
                    }
                    }elseif(!empty($ulevel) && $ulevel == '2'){
                      $id = $json_data->res_id;
                      $sched = $json_data->start_date;
                      $status = $json_data->status;
                      $event = $json_data->event;
                      $venue = translateVenue($json_data->venue);
                    }

                    $events_list[] = $event;
                    $startdate_list[] = $sched;
                    $venue_list[] = $venue;
                    
                    echo '
                      <tr>
                        <td>'.$id.'</td>
                        <td>'.ucfirst($event).'</td>
                        <td>'.ucfirst(str_replace('_', ' ', $venue)).'</td>
                        <td>'.$status.'</td>
                        <td>'.$sched.'</td>
                        '.$bt.'
                      </tr>
                    ';
                  }                  
                }else{
                  //<td></td>
                  if(!empty($ulevel) && $ulevel != '2'  && $_SESSION['ulevel'] == '1'){
                      $bt = '<th>Action</th>';
                  }
                    echo '
                      <tr>
                        '.$bt.'
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    ';
                }
              ?>
            </tbody>
          </table>

	</div>

  <?php if(!empty($ulevel) && $ulevel == '2'  && $_SESSION['ulevel'] == '1'){ ?>
  <button id="btnReaderSched" class="btn btn-primary"><i class="fa fa-plus"></i> Add Reader </button>
  <button id="btnCollectorSched" class="btn btn-primary"><i class="fa fa-plus"></i> Add Collector </button>
  <button id="btnMinistrySched" class="btn btn-primary"><i class="fa fa-plus"></i> Add Lay Minister </button>
  <button id="btnAcolytesSched" class="btn btn-primary"><i class="fa fa-plus"></i> Add Acolytes </button>
  <input type="hidden" name="priestid" id="priestid" value="<?php echo $user_id; ?>"> 
  <?php } ?>
</div>
<script>
function capitalizeFirstLetter(string) {
    return string[0].toUpperCase() + string.slice(1);
}
  $(document).ready(function(){
    $('select[name=revent1]').change(function(){
        var v = $(this).val();
        if(v != '-'){
          $.ajax({
              type: 'post',
              url: 'api/api.php',
              data : {
                action: 'getPriestVenue',
                pid: <?php echo $user_id; ?>,
                ev : v
              },
              dataType: 'json',
              success: function(result){
                console.log(result);
                var len = result.data.length;
                console.log(len);
                var html = '<option value="-">-Select-</option>';
                for(var i=0; i<len; i++){
                  var obj = JSON.parse(result.data[i]);
                  var str = obj.venue;
                  html += '<option value="'+ obj.venue +'">'+ capitalizeFirstLetter(str.replace('_', ' ')) +'</option>';
                }
                $('select[name=venue1]').html(html);
              }
              ,beforeSend: function(xhr){
                $('select[name=venue1]').html('<option value="-">Loading...</option>');
              }
          });
        }else{
        }
    });

    $('select[name=venue1]').change(function(){
        var v = $(this).val();
        var e = $('select[name=revent1]').val();
        if(v != '-' && e != '-'){
          $.ajax({
              type: 'post',
              url: 'api/api.php',
              data : {
                action: 'getPriestDateSched',
                pid: <?php echo $user_id; ?>,
                ev : e,
                ve : v
              },
              dataType: 'json',
              success: function(result){
                console.log(result);
                var len = result.data.length;
                console.log(len);
                var html = '<option value="-">-Select-</option>';
                for(var i=0; i<len; i++){
                  var obj = JSON.parse(result.data[i]);
                  var str = obj.start_date;
                  html += '<option value="'+ str +'">'+ str +'</option>';
                }
                $('select[name=datetime1]').html(html);
              }
              ,beforeSend: function(xhr){
                $('select[name=datetime1]').html('<option value="-">Loading...</option>');
              }
          });
        }else{
          alert('Please select Venue and/or Event!');
        }
    });


    $('select[name=revent2]').change(function(){
        var v = $(this).val();
        if(v != '-'){
          $.ajax({
              type: 'post',
              url: 'api/api.php',
              data : {
                action: 'getPriestVenue',
                pid: <?php echo $user_id; ?>,
                ev : v
              },
              dataType: 'json',
              success: function(result){
                console.log(result);
                var len = result.data.length;
                console.log(len);
                var html = '<option value="-">-Select-</option>';
                for(var i=0; i<len; i++){
                  var obj = JSON.parse(result.data[i]);
                  var str = obj.venue;
                  html += '<option value="'+ obj.venue +'">'+ capitalizeFirstLetter(str.replace('_', ' ')) +'</option>';
                }
                $('select[name=venue2]').html(html);
              }
              ,beforeSend: function(xhr){
                $('select[name=venue2]').html('<option value="-">Loading...</option>');
              }
          });
        }else{
        }
    });

    $('select[name=venue2]').change(function(){
        var v = $(this).val();
        var e = $('select[name=revent2]').val();
        if(v != '-' && e != '-'){
          $.ajax({
              type: 'post',
              url: 'api/api.php',
              data : {
                action: 'getPriestDateSched',
                pid: <?php echo $user_id; ?>,
                ev : e,
                ve : v
              },
              dataType: 'json',
              success: function(result){
                console.log(result);
                var len = result.data.length;
                console.log(len);
                var html = '<option value="-">-Select-</option>';
                for(var i=0; i<len; i++){
                  var obj = JSON.parse(result.data[i]);
                  var str = obj.start_date;
                  html += '<option value="'+ str +'">'+ str +'</option>';
                }
                $('select[name=datetime2]').html(html);
              }
              ,beforeSend: function(xhr){
                $('select[name=datetime2]').html('<option value="-">Loading...</option>');
              }
          });
        }else{
          alert('Please select Venue and/or Event!');
        }
    });


    $('select[name=revent3]').change(function(){
        var v = $(this).val();
        if(v != '-'){
          $.ajax({
              type: 'post',
              url: 'api/api.php',
              data : {
                action: 'getPriestVenue',
                pid: <?php echo $user_id; ?>,
                ev : v
              },
              dataType: 'json',
              success: function(result){
                console.log(result);
                var len = result.data.length;
                console.log(len);
                var html = '<option value="-">-Select-</option>';
                for(var i=0; i<len; i++){
                  var obj = JSON.parse(result.data[i]);
                  var str = obj.venue;
                  html += '<option value="'+ obj.venue +'">'+ capitalizeFirstLetter(str.replace('_', ' ')) +'</option>';
                }
                $('select[name=venue3]').html(html);
              }
              ,beforeSend: function(xhr){
                $('select[name=venue3]').html('<option value="-">Loading...</option>');
              }
          });
        }else{
        }
    });

    $('select[name=venue3]').change(function(){
        var v = $(this).val();
        var e = $('select[name=revent3]').val();
        if(v != '-' && e != '-'){
          $.ajax({
              type: 'post',
              url: 'api/api.php',
              data : {
                action: 'getPriestDateSched',
                pid: <?php echo $user_id; ?>,
                ev : e,
                ve : v
              },
              dataType: 'json',
              success: function(result){
                console.log(result);
                var len = result.data.length;
                console.log(len);
                var html = '<option value="-">-Select-</option>';
                for(var i=0; i<len; i++){
                  var obj = JSON.parse(result.data[i]);
                  var str = obj.start_date;
                  html += '<option value="'+ str +'">'+ str +'</option>';
                }
                $('select[name=datetime3]').html(html);
              }
              ,beforeSend: function(xhr){
                $('select[name=datetime3]').html('<option value="-">Loading...</option>');
              }
          });
        }else{
          alert('Please select Venue and/or Event!');
        }
    });

    $('select[name=revent4]').change(function(){
        var v = $(this).val();
        if(v != '-'){
          $.ajax({
              type: 'post',
              url: 'api/api.php',
              data : {
                action: 'getPriestVenue',
                pid: <?php echo $user_id; ?>,
                ev : v
              },
              dataType: 'json',
              success: function(result){
                console.log(result);
                var len = result.data.length;
                console.log(len);
                var html = '<option value="-">-Select-</option>';
                for(var i=0; i<len; i++){
                  var obj = JSON.parse(result.data[i]);
                  var str = obj.venue;
                  html += '<option value="'+ obj.venue +'">'+ capitalizeFirstLetter(str.replace('_', ' ')) +'</option>';
                }
                $('select[name=venue4]').html(html);
              }
              ,beforeSend: function(xhr){
                $('select[name=venue4]').html('<option value="-">Loading...</option>');
              }
          });
        }else{
        }
    });

    $('select[name=venue4]').change(function(){
        var v = $(this).val();
        var e = $('select[name=revent4]').val();
        if(v != '-' && e != '-'){
          $.ajax({
              type: 'post',
              url: 'api/api.php',
              data : {
                action: 'getPriestDateSched',
                pid: <?php echo $user_id; ?>,
                ev : e,
                ve : v
              },
              dataType: 'json',
              success: function(result){
                console.log(result);
                var len = result.data.length;
                console.log(len);
                var html = '<option value="-">-Select-</option>';
                for(var i=0; i<len; i++){
                  var obj = JSON.parse(result.data[i]);
                  var str = obj.start_date;
                  html += '<option value="'+ str +'">'+ str +'</option>';
                }
                $('select[name=datetime4]').html(html);
              }
              ,beforeSend: function(xhr){
                $('select[name=datetime4]').html('<option value="-">Loading...</option>');
              }
          });
        }else{
          alert('Please select Venue and/or Event!');
        }
    });

  });

</script>


                    <style>
                    .picker_4{z-index:1151 !important;}
                    </style>

                  <!-- Modal -->
                  <div class="modal fade" id="myreadersched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Reader Schedule</h4>
                        </div>
                        <div class="modal-body">
                            <form>

                               <div class="form-group">
                                    <label class="control-label">Reader's Name</label>
                                    <select id='col_name1' class='form-control'>
                                        <?php
                                            $res1 = $conn->dbquery("SELECT * FROM `users` WHERE `ulevel` = '3'" );
                                            $res1 = json_decode($res1);
                                            $res1 = $res1->data;
                                            echo '<option value="0">-Select-</option>';
                                            foreach ($res1 as $usersdata) {
                                                $json_usersdata = json_decode($usersdata);
                                                if($res->assigned_to == $json_usersdata->user_id){
                                                  $ssel = 'selected';
                                                }else{
                                                  $ssel = '';
                                                }
                                                echo '
                                                  <option value="'.$json_usersdata->user_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Event</label>
                                    <select name="revent1" class="form-control" >
                                        <option value="-">-Select-</option>
                                          <?php
                                          foreach ($events_list as $ev ) {
                                            # code...
                                            echo '<option value="'.$ev.'">'.ucfirst($ev).'</option>';
                                          }
                                          ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Venue</label>
                                    <select name="venue1" class="form-control" >
                                        <option value="-">-Select-</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Date & Time</label>
                                    <select name="datetime1" class="form-control" >
                                        <option value="-">-Select-</option>
                                    </select>
                                </div>
                                <input type="hidden" id="hidid1" value="">             
                            </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" id="btnSaveReader" style="margin-top: -5px;">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Modal -->
                  <div class="modal fade" id="mycollectorsched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Collectors Schedule</h4>
                        </div>
                        <div class="modal-body">
                            <form>
                               <div class="form-group">
                                    <label class="control-label">Collector's Name</label>
                                    <select id='col_name2' class='form-control'>
                                        <?php
                                            $res1 = $conn->dbquery("SELECT * FROM `users` WHERE `ulevel` = '4'" );
                                            $res1 = json_decode($res1);
                                            $res1 = $res1->data;
                                            echo '<option value="0">-Select-</option>';
                                            foreach ($res1 as $usersdata) {
                                                $json_usersdata = json_decode($usersdata);
                                                if($res->assigned_to == $json_usersdata->user_id){
                                                  $ssel = 'selected';
                                                }else{
                                                  $ssel = '';
                                                }
                                                echo '
                                                  <option value="'.$json_usersdata->user_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Event</label>
                                    <select name="revent2" class="form-control" >
                                        <option value="-">-Select-</option>
                                          <?php
                                          foreach ($events_list as $ev ) {
                                            # code...
                                            echo '<option value="'.$ev.'">'.ucfirst($ev).'</option>';
                                          }
                                          ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Venue</label>
                                    <select name="venue2" class="form-control" >
                                        <option value="-">-Select-</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Date & Time</label>
                                    <select name="datetime2" class="form-control" >
                                        <option value="-">-Select-</option>
                                    </select>
                                </div>
                                <input type="hidden" id="hidid2" value="">
                            </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" id="btnSaveCollector" style="margin-top: -5px;">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>



<style>
.picker_4{z-index:1151 !important;}
</style>

<!-- Modal -->
<div class="modal fade" id="myministrysched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Lay Minister Schedule</h4>
    </div>
    <div class="modal-body">
        <form>
           <div class="form-group">
                <label class="control-label">Lay Minister's Name</label>
                <select id='col_name3' class='form-control'>
                    <?php
                        $res1 = $conn->dbquery("SELECT * FROM `users` WHERE `ulevel` = '5'" );
                        $res1 = json_decode($res1);
                        $res1 = $res1->data;
                        echo '<option value="0">-Select-</option>';
                        foreach ($res1 as $usersdata) {
                            $json_usersdata = json_decode($usersdata);
                            if($res->assigned_to == $json_usersdata->user_id){
                              $ssel = 'selected';
                            }else{
                              $ssel = '';
                            }
                            echo '
                              <option value="'.$json_usersdata->user_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                            ';
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label class="form-label">Event</label>
                <select name="revent3" class="form-control" >
                    <option value="-">-Select-</option>
                      <?php
                      foreach ($events_list as $ev ) {
                        # code...
                        echo '<option value="'.$ev.'">'.ucfirst($ev).'</option>';
                      }
                      ?>
                </select>
            </div>
            <div class="form-group">
                <label class="form-label">Venue</label>
                <select name="venue3" class="form-control" >
                    <option value="-">-Select-</option>
                </select>
            </div>
            <div class="form-group">
                <label class="form-label">Date & Time</label>
                <select name="datetime3" class="form-control" >
                    <option value="-">-Select-</option>
                </select>
            </div>
            <input type="hidden" id="hidid3" value="">
        </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary" id="btnSaveMinistry" style="margin-top: -5px;">Save</button>
    </div>
  </div>
</div>
</div>


                  <!-- Modal -->
                  <div class="modal fade" id="myacolytessched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Acolytes Schedule</h4>
                        </div>
                        <div class="modal-body">
                            <form>
                               <div class="form-group">
                                    <label class="control-label">Acolyte's Name</label>
                                    <select id='col_name4' class='form-control'>
                                        <?php
                                            $res1 = $conn->dbquery("SELECT * FROM `users` WHERE `ulevel` = '6'" );
                                            $res1 = json_decode($res1);
                                            $res1 = $res1->data;
                                            echo '<option value="0">-Select-</option>';
                                            foreach ($res1 as $usersdata) {
                                                $json_usersdata = json_decode($usersdata);
                                                if($res->assigned_to == $json_usersdata->user_id){
                                                  $ssel = 'selected';
                                                }else{
                                                  $ssel = '';
                                                }
                                                echo '
                                                  <option value="'.$json_usersdata->user_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Event</label>
                                    <select name="revent4" class="form-control" >
                                        <option value="-">-Select-</option>
                                          <?php
                                          foreach ($events_list as $ev ) {
                                            # code...
                                            echo '<option value="'.$ev.'">'.ucfirst($ev).'</option>';
                                          }
                                          ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Venue</label>
                                    <select name="venue4" class="form-control" >
                                        <option value="-">-Select-</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Date & Time</label>
                                    <select name="datetime4" class="form-control" >
                                        <option value="-">-Select-</option>
                                    </select>
                                </div>
                                <input type="hidden" id="hidid4" value="">
                            </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" id="btnSaveAcolytes" style="margin-top: -5px;">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>



<script>
function deleteSched(gid){
  var q = confirm('Are you sure you want to delete?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'removeSchedule',
          sid : gid
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Schedule successfully deleted!');
              location.reload();
            }
        } 
      });    
  }
}

function editSched(gid){
  window.location = 'dashboard.php?page=schedules_add&res_id=' + gid;
}

function PrintMe(sel_id){
 $("#" + sel_id).printThis({
        debug: false,               //* show the iframe for debugging
        importCSS: false,            //* import page CSS
        importStyle: false,         //* import style tags
        printContainer: true,       //* grab outer container as well as the contents of the selector
        loadCSS: ["lupit/admin/includes/css/custom_print.css","admin/includes/css/custom_print.css"],                //* path to additional css file - us an array [] for multiple
        pageTitle: "Booking Report",              //* add title to print page
        removeInline: false,        //* remove all inline styles from print elements
        printDelay: 900,            //* variable print delay; depending on complexity a higher value may be necessary
        header: null,               //* prefix to html
        formValues: true            //* preserve input/form values
    });
}

$(document).ready(function(){
    $('#btnReaderSched').click(function(){
        $('#myreadersched').modal('show');
    });

    $('#btnSaveReader').click(function(){
        var cname = $('#col_name1').val();
        var ddate1 = $('#datetimepicker1').val();
        var dtime1 = $('#time1').val();
        var hidid1 = $('#hidid1').val();
        var ev1 = $('select[name=revent1]').val();
        var ve1 = $('select[name=venue1]').val();
        var dt1 = $('select[name=datetime1]').val();
        if(cname != '-' && ev1 != '-' && ve1 != '-' && dt1 != '-'){
          $.ajax({
            type: 'post',
            url: 'api/api.php',
            data: {
              action: 'saveSchedule2',
              user_id : cname,
              // ddate: ddate1,
              // dtime: dtime1,
              hid_id: hidid1,
              // titl : ev1,
              ddtm : dt1,
              ev : ev1,
              ve : ve1,
              pi : <?php echo $user_id; ?>,
              ul : '3'
            },
            beforeSend: function(xhr){

            },
            success: function(xhr){
                console.log(xhr);
                if(xhr == 'success'){
                  alert('Schedule is now set!');
                  location.reload();
                  // window.location = 'dashboard.php?page=sched_page#reader';
                }else{
                  alert(xhr);
                }
            } 
          });        
        }else{
          alert('Please select Reader, Event, Venue and Date/Time!');
        }
    });

    $('#btnCollectorSched').click(function(){
        $('#mycollectorsched').modal('show');
    });

    $('#btnSaveCollector').click(function(){
        var cname = $('#col_name2').val();
        var ddate1 = $('#datetimepicker2').val();
        var dtime1 = $('#time2').val();
        var hidid1 = $('#hidid2').val();
        var ev1 = $('select[name=revent2]').val();
        var ve1 = $('select[name=venue2]').val();
        var dt1 = $('select[name=datetime2]').val();
        if(cname != '-' && ev1 != '-' && ve1 != '-' && dt1 != '-'){
          $.ajax({
            type: 'post',
            url: 'api/api.php',
            data: {
              action: 'saveSchedule2',
              user_id : cname,
              // ddate: ddate1,
              // dtime: dtime1,
              hid_id: hidid1,
              // titl : ev1,
              ddtm : dt1,
              ev : ev1,
              ve : ve1,
              pi : <?php echo $user_id; ?>,
              ul : '4'
            },
            beforeSend: function(xhr){

            },
            success: function(xhr){
                console.log(xhr);
                if(xhr == 'success'){
                  alert('Schedule is now set!');
                  location.reload();
                  // window.location = 'dashboard.php?page=sched_page#reader';
                }else{
                  alert(xhr);
                }
            } 
          });        
        }else{
          alert('Please select Reader, Event, Venue and Date/Time!');
        }  
        
    });

    $('#btnMinistrySched').click(function(){
        $('#myministrysched').modal('show');
    });

    $('#btnSaveMinistry').click(function(){
        var cname = $('#col_name3').val();
        var ddate1 = $('#datetimepicker3').val();
        var dtime1 = $('#time3').val();
        var hidid1 = $('#hidid3').val();
        var ev1 = $('select[name=revent3]').val();
        var ve1 = $('select[name=venue3]').val();
        var dt1 = $('select[name=datetime3]').val();
        if(cname != '-' && ev1 != '-' && ve1 != '-' && dt1 != '-'){
          $.ajax({
            type: 'post',
            url: 'api/api.php',
            data: {
              action: 'saveSchedule2',
              user_id : cname,
              // ddate: ddate1,
              // dtime: dtime1,
              hid_id: hidid1,
              // titl : ev1,
              ddtm : dt1,
              ev : ev1,
              ve : ve1,
              pi : <?php echo $user_id; ?>,
              ul : '5'
            },
            beforeSend: function(xhr){

            },
            success: function(xhr){
                console.log(xhr);
                if(xhr == 'success'){
                  alert('Schedule is now set!');
                  location.reload();
                  // window.location = 'dashboard.php?page=sched_page#reader';
                }else{
                  alert(xhr);
                }
            } 
          });        
        }else{
          alert('Please select Reader, Event, Venue and Date/Time!');
        }
      
    });

    $('#btnAcolytesSched').click(function(){
        $('#myacolytessched').modal('show');
    });

    $('#btnSaveAcolytes').click(function(){   
        var cname = $('#col_name4').val();
        var ddate1 = $('#datetimepicker4').val();
        var dtime1 = $('#time4').val();
        var hidid1 = $('#hidid4').val();
        var ev1 = $('select[name=revent4]').val();
        var ve1 = $('select[name=venue4]').val();
        var dt1 = $('select[name=datetime4]').val();
        if(cname != '-' && ev1 != '-' && ve1 != '-' && dt1 != '-'){
          $.ajax({
            type: 'post',
            url: 'api/api.php',
            data: {
              action: 'saveSchedule2',
              user_id : cname,
              // ddate: ddate1,
              // dtime: dtime1,
              hid_id: hidid1,
              // titl : ev1,
              ddtm : dt1,
              ev : ev1,
              ve : ve1,
              pi : <?php echo $user_id; ?>,
              ul : '6'
            },
            beforeSend: function(xhr){

            },
            success: function(xhr){
                console.log(xhr);
                if(xhr == 'success'){
                  alert('Schedule is now set!');
                  location.reload();
                  // window.location = 'dashboard.php?page=sched_page#reader';
                }else{
                  alert(xhr);
                }
            } 
          });        
        }else{
          alert('Please select Reader, Event, Venue and Date/Time!');
        }
        
    });

});

</script>