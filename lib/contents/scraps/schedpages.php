<div class="row">
  <div class="col-md-12">

    <ul class="nav nav-tabs">
      <li class="active"><a id="tab_priest" data-toggle="tab" href="#priest">Priest</a></li>
      <li><a id="tab_reader"  data-toggle="tab" href="#reader">Reader</a></li>
      <li><a id="tab_collector"  data-toggle="tab" href="#collector">Collector</a></li>
      <li><a id="tab_ministry"  data-toggle="tab" href="#ministry">Lay Ministry</a></li>
      <li><a id="tab_acolytes"  data-toggle="tab" href="#acolytes">Acolytes</a></li>
    </ul>

    <div class="tab-content">

      <div id="priest" class="tab-pane fade in active">
        <h3>Priest Schedule</h3>
        <!-- <p>Some content in menu 1.</p> -->
        <p>
          <a href="dashboard.php?page=schedules_add" class="btn btn-success"><i class="fa fa-plus"></i> Add Schedule</a><br/>
          <small>Note: Priest schedule is much similar with bookings but instead of putting the name of the customer/reservee you just put any name / the name of the priest and assign to itself.</small>
        </p>

          <div class="row">
            <div class="col-md-12">
                    <table id="datatable1" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <!-- <th>ID</th> -->
                          <th>Name</th>
                          <th>Phone</th>
                          <th>Schedule</th>
                        </tr>
                      </thead>
                      <tbody id="tblAssets1">
                        <?php
                          include_once('config.php');
                          include_once('lib/funcjax.php');
                          $startdate = date("Y-m-d")." "."00:00:00";
                          $enddate = date("Y-m-d")." "."23:59:59";
                          $res = $conn->dbquery("SELECT * FROM `users` where `ulevel` = '2'" );


                          $res = json_decode($res);
                          $res = $res->data;
                          if(count($res) > 0){
                            // print_r($res);

                            $total_ = 0;
                            foreach ($res as $priest) {
                              # code...
                              /*
                              `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                              */
                              $json_priest = json_decode($priest);
                              
                              echo '
                                <tr>
                                  
                                  <td>'.$json_priest->name.'</td>
                                  <td>'.$json_priest->phone.'</td>
                                  <td>
                                    <a href="dashboard.php?page=calendar&uid='.$json_priest->user_id.'&axn=usercal"  class="btn btn-warning" type="button"><i class="fa fa-calendar"></i> Calendar View</a>
                                    <a href="dashboard.php?page=schedlist&uid='.$json_priest->user_id.'&axn=userlist"  class="btn btn-success" type="button"><i class="fa fa-list"></i> List View</a>
                                  </td>
                                </tr>
                              ';
                            }                  
                          }else{
                            //<td></td>
                              echo '
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              ';
                          }
                        ?>
                      </tbody>
                    </table>

            </div>
          </div>

      </div>

      <div id="reader" class="tab-pane fade">
        <h3>Reader Schedule</h3>
        <!-- <p>Some content in menu 2.</p> -->
        <p>
          <!-- <a href="#" class="btn btn-success" id="btnReaderSched"><i class="fa fa-plus"></i> Add Schedule</a><br/> -->
          <small></small>
        </p>

          <div class="row">
            <div class="col-md-12">
                    <table id="datatable2" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <!-- <th>ID</th> -->
                          <th>Name</th>
                          <th>Phone</th>
                          <th>Schedule</th>
                        </tr>
                      </thead>
                      <tbody id="tblAssets2">
                        <?php
                          include_once('config.php');
                          include_once('lib/funcjax.php');
                          $startdate = date("Y-m-d")." "."00:00:00";
                          $enddate = date("Y-m-d")." "."23:59:59";
                          $res = $conn->dbquery("SELECT * FROM `users` where `ulevel` = '3'" );


                          $res = json_decode($res);
                          $res = $res->data;
                          if(count($res) > 0){
                            // print_r($res);

                            $total_ = 0;
                            foreach ($res as $priest) {
                              # code...
                              /*
                              `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                              */
                              $json_priest = json_decode($priest);
                              
                              echo '
                                <tr>
                                  
                                  <td>'.$json_priest->name.'</td>
                                  <td>'.$json_priest->phone.'</td>
                                  <td>
                                  <a href="dashboard.php?page=calendar&uid='.$json_priest->user_id.'&axn=usercal"  class="btn btn-warning" type="button"><i class="fa fa-calendar"></i> Calendar View</a>
                                  <a href="dashboard.php?page=schedlist&uid='.$json_priest->user_id.'&axn=userlist"  class="btn btn-success" type="button"><i class="fa fa-list"></i> List View</a>
                                  </td>
                                </tr>
                              ';
                            }                  
                          }else{
                            //<td></td>
                              echo '
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              ';
                          }
                        ?>
                      </tbody>
                    </table>
                    <script>
                    $(document).ready(function(){
                      $('#btnReaderSched').click(function(){
                          $('#myreadersched').modal('show');
                      });

                      $('#btnSaveReader').click(function(){
                          var cname = $('#col_name1').val();
                          var ddate1 = $('#datetimepicker1').val();
                          var dtime1 = $('#time1').val();
                          var hidid1 = $('#hidid1').val();
                          var ev1 = $('select[name=revent1]').val();
                          $.ajax({
                            type: 'post',
                            url: 'api/api.php',
                            data: {
                              action: 'saveSchedule',
                              user_id : cname,
                              ddate: ddate1,
                              dtime: dtime1,
                              hid_id: hidid1,
                              titl : ev1,
                              ul : '3'
                            },
                            beforeSend: function(xhr){

                            },
                            success: function(xhr){
                                console.log(xhr);
                                if(xhr == 'success'){
                                  alert('Schedule is now set!');
                                  location.reload();
                                  // window.location = 'dashboard.php?page=sched_page#reader';
                                }else{
                                  alert(xhr);
                                }
                            } 
                          });        
                          
                      });
                      // user_id
                      // ddate
                      // dtime
                      // hid_id

                    });
                    </script>
                    <style>
                    .picker_4{z-index:1151 !important;}
                    </style>

                  <!-- Modal -->
                  <div class="modal fade" id="myreadersched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Reader Schedule</h4>
                        </div>
                        <div class="modal-body">
                            <form>
                               <div class="form-group">
                                    <label class="control-label">Collector's Name</label>
                                    <select id='col_name1' class='form-control'>
                                        <?php
                                            $res1 = $conn->dbquery("SELECT * FROM `users` WHERE `ulevel` = '3'" );
                                            $res1 = json_decode($res1);
                                            $res1 = $res1->data;
                                            echo '<option value="0">-Select-</option>';
                                            foreach ($res1 as $usersdata) {
                                                $json_usersdata = json_decode($usersdata);
                                                if($res->assigned_to == $json_usersdata->user_id){
                                                  $ssel = 'selected';
                                                }else{
                                                  $ssel = '';
                                                }
                                                echo '
                                                  <option value="'.$json_usersdata->user_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Event</label>
                                    <select name="revent1" class="form-control" >
                                        <option value="mass">Mass</option>
                                        <option value="wedding">Wedding Mass</option>
                                        <option value="burial">Burial</option>
                                        <option value="baptism">Baptism</option>
                                        <option value="blessing">Blessing</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Date</label>
                                    <div class="form-group">
                                        <div class='input-group date-picker' >
                                            <input type='text' class="form-control" id='datetimepicker1' value="<?php echo $start_date; ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Time</label>
                                    <select id="time1" class="form-control" >
                                        <?php
                                            $start = 7;
                                            for($i = 0; $i < 12; $i++){
                                                $seltime = ($start + $i).':00:00';
                                                // ltrim($start_time, "0");
                                                if($start_time == $seltime){
                                                  $sel = "selected";
                                                }else{
                                                  $sel = "";
                                                }
                                                echo '
                                                    <option value="'.$seltime.'" '.$sel.' >'.$seltime.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                    <input type="hidden" id="hidid1" value="">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" id="btnSaveReader" style="margin-top: -5px;">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>


            </div>
          </div>

      </div>

      <div id="collector" class="tab-pane fade">
        <h3>Collector Schedule</h3>
        <!-- <p>Some content in menu 1.</p> -->
        <p>
          <!-- <a href="#" class="btn btn-success" id="btnCollectorSched"><i class="fa fa-plus"></i> Add Schedule</a><br/> -->
          <small></small>
        </p>
          <div class="row">
            <div class="col-md-12">
                    <table id="datatable3" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <!-- <th>ID</th> -->
                          <th>Name</th>
                          <th>Phone</th>
                          <th>Schedule</th>
                        </tr>
                      </thead>
                      <tbody id="tblAssets3">
                        <?php
                          include_once('config.php');
                          include_once('lib/funcjax.php');
                          $startdate = date("Y-m-d")." "."00:00:00";
                          $enddate = date("Y-m-d")." "."23:59:59";
                          $res = $conn->dbquery("SELECT * FROM `users` where `ulevel` = '4'" );


                          $res = json_decode($res);
                          $res = $res->data;
                          if(count($res) > 0){
                            // print_r($res);

                            $total_ = 0;
                            foreach ($res as $priest) {
                              # code...
                              /*
                              `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                              */
                              $json_priest = json_decode($priest);
                              
                              echo '
                                <tr>
                                  
                                  <td>'.$json_priest->name.'</td>
                                  <td>'.$json_priest->phone.'</td>
                                  <td>
                                  <a href="dashboard.php?page=calendar&uid='.$json_priest->user_id.'&axn=usercal"  class="btn btn-warning" type="button"><i class="fa fa-calendar"></i> Calendar View</a>
                                  <a href="dashboard.php?page=schedlist&uid='.$json_priest->user_id.'&axn=userlist"  class="btn btn-success" type="button"><i class="fa fa-list"></i> List View</a>
                                  </td>
                                </tr>
                              ';
                            }                  
                          }else{
                            //<td></td>
                              echo '
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              ';
                          }
                        ?>
                      </tbody>
                    </table>

                    <script>
                    $(document).ready(function(){
                      $('#btnCollectorSched').click(function(){
                          $('#mycollectorsched').modal('show');
                      });

                      $('#btnSaveCollector').click(function(){
                          var cname = $('#col_name2').val();
                          var ddate1 = $('#datetimepicker2').val();
                          var dtime1 = $('#time2').val();
                          var hidid1 = $('#hidid2').val();
                          var ev2 = $('select[name=revent2]').val();
                          $.ajax({
                            type: 'post',
                            url: 'api/api.php',
                            data: {
                              action: 'saveSchedule',
                              user_id : cname,
                              ddate: ddate1,
                              dtime: dtime1,
                              hid_id: hidid1,
                              titl : ev2,
                              ul : '4'
                            },
                            beforeSend: function(xhr){

                            },
                            success: function(xhr){
                                console.log(xhr);
                                if(xhr == 'success'){
                                  alert('Schedule is now set!');
                                  location.reload();
                                  // window.location = 'dashboard.php?page=sched_page#collector';
                                }else{
                                  alert(xhr);
                                }
                            } 
                          });        
                          
                      });
                      // user_id
                      // ddate
                      // dtime
                      // hid_id

                    });
                    </script>
                    <style>
                    .picker_4{z-index:1151 !important;}
                    </style>

                  <!-- Modal -->
                  <div class="modal fade" id="mycollectorsched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Reader Schedule</h4>
                        </div>
                        <div class="modal-body">
                            <form>
                               <div class="form-group">
                                    <label class="control-label">Collector's Name</label>
                                    <select id='col_name2' class='form-control'>
                                        <?php
                                            $res1 = $conn->dbquery("SELECT * FROM `users` WHERE `ulevel` = '4'" );
                                            $res1 = json_decode($res1);
                                            $res1 = $res1->data;
                                            echo '<option value="0">-Select-</option>';
                                            foreach ($res1 as $usersdata) {
                                                $json_usersdata = json_decode($usersdata);
                                                if($res->assigned_to == $json_usersdata->user_id){
                                                  $ssel = 'selected';
                                                }else{
                                                  $ssel = '';
                                                }
                                                echo '
                                                  <option value="'.$json_usersdata->user_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Event</label>
                                    <select name="revent2" class="form-control" >
                                      <option value="mass">Mass</option>
                                        <option value="wedding">Wedding Mass</option>
                                        <option value="burial">Burial</option>
                                        <option value="baptism">Baptism</option>
                                        <option value="blessing">Blessing</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Date</label>
                                    <div class="form-group">
                                        <div class='input-group date-picker' >
                                            <input type='text' class="form-control" id='datetimepicker2' value="<?php echo $start_date; ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Time</label>
                                    <select id="time2" class="form-control" >
                                        <?php
                                            $start = 7;
                                            for($i = 0; $i < 12; $i++){
                                                $seltime = ($start + $i).':00:00';
                                                // ltrim($start_time, "0");
                                                if($start_time == $seltime){
                                                  $sel = "selected";
                                                }else{
                                                  $sel = "";
                                                }
                                                echo '
                                                    <option value="'.$seltime.'" '.$sel.' >'.$seltime.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                    <input type="hidden" id="hidid2" value="">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" id="btnSaveCollector" style="margin-top: -5px;">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>

            </div>
          </div>

      </div>

      <div id="ministry" class="tab-pane fade">
        <h3>Lay Ministry Schedule</h3>
        <!-- <p>Some content in menu 1.</p> -->
        <p>
          <!-- <a href="#" class="btn btn-success" id="btnMinistrySched"><i class="fa fa-plus"></i> Add Schedule</a><br/> -->
          <small></small>
        </p>
          <div class="row">
            <div class="col-md-12">
                    <table id="datatable4" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <!-- <th>ID</th> -->
                          <th>Name</th>
                          <th>Phone</th>
                          <th>Schedule</th>
                        </tr>
                      </thead>
                      <tbody id="tblAssets4">
                        <?php
                          include_once('config.php');
                          include_once('lib/funcjax.php');
                          $startdate = date("Y-m-d")." "."00:00:00";
                          $enddate = date("Y-m-d")." "."23:59:59";
                          $res = $conn->dbquery("SELECT * FROM `users` where `ulevel` = '5'" );


                          $res = json_decode($res);
                          $res = $res->data;
                          if(count($res) > 0){
                            // print_r($res);

                            $total_ = 0;
                            foreach ($res as $priest) {
                              # code...
                              /*
                              `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                              */
                              $json_priest = json_decode($priest);
                              
                              echo '
                                <tr>
                                  
                                  <td>'.$json_priest->name.'</td>
                                  <td>'.$json_priest->phone.'</td>
                                  <td>
                                  <a href="dashboard.php?page=calendar&uid='.$json_priest->user_id.'&axn=usercal"  class="btn btn-warning" type="button"><i class="fa fa-calendar"></i> Calendar</a>
                                  <a href="dashboard.php?page=schedlist&uid='.$json_priest->user_id.'&axn=userlist"  class="btn btn-success" type="button"><i class="fa fa-list"></i> List View</a>
                                  </td>
                                </tr>
                              ';
                            }                  
                          }else{
                            //<td></td>
                              echo '
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              ';
                          }
                        ?>
                      </tbody>
                    </table>
                    <script>
                    $(document).ready(function(){
                      $('#btnMinistrySched').click(function(){
                          $('#myministrysched').modal('show');
                      });

                      $('#btnSaveMinistry').click(function(){
                          var cname = $('#col_name3').val();
                          var ddate1 = $('#datetimepicker3').val();
                          var dtime1 = $('#time3').val();
                          var hidid1 = $('#hidid3').val();
                          var ev3 = $('select[name=revent3]').val();
                          $.ajax({
                            type: 'post',
                            url: 'api/api.php',
                            data: {
                              action: 'saveSchedule',
                              user_id : cname,
                              ddate: ddate1,
                              dtime: dtime1,
                              hid_id: hidid1,
                              titl: ev3,
                              ul : '5'
                            },
                            beforeSend: function(xhr){

                            },
                            success: function(xhr){
                                console.log(xhr);
                                if(xhr == 'success'){
                                  alert('Schedule is now set!');
                                  location.reload();
                                  // window.location = 'dashboard.php?page=sched_page#ministry';
                                }else{
                                  alert(xhr);
                                }
                            } 
                          });        
                          
                      });
                      // user_id
                      // ddate
                      // dtime
                      // hid_id

                    });
                    </script>
                    <style>
                    .picker_4{z-index:1151 !important;}
                    </style>

                  <!-- Modal -->
                  <div class="modal fade" id="myministrysched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Reader Schedule</h4>
                        </div>
                        <div class="modal-body">
                            <form>
                               <div class="form-group">
                                    <label class="control-label">Collector's Name</label>
                                    <select id='col_name3' class='form-control'>
                                        <?php
                                            $res1 = $conn->dbquery("SELECT * FROM `users` WHERE `ulevel` = '5'" );
                                            $res1 = json_decode($res1);
                                            $res1 = $res1->data;
                                            echo '<option value="0">-Select-</option>';
                                            foreach ($res1 as $usersdata) {
                                                $json_usersdata = json_decode($usersdata);
                                                if($res->assigned_to == $json_usersdata->user_id){
                                                  $ssel = 'selected';
                                                }else{
                                                  $ssel = '';
                                                }
                                                echo '
                                                  <option value="'.$json_usersdata->user_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Event</label>
                                    <select name="revent3" class="form-control" >
                                      <option value="mass">Mass</option>
                                        <option value="wedding">Wedding Mass</option>
                                        <option value="burial">Burial</option>
                                        <option value="baptism">Baptism</option>
                                        <option value="blessing">Blessing</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Date</label>
                                    <div class="form-group">
                                        <div class='input-group date-picker' >
                                            <input type='text' class="form-control" id='datetimepicker3' value="<?php echo $start_date; ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Time</label>
                                    <select id="time3" class="form-control" >
                                        <?php
                                            $start = 7;
                                            for($i = 0; $i < 12; $i++){
                                                $seltime = ($start + $i).':00:00';
                                                // ltrim($start_time, "0");
                                                if($start_time == $seltime){
                                                  $sel = "selected";
                                                }else{
                                                  $sel = "";
                                                }
                                                echo '
                                                    <option value="'.$seltime.'" '.$sel.' >'.$seltime.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                    <input type="hidden" id="hidid3" value="">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" id="btnSaveMinistry" style="margin-top: -5px;">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>

            </div>
          </div>
      </div>

      <div id="acolytes" class="tab-pane fade">
        <h3>Acolytes Schedule</h3>
        <p>
          <!-- <a href="#" class="btn btn-success" id="btnAcolytesSched"><i class="fa fa-plus"></i> Add Schedule</a><br/> -->
          <small></small>
        </p>
          <div class="row">
            <div class="col-md-12">
                    <table id="datatable5" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <!-- <th>ID</th> -->
                          <th>Name</th>
                          <th>Phone</th>
                          <th>Schedule</th>
                        </tr>
                      </thead>
                      <tbody id="tblAssets5">
                        <?php
                          include_once('config.php');
                          include_once('lib/funcjax.php');
                          $startdate = date("Y-m-d")." "."00:00:00";
                          $enddate = date("Y-m-d")." "."23:59:59";
                          $res = $conn->dbquery("SELECT * FROM `users` where `ulevel` = '6'" );


                          $res = json_decode($res);
                          $res = $res->data;
                          if(count($res) > 0){
                            // print_r($res);

                            $total_ = 0;
                            foreach ($res as $priest) {
                              # code...
                              /*
                              `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                              */
                              $json_priest = json_decode($priest);
                              
                              echo '
                                <tr>
                                  
                                  <td>'.$json_priest->name.'</td>
                                  <td>'.$json_priest->phone.'</td>
                                  <td>
                                  <a href="dashboard.php?page=calendar&uid='.$json_priest->user_id.'&axn=usercal"  class="btn btn-warning" type="button"><i class="fa fa-calendar"></i> Calendar View</a>
                                  <a href="dashboard.php?page=schedlist&uid='.$json_priest->user_id.'&axn=userlist"  class="btn btn-success" type="button"><i class="fa fa-list"></i> List View</a>
                                  </td>
                                </tr>
                              ';
                            }                  
                          }else{
                            //<td></td>
                              echo '
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              ';
                          }
                        ?>
                      </tbody>
                    </table>
                    <script>
                    $(document).ready(function(){
                      $('#btnAcolytesSched').click(function(){
                          $('#myacolytessched').modal('show');
                      });

                      $('#btnSaveAcolytes').click(function(){
                          var cname = $('#col_name4').val();
                          var ddate1 = $('#datetimepicker4').val();
                          var dtime1 = $('#time4').val();
                          var hidid1 = $('#hidid4').val();
                          var ev4 = $('select[name=revent4]').val();
                          $.ajax({
                            type: 'post',
                            url: 'api/api.php',
                            data: {
                              action: 'saveSchedule',
                              user_id : cname,
                              ddate: ddate1,
                              dtime: dtime1,
                              hid_id: hidid1,
                              titl : ev4,
                              ul : '6'
                            },
                            beforeSend: function(xhr){

                            },
                            success: function(xhr){
                                console.log(xhr);
                                if(xhr == 'success'){
                                  alert('Schedule is now set!');
                                  location.reload();
                                  // window.location = 'dashboard.php?page=sched_page#acolytes';
                                }else{
                                  alert(xhr);
                                }
                            } 
                          });        
                          
                      });
                      // user_id
                      // ddate
                      // dtime
                      // hid_id

                    });
                    </script>
                    <style>
                    .picker_4{z-index:1151 !important;}
                    </style>

                  <!-- Modal -->
                  <div class="modal fade" id="myacolytessched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Reader Schedule</h4>
                        </div>
                        <div class="modal-body">
                            <form>
                               <div class="form-group">
                                    <label class="control-label">Collector's Name</label>
                                    <select id='col_name4' class='form-control'>
                                        <?php
                                            $res1 = $conn->dbquery("SELECT * FROM `users` WHERE `ulevel` = '6'" );
                                            $res1 = json_decode($res1);
                                            $res1 = $res1->data;
                                            echo '<option value="0">-Select-</option>';
                                            foreach ($res1 as $usersdata) {
                                                $json_usersdata = json_decode($usersdata);
                                                if($res->assigned_to == $json_usersdata->user_id){
                                                  $ssel = 'selected';
                                                }else{
                                                  $ssel = '';
                                                }
                                                echo '
                                                  <option value="'.$json_usersdata->user_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Event</label>
                                    <select name="revent4" class="form-control" >
                                      <option value="mass">Mass</option>
                                        <option value="wedding">Wedding Mass</option>
                                        <option value="burial">Burial</option>
                                        <option value="baptism">Baptism</option>
                                        <option value="blessing">Blessing</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Date</label>
                                    <div class="form-group">
                                        <div class='input-group date-picker' >
                                            <input type='text' class="form-control" id='datetimepicker4' value="<?php echo $start_date; ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Time</label>
                                    <select id="time4" class="form-control" >
                                        <?php
                                            $start = 7;
                                            for($i = 0; $i < 12; $i++){
                                                $seltime = ($start + $i).':00:00';
                                                // ltrim($start_time, "0");
                                                if($start_time == $seltime){
                                                  $sel = "selected";
                                                }else{
                                                  $sel = "";
                                                }
                                                echo '
                                                    <option value="'.$seltime.'" '.$sel.' >'.$seltime.'</option>
                                                ';
                                            }
                                        ?>
                                    </select>
                                    <input type="hidden" id="hidid4" value="">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" id="btnSaveAcolytes" style="margin-top: -5px;">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>



            </div>
          </div>

      </div>      

    </div>


  </div>
</div>
