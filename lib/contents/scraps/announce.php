<div class="row">

	<div class="col-md-12">
          <table id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Announcements</th>
                <td>Create Date</td>
                <th>Created By</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="tblAssets">
              <?php
                include_once('config.php');
                include_once('lib/funcjax.php');
                $startdate = date("Y-m-d")." "."00:00:00";
                $enddate = date("Y-m-d")." "."23:59:59";
                $res = $conn->dbquery("SELECT * FROM `announcements` WHERE `created_date` between '".$startdate."' and '".$enddate."' " );
                $res = json_decode($res);
                $res = $res->data;
                if(count($res) > 0){
                  // print_r($res);
                  foreach ($res as $gospel) {
                    # code...
                    $json_gospel = json_decode($gospel);
                    echo '
                      <tr>
                        <td>'.$json_gospel->a_id.'</td>
                        <td>'.$json_gospel->announcements.'</td>
                        <td>'.$json_gospel->created_date.'</td>
                        <td>Admin</td>
                        <td>
                          <button onclick="editGospel(\''.$json_gospel->a_id.'\')" class="btn btn-success"><i class="fa fa-pencil"></i> Edit</button>
                          <button onclick="deleteGospel(\''.$json_gospel->a_id.'\')" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                        </td>
                      </tr>
                    ';

                  }                  
                }else{
                    echo '
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    ';
                }

              ?>

            </tbody>
          </table>

	</div>


</div>

<script>
function deleteGospel(gid){
  var q = confirm('Are you sure you want to delete?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'deleteAnnounce',
          a_id : gid
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Announcement successfully deleted!');
              location.reload();
            }
        } 
      });    
  }
}

function editGospel(gid){
  window.location = 'dashboard.php?page=announce_add&a_id=' + gid;
}
</script>