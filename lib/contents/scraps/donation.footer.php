  <script src="includes/js/bootstrap.min.js"></script>

  <script src="includes/js/nprogress.js"></script>
  
  <!-- bootstrap progress js -->
  <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="includes/js/icheck/icheck.min.js"></script>

  <script src="includes/js/custom.js"></script>

  <script src="includes/js/moment/moment.min.js"></script>
  <script src="includes/js/calendar/fullcalendar.min.js"></script>

  <!-- Datatables -->
  <!-- <script src="includes/js/datatables/js/jquery.dataTables.js"></script>
<script src="includes/js/datatables/tools/js/dataTables.tableTools.js"></script> -->

  <!-- Datatables-->
  <script src="includes/js/datatables/jquery.dataTables.min.js"></script>
  <script src="includes/js/datatables/dataTables.bootstrap.js"></script>
  <script src="includes/js/datatables/dataTables.buttons.min.js"></script>
  <script src="includes/js/datatables/buttons.bootstrap.min.js"></script>
  <script src="includes/js/datatables/jszip.min.js"></script>
  <script src="includes/js/datatables/pdfmake.min.js"></script>
  <script src="includes/js/datatables/vfs_fonts.js"></script>
  <script src="includes/js/datatables/buttons.html5.min.js"></script>
  <script src="includes/js/datatables/buttons.print.min.js"></script>
  <script src="includes/js/datatables/dataTables.fixedHeader.min.js"></script>
  <script src="includes/js/datatables/dataTables.keyTable.min.js"></script>
  <script src="includes/js/datatables/dataTables.responsive.min.js"></script>
  <script src="includes/js/datatables/responsive.bootstrap.min.js"></script>
  <script src="includes/js/datatables/dataTables.scroller.min.js"></script>

  <!-- pace -->
  <script src="includes/js/pace/pace.min.js"></script>
  <script>
    $(window).load(function() {
    });
    
    $(document).ready(function(){
      $('#datatable_donate').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
       "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 9 ).footer() ).html(
                'Php '+pageTotal +' ( Php '+ total +' total)'
            );
        }
      });
    });
  </script>
  <!-- /footer content -->