<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <?php
          include_once('config.php');
          include_once('lib/funcjax.php');
          $user_id = secure_get('uid');
          $axn = secure_get('axn');
          if(empty($user_id) && empty($axn)){
        ?>
        <h2>Calender Events <small>Sessions</small></h2>
        <?php } else { 
            echo "<h2>Schedule of <strong>".translateName($user_id, $conn)."</strong><small></small></h2>";
        }?>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div id='calendar'></div>

      </div>
    </div>
  </div>
</div>