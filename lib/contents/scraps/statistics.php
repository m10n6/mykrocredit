<?php
include_once('config.php');
include_once('lib/funcjax.php');

// $start = $month = strtotime('2009-02-01');
// $end = strtotime('2011-01-01');
// while($month < $end)
// {
//      echo date('F Y', $month), PHP_EOL;
//      $month = strtotime("+1 month", $month);
// }
$axn = secure_get('axn');
?>

        <!-- top tiles -->
        <div class="row tile_count">
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total Donations</span>
              <div class="count"><?php echo getDonations($conn); ?></div>
              <!-- <span class="count_bottom"><i class="green">4% </i> From last Week</span> -->
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total InValid Donations</span>
              <div class="count"><?php echo  getInvalidDonations($conn); ?></div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total Bookings</span>
              <div class="count"><?php echo  getBookings($conn); ?></div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span> -->
            </div>
          </div>

          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total New Bookings</span>
              <div class="count"><?php echo  getNewBookins($conn); ?></div>
              <!-- <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span> -->
            </div>
          </div>

          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total Paid Bookings</span>
              <div class="count"><?php echo  number_format(getPaidBookins($conn), 2); ?> Php</div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
            </div>
          </div>

          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total Collections</span>
              <div class="count"><?php echo  number_format(getCollections($conn), 2); ?> Php</div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
            </div>
          </div>

          <style>
          .count {
            font-size: 20px !important;

          }
          </style>
        </div>
        <!-- /top tiles -->
<!-- <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Collections <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>

            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div style="width: 100%;">
            <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height:270px;"></div>
          </div>
        </div>
      </div>
    </div>

</div> -->

<!-- <div class="row">

    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Donations <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>

            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <canvas id="mybarChart"></canvas>
        </div>
      </div>
    </div>

</div> -->

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><?php echo strtoupper($axn); ?> <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>

            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <canvas id="mymarkBar"></canvas>
        </div>
      </div>
    </div>

   <div class="col-md-12 col-sm-12 col-xs-12">
            <table id="datatable1" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Date</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody id="tblAssets2">
              <?php
                include_once('config.php');
                include_once('lib/funcjax.php');
                $startdate = date("Y-m-d")." "."00:00:00";
                $enddate = date("Y-m-d")." "."23:59:59";
                // $res = $conn->dbquery("SELECT * FROM `reservation` where `status` = 'approved' and `event` = '".$axn."' order by `res_id` desc");
                if($axn == 'wedding' || $axn == 'burial' || $axn == 'baptism' || $axn == 'blessing' || $axn == 'mass'  || $axn == 'others'){
                  $sql = "SELECT * FROM `reservation` where `status` = 'approved' and `event` = '".$axn."' order by `res_id` desc" ;
                }
                if($axn == 'collection'){
                  $sql = "SELECT * FROM `collections`" ;
                }
                if($axn == 'payment'){
                  $sql = "SELECT * FROM `reservation` where `status` = 'approved' order by `res_id` desc";
                }

                $res = $conn->dbquery($sql);
                $res = json_decode($res);
                $res = $res->data;
                if(count($res) > 0){
                  // print_r($res);

                  $total_ = 0;
                  foreach ($res as $data) {
                    # code...
                    /*
                    `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                    */
                    $json_data = json_decode($data);
                    
                    if($axn == 'wedding' || $axn == 'burial' || $axn == 'baptism' || $axn == 'blessing' || $axn == 'mass'  || $axn == 'others' || $axn == 'payment'){
                      echo '
                        <tr>
                          <td>'.$json_data->res_id.'</td>
                          <td>'.$json_data->name.'</td>
                          <td>'.$json_data->email.'</td>
                          <td>'.$json_data->start_date.'</td>
                          <td>'.$json_data->amount.'</td>
                        </tr>
                      ';
                    }else{
                      echo '
                        <tr>
                          <td>'.$json_data->c_id.'</td>
                          <td>'.translateName($json_data->user_id, $conn).'</td>
                          <td>'.getUser_Email($json_data->user_id, $conn).'</td>
                          <td>'.$json_data->date_added.'</td>
                          <td>'.$json_data->amount.'</td>
                        </tr>
                      ';
                    }
                  }                  
                }else{
                  //<td></td>
                    echo '
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    ';
                }
              ?>
            </tbody>
            <tfoot>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td align="right"><strong>Total :</strong></td>
                  <td></td>
                </tr>
            </tfoot>
          </table>
<!-- END DONATION REPORT -->
   </div>

</div>

