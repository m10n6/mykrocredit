
<?php
$client_id = secure_get('cid');

$interest_psched = getClientSchedPayment($client_id);

switch($interest_psched){
	case '1':
		$sched = 15;
	break;

	case '2':
		$sched = 30;
	break;
}

$int_sched_start = getClientSchedPaymentStart($client_id)." 00:00:00";
?>
 <div class="row">
 	<div class="col-md-12">
 		<a class="btn btn-primary" href="dashboard.php?page=clients_list" ><i class="fa fa-arrow-left"></i> BACK </a>
 		<a class="btn btn-success" href="dashboard.php?page=client_payment&cid=<?php echo $client_id; ?>"><i class="fa fa-money"></i> Payments </a>
 		<!-- <a class="btn btn-warning pull-right" ><i class="fa fa-print"></i> Print </a> -->
 		<!-- <button class="btn btn-primary" type="button" id="btnAddNew"><i class="fa fa-plus"></i> Add New Account </button> -->

 		<hr/>
 		<strong>Date Start of Interest: </strong><?php echo date("m/d/Y", strtotime($int_sched_start)); ?><br/>
		<strong>Interest Applied every : </strong><?php echo $sched.'th of the month'; ?>

 		<table class="table table-striped" id="tblUserLogs">
 			<thead>
 				<tr>
 					<th>Date</th>
 					<th>Voucher #</th>
 					<th>Loan Amount</th>
 					<th>Beg. Balance</th>
 					<th>OR#/ORS#</th>
 					<th>Payment Amount</th>
 					<th>Int. Rate</th>
 					<th>Interest</th>
 					<th>Net Amount</th>
 					<th>Balance</th>
 				</tr>
 			</thead>
 			<!-- <tfoot>
 				<tr>
 					<td>Account Code</td>
 					<td>Account Name</td>
 					<td>Category</td>
 					<td>Action</td>
 				</tr>
 			</tfoot> -->
 			<tbody>
 				<!-- <tr>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 				</tr> -->
  				<?php

					//txtBeginningBal
					$is_beg = false;
					$sqlBB = "select * from `client_meta` where `meta` = 'txtBeginningBal' and `client_id` = '".$client_id."' ";
					$rsBB = $conn->dbquery($sqlBB);
					// $rs1 = json_decode($rs1);
					// echo $rsBB;
					if($rsBB !== 'false'){
						
						$rsBB = json_decode($rsBB);
						$rsBB = json_decode($rsBB->data[0]);
						$beg_bal = $rsBB->value;	
						if(!empty($beg_bal)){
							$is_beg = true;
						}
					}
					// echo $beg_bal;

  					$is_interest_applied = false;
  					$is_interest_ctr = 0;
  					$ctr = 0;

  					$fixedRate_interest = 0;
  					$is_FR_set = false;

 					$sql = "select * from `finance` where `client_id` = '".$client_id."' and `status` = 'approved' order by `date_approved` asc";
 					$res = $conn->dbquery($sql);
 					// print_r($res);
 					
 					

 					if($res !== 'false'){
						$res = json_decode($res);

						$int_ctr = 0; //counter for applied interest

						foreach ($res->data as $key) {
							# code...
							// echo $ctr;
							$nres = json_decode($key);
							$extra = json_decode($nres->extra);
							if($extra->TypeRate == 'diminishing' || $extra->TypeRate == '-'){
								$type_rate = 'DR';
							}else{
								$type_rate = 'FR';
							}

							if($type_rate == 'DR'){
								//DEMINISHING RATE

								$intrate = $nres->intrate;
								$intrate_val = $intrate / 100;

								$voucher_num = $nres->voucher_id;
								$or_num = '';
								$payment = number_format(0,2);


								$interest = $loan_amnt_temp * $intrate_val;

								// $loan_amnt = number_format($nres->amount, 2);
								$loan_amnt = $nres->amount;

								// echo $nres->amount;
								if($ctr == 0){
									$loan_start = $loan_amnt;
									if(!empty($beg_bal)){	
									// if($is_beg == true){
										$loan_start = $beg_bal;		
										$loan_amnt = $beg_bal;
									}
								}

								if($nres->loan_type == 'new_loans'){
									$balance = $loan_amnt;
									$int_amount = 0;
									$net_amount = 0;
									// $minus_ctr = 2;
								// }else{
								}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
									$int_amount = 0;
									$net_amount = 0;

									$balance = $balance + $loan_amnt;
									// $minus_ctr = 1;
								}


								

								// else{
								// 	$int_amount = 0;
								// 	$net_amount = 0;
								// 	$balance = $loan_amnt;
								// }
								// echo $loan_amnt; //exit();
								//cash_advance
								// special_account
								// additional_loan
								// renewal
								// excess
								// if($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
								// 	$int_amount = 0;
								// 	$net_amount = 0;
								// 	$balance = $balance + $loan_amnt;
								// }else{
								// 	$balance = $loan_amnt;
								// 	$int_amount = 0;
								// 	$net_amount = 0;
								// }
								// echo "[".$bal_array[$ctr-2]."]";

								if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors' && $nres->loan_type != 'jv'){
									$loan_amnt = number_format(0,2);
									if($ctr == 0){
										$loan_start = $loan_amnt;
										if(!empty($beg_bal)){
										// if($is_beg == true){
											$loan_start = $beg_bal;		
											$loan_amnt = $beg_bal;
											$balance = $loan_amnt;
										}
									}

									$voucher_num = '-';
									$or_num = $nres->voucher_id;
									$payment = $nres->amount;
									//date paid and start deductions
									$start_interest = getClientSchedPaymentStart($client_id);

									$d_approved = strtotime($nres->date_approved);
									$int_date_start = strtotime($start_interest);

									$p_day = explode(" ", $nres->date_approved);
									$p_day = explode("-", $p_day[0]);
									$p_day = $p_day[2];
									// echo 'd = '.$d_approved;
									// echo 'i = '.$int_date_start; 

									if($d_approved == $int_date_start){
										// $balance = $loan_start;
										// $int_amount = ($balance * ($nres->intrate/100));
										// $int_amount = ($loan_start * ($nres->intrate/100)); //original
										// $int_amount = ($bal_array[0] * ($nres->intrate/100));
										$int_amount = (end($last_month_balance) * ($nres->intrate/100));
										$net_amount = $payment - $int_amount;	

										// $loan_start = $				
										$balance = $balance - $net_amount;
										// $loan_start = $balance;
										$int_ctr++;
									}else{

										if($p_day >= 1 && $p_day <= 15){
											$sched_int = 1;
										}elseif($p_day >= 15 && $p_day <= 31){
											$sched_int = 2;
										}


										if($interest_psched == $sched_int && $d_approved > $int_date_start){
											// $int_amount = ($loan_start * ($nres->intrate/100)); //original
											// if($int_ctr == 0){
											// 	$int_amount = ($loan_start * ($nres->intrate/100)); //original
											// }else{
												// $int_amount = ($bal_array[$ctr-$minus_ctr] * ($nres->intrate/100));	
												// $int_amount = ($bal_array[$ctr-2] * ($nres->intrate/100));	
												$int_amount = (end($last_month_balance) * ($nres->intrate/100));	
											// }
											$net_amount = $payment - $int_amount;

											$balance = $balance - $net_amount;
											$loan_start = $balance;
											// $loan_amnt = $balance;
											$int_ctr++;
										}else{
											$int_amount = 0;
											// if($ctr == 0){
											// 	// $loan_start = $loan_amnt;
											// 	if($is_beg == true){
											// 		// $loan_start = $beg_bal;		
											// 		// $loan_amnt = $loan_start;
											// 		$balance = $beg_bal;
											// 	}
											// }
											$net_amount = $payment;			
											$balance = $balance - $net_amount;	

											// $loan_amnt = $balance;
											$loan_start = $balance;
										}

									}

									// if($extra->ApplyInt == 'on'){
									// 	$int_amount = ($balance * ($nres->intrate/100));
									// 	$net_amount = $payment - $int_amount;					
									// }

									// if($extra->ApplyInt == 'off' || $extra->ApplyInt == ''){
									// 	$int_amount = 0;
									// 	$net_amount = $payment;
									// }
									// $loan_amnt = $balance;

								}
								$bal_array[$ctr] = $balance; //added 01-16-2018
								// echo $bal_array[$ctr].'='.$ctr;
								$ctr++;
							}else{
								//FIXED RATE

								$intrate = $nres->intrate;
								$intrate_val = $intrate / 100;

								$voucher_num = $nres->voucher_id;
								$or_num = '';
								$payment = number_format(0,2);

								$loan_amnt = $nres->amount;
								if($is_FR_set == false){
									$int_amount = ($loan_amnt * ($nres->intrate/100));
									$loan_start = $loan_amnt;
									$is_FR_set = true;
								}

								if($nres->loan_type == 'new_loans'){
									$balance = $loan_amnt;
									$int_amount = 0;
									$net_amount = 0;
								// }else{
								}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors' && $nres->loan_type != 'jv'){
									$int_amount = 0;
									$net_amount = 0;
									$balance = $balance + $loan_amnt;
									// $net_amount = $payment - $int_amount;	
								}

								if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors'){
									$loan_amnt = number_format(0,2);
									$voucher_num = '-';
									$or_num = $nres->voucher_id;
									$payment = $nres->amount;
									//date paid and start deductions
									$start_interest = getClientSchedPaymentStart($client_id);

									$d_approved = strtotime($nres->date_approved);
									$int_date_start = strtotime($start_interest);

									$p_day = explode(" ", $nres->date_approved);
									$p_day = explode("-", $p_day[0]);
									$p_day = $p_day[2];


									if($d_approved == $int_date_start){
										// $balance = $loan_start;
										// $int_amount = ($balance * ($nres->intrate/100));
										// $int_amount = ($loan_start * ($nres->intrate/100));
										$int_amount = ($bal_array[0] * ($nres->intrate/100));
										$net_amount = $payment - $int_amount;	

										// $loan_start = $										
										$balance = $balance - $net_amount;
										$loan_start = $balance;
										// $loan_amnt = $balance;
									}else{

										if($p_day >= 1 && $p_day <= 15){
											$sched_int = 1;
										}elseif($p_day >= 15 && $p_day <= 31){
											$sched_int = 2;
										}

										if($interest_psched == $sched_int && $d_approved > $int_date_start){
											// $int_amount = ($loan_start * ($nres->intrate/100));
											$int_amount = ($bal_array[0] * ($nres->intrate/100));
											
											$net_amount = $payment - $int_amount;											

											$balance = $balance - $net_amount;
											$loan_start = $balance;
											// $loan_amnt = $balance;
										}else{
											$int_amount = 0;
											$net_amount = $payment;			
											$balance = $balance - $net_amount;		
											$loan_start = $balance;	
											// $loan_amnt = $balance;													
										}

										
									}
								}							
								$bal_array[$ctr] = $balance; //added 01-16-2018
								// echo $bal_array[$ctr].'='.$ctr;
								$ctr++;

							}


							$loan_amount_disp = $loan_amnt;

							//for JV
							
							if(empty($extra->TypeRate) && $nres->loan_type == 'jv'){
								if(!empty($extra->JVTypeID)){
									$chName = getChartAccountName($extra->JVTypeID);
								}

								if(stripos($chName, "A/R") !== false){

									if($nres->debit > 0){
										$balance = $balance + $loan_amnt;	
									}
									if($nres->credit > 0){
										$balance = $balance - $loan_amnt;	
									}


									$int_amount = 0;
									$net_amount = 0;
									echo '
						 				<tr>
						 					<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
						 					<td> JV #'.$voucher_num.'</td>
						 					<td>'.number_format($loan_amount_disp, 2).'</td>
						 					<td>-</td>
						 					<td>'.$or_num.'</td>
						 					<td>'.number_format($payment, 2).'</td>
						 					<td> - </td>
						 					<td>'.number_format($int_amount,2).'</td>
						 					<td>'.number_format($net_amount,2).'</td>
						 					<td>'.number_format($balance, 2).'</td>
						 				</tr>
									';	
									$is_start_beg = false; // for display loan amount flag								
								}
							}else{

								// echo $loan_type;
								if($nres->loan_type == 'beg_bal'){
									echo '
						 				<tr>
						 					<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
						 					<td>'.$voucher_num.'</td>
						 					<td>-</td>
						 					<td>'.number_format($loan_amount_disp, 2).'</td>
						 					<td>'.$or_num.'</td>
						 					<td>'.number_format($payment, 2).'</td>
						 					<td>'.$intrate.'% '.$type_rate.'</td>
						 					<td>'.number_format($int_amount,2).'</td>
						 					<td>'.number_format($net_amount,2).'</td>
						 					<td>'.number_format($balance, 2).'</td>
						 				</tr>
									';	
									$is_start_beg = true; // for display loan amount flag
								}else{
									// check display loan amount flag
									if($is_start_beg){
										$loan_amount_disp = 0;
										$is_start_beg = false;
									}
									echo '
						 				<tr>
						 					<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
						 					<td>'.$voucher_num.'</td>
						 					<td>'.number_format($loan_amount_disp, 2).'</td>
						 					<td>-</td>
						 					<td>'.$or_num.'</td>
						 					<td>'.number_format($payment, 2).'</td>
						 					<td>'.$intrate.'% '.$type_rate.'</td>
						 					<td>'.number_format($int_amount,2).'</td>
						 					<td>'.number_format($net_amount,2).'</td>
						 					<td>'.number_format($balance, 2).'</td>
						 				</tr>
									';	
								}

							}

							//get end of the month balance
							$temp_date_approved = explode(" ", $nres->date_approved);
							$temp_date_approved = explode("-", $temp_date_approved[0]);
							$temp_day_approved = $temp_date_approved[2];
							$temp_month_approved = $temp_date_approved[1];

							if(($temp_month_approved == '02' && ($temp_day_approved >= 28 && $temp_day_approved < 30)) 
								|| ($temp_day_approved >= 30 && $temp_day_approved <= 31)
								):
								// echo $balance;
								// echo $temp_day_approved;
								$last_month_balance[] = $balance;
							endif;

							$loan_amnt = $balance;

							//<td>'.number_format($balance, 2).'</td>
						}
 					}else{
 						echo '
			 				<tr>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 				</tr>
 						';	
 					}

 				?>
 			</tbody>
 		</table>
 	
 	</div>

 </div>
