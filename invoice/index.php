<?php
include_once('../config.php');
include_once('../lib/funcjax.php');

$resid = secure_get('resid');

$sql = "select * from `reservation`
  where
  `res_id` = '".$resid."'
";

$res = $conn->dbquery($sql);
$res = json_decode($res);
$res = $res->data;
if(count($res) > 0){
  // print_r($res);

  $total_ = 0;
  foreach ($res as $reservation) {
    # code...
    /*
    `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
    */
    $json_reservation = json_decode($reservation);
    //<td>'.$json_reservation->res_id.'</td>
    if(empty($json_reservation->amount) || $json_reservation->amount == ''){
      $json_reservation->amount = 0;
    }
    
    $button = '';

    $name = $json_reservation->name;
    $phone = $json_reservation->phone;
    $email = $json_reservation->email;
    $amount = $json_reservation->amount;
    $event = $json_reservation->event;
    $venue = $json_reservation->venue;
    $start_date = $json_reservation->start_date;

    // if($json_reservation->payment_type != 'full_payment' && $json_reservation->status == 'approved'){
    //   $button = '<button type="button" onclick="fullpaySched(\''.$json_reservation->res_id.'\')" class="btn btn-success"><i class="fa fa-money"></i> Full Payment</button>';
    // }
    // echo '
    //   <tr>
    //     <td>'.$json_reservation->res_id.'</td>
    //     <td>'.$json_reservation->name.'</td>
    //     <td>'.$json_reservation->phone.'</td>
    //     <td>'.$json_reservation->email.'</td>
    //     <td><strong>Event: </strong>'.$json_reservation->event.'<br/><strong>Venue: </strong>'.translateVenue($json_reservation->venue).'</td>
    //     <td>'.$json_reservation->details.'</td>
    //     <td>'.$json_reservation->start_date.'</td>
        
    //     <td>'.translateName($json_reservation->insert_by, $conn).'</td>
    //     <td>'.translateName($json_reservation->assigned_to, $conn).'</td>
    //     <td>'.$json_reservation->status.'</td>
    //     <td>'.$json_reservation->amount.'</td>
    //     <td>
    //       <button onclick="editSched(\''.$json_reservation->res_id.'\')" class="btn btn-primary"><i class="fa fa-pencil"></i> Update</button>
    //       '.$button.'
    //     </td>
    //   </tr>
    // ';
    /*
          <button onclick="pendingSched(\''.$json_reservation->res_id.'\')" class="btn btn-warning"><i class="fa fa-pause"></i> Pending</button>
          <button onclick="approveSched(\''.$json_reservation->res_id.'\')" class="btn btn-success"><i class="fa fa-check"></i> Approved</button>
          <button onclick="deleteSched(\''.$json_reservation->res_id.'\')" class="btn btn-danger"><i class="fa fa-trash"></i> Cancelled</button>
    */
    $total_ = $total_ + $json_reservation->amount;
  }
  }else{

  }
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Invoice</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
	  <!-- CUSTOM STYLE  -->
    <link href="assets/css/custom-style.css" rel="stylesheet" />
    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />

</head>
<body>
 <div class="container">
     
      <div class="row pad-top-botm ">
         <div class="col-lg-6 col-md-6 col-sm-6 ">
            <img src="../images/cross-sphere-logo.jpg" style="padding-bottom:20px;" /> 
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            
               <strong>Lupit Church Memorial Chapel
</strong>
              <br />
                  <i>Address :</i> Sacred Heart Seminary Compound, 
              <br />
                  Lizares St., Bacolod City 6100
              <br />
                  Philippines
              
         </div>
     </div>
     <div  class="row text-center contact-info">
         <div class="col-lg-12 col-md-12 col-sm-12">
             <hr />
             <span>
                 <strong>Email : </strong>  info@lupit.com
             </span>
             <span>
                 <strong>Call : </strong>  (034) 433-9826 to 27
             </span>
              <span>
                 <strong>Invoice # : <?php echo str_pad($resid, 10, "0", STR_PAD_LEFT); ?></strong>  
             </span>
             <hr />
         </div>
     </div>
     <div  class="row pad-top-botm client-info">
         <div class="col-lg-6 col-md-6 col-sm-6">
         <h4>  <strong>Client Information</strong></h4>
           <strong>  <?php echo $name; ?></strong>
             <br />
                  <b>Address :</b>
             <br />
             <b>Call :</b> <?php echo $phone; ?>
              <br />
             <b>E-mail :</b> <?php echo $email; ?>
         </div>
<!--           <div class="col-lg-6 col-md-6 col-sm-6">
            
               <h4>  <strong>Payment Details </strong></h4>
            <b>Bill Amount :  990 USD </b>
              <br />
               Bill Date :  01th August 2014
              <br />
               <b>Payment Status :  Paid </b>
               <br />
               Delivery Date :  10th August 2014
              <br />
               Purchase Date :  30th July 2014
         </div> -->
     </div>
     <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
           <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>S. No.</th>
                                    <th>Perticulars</th>
                                    <th>Quantity.</th>
                                    <th>Unit Price</th>
                                     <th>Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>-</td>
                                    <td><?php
                                    echo '<strong>Event: </strong>'.$event.'<br/><strong>Venue: </strong>'.translateVenue($venue).'';
                                    echo '<br/><strong>Date/Time: </strong>'.$start_date.'';
                                    ?></td>
                                    <td>-</td>
                                    <td><?php echo $amount; ?>Php</td>
                                    <td><?php echo $amount; ?>Php</td>
                                </tr>
<!--                                 <tr>
                                    <td>2</td>
                                    <td>Plugin Dev.</td>
                                    <td>2</td>
                                    <td>200 USD</td>
                                    <td>400 USD</td>
                                </tr>
                                <tr>
                                     <td>3</td>
                                    <td>Hosting Domains</td>
                                    <td>2</td>
                                    <td>100 USD</td>
                                    <td>200 USD</td>
                                </tr> -->
                                
                            </tbody>
                        </table>
               </div>
             <hr />
             <div class="ttl-amts">
               <h5>  Total Amount : <?php echo $amount; ?>Php </h5>
             </div>
<!--              <hr />
              <div class="ttl-amts">
                  <h5>  Tax : 90 USD ( by 10 % on bill ) </h5>
             </div> -->
             <hr />
              <div class="ttl-amts">
                  <h4> <strong>Bill Amount : <?php echo $amount; ?>Php</strong> </h4>
             </div>
         </div>
     </div>
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <strong> Important: </strong>
             <ol>
                  <li>
                    This is an electronic generated invoice so doesn't require any signature.

                 </li>
                 <li>
                     Please read all terms and polices on  lupit.geekbacolod.com for returns, replacement and other issues.

                 </li>
             </ol>
             </div>
         </div>
<!--       <div class="row pad-top-botm">
         <div class="col-lg-12 col-md-12 col-sm-12">
             <hr />
             <a href="#" class="btn btn-primary btn-lg" >Print Invoice</a>
             &nbsp;&nbsp;&nbsp;
              <a href="#" class="btn btn-success btn-lg" >Download In Pdf</a>

             </div>
      </div> -->
 </div>
<script>
window.print();
</script>

</body>
</html>
