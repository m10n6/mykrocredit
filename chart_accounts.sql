-- phpMyAdmin SQL Dump
-- version 4.2.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 06, 2018 at 08:09 AM
-- Server version: 5.5.41-log
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mykrocredit`
--

-- --------------------------------------------------------

--
-- Table structure for table `chart_accounts`
--

CREATE TABLE IF NOT EXISTS `chart_accounts` (
`caid` bigint(20) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `category` varchar(255) NOT NULL,
  `is_display` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `chart_accounts`
--

INSERT INTO `chart_accounts` (`caid`, `code`, `name`, `category`, `is_display`, `date_added`) VALUES
(1, '100-001', 'Cash in Bank (BDO Hilado)', 'Cash', '1', '2017-07-31 02:28:08'),
(2, '101-001', 'A/R Trade', 'Loans and Other Receivables', '1', '2017-07-31 02:32:19'),
(4, '100-003', 'Cash in Bank (China Bank)', 'Cash', '1', '2017-07-31 02:28:00'),
(5, '101-001-01', 'A/R Loans', 'Loans and Other Receivables', '1', '2017-07-31 02:33:01'),
(6, '101-001-02', 'A/R Cash Advance', 'Loans and Other Receivables', '1', '2017-07-31 02:33:38'),
(7, '101-001-03', 'A/R Employee', 'Loans and Other Receivables', '1', '2017-07-31 02:34:03'),
(8, '101-001-04', 'A/R Blacklist', 'Loans and Other Receivables', '1', '2017-07-31 02:36:21'),
(9, '101-001-05', 'A/R Special Accounts', 'Loans and Other Receivables', '1', '2017-07-31 02:36:51'),
(10, '101-001-06', 'A/R Excess', 'Loans and Other Receivables', '1', '2017-07-31 02:37:14'),
(11, '400-003', 'Service Fee', 'Revenue', '1', '2017-07-31 02:38:04'),
(12, '400-003-01', 'Processing Fee', 'Revenue', '1', '2017-07-31 02:38:37'),
(13, '400-003-02', 'Notarial Fee', 'Revenue', '1', '2017-07-31 02:39:06'),
(14, '400-003-03', 'Miscellaneous', 'Revenue', '1', '2017-07-31 02:39:30'),
(15, '400-003-04', 'Finders Fee', 'Revenue', '1', '2017-07-31 02:39:51'),
(16, '400-005', 'Rebates/Discounts', 'Revenue', '1', '2017-07-31 02:40:22'),
(17, '100-002', 'Cash in Bank (BDO East)', 'Cash', '1', '2017-07-31 03:10:43'),
(18, '102-000', 'DUE FROM-MARC ACCOUNT', 'Loans and Other Receivables', '1', '2018-02-06 04:54:13'),
(19, '101-001-009', 'Bonuses', 'Others', '1', '2018-02-06 13:06:16'),
(20, '101-001-008', 'A/R Inactive Account', 'Loans and Other Receivables', '1', '2018-02-06 04:51:54'),
(21, '102-001', 'DUE FROM-MARC JOB', 'Loans and Other Receivables', '1', '2018-02-06 04:54:56'),
(22, '102-002', 'DUE FROM-RGR', 'Loans and Other Receivables', '1', '2018-02-06 04:55:17'),
(23, '102-003', 'DUE FROM-JDR', 'Loans and Other Receivables', '1', '2018-02-06 04:55:38'),
(24, '102-004', 'DUE FROM-MYKRO', 'Loans and Other Receivables', '1', '2018-02-06 04:56:04'),
(25, '103-000', 'ADVANCES FROM OFFICERS & EMPLOYEES', 'Loans and Other Receivables', '1', '2018-02-06 04:56:32'),
(26, '104-000', 'LAND & BLGD. IMPROVEMENTS', 'Assets', '1', '2018-02-06 04:57:12'),
(27, '106-000', 'SERVICE VEHICLE', 'Assets', '1', '2018-02-06 04:58:24'),
(28, '200-001', 'ACCOUNTS PAYABLE', 'Payable', '1', '2018-02-06 04:58:43'),
(29, '200-002', 'VAT PAYABLE', 'Payable', '1', '2018-02-06 04:59:04'),
(30, '200-003', 'INCOME TAX PAYABLE', 'Payable', '1', '2018-02-06 04:59:24'),
(31, '200-004', 'WITHHOLDING TAX PAYABLE', 'Payable', '1', '2018-02-06 04:59:47'),
(32, '200-005', 'SSS LOAN PAYABLE', 'Payable', '1', '2018-02-06 05:00:10'),
(33, '200-006', 'PHILHEALTH PAYABLE', 'Payable', '1', '2018-02-06 05:00:36'),
(34, '200-007', 'PAG-IBIG PAYABLE', 'Payable', '1', '2018-02-06 05:06:39'),
(35, '200-008', 'SALARIES & WAGES PAYABLE', 'Payable', '1', '2018-02-06 05:06:56'),
(36, '200-009', 'DUE TO - MARC ACCOUNT', 'Payable', '1', '2018-02-06 05:07:14'),
(37, '200-010', 'DUE TO - MARC JOB', 'Payable', '1', '2018-02-06 05:07:30'),
(38, '200-011', 'DUE TO - RGR', 'Payable', '1', '2018-02-06 05:07:44'),
(39, '200-012', 'DUE TO -JDR', 'Payable', '1', '2018-02-06 05:07:57'),
(40, '200-013', 'DUE TO - MYKRO', 'Payable', '1', '2018-02-06 05:08:10'),
(41, '500-000', 'EXPENSES', 'Expenses', '1', '2018-02-06 05:08:27'),
(42, '500-001', 'ADVERTISING & PROMOTION EXPENSE', 'Expenses', '1', '2018-02-06 05:08:41'),
(43, '500-002', 'DONATION EXPENSE', 'Expenses', '1', '2018-02-06 05:08:56'),
(44, '500-003', 'FUEL & OIL EXPENSE', 'Expenses', '1', '2018-02-06 05:09:16'),
(45, '500-004', 'INTEREST EXPENSE', 'Expenses', '1', '2018-02-06 05:09:31'),
(46, '500-005', 'LIGHT & WATER EXPENSE', 'Expenses', '1', '2018-02-06 05:09:47'),
(47, '500-006', 'ALLOWANCE EXPENSE', 'Expenses', '1', '2018-02-06 05:10:01'),
(48, '500-007', 'OFFICE EXPENSE', 'Expenses', '1', '2018-02-06 05:10:14'),
(49, '500-008', 'OTHER EXPENSE', 'Expenses', '1', '2018-02-06 05:10:30'),
(50, '500-009', 'PROFESSIONAL FEES', 'Expenses', '1', '2018-02-06 05:10:47'),
(51, '500-010', 'RENT EXPENSE', 'Expenses', '1', '2018-02-06 05:11:00'),
(52, '500-011', 'REPAIRS & MAINTENANCE', 'Expenses', '1', '2018-02-06 05:11:16'),
(53, '500-012', 'REPRESENTATION', 'Expenses', '1', '2018-02-06 05:11:30'),
(54, '500-013', 'SALARIES & WAGES EXPENSE', 'Expenses', '1', '2018-02-06 05:11:45'),
(55, '500-014', 'TRANSPORTATION EXPENSE', 'Expenses', '1', '2018-02-06 05:12:01'),
(56, '500-015', 'TELECOM & POSTAGE', 'Expenses', '1', '2018-02-06 05:12:20'),
(57, '500-016', 'TAXES & LICENSES EXPENSE', 'Expenses', '1', '2018-02-06 05:12:39'),
(58, '105-000', 'OFFICE FURNITURE & FIXTURES', 'Assets', '1', '2018-02-06 13:07:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chart_accounts`
--
ALTER TABLE `chart_accounts`
 ADD PRIMARY KEY (`caid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chart_accounts`
--
ALTER TABLE `chart_accounts`
MODIFY `caid` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
