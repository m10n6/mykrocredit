<?php
session_start();

include_once('../../config.php');
include_once('../funcjax.php');

if(empty($_SESSION['authenticated'])){
	// deny action

}else{
	$acc_caid = $_POST['acc_caid'];
	$client_id = $_POST['client_id'];
	$debits = $_POST['debits'];
	$credits = $_POST['credits'];
	$notes = $_POST['notes'];
	$date_trans = $_POST['date_trans'];

	$array_ctr = count($acc_caid);


	//check if table is empty 
	// $sql = "select * from `jv_numbers`";
	// $res = $conn->dbquery($sql);
	// if($res == 'false'){
		// $jvnid = ''
		// insert empty data for the ID
		$ins1 = "insert into `jv_numbers` set 
				`prefix` = '',
				`number` = '',
				`dateadded` = '".date('Y-m-d H:i:s')."'";
		$res_ins1 = $conn->dbquery($ins1);
		// echo $res_ins1;
	// }
	// $res = json_decode($res);
	// print_r($res);

	$now = date('Y-m-d H:i:s');
	for($i = 0; $i < $array_ctr; $i++){
		// `journal_voucher` WHERE `jvid`, `jvnid`, `client_id`, `chart_account_id`, `debit`, `credit`, `date_trans`, `addby`, `date_added`
		$ins2 = "insert into `journal_voucher` set
		 		`jvnid` = '".$res_ins1."',
		 		`client_id` = '".$client_id[$i]."',
		 		`chart_account_id` = '".$acc_caid[$i]."',
		 		`debit` = '".$debits[$i]."',
		 		`credit` = '".$credits[$i]."',
		 		`date_trans` = '".$date_trans[$i]."',
		 		`addby` = '".$_SESSION['uuid']."',
		 		`date_added` = '".$now."'";

		$conn->dbquery($ins2);

		$inAmnt = $debits[$i] + $credits[$i];

		$extra = '{ "guarantor": "", "TypeRate" : "", "ApplyInt" : "", "JVTypeID": "'.$acc_caid[$i].'" }' ;
		// $balance = $inpBalance - $inpLoanAmount;
		$insert = "insert into `finance` set 
				`client_id` = '".$client_id[$i]."', 
				`intrate` = '', 
				`debit` = '".$debits[$i]."', 
				`credit` = '".$credits[$i]."', 
				`balance` = '0', 
				`extra` = '".$extra."',  
				`status` = 'approved',
				`loan_type` = 'jv', 
				`voucher_id` = '".$res_ins1."', 
				`term` = '', 
				`amount` = '".$inAmnt."', 
				`date_added` = '".date('Y-m-d H:i:s')."' , 
				`loan_date` = '".date('Y-m-d H:i:s')."',
				`processedby` = '".$_SESSION['uuid']."',  
				`cancelledby` = '',  
				`addedby` = '".$_SESSION['uuid']."', 
				`date_approved` = '".$date_trans[$i]."'
		";

		$conn->dbquery($insert);
	}

	//insert the notes
	//  `SELECT * FROM `jv_notes` WHERE 1
	$ins3 = "insert into `jv_notes` set 
			`jvnid` = '".$res_ins1."',
			`notes` = '".$notes."',
			`dateadded`	= '".date('Y-m-d H:i:s')."'	
			";
	$conn->dbquery($ins3);
	// echo $array_ctr;
}
?>