<?php
session_start();

include_once('../../config.php');
include_once('../funcjax.php');

if(empty($_SESSION['authenticated'])){
	// deny action
}else{

		//get prefix
		$sqlprefix = "select * from `settings` where `meta` = 'orsprefix' ";
		$res = $conn->dbquery($sqlprefix);
		$res = json_decode($res);
		$res = json_decode($res->data[0]);
		$vprefix = $res->value;

		$sqldigit = "select * from `settings` where `meta` = 'orsdigit' ";
		$resd = $conn->dbquery($sqldigit);
		$resd = json_decode($resd);
		$resd = json_decode($resd->data[0]);
		$vstart = $resd->value; //digit

		//update 
		// `vid`, `prefix`, `number`, `date_added`

		$getLatest = "select * from `ors_numbers` where `prefix` = '".$vprefix."' order by `ors_id` desc  ";
		$res1 = $conn->dbquery($getLatest);
		$res1 = json_decode($res1);
		$res1 = json_decode($res1->data[0]);

		$vnum = $res1->code + 1;

		$newCode = $vprefix.''.str_pad($vnum, $vstart, '0', STR_PAD_LEFT);

		$newCode = CheckORS($newCode);

		echo $newCode;
}