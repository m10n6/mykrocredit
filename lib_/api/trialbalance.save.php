<?php
session_start();

include_once('../../config.php');
include_once('../funcjax.php');

if(empty($_SESSION['authenticated'])){
	// deny action

}else{
	$month_year = $_POST['month_year'];
	$grid_ctr = $_POST['grid_ctr'];
	$caid = $_POST['caid'];
	$prev_deb = $_POST['prev_deb'];
	$prev_cre = $_POST['prev_cre'];
	$trans_deb = $_POST['trans_deb'];
	$trans_cre = $_POST['trans_cre'];
	$up_deb = $_POST['up_deb'];
	$up_cre = $_POST['up_cre'];

	$axn = $_POST['axn'];

	switch($axn){
		case 'insert':
			$date_added = date('Y-m-d H:i:s');
			for($i=0; $i<=$grid_ctr; $i++){
				$sql = "insert into `trial_balance` set 
						`month_year` = '".$month_year."',
						`chart_account_id` = '".$caid[$i]."',
						`prev_debit` = '".$prev_deb[$i]."',
						`prev_credit` = '".$prev_cre[$i]."',
						`trans_debit` = '".$trans_deb[$i]."',
						`trans_credit` = '".$trans_cre[$i]."',
						`ud_debit` = '".$up_deb[$i]."',
						`ud_credit` = '".$up_cre[$i]."',
						`date_added` = '".$date_added."'
					";
				$conn->dbquery($sql);
			}

			echo 'Trial Balance Data for "'.$month_year.'" has been saved!';
		break;

		case 'update':
			$del = "delete from `trial_balance` where `month_year` = '".$month_year."'";
			$conn->dbquery($del);

			$date_added = date('Y-m-d H:i:s');
			for($i=0; $i<=$grid_ctr; $i++){
				$sql = "insert into `trial_balance` set 
						`month_year` = '".$month_year."',
						`chart_account_id` = '".$caid[$i]."',
						`prev_debit` = '".$prev_deb[$i]."',
						`prev_credit` = '".$prev_cre[$i]."',
						`trans_debit` = '".$trans_deb[$i]."',
						`trans_credit` = '".$trans_cre[$i]."',
						`ud_debit` = '".$up_deb[$i]."',
						`ud_credit` = '".$up_cre[$i]."',
						`date_added` = '".$date_added."'
					";
				$conn->dbquery($sql);
			}
			echo 'Trial Balance Data for "'.$month_year.'" has been overwritten!';
		break;

		default:
			echo 'Please contact developer if you see this error.\nEmail: info@markramosonline.com';
		break;
	}


}
?>