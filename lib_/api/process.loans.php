<?php
// function for adding clients
session_start();
include_once('../../config.php');
include_once('../funcjax.php');

if(empty($_SESSION['authenticated'])){
	// deny action
	$result['message'] = 'Error: Session Expired!';
	echo json_encode($result);
}else{
	$inpHiddenUID = $_POST['inpHiddenUID'];
	$userID = $_POST['userID'];
	$selTypeLoans = $_POST['selTypeLoans'];
	$inpLoanAmount = $_POST['inpLoanAmount'];
	$selIntRate = $_POST['selIntRate'];
	$selTypeRate = $_POST['selTypeRate'];
	$inpTerm = $_POST['inpTerm'];
	$inpGuarantor = $_POST['inpGuarantor'];
	$inpBonus = $_POST['inpBonus'];
	$inpDOL = $_POST['inpDOL'];


	// `client_id`, `intrate`, `debit`, `credit`, `balance`, `extra`, `status`, `loan_type`, `voucher_id`, `term`, `amount`, `date_added`, `processedby`, `cancelledby`, `addedby`, `date_approved`

	// txtCreditLimit
	$sql = "select * from `client_meta` where `meta` = 'txtCreditLimit' and `client_id` = '".$inpHiddenUID."'";
	$res = $conn->dbquery($sql);
	$res = json_decode($res);
	$res = json_decode($res->data[0]);
	$creditLimit = $res->value;

	$sql1 = "select * from `client_meta` where `meta` = 'txtAdditionalBalance' and `client_id` = '".$inpHiddenUID."'";
	$res1 = $conn->dbquery($sql1);
	$res1 = json_decode($res1);
	$res1 = json_decode($res1->data[0]);
	$txtAdditionalBalance = $res1->value;

	if($inpLoanAmount == 0){
		$result['message'] = 'Error: Amount is Zero (0) or Empty!';
		echo json_encode($result);
		exit();
	}

	$result['test_am'] = $creditLimit.'<=>'.$inpLoanAmount;

	$totalCreditLimit = $creditLimit + $txtAdditionalBalance;
	$creditLimit = $totalCreditLimit;

	$isActive = getIsInActive($inpHiddenUID);
	if(!empty($isActive) && $isActive == 'on'){
		$creditLimit = 0;
	}

	if($inpLoanAmount <= $creditLimit){
	// if($inpLoanAmount <= $totalCreditLimit){

		//get balance here
		#balance code here....
		$currBalance = str_replace(',', '', getClientBalance($inpHiddenUID));
		$bal_less_creditlim = $creditLimit - $currBalance;

		$isActive = getIsInActive($inpHiddenUID);
		if(!empty($isActive) && $isActive == 'on'){
			$bal_less_creditlim = 0;
		}

		// $result['message'] = $bal_less_creditlim;
		if($inpLoanAmount > $bal_less_creditlim){
			$result['message'] = 'Error: Credit exceeds from Client\'s current balance!<br>
				Must not exceed to <span style="color: #fff"> Php '.number_format($bal_less_creditlim,2).'</span>
			';
		}else{
			// $sql = "select * from `finance` where `client_id` = '".."'";
			// if(){
			// }
			if($selTypeLoans == 'cash_advance'){
				$inpTerm = '1';
			}

			$extra = '{ "guarantor": "'.$inpGuarantor.'", "TypeRate" : "'.$selTypeRate.'" }' ;
			$balance = $creditLimit - $inpLoanAmount;
			// $insert = "insert into `finance` set 
			// 		`client_id` = '".$inpHiddenUID."', 
			// 		`intrate` = '".$selIntRate."', 
			// 		`debit` = '0', 
			// 		`credit` = '".$inpLoanAmount."', 
			// 		`balance` = '".$balance."', 
			// 		`extra` = '".$extra."',  
			// 		`status` = 'pending',
			// 		`loan_type` = '".$selTypeLoans."', 
			// 		`voucher_id` = '', 
			// 		`term` = '".$inpTerm."', 
			// 		`amount` = '".$inpLoanAmount."', 
			// 		`date_added` = '".date('Y-m-d H:i:s')."' , 
			// 		`loan_date` = '".date('Y-m-d 00:00:00', strtotime($inpDOL))."',
			// 		`processedby` = '',  
			// 		`cancelledby` = '',  
			// 		`addedby` = '".$userID."', 
			// 		`date_approved` = '1970-01-01 00:00:00'
			// ";
			$insert = "insert into `finance` set 
					`client_id` = '".$inpHiddenUID."', 
					`intrate` = '".$selIntRate."', 
					`debit` = '0', 
					`credit` = '".$inpLoanAmount."', 
					`balance` = '".$balance."', 
					`extra` = '".$extra."',  
					`status` = 'pending',
					`loan_type` = '".$selTypeLoans."', 
					`voucher_id` = '', 
					`term` = '".$inpTerm."', 
					`amount` = '".$inpLoanAmount."', 
					`date_added` = '".date('Y-m-d H:i:s')."' , 
					`loan_date` = '".date('Y-m-d 00:00:00', strtotime($inpDOL))."',
					`processedby` = '',  
					`cancelledby` = '',  
					`addedby` = '".$userID."', 
					`date_approved` = '".date('Y-m-d 00:00:00', strtotime($inpDOL))."'
			";
			$conn->dbquery($insert);

			$result['message'] = 'success';
		}
	}else{
		// credit limit exceeds	
		$result['message'] = 'Error: Credit exceeds from Client\'s credit limit!';
	}
	// echo json_encode($result).$inpLoanAmount.$insert;
	echo json_encode($result);
}
?>