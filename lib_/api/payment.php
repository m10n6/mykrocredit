<?php
// function for adding clients
session_start();
include_once('../../config.php');
include_once('../funcjax.php');


if(empty($_SESSION['authenticated'])){
	// deny action
	$result['message'] = 'Error: Session Expired!';
	echo json_encode($result);
}else{

	$inpORNum = $_POST['inpORNum'];
	$inpClientID = $_POST['inpClientID'];
	$inpBalance = $_POST['inpBalance'];
	$inpPayAmnt = $_POST['inpPayAmnt'];
	$selIntRate = $_POST['selIntRate'];
	$selTypeRate = $_POST['selTypeRate'];
	$inpApplyInt = $_POST['inpApplyInt'];
	$inpPaymentDate = $_POST['inpPaymentDate'];

	// $inpHiddenUID = $_POST['inpHiddenUID'];
	$userID = $_SESSION['uuid'];

	$selPaymentType = $_POST['selPaymentType'];
	if($selPaymentType == 'orss_payment'){
		$inpORNum = CheckORS($inpORNum);
		$selTypeLoans ='payment_ors';
	}else{
		$selTypeLoans = 'payment';
	}

	$FirstOrSecond =  getClientSchedPayment($inpClientID);

	$InterestDateStart = getClientSchedPaymentStart($inpClientID);

	// echo $inpPaymentDate.' = '.$FirstOrSecond.' = '.$InterestDateStart .'\n';
	// echo strtotime($inpPaymentDate).' = '.$FirstOrSecond.' = '.strtotime($InterestDateStart) .'\n';

	// if(strtotime($inpPaymentDate) < strtotime($InterestDateStart)){
	// 	$inpApplyInt = 'off';
	// }else{
	// 	// if(strtotime($inpPaymentDate) == strtotime($InterestDateStart))
	// 	//check if payment date is less than or equal to 15 the applied interest is every 15th or 1

	// 	//if is greater than or equal to 30 the applied interest is every 30th or 2

	// }


		// if(empty($inpApplyInt)){
		// 	$interest_psched = getClientSchedPayment($inpClientID);
		// 	$sql_ltype = "select `loan_type` from `finance` where `loan_type` like 'payment%' and `client_id` = '".$inpClientID."'  ";
		// 	$rs_ltype = $conn->dbquery($sql_ltype);
		// 	if($rs_ltype == 'false'){
		// 		if($interest_psched == 1){
		// 			$inpApplyInt = 'on';
		// 		}else{
		// 			$inpApplyInt = 'off';
		// 		}
		// 	}else{
		// 		//counter
		// 		$xctr = 1;
		// 		$rs_ltype = json_decode($rs_ltype);
		// 		$count_rs_ltype = count($rs_ltype->data);
		// 		$inpApplyInt = 'on';
		// 		if(($count_rs_ltype % 2) == 1 && $interest_psched == 1){
		// 			$inpApplyInt = 'off';
		// 		}
		// 		if(($count_rs_ltype % 2) == 0 && $interest_psched == 2){
		// 			$inpApplyInt = 'off';
		// 		}
		// 	}
		// 	$inpApplyInt = 'off';
		// }else{
		// 	$inpApplyInt = 'on';
		// }


		$inpApplyInt = 'off';
		

		$extra = '{ "guarantor": "'.$inpGuarantor.'", "TypeRate" : "'.$selTypeRate.'", "ApplyInt" : "'.$inpApplyInt.'" }' ;
		// $balance = $inpBalance - $inpLoanAmount;
		$insert = "insert into `finance` set 
				`client_id` = '".$inpClientID."', 
				`intrate` = '".$selIntRate."', 
				`debit` = '".$inpLoanAmount."', 
				`credit` = '0', 
				`balance` = '".$inpBalance."', 
				`extra` = '".$extra."',  
				`status` = 'approved',
				`loan_type` = '".$selTypeLoans."', 
				`voucher_id` = '".$inpORNum."', 
				`term` = '".$inpTerm."', 
				`amount` = '".$inpPayAmnt."', 
				`date_added` = '".date('Y-m-d H:i:s')."' , 
				`loan_date` = '".date('Y-m-d H:i:s')."',
				`processedby` = '".$userID."',  
				`cancelledby` = '',  
				`addedby` = '".$userID."', 
				`date_approved` = '".$inpPaymentDate."'
		";

		echo $insert;

		$conn->dbquery($insert);

		if($selPaymentType == 'orss_payment'){

			$pref = getORSPrefix();
			$noprefCode = str_replace($pref,'', $inpORNum);

			$insertORS = "insert into `ors_numbers` set `code` = '".$noprefCode."', `prefix` = '".$pref."', `ors_num` = '".$inpORNum."', `dateadded` = '".date('Y-m-d H:i:s')."' ";
			$conn->dbquery($insertORS);
		}

		// $result['message'] = 'success';

		echo json_encode($result);

}
?>

