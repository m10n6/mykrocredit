    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/js/moment/moment.min.js"></script>
    <script src="vendors/js/datepicker/daterangepicker.js"></script>

    <!-- bootstrap progress js -->
    <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>



    <!-- Datatables-->
    <script src="includes/js/datatables/jquery.dataTables.min.js"></script>
    <script src="includes/js/datatables/dataTables.bootstrap.js"></script>
    <script src="includes/js/datatables/dataTables.buttons.min.js"></script>
    <script src="includes/js/datatables/buttons.bootstrap.min.js"></script>
    <script src="includes/js/datatables/jszip.min.js"></script>
    <script src="includes/js/datatables/pdfmake.min.js"></script>
    <script src="includes/js/datatables/vfs_fonts.js"></script>
    <script src="includes/js/datatables/buttons.html5.min.js"></script>
    <script src="includes/js/datatables/buttons.print.min.js"></script>
    <script src="includes/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="includes/js/datatables/dataTables.keyTable.min.js"></script>
    <script src="includes/js/datatables/dataTables.responsive.min.js"></script>
    <script src="includes/js/datatables/responsive.bootstrap.min.js"></script>
    <script src="includes/js/datatables/dataTables.scroller.min.js"></script>


    <!-- iCheck -->
    <script src="includes/js/icheck/icheck.min.js"></script>
    <!-- autocomplete -->
    <script src="includes/js/autocomplete/jquery.autocomplete.js"></script>

    <script src="includes/js/custom.js?v=0.0.1"></script>
  <script>
    $(window).load(function() {

   
    });

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    function toTitleCase(str){
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }

    var dt_table;

    function loadVoucherList(callback){
        $.ajax({
            type: 'post',
            url : 'lib/api/voucher.list.php',
            dataType: 'json',
            data : {

            },
            beforeSend: function() {
                var html = '\
                <tr>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                </tr>\
                ';
                $('#tblVoucherList tbody').html(html);
            },
            success : function(x) {
                // alert(x);
                if(callback){
                    callback(x);
                }
            },
            error: function(){
                alert('Please contact developer!');
            }
        });
    }


    function plot_voucherlist_table(){
      
      loadVoucherList(function(result){
        // console.log(result.data);
        var len = result.data.length;
        // console.log(len);
        var html = '';
        for(var i = 0; i < len; i++){
            var obj = JSON.parse(result.data[i]);
            html += '\
                <tr>\
                    <td class="td_name"><strong>'+ obj.last_name +', '+obj.name + ' ' + obj.middle_name +'</strong><br>\
                    <div class=""><a href="#" class="cls_procar" data-value="'+ obj.client_id +'" >Process Loans</a> | \
                    <a href="#" class="cls_payment" data-value="'+ obj.client_id +'" >Payment</a> | \
                    <a href="#" class="cls_viewdata" data-value="'+ obj.client_id +'" >View Data</a> | \
                    <a href="#" class="cls_editprofile" data-value="'+ obj.client_id +'" >Edit Profile</a>\
                    </div>\
                    </td>\
                    <td>' + obj.contact_num +'</td>\
                    <td>' + obj.address +'</td>\
                    <td>' + obj.company +'</td>\
                </tr>\
            ';
        }
        $('#tblVoucherList tbody').html(html);
        // var obj = JSON.parse(result)
        dt_table = $('#tblVoucherList').dataTable({
            aLengthMenu: [
              [10, 25, 50, 100, -1],
              [10, 25, 50, 100, "All"]
            ]
        });
      });        
    }

    function loadVoucherListWithRange(start_date, end_date, loan_type = '',callback){
        $.ajax({
            type: 'post',
            url : 'lib/api/ar.list2.php',
            dataType: 'json',
            data : {
                'start_date' : start_date,
                'end_date' : end_date,
                'loan_type' : loan_type
            },
            beforeSend: function() {
                var html = '\
                <tr>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                    <td>Loading <i class="fa fa-spinner fa-pulse"></i> </td>\
                </tr>\
                ';
                $('#tblVoucherList tbody').html(html);
            },
            success : function(x) {
                // alert(x);
                console.log(x);
                if(callback){
                    callback(x);
                }
            },
            error: function(){
                alert('Please contact developer!');
            }
        });
    }
     var dt_table;

    $(document).ready(function(){
      // plot_voucherlist_table();
       dt_table =  $('#tblVoucherList').DataTable({
                    aLengthMenu: [
                      [10, 25, 50, 100, -1],
                      [10, 25, 50, 100, "All"]
                    ],
                    "bSort": false
                    // ,"footerCallback": function ( row, data, start, end, display ) {
                    //     var api = this.api(), data;
             
                    //     // Remove the formatting to get integer data for summation
                    //     var intVal = function ( i ) {
                    //         return typeof i === 'string' ?
                    //             i.replace(/[\$,]/g, '')*1 :
                    //             typeof i === 'number' ?
                    //                 i : 0;
                    //     };
             
                    //     // Total over all pages
                    //     total = api
                    //         .column(4)
                    //         .data()
                    //         .reduce( function (a, b) {
                    //             return intVal(a) + intVal(b);
                    //         }, 0 );
             
                    //     // Total over this page
                    //     pageTotal = api
                    //         .column( 4, { page: 'current'} )
                    //         .data()
                    //         .reduce( function (a, b) {
                    //             return intVal(a) + intVal(b);
                    //         }, 0 );
             
                    //     // Update footer
                    //     $( api.column( 4 ).footer() ).html(
                    //         'Php '+pageTotal +' ( Php '+ total +' total)'
                    //     );
                    // }
                });
      $('#divEditData').hide();

      $('#tblVoucherList').on('click', ".cls_procar", function(){
        var dataID = $(this).data('value');
        $('#modLoans').modal('show');
      });

      $('.btn-editTrans').click(function(){
        $('#divMainDisplay').hide(function(){
            $('#divEditData').show();    
        });
      });

      // $('#chkIsAdvanced').click(function(e){
      //   e.preventDefault();
      //   alert('asd');
      // });
        $('#divAdvanceOpt').hide();

        $('input').on('ifChecked', function(event){
          // alert(event.type + ' callback');
          $('#divAdvanceOpt').show();
          $('#advOpt').val('1');

        });

        $('input').on('ifUnchecked', function(event){
          // alert(event.type + ' callback');
          $('#divAdvanceOpt').hide();
          $('#advOpt').val('0');
        });
        // 


        $('#btnLoadRep').click(function(){
            var frmInpDstart = $('#frmInpDstart').val();
            var frmInpDend  = $('#frmInpDend').val();

            var selTypeLoans = $('#selTypeLoans').val();
            loadVoucherListWithRange(frmInpDstart, frmInpDend, selTypeLoans,function(result){
                dt_table.destroy();

                var temp_netProceeds = 0;
                var temp_amnt = 0;
                console.log(result.data);
                var html = '';
                if(result){

                    var len = result.data.length;
                    // console.log(len);
                    
                    for(var i = 0; i < len; i++){
                        var obj = JSON.parse(result.data[i]);

                            // <tr>
                            //     <th>CV #</th>
                            //     <th>Name</th>
                            //     <th>Amount</th>
                            //     <th>Interest</th>
                            //     <th>Interest Type</th>
                            //     <th>Term</th>
                            //     <th>Loan Type</th>
                            //     <th>Date</th>
                            //     <th>Action</th>
                            // </tr>
                        var obj2 = JSON.parse(obj.extra);
                        // console.log(obj.extra);

                        var ld = obj.loan_date;

                        var axn;
                        ld = ld.split(' ');
                        if(obj.status != 'approved'){
                            axn = '\
                                    <button type="button" class="btn btn-success btnApprove" data-value="'+ obj.fid +'" data-clientid="'+ obj.client_id +'" data-toggle="tooltip" data-placement="top" title="Approve Transacton" ><i class="fa fa-thumbs-o-up"></i></button>\
                                    <button type="button" class="btn btn-danger btnDecline"  data-value="'+ obj.fid +'" data-clientid="'+ obj.client_id +'" data-toggle="tooltip" data-placement="top" title="Decline Transacton" ><i class="fa fa-thumbs-o-down"></i></button>\
                            ';
                        }else{
                            axn = '\
                                <button type="button" class="btn btn-primary btnView" data-value="'+ obj.fid +'" data-clientid="'+ obj.client_id +'" data-toggle="tooltip" data-placement="top" title="View Transacton" ><i class="fa fa-eye"></i></button>\
                            ';
                        }

                        if(obj.status == 'declined'){
                            is_declined = 'declined';
                            axn = 'DECLINED!';
                        }else{
                            is_declined = '';
                        }

                        if( obj.loan_type.indexOf('payment') > -1 || obj.loan_type.indexOf('jv') > -1 || obj.loan_type.indexOf('beg_bal') > -1){

                        }else{
                    // <th>Issuance Date</th>
                    // <th>Voucher No.</th>
                    // <th>Client</th>
                    // <th>Amount</th>
                    // <th>Processing Fee</th>
                    // <th>Finders Fee</th>
                    // <th>Notarial</th>
                    // <th>Miscellaneous</th>
                    // <th>Insurance</th>
                    // <th>Others</th>
                    // <th>Adjustment</th>
                    // <th>Net Proceeds</th>
                    //`inProcFee`, `inpFindersFee`, `inpNotarial`, `inpMisc`, `inpInsurance`, `inpOthers`, `inpAdjustment`, `inpBonus`
                    //<td>'+ obj2.TypeRate.capitalize() +'</td>\
                    //$netProceeds = $nres->amount - ($nres->inProcFee + $nres->inpFindersFee + $nres->Notarial + $nres->inpMisc + $nres->inpInsurance + $nres->inpOthers + $nres->inpAdjustment );
                            var s = obj.amount;
                    // s = s.split(',').join('');
                            var netProceeds =  obj.amount - (parseFloat(obj.inProcFee) +  parseFloat(obj.inpFindersFee) + parseFloat(obj.inpNotarial) +parseFloat( obj.inpMisc) + parseFloat(obj.inpInsurance) + parseFloat(obj.inpOthers) + parseFloat(obj.inpAdjustment) );
                    // alert(parseFloat(obj.inProcFee) +  parseFloat(obj.inpFindersFee) + parseFloat(obj.inpNotarial) +parseFloat( obj.inpMisc) + parseFloat(obj.inpInsurance) + parseFloat(obj.inpOthers) + parseFloat(obj.inpAdjustment) );
                    //<td>'+ toTitleCase(valueToWords(obj.loan_type)) +'</td>\
                            // html += '\
                            //     <tr class="'+ is_declined +'">\
                            //         <td>'+ obj.date_approved +'</td>\
                            //         <td>'+ obj.voucher_id +'</td>\
                            //         <td class="details-control" data-fid="'+ obj.fid +'">'+ obj.last_name +', '+ obj.name +' '+ obj.middle_name +'</td>\
                            //         <td>'+ numberWithCommas(Number(obj.amount).toFixed(2)) +'</td>\
                            //         <td>'+ obj.inProcFee +'%</td>\
                            //         <td>'+ obj.inpFindersFee +'</td>\
                            //         <td>'+ obj.inpNotarial +'</td>\
                            //         <td>'+ obj.inpMisc +'</td>\
                            //         <td>'+ Number(obj.inpInsurance).toFixed(2) +'</td>\
                            //         <td>'+ obj.inpOthers +'</td>\
                            //         <td>'+ obj.inpAdjustment +'</td>\
                            //         <td>'+ numberWithCommas(Number( netProceeds).toFixed(2)) +'</td>\
                            //         <td>'+ axn +'</td>\
                            //     </tr>\
                            // ';

                            var ltype= obj.loan_type;
                            ltype = ltype.replace('_', ' ');
                            html += '\
                                <tr class="'+ is_declined +'">\
                                    <td>'+ formatShortDate(obj.date_approved) +'</td>\
                                    <td>'+ obj.voucher_id +'</td>\
                                    <td class="details-control" data-fid="'+ obj.fid +'">'+ obj.last_name +', '+ obj.name +' '+ obj.middle_name +'</td>\
                                    <td style="text-transform: uppercase;">'+ ltype +'</td>\
                                    <td>'+ numberWithCommas(Number(obj.amount).toFixed(2)) +'</td>\
                                    <td class="hidden-print">\
                                        <!--<a href="javascript:void(0);" class="btn btn-primary btn-editTrans" ><i class="fa fa-pencil"></i></a>-->\
                                        <a href="javascript:void(0);" class="btn btn-primary btn-viewTrans" data-transid="'+ obj.fid +'"><i class="fa fa-eye"></i></a>\
                                    </td>\
                                </tr>\
                            ';

                            temp_netProceeds = temp_netProceeds + netProceeds;

                            temp_amnt += Number(s);
                            console.log(temp_amnt);
                        }

                        // html += '\
                        //     <tr>\
                        //         <td class="td_name"><strong>'+ obj.last_name +', '+obj.name + ' ' + obj.middle_name +'</strong><br>\
                        //         <div class=""><a href="#" class="cls_procar" data-value="'+ obj.client_id +'" >Process Loans</a> | \
                        //         <a href="#" class="cls_payment" data-value="'+ obj.client_id +'" >Payment</a> | \
                        //         <a href="#" class="cls_viewdata" data-value="'+ obj.client_id +'" >View Data</a> | \
                        //         <a href="#" class="cls_editprofile" data-value="'+ obj.client_id +'" >Edit Profile</a>\
                        //         </div>\
                        //         </td>\
                        //         <td>' + obj.contact_num +'</td>\
                        //         <td>' + obj.address +'</td>\
                        //         <td>' + obj.company +'</td>\
                        //     </tr>\
                        // ';
                    }


                }else{
                    html = '\
                        <tr>\
                            <td></td>\
                            <td class="details-control"></td>\
                            <td></td>\
                            <td></td>\
                            <td></td>\
                            <td></td>\
                        </tr>\
                    ';
                }

                $('#ar_overall_total').html(numberWithCommas(Number(temp_amnt).toFixed(2)));
                $('#tblVoucherList tbody').html(html);
                // var obj = JSON.parse(result)
                dt_table = $('#tblVoucherList').DataTable({
                    aLengthMenu: [
                      [10, 25, 50, 100, -1],
                      [10, 25, 50, 100, "All"]
                    ],
                    "bSort": false
                });

            });


        });

        
        $('#frmInpDstart').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_1",
          showDropdowns: true,
          "opens": "center",
          "drops": "down",
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#frmInpDend').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_1",
          showDropdowns: true,
          "opens": "center",
          "drops": "down",
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#optClient').autocomplete({
            minChars : 3,
            serviceUrl: 'lib/api/get.clientdata.php',
            onSelect: function (suggestion) {
                // alert('You selected: ' + suggestion.value + ', ' + suggestion.data + ', ID =' + suggestion.id);
                $('#divClientSearchRes').html('');
                $('#optClientID').val(suggestion.id);
                $('#optClient').val(suggestion.value);
                // $.ajax({
                //     type : 'post',
                //     url : 'lib/api/get.clientbalance.php',
                //     data : {
                //         'cid' : suggestion.id
                //     },success: function(result){
                //          $('#inpBalance').val(result);
                //     }
                // })
            },
            onHint: function(){
                $('#divClientSearchRes').html('');
            },
            onSearchStart : function(params){
                $('#divClientSearchRes').html('<div class="alert alert-info"><i class="fa fa-pulse fa-spinner"></i> Searching Client from Database...</div>');
            }
        });

        $('#optCompany').autocomplete({
            serviceUrl: 'lib/api/get.companies.php',
            onSelect: function (suggestion) {
                // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                $('#optCompany').val(suggestion.value);
                $('#divCompanySearchRes').html('');
            },
            onHint: function(){
                $('#divCompanySearchRes').html('');
            },
            onSearchStart: function(){
                $('#divCompanySearchRes').html('<div class="alert alert-info"><i class="fa fa-pulse fa-spinner"></i> Searching Client from Database...</div>');
            }   

        });


        $('#optUser').autocomplete({
            serviceUrl: 'lib/api/get.users.autocomplete.php',
            onSelect: function (suggestion) {
                // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                $('#optUser').val(suggestion.value);
                $('#optUserID').val(suggestion.id);
                $('#divUserSearchRes').html('');
            },
            onHint: function(){
                $('#divUserSearchRes').html('');
            },
            onSearchStart: function(){
                $('#divUserSearchRes').html('<div class="alert alert-info"><i class="fa fa-pulse fa-spinner"></i> Searching Client from Database...</div>');
            }   

        });
        


        $('#btnArReport').click(function(){
            $('#tblVoucherList_wrapper select').val('-1');
            $('#tblVoucherList_wrapper > div:nth-of-type(1)').addClass('hidden-print');
            $('#tblVoucherList_wrapper #tblVoucherList_info').addClass('hidden-print');
            $('#tblVoucherList_wrapper #tblVoucherList_paginate').addClass('hidden-print');
            alert('Prepare For Printing...');
            window.print();    
            window.location.reload();
        });

        $('#tblVoucherList').on('click','.btn-viewTrans',function(e){
            e.preventDefault();
            var fid = $(this).data("transid");
            // alert(fid);
            $.ajax({
                type : 'post',
                url  : 'lib/api/get.finance.details.php',
                data : {
                    'fid' : fid
                },
                dataType : "json",
                beforeSend: function(){

                },
                success : function(result){
                    console.log(result);
                    var obj = JSON.parse(result.data[0]);
                    //"{"fid":"39","client_id":"4","intrate":"4","debit":"0","credit":"1000","balance":"24000","extra":"{ \"guarantor\": \"\", \"TypeRate\" : \"diminishing\" }","status":"approved","loan_type":"cash_advance","voucher_id":"MYK00009","term":"1","amount":"1000","date_added":"2018-01-31 12:39:47","loan_date":"2018-02-15 00:00:00","processedby":"","cancelledby":"","addedby":"3393","date_approved":"2018-02-15 00:00:00","inProcFee":"0.00","inpFindersFee":"0.00","inpNotarial":"0.00","inpMisc":"30.00","inpInsurance":"0.00","inpOthers":"0.00","inpAdjustment":"0.00","inpBonus":"0.00","approvedby":"3393","client_name":"Mark","middle_name":"B.","last_name":"Ramos"}"
                    var netProceeds =  (parseFloat(obj.amount) - (parseFloat(obj.inProcFee) +  parseFloat(obj.inpFindersFee) + parseFloat(obj.inpNotarial) +parseFloat( obj.inpMisc) + parseFloat(obj.inpInsurance) + parseFloat(obj.inpOthers) + parseFloat(obj.inpAdjustment) )) + parseFloat(obj.inpBonus);
                    console.log(netProceeds);
                    // alert(netProceeds);

                    $('#mdtxtClientName').val(obj.client_name + ' ' + obj.middle_name + '. ' + obj.last_name);
                    $('#mdtxtProcFee').val(numberWithCommas(obj.inProcFee));
                    $('#mdtxtNotarFee').val(numberWithCommas(obj.inpNotarial));
                    $('#mdtxtInsurance').val(numberWithCommas(obj.inpInsurance));
                    $('#mdtxtAdjustments').val(numberWithCommas(obj.inpAdjustment));
                    $('#mdtxtVoucher').val(obj.voucher_id);
                    $('#mdtxtFindersFee').val(numberWithCommas(obj.inpFindersFee));
                    $('#mdtxtMiscFee').val(numberWithCommas(obj.inpMisc));
                    $('#mdtxtOthersFee').val(numberWithCommas(obj.inpOthers));
                    $('#mdtxtBonus').val(numberWithCommas(obj.inpBonus));
                    $('#mdtxtNetProceeds').val(numberWithCommas(netProceeds.toFixed(2)));
                    $('#mdtxtAmount').val(numberWithCommas(Number(obj.amount).toFixed(2)));
                    $('#md-viewtrans').modal("show");
                },
                error : function(){
                    alert('Error');
                }

            });

        });

    });
  </script>
  <style>
  .error-input {
    border: 1px solid red !important;
  }

  .td_name > div {
    display: none;
  }

  .td_name:hover {
    cursor: pointer;
  }

  .td_name:hover > div{
    display: block;
  }

  .td_name:hover a {
    cursor: pointer;
  }

  .hideme { display: none !important; }

  #frmLoans2 { display: none; }
  </style>

<div class="modal fade" id="md-viewtrans">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title">Details</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Client Name:</label>
                    <input type="text" id="mdtxtClientName" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label>Processing Fee:</label>
                    <input type="text" id="mdtxtProcFee" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label>Notarial Fee:</label>
                    <input type="text" id="mdtxtNotarFee" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label>Insurance:</label>
                    <input type="text" id="mdtxtInsurance" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label>Adjustments:</label>
                    <input type="text" id="mdtxtAdjustments" class="form-control" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Voucher #:</label>
                    <input type="text" id="mdtxtVoucher" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label>Finder's Fee:</label>
                    <input type="text" id="mdtxtFindersFee" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label>Misc Fee:</label>
                    <input type="text" id="mdtxtMiscFee" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label>Others:</label>
                    <input type="text" id="mdtxtOthersFee" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label>Bonus:</label>
                    <input type="text" id="mdtxtBonus" class="form-control" readonly>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Net Proceeds:</label>
                    <input type="text" id="mdtxtNetProceeds" class="form-control" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Amount:</label>
                    <input type="text" id="mdtxtAmount" class="form-control" readonly>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>