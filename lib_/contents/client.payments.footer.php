    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/js/moment/moment.min.js"></script>
    <script src="vendors/js/datepicker/daterangepicker.js"></script>


    <!-- bootstrap progress js -->
    <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>

    <!-- Growl -->
    <script src="includes/js/jquery.growl/javascripts/jquery.growl.js"></script>
    <link href="includes/js/jquery.growl/stylesheets/jquery.growl.css" rel="stylesheet" type="text/css">

    <script src="includes/js/custom.js"></script>
<!-- JAVASCRIPT -->
<script>
$(document).ready(function(){


    $('#btnAddPayment').click(function(){
        $('#modNewPayment').modal('show');
        $('#frmPaymentForm').hide(function(){
            $('#frmPaymentSelection').show(); 
        });
    });

    $('#btnPaymentMethodProceed').click(function(){
        $('#frmPaymentSelection').hide(function(){
            $('#frmPaymentForm').show(); 
            var ptype = $('#selPaymentType').val();
            if(ptype == 'orss_payment'){
                $('#divOR-warning small').show();
                $('#divOR-container label').html('ORS # :');
                generateORS('inpORNum');
            }else{
                $('#divOR-warning small').hide();
                $('#divOR-container label').html('OR # :');
                $('#inpORNum').val('');
            }
        });
    });

    $('#btnPaymentMethodBack').click(function(){
        $('#frmPaymentForm').hide(function(){
            $('#frmPaymentSelection').show(); 
        });
    });

    $('#modNewPayment').on('click', '#btnPayhMents', function(){
        // alert('asda');
        var inpORNum = $('#inpORNum').val();
        var inpClientID = $('#inpClientID').val();
        var inpBalance = $('#inpBalance').val();
        var inpPayAmnt = $('#inpPayAmnt').val();
        var selIntRate = $('#selIntRate').val();
        var selTypeRate = $('#selTypeRate').val();
        // var inpApplyInt = $('#inpApplyInt:checked').val();
        var inpApplyInt;
        if($('#inpApplyInt').prop('checked')){
            inpApplyInt = 'on';
        }else{
            inpApplyInt = '';
        }
        var inpPaymentDate = $('#inpPaymentDate').val();

        var selPaymentType = $('#selPaymentType').val();

        var errm = '';
        if(inpORNum == ''){
            errm += '- Please enter OR # and Client Name.\n';
        }

        if(inpPayAmnt == ''){
            errm += '- Please enter Payment Amount.\n';
        }

        if(selTypeRate == '-'){
            errm += '- Please select Type of Rate.\n';
        }

        if(inpORNum !== '' && inpPayAmnt !== '' && selTypeRate !== '-'){
            $.ajax({
                type : 'post',
                url  : 'lib/api/payment.php',
                data : {
                    'inpORNum' : inpORNum,
                    'inpClientID' : inpClientID,
                    'inpBalance' : inpBalance,
                    'inpPayAmnt' : inpPayAmnt,
                    'selIntRate' : selIntRate,
                    'selTypeRate' : selTypeRate,
                    'inpApplyInt' : inpApplyInt,
                    'inpPaymentDate' : inpPaymentDate,
                    'selPaymentType' : selPaymentType
                },
                // dataType : 'json',
                success : function(result){
                    console.log('Payment Processed: '+result);
                    alert('Payment proccess ' + result.message + '!\n Page will reload in 2secs.');
                    $('#modNewPayment').modal('hide');
                    setTimeout(function(){
                        window.location.reload();
                    },2000);
                }
            });
        }else{
            alert(errm);
        }

    });

    $('#modNewPayment').on('change', '#selPaymentType', function(){
        var ptype = $(this).val();
        var divOR = $('#divOR-container');

        if(ptype == 'orss_payment'){
            $('#divOR-warning small').show();
            $('#divOR-container label').html('ORS # :');
            generateORS('inpORNum');
        }else{
            $('#divOR-warning small').hide();
            $('#divOR-container label').html('OR # :');
            $('#inpORNum').val('');
        }
    });

    $('#inpPaymentDate').daterangepicker({
      singleDatePicker: true,
      calender_style: "picker_1",
      showDropdowns: true,
      "opens": "center",
      "drops": "up",
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
    });

    $('#inpPayAmnt').focus(function(){
        $(this).select();
    });

    $('#inpPayAmnt').blur(function(){
        var val = $(this).val();
        val = Number(val);
        $(this).val(val.toFixed(2));
    });

    $('#frmInpDstart').daterangepicker({
      singleDatePicker: true,
      calender_style: "picker_1",
      showDropdowns: true,
      "opens": "center",
      "drops": "down",
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
    });

    $('#frmInpDend').daterangepicker({
      singleDatePicker: true,
      calender_style: "picker_1",
      showDropdowns: true,
      "opens": "center",
      "drops": "down",
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
    });


    $('#btnLoadRange').click(function(){
        var dstart = $('#frmInpDstart').val();
        var dend_ = $('#frmInpDend').val();
        var cid = $(this).data('id');
        window.location = 'dashboard.php?page=client_payment&axn=load&cid='+ cid +'&dstart=' + dstart + '&dend=' + dend_;
    });

    $('#btnReloadPage').click(function(){
        var cid = $(this).data('id');
        window.location = 'dashboard.php?page=client_payment&cid='+ cid ;
    });

    $('#btnPrintMe').click(function(){
        window.print();
    });
});
</script>