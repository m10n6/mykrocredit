 <?php

 // print_r($_SESSION);
 ?>
 <div class="row">
 	<div class="col-md-12">
 		<div id="alert_message"></div>

 		<button class="btn btn-primary" type="button" id="btnAddNew"><i class="fa fa-plus"></i> Add New Client </button>

 		<hr/>
		<ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" href="#active">Active Accounts</a></li>
		  <li><a data-toggle="tab" href="#inactive">InActive Accounts</a></li>
		</ul>

		<div class="tab-content">
		  <div id="active" class="tab-pane fade in active">
			<!-- -->
		    <h3>ACTIVE ACCOUNTS</h3>
		    <div class="row">
		    	<div class="col-md-12">
			 		<table class="table table-striped" id="tblClientList">
			 			<thead>
			 				<tr>
			 					<th>ID</th>
			 					<th>Name</th>
			 					<th>Contact #</th>
			 					<th>Address</th>
			 					<th>Company</th>
			 					<th></th>
			 				</tr>
			 			</thead>
			 			<tfoot>
			 				<tr>
			 					<th>ID</th>
			 					<td>Name</td>
			 					<td>Contact #</td>
			 					<td>Address</td>
			 					<td>Company</td>
			 					<th></th>
			 				</tr>
			 			</tfoot>
			 			<tbody>
			 				<tr>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 				</tr>
			 			</tbody>
			 		</table>

		    	</div>
		    </div>
		    <!-- -->
		  </div>
		  <div id="inactive" class="tab-pane fade">

		    <!-- -->
		    <h3>INACTIVE ACCOUNTS</h3>
		    <div class="row">
		    	<div class="col-md-12">
			 		<table class="table table-striped" id="tblClientListInActive" style="width: 100% !important;">
			 			<thead>
			 				<tr>
			 					<th>ID</th>
			 					<th>Name</th>
			 					<th>Contact #</th>
			 					<th>Address</th>
			 					<th>Company</th>
			 					<th></th>
			 				</tr>
			 			</thead>
			 			<tfoot>
			 				<tr>
			 					<th>ID</th>
			 					<td>Name</td>
			 					<td>Contact #</td>
			 					<td>Address</td>
			 					<td>Company</td>
			 					<th></th>
			 				</tr>
			 			</tfoot>
			 			<tbody>
			 				<tr>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 					<td></td>
			 				</tr>
			 			</tbody>
			 		</table>
		    	</div>
		    </div>
		    <!-- -->

		  </div>
		</div>


		<div style="clear: both"></div>
 	</div>
 </div>


 <!-- Modal -->
<div id="modClient" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New Client</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-md-12">


	        	<form id="frmPersonDetails">

					<div>
					  <div class="row">
					  	<div class="col-md-12">
                          <div class="checkbox pull-right">
                            <label>
                              <input type="checkbox" id="chkIsInActive" class="flat" > INACTIVE ACCOUNT
                            </label>
                          </div>
                          <input type="hidden" id="txtIsInActive" value="off">
					  	</div>
					  </div>
					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">Personal Info</a></li>
					    <li role="presentation"><a href="#guarantor" aria-controls="guarantor" role="tab" data-toggle="tab">Guarantor Info</a></li>
					    <li role="presentation"><a href="#atm" aria-controls="atm" role="tab" data-toggle="tab">ATM/Card Details</a></li>
					    <li role="presentation"><a href="#others" aria-controls="others" role="tab" data-toggle="tab">Others</a></li>
					  </ul>

					  <!-- Tab panes -->
					  <div class="tab-content">
					    <div role="tabpanel" class="tab-pane active" id="personal">
		        			<h2>Personal Info</h2>
		        			<div class="row">

		        				<div class="col-md-6">

		        					<div class="row">
		        						<div class="col-md-5">
					        				<div class="form-group">
					        					<label>First Name</label>
					        					<input type="text" id="txtName" class="form-control" />
					        					<input type="hidden" id="txtCID" class="form-control" />
					        				</div>
				        				</div>

				        				<div class="col-md-2">
					        				<div class="form-group">
					        					<label>M.I.</label>
					        					<input type="text" id="txtmname" class="form-control" maxlength="1"/>
					        				</div>
				        				</div>

				        				<div class="col-md-5">
					        				<div class="form-group">
					        					<label>Last Name</label>
					        					<input type="text" id="txtlname" class="form-control" />
					        				</div>
				        				</div>
			        				</div>

			        				<div class="form-group">
			        					<label>Civil Status</label>
			        					<!-- <input type="text" id="txtCivil" class="form-control" /> -->
			        					<select id="txtCivil" class="form-control">
			        						<option value="-">-select-</option>
			        						<option value="single">Single</option>
			        						<option value="married">Married</option>
			        						<option value="widow">Widow</option>
			        						<option value="widower">Widower</option>
			        						<option value="separated">Separated</option>
			        					</select>
			        				</div>	  
			        				<div class="form-group">
			        					<label>Date of Birth <small>(yyyy-mm-dd)</small></label>
			        					<input type="text" id="txtDBirth" class="form-control" />
			        				</div>	  

			        				<div class="form-group">
			        					<label>Address</label>
			        					<input type="text" id="txtAddress" class="form-control" />
			        				</div>
			        				<div class="form-group">
			        					<label>Contact #</label>
			        					<input type="text" id="txtContact" class="form-control" />
			        				</div>
			        				<div class="form-group">
			        					<label>SSS#/ TIN</label>
			        					<input type="text" id="txtSSS" class="form-control" />
			        				</div>	
		        				</div>

		        				<div class="col-md-6">
			        				<div class="form-group">
			        					<label>Company</label>
			        					<input type="text" id="txtCompany" class="form-control" />
			        				</div>	  
			        				<div class="form-group">
			        					<label>Company Address</label>
			        					<input type="text" id="txtCompanyAddress" class="form-control" />
			        				</div>	  

			        				<div class="form-group">
			        					<label>Spouse</label>
			        					<input type="text" id="txtS_name" class="form-control" />
			        				</div>	  

			        				<div class="form-group">
			        					<label>Contact #</label>
			        					<input type="text" id="txtS_contact" class="form-control" />
			        				</div>	

			        				<div class="form-group">
			        					<label>Parents Name</label>
			        					<input type="text" id="txtP_name" class="form-control" />
			        				</div>	

			        				<div class="form-group">
			        					<label>Parents Address</label>
			        					<input type="text" id="txtP_address" class="form-control" />
			        				</div>	

		        				</div>

		        			</div>	
					    </div>
					    <div role="tabpanel" class="tab-pane" id="guarantor">
	        				<h2>Guarantor Info</h2>
		        			<div class="row">
		        				<div class="col-md-6">

			        				<div class="form-group">
			        					<label>Name</label>
			        					<input type="text" id="txtG_name" class="form-control" />
			        				</div>

			        				<div class="form-group">
			        					<label>Address</label>
			        					<input type="text" id="txtG_address" class="form-control" />
			        				</div>
			        				<div class="form-group">
			        					<label>Contact #</label>
			        					<input type="text" id="txtG_contactnum" class="form-control" />
			        				</div>
			        				<div class="form-group">
			        					<label>SSS#/ TIN</label>
			        					<input type="text" id="txtG_SSS" class="form-control" />
			        				</div>

		        				</div>

		        				<div class="col-md-6">

		        				</div>
		        			</div>
					    </div>
					    <div role="tabpanel" class="tab-pane" id="atm">
		        			<h2>ATM / Card Details</h2>
		        			<div class="row">
		        				<div class="col-md-6">

			        				<div class="form-group">
			        					<label>Card #1</label>
			        					<input type="text" id="txtCard1" class="form-control" />
			        				</div>	 
			        				<div class="form-group">
			        					<label>PIN</label>
			        					<input type="text" id="txtPin1" class="form-control" />
			        				</div>	 

			        			</div>
			        			<div class="col-md-6">

			        				<div class="form-group">
			        					<label>Card #2</label>
			        					<input type="text" id="txtCard2" class="form-control" />
			        				</div>	 
			        				<div class="form-group">
			        					<label>PIN</label>
			        					<input type="text" id="txtPin2" class="form-control" />
			        				</div>	 


			        				<div class="form-group">
			        					<label>Card #3</label>
			        					<input type="text" id="txtCard3" class="form-control" />
			        				</div>	 
			        				<div class="form-group">
			        					<label>PIN</label>
			        					<input type="text" id="txtPin3" class="form-control" />
			        				</div>	 

			        			</div>

			        		</div>
					    </div>
					    <div role="tabpanel" class="tab-pane" id="others">
					    	<div class="row">
					    		<div class="col-md-6">
				        			<div class="col-md-12">

				        				<div class="form-group">
				        					<label>Credit Limit</label>
				        					<input type="text" id="txtCreditLimit" class="form-control" />
				        				</div>	 

				        			</div>

				        			<div class="col-md-12">
				        				<div class="form-group">
				        					<label>Beginning Balance (for Old Clients)</label>
				        					<input type="text" id="txtBeginningBal" class="form-control" />
				        				</div>	 

				        			</div>
				        			<div class="col-md-12">
				        				<div class="form-group">
				        					<label>Type Of Rate</label>
				        					<select id="txtBegBalTypeRate" class="form-control">
						        				<option value="diminishing">Diminishing Rate</option>
												<option value="flat">Flat Rate</option>
				        					</select>
				        				</div>
				        			</div>	
				        			<div class="col-md-12">
				        				<div class="form-group">
				        					<label>Interest Rate</label>
				        					<select id="txtBegBalIntRate" class="form-control">
				        						<option value="4">4</option>
				        						<option value="3.5">3.5</option>
				        						<option value="3">3</option>
				        						<option value="2.5">2.5</option>
				        						<option value="2">2</option>
				        						<option value="1.5">1.5</option>
				        						<option value="1">1</option>
				        						<option value=".5">.5</option>
				        						<option value="0">0</option>
				        					</select>
				        				</div>
				        			</div>	
				        			<div class="col-md-12">
				        				<div class="form-group">
				        					<label>Beginning Balance Date <small>(yyyy-mm-dd)</small></label>
				        					<input type="text" class="form-control" id="txtBegBalDate" />

				        				</div>
				        			</div>
				        			

				        			
				        			<div class="col-md-12" <?php if($_SESSION['ulevel'] != '1'){ echo 'style="display: none;"';  }  ?> >
				        				<strong>Additional Credit: </strong>
				        				<div class="form-group">
				        					<input type="text" id="txtAdditionalBalance" class="form-control">
				        				</div>
				        			</div>	

				        		</div>
			        			<div class="col-md-6">
				        			<div class="col-md-12">
				        				<div class="form-group">
				        					<label>Type Of Clients</label>
				        					<select id="txtClientType" class="form-control">
				        						<option value="regular">Regular</option>
				        						<option value="casual">Casual</option>
				        						<option value="employee">Employee</option>
				        					</select>
				        				</div>
				        			</div>	

				        			<div class="col-md-12">
				        				<div class="form-group" style="display:none;">
				        					<label>Payment Schedule when to START interest</label>
				        					<input type="text" class="form-control" id="txtInterestSchedStart" />
				        					<!-- <select id="txtPaymentSched" class="form-control">
				        						<option value="1">1st (15th of Month)</option>
				        						<option value="2">2nd (End of Month)</option>
				        					</select> -->
				        				</div>
				        				<div class="form-group">
				        					<label>Apply Interest every: </label>
				        					<select id="txtPaymentSched" class="form-control">
				        						<option value="1">1st (15th of Month)</option>
				        						<option value="2">2nd (End of Month)</option>
				        					</select>
				        				</div>

				        				<div class="form-group">
				        					<label>Payment Amount: </label>
				        					<input type="text" class="form-control" id="txtActualPaymentAmount" />
				        					<!-- <select id="txtPaymentSched" class="form-control">
				        						<option value="1">1st (15th of Month)</option>
				        						<option value="2">2nd (End of Month)</option>
				        					</select> -->
				        				</div>
				        			</div>
			        			</div>
			        		</div>

			        		<div class="row">
			        			<div class="col-md-6">
			        				<strong>Total Limit : </strong>
			        				<span id="spnTotalLimit"></span>
			        			</div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-6">
			        				<strong>Current Balance : </strong>
			        				<span id="spnCurrentBal"></span>
			        			</div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-6">
			        				<strong>Loanable Amount : </strong>
			        				<span id="spnLoanable"></span>
			        			</div>
			        		</div>
					    </div>
					  </div>

					</div>


<!-- 	        		<div class="row">
	        			<div class="col-md-6">
			        		
			        		<div class="form-group">
			        			<label>Type:</label>
			        			<select class="form-control" id="selClientType">
			        				<option value="nl">New Loan</option>
			        				<option value="rl">Renewal</option>
			        				<option value="al">Additional Loans</option>
			        				<option value="sl">Special Loans</option>
			        			</select>
			        		</div>

	        			</div>
	        		</div>
	        		<hr/> -->

	        	</form>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-primary" id="btnSaveClient"> <i class="fa fa-save"></i> SAVE </button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 5px;">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Process A/R -->
<div class="modal fade" role="dialog" id="modLoans">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Loans</h4>

      </div>
      <div class="modal-body">
        <!-- <p>Modal body text goes here.</p> -->
        <h5 id="dispClientName"></h5>
        <div class="row">
	        <div class="col-md-12">
	        	<form id="frmLoans1">
	        		<input type="hidden" id="inpHiddenUID" value="" />

	        		<div class="form-group">
	        			<label>Types of Loans</label>
	        			<select id="selTypeLoans" class="form-control">
<!-- 	        				<option value="new_loans">New Loan</option>
	        				<option value="cash_advance">Cash Advance</option>
	        				<option value="special_account">Special Account</option>
	        				<option value="additional_loan">Additional Loan</option>
	        				<option value="renewal">Renewal</option>
	        				<option value="excess">Excess</option> -->
	        			</select>
	        		</div>
	        		<div class="form-group">
	        			<button type="button" id="btnSelectLoan" class="btn btn-primary">Proceed <i class="fa fa-arrow-right" aria-hidden="true"></i> </button>
	        		</div>
	        	</form>

	        	<form id="frmLoans2">
	        		<div class="form-group">
	        			<label>Loan Amount</label>
	        			<input type="text" id="inpLoanAmount" class="form-control" placeholder="0.00">
	        		</div>
	        		<div class="form-group">
	        			<label>Interest Rate</label>
	        			<select id="selIntRate" class="form-control">
	        				<option value="4">4%</option>
	        				<option value="3.5">3.5%</option>
							<option value="3">3%</option>
							<option value="2">2%</option>
							<option value="2.5">2.5%</option>
							<option value="1">1%</option>
	        			</select>
	        		</div>
	        		<div class="form-group">
	        			<label>Type of Rate</label>
	        			<select id="selTypeRate" class="form-control">
	        				<option value="-">-select type-</option>
	        				<option value="diminishing">Diminishing Rate</option>
							<option value="flat">Flat Rate</option>
	        			</select>
	        		</div>

					<div class="form-group">
					    <label for="term">Term</label>
					    <input type = 'text' id = 'inpTerm' class = 'form-control' name = 'term' >
					</div>

					<div class="form-group">
					    <label for="guarantor">Guarantor</label>
					    <input type = 'text' id = 'inpGuarantor' class = 'form-control' name = 'guarantorName' >
					</div>

					<div class="form-group hideme">
					    <label for="bonus">Bonuses</label>
					    <input type = 'text' id = 'inpBonus' class = 'form-control' name = 'bonuses' >
					</div>

					<div class="form-group">
						<label for="dol">Date of Loan <small>(yyyy-mm-dd)</small></label>
						<input type = 'text' id = 'inpDOL' class = 'form-control datepicker' name = 'loanDate' value="" >
						<!-- <input type = 'text' id = 'inpDOL' class = 'date-picker form-control' name = 'loanDate' value="" > -->
						
					</div>

					<div class="form-group">
						<button type="button" id="btnBackSelectLoan" class="btn btn-primary"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
						<!-- <button type="button" id="btnBackSelectLoan" class="btn btn-primary">Back</button> -->
					</div>
	        	</form>

	        	<form id="frmExcess">
					<div class="form-group">
					    <label for="guarantor">Refund Amount:</label>
					    <input type = 'number' id = 'inpRefundAmount' class = 'form-control' name = '' min="0" >
					</div>
					<div class="form-group">
						<label for="dol">Transaction Date <small>(yyyy-mm-dd)</small></label>
						<input type = 'text' id = 'inpDOL2' class = 'form-control datepicker' name = 'loanDate' value="" >
						<!-- <input type = 'text' id = 'inpDOL' class = 'date-picker form-control' name = 'loanDate' value="" > -->
						
					</div>
					<div class="form-group">
						<button type="button" id="btnBackSelectLoan2" class="btn btn-primary"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
						<!-- <button type="button" id="btnBackSelectLoan" class="btn btn-primary">Back</button> -->
					</div>
	        	</form>
	        </div>
        </div>

       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnProcessLoans" disabled>Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 5px;">Close</button>
      </div>
    </div>
  </div>
</div>


<style>
/*.modal-lg {
	width: 1200px;
}
*/
.hideme {
	display: none !important; 
}

.show-calendar {
	z-index: 99999;
}

.prev.available > .icon {
	background-image: none !important;
}

.next.available > .icon {
	background-image: none !important;
}
</style>