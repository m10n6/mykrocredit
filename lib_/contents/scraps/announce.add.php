<?php
  $gid = secure_get('a_id');
  if(!empty($gid)){
    include_once('config.php');
    include_once('lib/funcjax.php');
    $res = $conn->dbquery("SELECT * FROM `announcements` WHERE `a_id` = '".$gid."' " );
    $res = json_decode($res);
    $res = json_decode($res->data[0]);
    // echo $res->gospel; 
  }
?>
<div class="row">

	<div class="col-md-12">

      <div class="col-md-4">
          <form>
              <div class="form-group">
                  <label class="control-label">Announcement</label>
                  <textarea id="txtGospel" class="form-control"><?php echo $res->announcements; ?></textarea>
                  <input type="hidden" id="hgos_id" value="<?php echo $gid; ?>" />                  
              </div>

              <input type="button" class="btn btn-primary" name="save" value="SAVE" />

              <a href="dashboard.php?page=announce" class="btn btn-default" >CANCEL</a>
          </form>
      </div>

	</div>


</div>

<script type="text/javascript">
$(document).ready(function(){
  $('input[name=save]').click(function(){
    var txtg = $('#txtGospel').val();
    var gid = $('#hgos_id').val();
    if(txtg == ''){
      alert('Please enter Announcement for today!');
    }else{
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'saveAnnounce',
          announce : txtg,
          a_id : gid
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Announcement successfully saved!');
              $('#txtGospel').val('');
              $('#hgos_id').val('');
            }
        } 
      });      
    }

  });
});
</script>