<div class="row">

	<div class="col-md-12">
    <button id="" onclick="PrintMe('datatable')" class="btn btn-warning"><i class="fa fa-print fa-lg"></i> PRINT REPORT</button>
    <hr/>
          <table id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Event & Venue</th>
                <th>Details</th>
                <th>Date</th>
                <!-- <th>End Date</th> -->
                <th>Created By</th>
                <th>Assigned To</th>
                <th>Status</th>
                <th>Amount</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="tblAssets">
              <?php
                include_once('config.php');
                include_once('lib/funcjax.php');
                $startdate = date("Y-m-d")." "."00:00:00";
                $enddate = date("Y-m-d")." "."23:59:59";
                $res = $conn->dbquery("SELECT * FROM `reservation` order by `res_id` desc" );
                $res = json_decode($res);
                $res = $res->data;
                if(count($res) > 0){
                  // print_r($res);

                  $total_ = 0;
                  foreach ($res as $reservation) {
                    # code...
                    /*
                    `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                    */
                    $json_reservation = json_decode($reservation);
                    //<td>'.$json_reservation->res_id.'</td>
                    if(empty($json_reservation->amount) || $json_reservation->amount == ''){
                      $json_reservation->amount = 0;
                    }
                    
                    $button = '';
                    if($json_reservation->payment_type != 'full_payment' && $json_reservation->status == 'approved'){
                      $button = '<button type="button" onclick="fullpaySched(\''.$json_reservation->res_id.'\')" class="btn btn-success"><i class="fa fa-money"></i> Full Payment</button>';
                    }
                    echo '
                      <tr>
                        <td>'.$json_reservation->res_id.'</td>
                        <td>'.$json_reservation->name.'</td>
                        <td>'.$json_reservation->phone.'</td>
                        <td>'.$json_reservation->email.'</td>
                        <td><strong>Event: </strong>'.$json_reservation->event.'<br/><strong>Venue: </strong>'.translateVenue($json_reservation->venue).'</td>
                        <td>'.$json_reservation->details.'</td>
                        <td>'.$json_reservation->start_date.'</td>
                        
                        <td>'.translateName($json_reservation->insert_by, $conn).'</td>
                        <td>'.translateName($json_reservation->assigned_to, $conn).'</td>
                        <td>'.$json_reservation->status.'</td>
                        <td>'.$json_reservation->amount.'</td>
                        <td>
                          <button onclick="editSched(\''.$json_reservation->res_id.'\')" class="btn btn-primary"><i class="fa fa-pencil"></i> Update</button>
                          '.$button.'
                          <button onclick="printInv(\''.$json_reservation->res_id.'\');" class="btn btn-warning"><i class="fa fa-print"></i> Print Invoice</button>
                        </td>
                      </tr>
                    ';
                    /*
                          <button onclick="pendingSched(\''.$json_reservation->res_id.'\')" class="btn btn-warning"><i class="fa fa-pause"></i> Pending</button>
                          <button onclick="approveSched(\''.$json_reservation->res_id.'\')" class="btn btn-success"><i class="fa fa-check"></i> Approved</button>
                          <button onclick="deleteSched(\''.$json_reservation->res_id.'\')" class="btn btn-danger"><i class="fa fa-trash"></i> Cancelled</button>
                    */
                    $total_ = $total_ + $json_reservation->amount;
                  }                  
                }else{
                  //<td></td>
                    echo '
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    ';
                }
              ?>
            </tbody>
            <tfoot>
              <td></td>
              <td align="right" colspan="9"><strong>Total:</strong></td>
              <td align="left" style="white-space: nowrap;"></td>
              <td></td>
            </tfoot>
          </table>

	</div>


</div>

<!-- Modal -->
<div class="modal fade" id="myPaymentFull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Full Payment Info</h4>
      </div>
      <div class="modal-body">
          <form>
              <div class="form-group">
                  <label class="control-label">Amount</label>
                  <input type="text" id="amount_payedfull" value="" class="form-control" />   
                  <input type="hidden" id="hu_id" value="" />                  
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnSavePaymentFull" style="margin-top: -5px;">Save</button>
      </div>
    </div>
  </div>
</div>


<script>
$(document).ready(function(){
  $('#btnSavePaymentFull').click(function(){
        var am = $('#amount_payedfull').val();
        var rid = $('#hu_id').val();
        
        // if(isNan(am)){
        if(am != ''){
            $.ajax({
              type: 'post',
              url: 'api/api.php',
              data: {
                action: 'fullPayment',
                res_id : rid,
                amount : am,
                ptype : 'full_payment'
              },
              beforeSend: function(xhr){

              },
              success: function(xhr){
                  console.log(xhr);
                  if(xhr == 'success'){
                    alert('Schedule successfully paid in Full!');
                    // location.reload();
                    var win = window.open('invoice/index.php?resid='+rid, '_blank');
                    if (win) {
                        //Browser has allowed it to be opened
                        win.focus();
                    } else {
                        window.location = 'invoice/index.php?resid='+rid;
                        //Browser has blocked it
                        alert('Please allow popups for this website');

                    }
                    window.location = 'dashboard.php?page=schedules';
                  }else{
                    alert(xhr);
                  }
              } 
            }); 
        }else{
            alert('Please enter amount!');
        }

  });
});

function fullpaySched(id){
  $('#hu_id').val(id);
  $('#myPaymentFull').modal('show');
}

function deleteSched(gid){
  var q = confirm('Are you sure you want to delete?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'deleteSchedule',
          res_id : gid
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Schedule successfully deleted!');
              location.reload();
            }
        } 
      });    
  }
}

function editSched(gid){
  window.location = 'dashboard.php?page=schedules_add&res_id=' + gid;
}

function PrintMe(sel_id){
 $("#" + sel_id).printThis({
        debug: false,               //* show the iframe for debugging
        importCSS: false,            //* import page CSS
        importStyle: false,         //* import style tags
        printContainer: true,       //* grab outer container as well as the contents of the selector
        loadCSS: ["lupit/admin/includes/css/custom_print.css","admin/includes/css/custom_print.css"],                //* path to additional css file - us an array [] for multiple
        pageTitle: "Booking Report",              //* add title to print page
        removeInline: false,        //* remove all inline styles from print elements
        printDelay: 900,            //* variable print delay; depending on complexity a higher value may be necessary
        header: null,               //* prefix to html
        formValues: true            //* preserve input/form values
    });
}

function printInv(rid){
    var win = window.open('invoice/index.php?resid='+rid, '_blank');
    if (win) {
        //Browser has allowed it to be opened
        win.focus();
    } else {
        window.location = 'invoice/index.php?resid='+rid;
        //Browser has blocked it
        alert('Please allow popups for this website');

    }
}
</script>