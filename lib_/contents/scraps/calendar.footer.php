  <script src="includes/js/bootstrap.min.js"></script>

  <script src="includes/js/nprogress.js"></script>
  
  <!-- bootstrap progress js -->
  <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="includes/js/icheck/icheck.min.js"></script>

  <script src="includes/js/custom.js"></script>

  <script src="includes/js/moment/moment.min.js"></script>
  <script src="includes/js/calendar/fullcalendar.min.js"></script>
  <!-- pace -->
  <script src="includes/js/pace/pace.min.js"></script>
  <script>
    $(window).load(function() {

      var date = new Date();
      var d = date.getDate();
      var m = date.getMonth();
      var y = date.getFullYear();
      var started;
      var categoryClass;

      var calendar = $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
          $('#fc_create').click();

          started = start;
          ended = end

          $(".antosubmit").on("click", function() {
            var title = $("#title").val();
            if (end) {
              ended = end
            }
            categoryClass = $("#event_type").val();

            if (title) {
              calendar.fullCalendar('renderEvent', {
                  title: title,
                  start: started,
                  end: end,
                  allDay: allDay
                },
                true // make the event "stick"
              );
            }
            $('#title').val('');
            calendar.fullCalendar('unselect');

            $('.antoclose').click();

            return false;
          });
        },
        eventClick: function(calEvent, jsEvent, view) {
          //alert(calEvent.title, jsEvent, view);

          $('#fc_edit').click();
          $('#title2').val(calEvent.title);
          categoryClass = $("#event_type").val();

          $(".antosubmit2").on("click", function() {
            calEvent.title = $("#title2").val();

            calendar.fullCalendar('updateEvent', calEvent);
            $('.antoclose2').click();
          });
          calendar.fullCalendar('unselect');
        },
        editable: true,
        events: [
        <?php
          include_once('config.php');
          include_once('lib/funcjax.php');

          $user_id = secure_get('uid');
          $axn = secure_get('axn');



          if(empty($user_id) && empty($axn)){
              $res6 = $conn->dbquery("SELECT * FROM `reservation` where `status` = 'confirmed' or `status` = 'approved' order by `created_date`" );
              $res6 = json_decode($res6);
              $res6 = $res6->data;

              if(count($res6) > 0){
                
                 foreach($res6 as $rs6){
                    $rs1 = json_decode($rs6);

                    
                    $ev = $rs1->event;
                    // if($rs1->event == 'mass'){
                      $sql1 = "select * from `schedule` where `title` = '". $ev."' and `start_date`='".$rs1->start_date."'";
                      $resc1 = $conn->dbquery($sql1);
                      $resc1 = json_decode($resc1);
                      $resc1 = $resc1->data;                    
                      $mem = '';
                      foreach($resc1 as $rss1){
                          
                          $rs123 = json_decode($rss1);
                          $mem .=  translateName($rs123->user_id,$conn).' - ['.translateUlevel($rs123->ulevel).']\n';
                      }                      
                    // }


                    $xt .= '{';
                    $xt .= 'title:\'Event: '.$ev.'\nDetails: '.$rs1->details.'\nPriest: '.translateName($rs1->assigned_to, $conn).'\nVenue: '.translateVenue($rs1->venue).'\nStatus: '.$rs1->status.'\n'.$mem.'\',';
                    $xt .= 'start: new Date("'.$rs1->start_date.'"),';
                    $xt .= 'end: new Date("'.$rs1->end_date.'")';
                    $xt .= '},';
                 }
                 echo rtrim($xt, ",");
              }
          }else{

              if($axn == 'usercal'){
                  $ulevel = getUser_levelid($user_id, $conn);
                  // echo $ulevel.'<br>';
                  // echo $user_id;
                  switch($ulevel){
                    case "2":
                        $res = $conn->dbquery("SELECT * FROM `reservation` where `assigned_to` = '".$user_id."' and (`status` = 'confirmed' or `status` = 'approved') order by `created_date`" );
                        $res = json_decode($res);
                        $res = $res->data;

                        if(count($res) > 0){
                          

                           foreach($res as $rs){
                              $rs1 = json_decode($rs);

                              // if($rs1->event == 'mass'){
                              $ev = $rs1->event;
                                $sql1 = "select * from `schedule` where `title` = '".$ev."' and `start_date`='".$rs1->start_date."'";
                                $resc1 = $conn->dbquery($sql1);
                                $resc1 = json_decode($resc1);
                                $resc1 = $resc1->data;                    
                                
                                $mem = '';
                                foreach($resc1 as $rss1){
                                    $rs123 = json_decode($rss1);
                                    $mem .=  translateName($rs123->user_id,$conn).' - ['.translateUlevel($rs123->ulevel).']\n';
                                }                      
                              // }

                              $xt .= '{';
                              $xt .= 'title:\'Event: '.$ev.'\nDetails: '.$rs1->details.'\nPriest: '.translateName($rs1->assigned_to, $conn).'\nVenue: '.translateVenue($rs1->venue).'\nStatus: '.$rs1->status.'\n'.$mem.'\',';
                              $xt .= 'start: new Date("'.$rs1->start_date.'"),';
                              $xt .= 'end: new Date("'.$rs1->end_date.'")';
                              $xt .= '},';
                           }
                           echo rtrim($xt, ",");
                        }
                    break;
                    // case "3":
                    //     $res = $conn->dbquery("SELECT * FROM `schedule` where `user_id` = '".$user_id."' order by `created_date` desc" );
                    //     $res = json_decode($res);
                    //     $res = $res->data;

                    //     if(count($res) > 0){
                          
                    //        foreach($res as $rs){
                    //           $rs1 = json_decode($rs);
                    //           $xt .= '{';
                    //           $xt .= 'title:\'Event: '.$rs1->event.'\nDetails: '.$rs1->details.'\nPriest: '.translateName($rs1->assigned_to, $conn).'\nVenue: '.translateVenue($rs1->venue).'\nStatus: '.$rs1->status.'\',';
                    //           $xt .= 'start: new Date("'.$rs1->start_date.'"),';
                    //           $xt .= 'end: new Date("'.$rs1->end_date.'")';
                    //           $xt .= '},';
                    //        }
                    //        echo rtrim($xt, ",");
                    //     }
                    // break;

                    default:
                        $res = $conn->dbquery("SELECT * FROM `schedule` where `user_id` = '".$user_id."' order by `created_date` desc" );
                        $res = json_decode($res);
                        $res = $res->data;

                        if(count($res) > 0){
                          
                           foreach($res as $rs){
                              $rs1 = json_decode($rs);
                              $xt .= '{';
                              $xt .= 'title:\'Event: '.$rs1->title.'\',';
                              $xt .= 'start: new Date("'.$rs1->start_date.'"),';
                              $xt .= 'end: new Date("'.$rs1->end_date.'")';
                              $xt .= '},';
                           }
                           echo rtrim($xt, ",");
                        }
                    break;
                  }                
              }


          }

        ?>
        // {
        //   title: 'All Day Event',
        //   start: new Date(y, m, 1)
        // }, {
        //   title: 'Long Event',
        //   start: new Date(y, m, d - 5),
        //   end: new Date(y, m, d - 2)
        // }, {
        //   title: 'Meeting',
        //   start: new Date(y, m, d, 10, 30),
        //   allDay: false
        // }, {
        //   title: 'Lunch',
        //   start: new Date(y, m, d + 14, 12, 0),
        //   end: new Date(y, m, d, 14, 0),
        //   allDay: false
        // }, {
        //   title: 'Birthday Party',
        //   start: new Date(y, m, d + 1, 19, 0),
        //   end: new Date(y, m, d + 1, 22, 30),
        //   allDay: false
        // }, {
        //   title: 'Click for Google',
        //   start: new Date(y, m, 28),
        //   end: new Date(y, m, 29),
        //   url: 'http://google.com/'
        // }
        ]
      });
    });
  </script>
  <!-- /footer content -->