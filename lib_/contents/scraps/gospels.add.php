<?php
  $gid = secure_get('gos_id');
  if(!empty($gid)){
    include_once('config.php');
    include_once('lib/funcjax.php');
    $res = $conn->dbquery("SELECT * FROM `daily_gospel` WHERE `gos_id` = '".$gid."' " );
    $res = json_decode($res);
    $res = json_decode($res->data[0]);
    // echo $res->gospel; 
  }
?>
<div class="row">

	<div class="col-md-12">

      <div class="col-md-4">
          <form>
              <div class="form-group">
                  <label class="control-label">Gospel</label>
                  <textarea id="txtGospel" class="form-control"><?php echo $res->gospel; ?></textarea>
                  <input type="hidden" id="hgos_id" value="<?php echo $gid; ?>" />                  
              </div>

              <input type="button" class="btn btn-primary" name="save" value="SAVE" />

              <a href="dashboard.php?page=gospels" class="btn btn-default" >CANCEL</a>
          </form>
      </div>

	</div>


</div>

<script type="text/javascript">
$(document).ready(function(){
  $('input[name=save]').click(function(){
    var txtg = $('#txtGospel').val();
    var gid = $('#hgos_id').val();
    if(txtg == ''){
      alert('Please enter Gospel for today!');
    }else{
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'saveDailyGospel',
          gospel : txtg,
          gos_id : gid
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Gospel successfully saved!');
              $('#txtGospel').val('');
              $('#hgos_id').val('');
            }
        } 
      });      
    }

  });
});
</script>