<div class="row">
  <div class="col-md-12">
    <button id="" onclick="PrintMe('datatable')" class="btn btn-warning"><i class="fa fa-print fa-lg"></i> PRINT REPORT</button>
    <button data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-upload fa-lg"></i> UPLOAD AUDIO</button>
    <hr/>
          <table id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>FileName</th>
                <th>Date Uploaded</th>
                <th class="td_audio">Audio</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="tblAssets-audio">
              <?php


                    $folder = "uploads";
                    $files1 = scandir($folder, 0);
                    if(count($files1) > 0){
                            foreach($files1 as $fcont){
                                    $fpath = $folder. DIRECTORY_SEPARATOR . $fcont;
                                    if(file_exists($fpath) && is_dir($fpath)){
                                    }else{
                                            $len = strlen($fcont);
                                            // if($len > 17){
                                            //         $link = substr($fcont, 0, 18);
                                            // }else{
                                            //         $spacelen = abs($len - 18);
                                            //         $link = $fcont.str_repeat('&nbsp;', $spacelen);
                                            // }
                                            $fileTypes = array('mp3', 'ogg', 'wav'); // File extensions
                                            $fileParts = pathinfo($fpath);

                                            $audio = '';
                                            if ($fileParts['extension'] == 'mp3') {
                                                $audio .= '<source src="'.$fpath.'" type="audio/mpeg">';
                                            }
                                            
                                            if ($fileParts['extension'] == 'ogg') {
                                                $audio .= '<source src="'.$fpath.'" type="audio/ogg">';
                                            }
                                            
                                            if ($fileParts['extension'] == 'wav') {
                                                $audio .= '<source src="'.$fpath.'" type="audio/wav">';
                                            }

                                            echo '
                                              <tr>
                                                <td>'.$fcont.'</td>
                                                <td>'.date("Y-m-d H:i:s", filemtime($fpath)).'</td>
                                                <td class="td_audio">
                                                      <audio controls>
                                                        '.$audio.'
                                                        Your browser does not support the audio element.
                                                      </audio>
                                                </td>
                                                <td>
                                                  <a href="#" onclick="deleteFile(\''.urlencode($fcont).'\'); return false;" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                                </td>
                                              </tr>
                                            ';
                                            // echo $link."
                                            // <a href=\"javascript:void(0);\" onclick=\"deleteFile('".urlencode($fcont)."'); return false;\" style=\"margin-left: 10px;\">Remove</a>
                                            // <a href=\"javascript:void(0);\" onclick=\"sendLink('".urlencode($fcont)."'); return false;\" style=\"margin-left: 10px;\">Email Link</a>
                                            // <a href=\"http://geekbacolod.com/uploads/".$fcont."\" target=\"_blank\" style=\"margin-left: 10px;\">View</a>
                                            // ".date("Y-m-d H:i:s", filemtime($fpath))."
                                            // <br/>";
                                    }
                            }
                    }
              ?>
            </tbody>

          </table>
  </div>
</div>

<script>
function PrintMe(sel_id){
   $("#" + sel_id).printThis({
          debug: false,               //* show the iframe for debugging
          importCSS: false,            //* import page CSS
          importStyle: false,         //* import style tags
          printContainer: true,       //* grab outer container as well as the contents of the selector
          loadCSS: ["lupit/admin/includes/css/custom_print.css","admin/includes/css/custom_print.css"],                //* path to additional css file - us an array [] for multiple
          pageTitle: "Audio Files",              //* add title to print page
          removeInline: false,        //* remove all inline styles from print elements
          printDelay: 2000,            //* variable print delay; depending on complexity a higher value may be necessary
          header: null,               //* prefix to html
          formValues: true            //* preserve input/form values
    });

}

var deleteFile = function(fiName){
  var q = confirm("Are you sure you want to delete this file?");
  if(q){
    $.ajax({
      type: 'post',
      url : 'lib/deletefile.php',
      data: {
        f: fiName
      },
      success : function(response){
        location.reload();
      },
      beforeSend: function(xhr){
      }
    });
  }
};
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">File Upload</h4>
      </div>
      <div class="modal-body">
          <form>
              <div class="form-group">
                  <!-- <label class="control-label">File</label> -->
                  <input type="file" id="file_upload" name="file_upload" />   
                  <div id="progress"></div>
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>