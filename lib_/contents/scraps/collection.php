<div class="row">
  <div class="col-md-12">
    <button id="" onclick="addCollection();" class="btn btn-success"><i class="fa fa-money fa-lg"></i> ADD COLLECTION</button>

    <button id="" onclick="PrintMe('datatable_donate')" class="btn btn-warning"><i class="fa fa-print fa-lg"></i> PRINT REPORT</button>
    <hr/>
          <table id="datatable_donate" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Collector</th>
                <th>Date</th>
                <th>Amount</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="tblAssets_donate">
              <?php
                include_once('config.php');
                include_once('lib/funcjax.php');
                $res = $conn->dbquery("SELECT * FROM `collections` order by `c_id` desc" );
                $res = json_decode($res);
                $res = $res->data;
                if(count($res) > 0){
                  // print_r($res);
                  $total_amount = 0;
                  foreach ($res as $usersdata) {
                    # code...
                    /*
                    `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                    */
                    $json_usersdata = json_decode($usersdata);
                    // <td>'.$json_usersdata->don_id.'</td>
                    // <td>'.$json_usersdata->account_num.'</td>
                    $invalid_button_status = '';
                    $valid_button_status = '';
                    if($json_usersdata->status == 'invalid'){
                      $invalid_button_status = 'disabled';
                    }

                    if($json_usersdata->status == 'valid'){
                      $valid_button_status = 'disabled';
                    }

                    if($_SESSION['ulevel'] == '1'){
                       $b = '<a href="#" class="btn btn-primary" onclick="updateCollection(\''.$json_usersdata->c_id.'\',\''.$json_usersdata->user_id.'\', \''.$json_usersdata->amount.'\');"><i class="fa fa-pencil"></i> Update</a>';
                    }else{
                       $b = '<a href="#" class="btn btn-primary" onclick="updateCollection(\''.$json_usersdata->c_id.'\',\''.$_SESSION['userID'].'\', \''.$json_usersdata->amount.'\');"><i class="fa fa-pencil"></i> Update</a>';
                    }
                    echo '
                      <tr>
                        <td>'.$json_usersdata->c_id.'</td>
                        <td>'.translateName($json_usersdata->user_id, $conn).'</td>
                        <td>'.$json_usersdata->date_added.'</td>
                        <td align="right">'.$json_usersdata->amount.'</td>
                        <td>
                        '.$b.'
                        </td>
                      </tr>
                    ';
                    /*
                        <a href="#" class="btn btn-danger" onclick="removeCollection('.$json_usersdata->c_id.');"><i class="fa fa-trash"></i> Remove</a>
                        <a href="#" class="btn btn-danger" onclick="validDonate('.$json_usersdata->don_id.');"><i class="fa fa-trash"></i> Valid</a>
                        <a href="#" class="btn btn-danger" onclick="invalidDonate('.$json_usersdata->don_id.');"><i class="fa fa-trash"></i> Invalid</a>


                        <a href="#" class="btn btn-danger" onclick="delDonate('.$json_usersdata->don_id.');"><i class="fa fa-trash"></i> Valid</a>
                        <a href="#" class="btn btn-danger" onclick="delDonate('.$json_usersdata->don_id.');"><i class="fa fa-trash"></i> Invalid</a>
                    */
                    // <a href="#" class="btn btn-danger" onclick="delDonate('.$json_usersdata->don_id.');"><i class="fa fa-trash"></i> Remove</a>
                    // $total_amount = $total_amount + $json_usersdata->amount;
                  }                  
                }else{
                  //<td></td>
                  //<td></td>
                    echo '
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    ';
                }
              ?>
            </tbody>
              <tfoot>
                <td></td>
                <td></td>
                <td align="right"><strong>Total : </strong></td>
                <td align="right" style="white-space: nowrap;"></td>
                <td></td>
              </tfoot>
          </table>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myCollections" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Todays Collection</h4>
      </div>
      <div class="modal-body">
          <form>
             <div class="form-group">
                  <?php if($_SESSION['ulevel'] == '1'){ ?>
                  <label class="control-label">Collector's Name</label>
                  <select name='col_name' class='form-control'>
                      <?php
                          $res1 = $conn->dbquery("SELECT * FROM `users` WHERE `ulevel` = '4'" );
                          $res1 = json_decode($res1);
                          $res1 = $res1->data;
                          echo '<option value="0">-Select-</option>';
                          foreach ($res1 as $usersdata) {
                              $json_usersdata = json_decode($usersdata);
                              if($res->assigned_to == $json_usersdata->user_id){
                                $ssel = 'selected';
                              }else{
                                $ssel = '';
                              }
                              echo '
                                <option value="'.$json_usersdata->user_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                              ';
                          }
                      ?>
                  </select>
                  <?php }else{ ?>
                    <input type="hidden" id="hsid" value="<?php echo $_SESSION['userID']; ?>"/>
                  <?php   
                  } ?>
              </div>
              <div class="form-group">
                  <label class="control-label">Amount</label>
                  <input type="text" id="amount_payed" value="" class="form-control" />   
                  <input type="hidden" id="hu_id" value="<?php echo $gid; ?>" />                  
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnSave" style="margin-top: -5px;">Save</button>
      </div>
    </div>
  </div>
</div>


<script>

function updateCollection(cid, uid, amntval){
  $('#amount_payed').val(amntval);
  $('#hu_id').val(cid);
  $('select[name=col_name]').val(uid);
  setTimeout(function(){
    $('#myCollections').modal('show');
  }, 1000);
}

function addCollection(){
  $('#myCollections').modal('show');
}

function removeCollection(cid){
  var q = confirm('Are you sure you want to remove?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'removeCollection',
          cid_ : cid
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Collection has been removed!');
              location.reload();
            }else{
              alert(xhr);
            }
        } 
      });    
  }
}


function PrintMe(sel_id){
 $("#" + sel_id).printThis({
        debug: false,               //* show the iframe for debugging
        importCSS: false,            //* import page CSS
        importStyle: false,         //* import style tags
        printContainer: true,       //* grab outer container as well as the contents of the selector
        loadCSS: [/*"lupit/admin/includes/css/custom_print.css",*/"admin/includes/css/custom_print.css?v=<?php echo date('YmdHis'); ?>"],                //* path to additional css file - us an array [] for multiple
        pageTitle: "Donation Report",              //* add title to print page
        removeInline: false,        //* remove all inline styles from print elements
        printDelay: 1900,            //* variable print delay; depending on complexity a higher value may be necessary
        header: null,               //* prefix to html
        formValues: true            //* preserve input/form values
    });
}

function delDonate(gid){
  var q = confirm('Are you sure you want to delete?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'deleteDonate',
          did : gid
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Donation successfully deleted!');
              location.reload();
            }
        } 
      });    
  }
}

function invalidDonate(id){
  var q = confirm('Are you sure that this is Invalid?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'invalidDonate',
          did : id
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Donation is Invalid!');
              location.reload();
            }
        } 
      });    
  }
}

function validDonate(uid){
  var q = confirm('Are you sure that this is Valid?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'validDonate',
          user_id : uid,
          amount_ : amount
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('Donation is Vaid!');
              location.reload();
            }
        } 
      });    
  }
}

$(document).ready(function(){

  <?php if($_SESSION['ulevel'] == '1'){ ?>
    $('#btnSave').click(function(){
      var uid = $('select[name=col_name]').val();
      var amount = $('#amount_payed').val();
      var hid = $('#hu_id').val();
      if(uid != '0' && amount != ''){
        $.ajax({
          type: 'post',
          url: 'api/api.php',
          data: {
            action: 'addCollection',
            user_id : uid,
            amount_ : amount,
            hid_id  : hid
          },
          beforeSend: function(xhr){

          },
          success: function(xhr){
              console.log(xhr);
              if(xhr == 'success'){
                alert('Collection Saved!');
                location.reload();
              }else{
                alert(xhr);
              }
          } 
        });  
      }else{
        alert('Please select collector and set the amount!');
      } 
    });    
    <?php }else{ ?>
    $('#btnSave').click(function(){
      var uid = $('#hsid').val();
      var amount = $('#amount_payed').val();
      var hid = $('#hu_id').val();
      if(uid != '0' && amount != ''){
        $.ajax({
          type: 'post',
          url: 'api/api.php',
          data: {
            action: 'addCollection',
            user_id : uid,
            amount_ : amount,
            hid_id  : hid
          },
          beforeSend: function(xhr){

          },
          success: function(xhr){
              console.log(xhr);
              if(xhr == 'success'){
                alert('Collection Saved!');
                location.reload();
              }else{
                alert(xhr);
              }
          } 
        });  
      }else{
        alert('Please select collector and set the amount!');
      } 
    });  
    <?php } ?>


});

</script>