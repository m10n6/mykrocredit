  <script src="includes/js/bootstrap.min.js"></script>

  <script src="includes/js/nprogress.js"></script>
  
  <!-- bootstrap progress js -->
  <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="includes/js/icheck/icheck.min.js"></script>

  <script src="includes/js/custom.js"></script>

  <script src="includes/js/moment/moment.min.js"></script>
  <script src="includes/js/calendar/fullcalendar.min.js"></script>

  <!-- Datatables -->
  <!-- <script src="includes/js/datatables/js/jquery.dataTables.js"></script>
<script src="includes/js/datatables/tools/js/dataTables.tableTools.js"></script> -->

  <!-- Datatables-->
  <script src="includes/js/datatables/jquery.dataTables.min.js"></script>
  <script src="includes/js/datatables/dataTables.bootstrap.js"></script>
  <script src="includes/js/datatables/dataTables.buttons.min.js"></script>
  <script src="includes/js/datatables/buttons.bootstrap.min.js"></script>
  <script src="includes/js/datatables/jszip.min.js"></script>
  <script src="includes/js/datatables/pdfmake.min.js"></script>
  <script src="includes/js/datatables/vfs_fonts.js"></script>
  <script src="includes/js/datatables/buttons.html5.min.js"></script>
  <script src="includes/js/datatables/buttons.print.min.js"></script>
  <script src="includes/js/datatables/dataTables.fixedHeader.min.js"></script>
  <script src="includes/js/datatables/dataTables.keyTable.min.js"></script>
  <script src="includes/js/datatables/dataTables.responsive.min.js"></script>
  <script src="includes/js/datatables/responsive.bootstrap.min.js"></script>
  <script src="includes/js/datatables/dataTables.scroller.min.js"></script>

  <script type="text/javascript" src="includes/js/uploadify/jquery.uploadify.js"></script>
  <link rel="stylesheet" type="text/css" href="includes/js/uploadify/uploadify.css" />

  <!-- pace -->
  <script src="includes/js/pace/pace.min.js"></script>
  <script>
    $(window).load(function() {
    });
    
    $(document).ready(function(){
      $('#datatable').dataTable({
        aLengthMenu: [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ]
      });
    });


    $(function() {
        $('#file_upload').uploadify({
            'checkExisting' : 'includes/js/uploadify/check-exists.php',
            'swf'      : 'includes/js/uploadify/uploadify.swf',
            'uploader' : 'includes/js/uploadify/uploadify.php',
            'onUploadComplete' : function(file) {
                $('#progress').html('The file ' + file.name + ' finished processing.');
                location.reload();
            },
            'onUploadSuccess' : function(file, data, response) {
                $('#progress').html('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
            },
            'onUploadProgress' : function(file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal) {
                $('#progress').html(totalBytesUploaded + ' bytes uploaded of ' + totalBytesTotal + ' bytes.');
            }
            // Put your options here
        });
    });    
  </script>
  <!-- /footer content -->