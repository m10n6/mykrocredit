<div class="row">
  <div class="col-md-12">
    <!-- <button id="" onclick="PrintMe('datatable')" class="btn btn-warning"><i class="fa fa-print fa-lg"></i> PRINT REPORT</button> -->
    
    <hr/>
          <table id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>From</th>
                <th>Subject</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="tblAssets">
              <?php
                include_once('config.php');
                include_once('lib/funcjax.php');
                $res = $conn->dbquery("SELECT * FROM `messages` order by `date_added` desc" );
                $res = json_decode($res);
                $res = $res->data;
                if(count($res) > 0){
                  // print_r($res);
                  foreach ($res as $usersdata) {
                    # code...
                    /*
                    `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                    */
                    $json_usersdata = json_decode($usersdata);
                    echo '
                      <tr>
                        <td>'.$json_usersdata->name.'<'.$json_usersdata->email.'></td>
                        <td>'.$json_usersdata->subject.'</td>
                        <td>'.$json_usersdata->date_added.'</td>
                        <td>
                        <a href="#" class="btn btn-primary" onclick="readMsg(\''.$json_usersdata->m_id.'\')"><i class="fa fa-eye"></i> Read</a>
                        <a href="#" class="btn btn-success" onclick="msgReply(\''.$json_usersdata->m_id.'\')"><i class="fa fa-reply"></i> Reply</a>
                        <a href="#" class="btn btn-danger" onclick="msgDel(\''.$json_usersdata->m_id.'\')"><i class="fa fa-trash"></i> Delete</a>
                        </td>
                      </tr>
                    ';

                  }                  
                }else{
                    echo '
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    ';
                }
              ?>
            </tbody>

          </table>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myReply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Reply</h4>
      </div>
      <div class="modal-body">
          <form>
              <div class="form-group">
                  <label class="form-label">To</label>
                  <input type="text" id="remail" class="form-control" />
              </div>
              <div class="form-group">
                  <label class="form-label">Subject</label>
                  <input type="text"id="rsubject" class="form-control" />
              </div>
              <div class="form-group">
                  <label class="form-label">Message</label>
                  <textarea class="form-control" id="rmessage"></textarea>
              </div>
              <div class="form-group">
                  <button type="button" id="btnSaveRep" class="btn btn-primary"><i class="fa fa-send"></i> Send</button>
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Message</h4>
      </div>
      <div class="modal-body">
          <form>
              <div class="form-group">
                  <label class="form-label">Subject</label>
                  <input type="text" id="vsubject" class="form-control" />
              </div>
              <div class="form-group">
                  <label class="form-label">Message</label>
                  <textarea class="form-control" id="vmessage"></textarea>
              </div>
              <div class="form-group">
                  <!-- <button type="button" class="btn btn-primary"><i class="fa fa-send"></i> Send</button> -->
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<script>
function PrintMe(sel_id){
 $("#" + sel_id).printThis({
        debug: false,               //* show the iframe for debugging
        importCSS: false,            //* import page CSS
        importStyle: false,         //* import style tags
        printContainer: true,       //* grab outer container as well as the contents of the selector
        loadCSS: [/*"lupit/admin/includes/css/custom_print.css",*/"admin/includes/css/custom_print.css"],                //* path to additional css file - us an array [] for multiple
        pageTitle: "",              //* add title to print page
        removeInline: false,        //* remove all inline styles from print elements
        printDelay: 900,            //* variable print delay; depending on complexity a higher value may be necessary
        header: null,               //* prefix to html
        formValues: true            //* preserve input/form values
    });
}

var deleteFile = function(fiName){
  var q = confirm("Are you sure you want to delete this file?");
  if(q){
    $.ajax({
      type: 'post',
      url : 'api/api.php',
      data: {
        a
      },
      success : function(response){
        location.reload();
      },
      beforeSend: function(xhr){
      }
    });
  }
};

function readMsg(id){
    $.ajax({
      type: 'post',
      url : 'api/api.php',
      data: {
        action: 'getMsg',
        mid : id
      },
      success : function(xhr){
             console.log(xhr)
            var obj = JSON.parse(xhr);
            var xy = JSON.parse(obj);

            $('#vsubject').val(xy.subject);
            $('#vmessage').val(xy.message);
            $('#myMessage').modal('show');
      },
      beforeSend: function(xhr){
      }
    });
}

function msgReply(id){
    $.ajax({
      type: 'post',
      url : 'api/api.php',
      data: {
        action: 'getMsg',
        mid : id
      },
      success : function(xhr){
             console.log(xhr)
            var obj = JSON.parse(xhr);
            var xy = JSON.parse(obj);
            console.log(xy)
            $('#remail').val(xy.email);
            $('#rsubject').val(xy.subject);
            $('#rmessage').val(xy.message);
            $('#myReply').modal('show');
      },
      beforeSend: function(xhr){
      }
    });
}

function msgDel(id){
  var q = confirm("Are you sure you want to delete this file?");
  if(q){
    $.ajax({
      type: 'post',
      url : 'api/api.php',
      data: {
        action: 'deleteMsg',
        mid : id
      },
      success : function(response){
        location.reload();
      },
      beforeSend: function(xhr){
      }
    });
  }
}

$('#btnSaveRep').click(function(){
      var remail = $('#remail').val();
      var rsubj = $('#rsubject').val();
      var rmsg = $('#rmessage').val();
    $.ajax({
      type: 'post',
      url : 'api/api.php',
      data: {
        action: 'sendMail',
        'email': remail,
        'subj':  rsubj,
        'msg' : rmsg
      },
      success : function(xhr){
          alert(xhr);
      },
      beforeSend: function(xhr){
      }
    });
});

</script>

