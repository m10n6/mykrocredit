<div class="row">

	<div class="col-md-12">
          <!-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add New</a> -->
          <hr/>

          <table id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Userlevel</th>
                <th>UserAccess</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
                include_once('config.php');
                include_once('lib/funcjax.php');
                $res1 = $conn->dbquery("SELECT * FROM `ulevels`");
                $res1 = json_decode($res1);
                $res1 = $res1->data;
                foreach ($res1 as $usersdata) {
                    $json_usersdata = json_decode($usersdata);
                    // if($res->assigned_to == $json_usersdata->user_id){
                    //   $ssel = 'selected';
                    // }else{
                    //   $ssel = '';
                    // }
                    // echo '
                    //   <option value="'.$json_usersdata->level_id.'"  '.$ssel.'>'.$json_usersdata->name.'</option>
                    // ';
                    echo '
                      <tr>
                        <td>'.$json_usersdata->name.'</td>
                        <td>'.$json_usersdata->pages.'</td>
                        <td></td>
                      </tr>
                    ';
                }
                // $res = $conn->dbquery("SELECT * FROM `users`" );
                // $res = json_decode($res);
                // $res = $res->data;
                // if(count($res) > 0){
                //   // print_r($res);
                //   foreach ($res as $usersdata) {
                //     # code...
                //     /*
                //     `res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `insert_by`, `assigned_to`, `status`, `action`
                //     */
                //     $json_usersdata = json_decode($usersdata);
                //     echo '
                //       <tr>
                //         <td>'.$json_usersdata->name.'</td>
                //         <td>'.$json_usersdata->email.'</td>
                //         <td>'.translateUlevel($json_usersdata->ulevel).'</td>
                //         <td>
                //         <a href="#" class="btn btn-success" onclick="editUser(\''.$json_usersdata->user_id.'\')"><i class="fa fa-pencil"></i> Update</a>
                //         <!--<a href="#" class="btn btn-warning" onclick="schedUser(\''.$json_usersdata->user_id.'\')"><i class="fa fa-calendar"></i> Schedule</a>-->
                //         <a href="#" class="btn btn-danger" onclick="delUser(\''.$json_usersdata->user_id.'\')"><i class="fa fa-trash"></i> Remove</a>
                //         </td>
                //       </tr>
                //     ';

                //   }                  
                // }else{
                    // echo '
                    //   <tr>
                    //     <td></td>
                    //     <td></td>
                    //     <td></td>
                    //   </tr>
                    // ';
                // }
              ?>
            </tbody>
          </table>

	</div>

</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">User Level</h4>
      </div>
      <div class="modal-body">
          <form>
              <div class="form-group">
                  <label class="control-label">Name</label>
                  <input type="text" id="name" value="<?php echo $gid; ?>" class="form-control" />   
                  <input type="hidden" id="hu_id" value="<?php echo $gid; ?>" />                  
              </div>
              <div class="form-group">
                  <label class="control-label">Access</label>
                  <select id="uaccess_list" class="form-control">
                    <option value="all">ALL</option>
                    <option value="loans">Loans</option>
                    <option value="payments">Payments</option>
                    <option value="payments">Clients</option>
                  </select>
              </div>
              <div class="form-group">
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnSave" style="margin-top: -5px;"><i class="fa fa-save"></i> Save</button>
      </div>
    </div>
  </div>
</div>

<script>
function delUser(gid){
  var q = confirm('Are you sure you want to delete?');
  if(q){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'deleteUser',
          user_id : gid
        },
        beforeSend: function(xhr){

        },
        success: function(xhr){
            console.log(xhr);
            if(xhr == 'success'){
              alert('User successfully deleted!');
              location.reload();
            }
        } 
      });    
  }
}

function editUser(gid){
      $.ajax({
        type: 'post',
        url: 'api/api.php',
        data: {
          action: 'getUser',
          user_id : gid
        },
        dataType:'json',
        beforeSend: function(xhr){

        },
        success: function(xhr){
            // console.log(xhr);
            var obj = JSON.parse(xhr);
            // console.log(obj.user_id);
            $('#passnote').html('(Please leave empty if you don\'t want to change the password)');
            $('#name').val(obj.name);
            $('#email').val(obj.email);
            $('#pword').val('');
            $('#spost').val(obj.ulevel);
            $('#hu_id').val(obj.user_id);

            $('#myModal').modal('show');
        } 
      });     
  
}

function schedUser(id){
  window.location = 'dashboard.php?page=calendar&uid='+ id +'&axn=usercal';
}

$(document).ready(function(){
  $('#btnSave').click(function(){
      var nname = $('#name').val();
      var nemail = $('#email').val();
      var npwd = $('#pword').val();
      var npost = $('#spost').val();
      var uid = $('#hu_id').val();

      if(uid != ''){
        alert('asd');
        if(nname !='' && nemail != '' ){
            $.ajax({
              type: 'post',
              url : 'api/api.php',
              data : {
                action : 'saveUser',
                nn : nname,
                ne : nemail,
                np : npwd,
                nps : npost,
                ni : uid
              },
              success: function(xhr){
                // console.log(xhr);
                  alert(xhr);
                  location.reload();
              }
            });
        }else{
            alert('Please fill in all fields!');
        }        
      }else{
        if(nname !='' && nemail != '' && npwd != ''){
            $.ajax({
              type: 'post',
              url : 'api/api.php',
              data : {
                action : 'saveUser',
                nn : nname,
                ne : nemail,
                np : npwd,
                nps : npost,
                ni : uid
              },
              success: function(xhr){
                // console.log(xhr);
                  alert(xhr);
                  location.reload();
              }
            });
        }else{
            alert('Please fill in all fields!');
        }
      }
  });
});
</script>