 <?php

 // print_r($_SESSION);
 ?>
 <div class="">
 	<div class="col-md-12">

 		<!-- <button class="btn btn-primary" type="button" id="btnAddNew"><i class="fa fa-plus"></i> Add New Client </button> -->

 		<div class="row">
 			<div class="col-md-8">
 				<div class="row">
			 		<form id="frmVoucherRange">
			 			<div class="col-md-4">
				 			<div class="form-group">	
				 				<label>Date From:</label>
				 				<input type="date" id="frmInpDstart" class="form-control">
				 			</div>
			 			</div>
			 			<div class="col-md-4">
				 			<div class="form-group">	
				 				<label>Date To:</label>
				 				<input type="date" id="frmInpDend" class="form-control">
				 			</div>
			 			</div>
			 			<div class="col-md-4">
				 			<div class="form-group">	
				 				<label>Type of Loans:</label>
			        			<select id="selTypeLoans" class="form-control">
			        				<option value="-">-Select Loan-</option>
			        				<option value="new_loans">New Loan</option>
			        				<option value="cash_advance">Cash Advance</option>
			        				<option value="special_account">Special Account</option>
			        				<option value="additional_loan">Additional Loan</option>
			        				<option value="renewal">Renewal</option>
			        				<option value="excess">Excess</option>
			        				<option value="all">All Loans</option>
			        			</select>
				 			</div>
			 			</div>
<!-- 			 			<div class="col-md-4" style="padding-top: 25px;">
			 				<button type="button" class="btn btn-success"><i class="fa fa-refresh"></i> Load </button>
			 				<button type="button" class="btn btn-danger"><i class="fa fa-square-o"></i> Clear </button>
			 			</div> -->
			 		</form>
		 		</div>
	 		</div>
 			<div class="col-md-4">
 				<div class="row">
			 		<form id="frmVoucherRange">
<!-- 			 			<div class="col-md-4">
				 			<div class="form-group">	
				 				<label>Type of Loans:</label>
			        			<select id="selTypeLoans" class="form-control">
			        				<option value="new_loans">New Loan</option>
			        				<option value="cash_advance">Cash Advance</option>
			        				<option value="special_account">Special Account</option>
			        				<option value="additional_loan">Additional Loan</option>
			        				<option value="renewal">Renewal</option>
			        				<option value="excess">Excess</option>
			        				<option value="all">All Loans</option>
			        			</select>
				 			</div>
			 			</div> -->

			 			<div class="col-md-4" style="padding-top: 25px;">
			 				<button type="button" class="btn btn-success"><i class="fa fa-refresh"></i> Load </button>
			 			</div>
			 		</form>
		 		</div>
	 		</div>
 		</div>


 		<hr/>

 		<table class="table table-striped" id="tblVoucherList">
 			<thead>
 				<tr>
                    <th>Issuance Date</th>
                    <th>Voucher No.</th>
                    <th>Client</th>
                    <th>Amount</th>
                    <th>Processing Fee</th>
                    <th>Finders Fee</th>
                    <th>Notarial</th>
                    <th>Miscellaneous</th>
                    <th>Insurance</th>
                    <th>Others</th>
                    <th>Adjustment</th>
                    <th>Net Proceeds</th>
                    <th class = 'hidden-print'>Action</th>
 				</tr>
 			</thead>
<!--  			<tfoot>
 				<tr>
 					<td>Date</td>
 					<td>Name</td>
 					<td>Company</td>
 					<td>Amount</td>
 				</tr>
 			</tfoot> -->
 			<tbody>
<!--  				<tr>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 				</tr> -->
 				<?php
 				// $sql_list = "select * from `finance` where `status` = 'approved' order by `fid` desc ";
 				$sql_list = "select 
					 `finance`.`fid`, `finance`.`client_id`, `finance`.`intrate`, `finance`.`debit`, `finance`.`credit`, `finance`.`balance`, `finance`.`extra`, `finance`.`status`, `finance`.`loan_type`, `finance`.`voucher_id`, `finance`.`term`, `finance`.`amount`, `finance`.`date_added`, `finance`.`loan_date`, `finance`.`processedby`, `finance`.`cancelledby`, `finance`.`addedby`, `finance`.`date_approved`, `client_data`.`name`, `client_data`.`middle_name`, `client_data`.`last_name`, `client_data`.`company`, `finance`.`inProcFee`, `finance`.`inpFindersFee`, `finance`.`inpNotarial`, `finance`.`inpMisc`, `finance`.`inpInsurance`, `finance`.`inpOthers`,`finance`.`inpAdjustment`, `finance`.`inpBonus`					
					from `finance` 
					INNER JOIN `client_data` 
					ON `finance`.`client_id` = `client_data`.`client_id` 
					where `finance`.`status` = 'approved' order by `finance`.`date_approved` desc
					";

 				$rs_list = $conn->dbquery($sql_list);

				$rs_list = json_decode($rs_list);
				foreach ($rs_list->data as $key) {
				  # code...
				  $nres = json_decode($key);

				  $netProceeds = $nres->amount - ($nres->inProcFee + $nres->inpFindersFee + $nres->Notarial + $nres->inpMisc + $nres->inpInsurance + $nres->inpOthers + $nres->inpAdjustment );
				  echo '
					<tr>
						<td>'.date("m/d/Y", strtotime($nres->date_approved)).'</td>
						<td>'.$nres->voucher_id.'</td>
						<td>'.$nres->name.' '.$nres->middle_name.' '.$nres->last_name.'</td>
						<td align="right">'.number_format ( $nres->amount , 2 , "." , "," ).'</td>
						<td align="right">'.number_format ( $nres->inProcFee , 2 , "." , "," ).'</td>
						<td align="right">'.number_format ( $nres->inpFindersFee , 2 , "." , "," ).'</td>
						<td align="right">'.number_format ( $nres->inpNotarial , 2 , "." , "," ).'</td>
						<td align="right">'.number_format ( $nres->inpMisc , 2 , "." , "," ).'</td>
						<td align="right">'.number_format ( $nres->inpInsurance , 2 , "." , "," ).'</td>
						<td align="right">'.number_format ( $nres->inpOthers , 2 , "." , "," ).'</td>
						<td align="right">'.number_format ( $nres->inpAdjustment , 2 , "." , "," ).'</td>
						<td align="right">'.number_format ( $netProceeds , 2 , "." , "," ).'</td>
						<td><a href="javascript:void(0);" class="btn btn-primary" ><i class="fa fa-pencil"></i></a></td>
					</tr>

				  ';
				  $totalMonthlyProceeds += $netProceeds;
				}
 				?>
 			</tbody>
 		</table>

 		<div class="col-md-6 pull-right text-right">
 			<strong>Monthly Total : <?php echo number_format ( $totalMonthlyProceeds , 2 , "." , "," ); ?> </strong>
 			<div class="spacer30"></div>
 		</div>
 		<div style="clear: both;"></div>

 	</div>
 </div>


 <!-- Modal -->
<div id="modClient" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New Client</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-md-12">

				<div>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">Personal Info</a></li>
				    <li role="presentation"><a href="#guarantor" aria-controls="guarantor" role="tab" data-toggle="tab">Guarantor Info</a></li>
				    <li role="presentation"><a href="#atm" aria-controls="atm" role="tab" data-toggle="tab">ATM/Card Details</a></li>
				    <li role="presentation"><a href="#others" aria-controls="others" role="tab" data-toggle="tab">Others</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="personal">
	        			<h2>Personal Info</h2>
	        			<div class="row">

	        				<div class="col-md-6">

	        					<div class="row">
	        						<div class="col-md-5">
				        				<div class="form-group">
				        					<label>First Name</label>
				        					<input type="text" id="txtName" class="form-control" />
				        				</div>
			        				</div>

			        				<div class="col-md-2">
				        				<div class="form-group">
				        					<label>M.I.</label>
				        					<input type="text" id="txtmname" class="form-control" />
				        				</div>
			        				</div>

			        				<div class="col-md-5">
				        				<div class="form-group">
				        					<label>Last Name</label>
				        					<input type="text" id="txtlname" class="form-control" />
				        				</div>
			        				</div>
		        				</div>

		        				<div class="form-group">
		        					<label>Civil Status</label>
		        					<!-- <input type="text" id="txtCivil" class="form-control" /> -->
		        					<select id="txtCivil" class="form-control">
		        						<option value="-">-select-</option>
		        						<option value="single">Single</option>
		        						<option value="married">Married</option>
		        						<option value="widow">Widow</option>
		        						<option value="widower">Widower</option>
		        						<option value="separated">Separated</option>
		        					</select>
		        				</div>	  
		        				<div class="form-group">
		        					<label>Date of Birth</label>
		        					<input type="date" id="txtDBirth" class="form-control" />
		        				</div>	  

		        				<div class="form-group">
		        					<label>Address</label>
		        					<input type="text" id="txtAddress" class="form-control" />
		        				</div>
		        				<div class="form-group">
		        					<label>Contact #</label>
		        					<input type="text" id="txtContact" class="form-control" />
		        				</div>
		        				<div class="form-group">
		        					<label>SSS#/ TIN</label>
		        					<input type="text" id="txtSSS" class="form-control" />
		        				</div>	
	        				</div>

	        				<div class="col-md-6">
		        				<div class="form-group">
		        					<label>Company</label>
		        					<input type="text" id="txtCompany" class="form-control" />
		        				</div>	  
		        				<div class="form-group">
		        					<label>Company Address</label>
		        					<input type="text" id="txtCompanyAddress" class="form-control" />
		        				</div>	  

		        				<div class="form-group">
		        					<label>Spouse</label>
		        					<input type="text" id="txtS_name" class="form-control" />
		        				</div>	  

		        				<div class="form-group">
		        					<label>Contact #</label>
		        					<input type="text" id="txtS_contact" class="form-control" />
		        				</div>	

		        				<div class="form-group">
		        					<label>Parents Name</label>
		        					<input type="text" id="txtP_name" class="form-control" />
		        				</div>	

		        				<div class="form-group">
		        					<label>Parents Address</label>
		        					<input type="text" id="txtP_address" class="form-control" />
		        				</div>	

	        				</div>

	        			</div>	
				    </div>
				    <div role="tabpanel" class="tab-pane" id="guarantor">
        				<h2>Guarantor Info</h2>
	        			<div class="row">
	        				<div class="col-md-6">

		        				<div class="form-group">
		        					<label>Name</label>
		        					<input type="text" id="txtG_name" class="form-control" />
		        				</div>

		        				<div class="form-group">
		        					<label>Address</label>
		        					<input type="text" id="txtG_address" class="form-control" />
		        				</div>
		        				<div class="form-group">
		        					<label>Contact #</label>
		        					<input type="text" id="txtG_contactnum" class="form-control" />
		        				</div>
		        				<div class="form-group">
		        					<label>SSS#/ TIN</label>
		        					<input type="text" id="txtG_SSS" class="form-control" />
		        				</div>

	        				</div>

	        				<div class="col-md-6">

	        				</div>
	        			</div>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="atm">
	        			<h2>ATM / Card Details</h2>
	        			<div class="row">
	        				<div class="col-md-6">

		        				<div class="form-group">
		        					<label>Card #1</label>
		        					<input type="text" id="txtCard1" class="form-control" />
		        				</div>	 
		        				<div class="form-group">
		        					<label>PIN</label>
		        					<input type="text" id="txtPin1" class="form-control" />
		        				</div>	 

		        			</div>
		        			<div class="col-md-6">

		        				<div class="form-group">
		        					<label>Card #2</label>
		        					<input type="text" id="txtCard2" class="form-control" />
		        				</div>	 
		        				<div class="form-group">
		        					<label>PIN</label>
		        					<input type="text" id="txtPin2" class="form-control" />
		        				</div>	 


		        				<div class="form-group">
		        					<label>Card #3</label>
		        					<input type="text" id="txtCard3" class="form-control" />
		        				</div>	 
		        				<div class="form-group">
		        					<label>PIN</label>
		        					<input type="text" id="txtPin3" class="form-control" />
		        				</div>	 

		        			</div>

		        		</div>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="others">
				    	<div class="row">
		        			<div class="col-md-6">

		        				<div class="form-group">
		        					<label>Credit Limit</label>
		        					<input type="text" id="txtCreditLimit" class="form-control" />
		        				</div>	 

		        			</div>

		        			<div class="col-md-6">
		        				<div class="form-group">
		        					<label>Type Of Clients</label>
		        					<select id="txtClientType" class="form-control">
		        						<option value="regular">Regular</option>
		        						<option value="special">Special</option>
		        						<option value="employee">Employee</option>
		        					</select>

		        				</div>
		        			</div>
		        		</div>
				    </div>
				  </div>

				</div>

	        	<form>
<!-- 	        		<div class="row">
	        			<div class="col-md-6">
			        		
			        		<div class="form-group">
			        			<label>Type:</label>
			        			<select class="form-control" id="selClientType">
			        				<option value="nl">New Loan</option>
			        				<option value="rl">Renewal</option>
			        				<option value="al">Additional Loans</option>
			        				<option value="sl">Special Loans</option>
			        			</select>
			        		</div>

	        			</div>
	        		</div>
	        		<hr/> -->

	        	</form>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-primary" id="btnSaveClient"> <i class="fa fa-save"></i> SAVE </button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 5px;">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Process A/R -->
<div class="modal fade" role="dialog" id="modLoans">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Loans</h4>

      </div>
      <div class="modal-body">
        <!-- <p>Modal body text goes here.</p> -->
        <div class="row">
	        <div class="col-md-12">
	        	<form id="frmLoans1">
	        		<div class="form-group">
	        			<label>Types of Loans</label>
	        			<select id="selTypeLoans" class="form-control">
	        				<option value="new_loans">New Loan</option>
	        				<option value="cash_advance">Cash Advance</option>
	        				<option value="renewal">Renewal</option>
	        				<option value="excess">Excess</option>
	        			</select>
	        		</div>
	        		<div class="form-group">
	        			<button type="button" id="btnSelectLoan" class="btn btn-primary">Proceed <i class="fa fa-arrow-right" aria-hidden="true"></i> </button>
	        		</div>
	        	</form>

	        	<form id="frmLoans2">
	        		<div class="form-group">
	        			<label>Loan Amount</label>
	        			<input type="text" id="inpLoanAmount" class="form-control" placeholder="0.00">
	        		</div>
	        		<div class="form-group">
	        			<label>Interest Rate</label>
	        			<select id="selIntRate" class="form-control">
	        				<option value="4">4%</option>
	        				<option value="3.5">3.5%</option>
							<option value="3">3%</option>
							<option value="2">2%</option>
							<option value="2.5">2.5%</option>
							<option value="1">1%</option>
	        			</select>
	        		</div>
	        		<div class="form-group">
	        			<label>Type of Rate</label>
	        			<select id="selTypeRate" class="form-control">
	        				<option value="-">-select type-</option>
	        				<option value="dimishing">Diminishing Rate</option>
							<option value="flat">Flat Rate</option>
	        			</select>
	        		</div>

					<div class="form-group">
					    <label for="term">Term</label>
					    <input type = 'text' id = 'inpTerm' class = 'form-control' name = 'term' >
					</div>

					<div class="form-group">
					    <label for="guarantor">Guarantor</label>
					    <input type = 'text' id = 'inpGuarantor' class = 'form-control' name = 'guarantorName' >
					</div>

					<div class="form-group hideme">
					    <label for="bonus">Bonuses</label>
					    <input type = 'text' id = 'inpBonus' class = 'form-control' name = 'bonuses' >
					</div>

					<div class="form-group">
						<label for="dol">Date of Loan</label>
						<input type = 'date' id = 'inpDOL' class = 'form-control datepicker' name = 'loanDate' >
					</div>

					<div class="form-group">
						<button type="button" id="btnBackSelectLoan" class="btn btn-primary"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
						<!-- <button type="button" id="btnBackSelectLoan" class="btn btn-primary">Back</button> -->
					</div>
	        	</form>
	        </div>
        </div>

       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnProcessLoans" disabled>Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 5px;">Close</button>
      </div>
    </div>
  </div>
</div>