
<?php
$client_id = secure_get('cid');
//applyZeroPayments($client_id, true);
//deleteExtraZeroPayments($client_id, false);
?>
 <div class="row">
 	<div class="col-md-12">

 		<a class="btn btn-primary  hidden-print" href="dashboard.php?page=clients_list"  ><i class="fa fa-arrow-left"></i> Back / Cancel </a>

 		<?php if($_SESSION['ulevel'] != '2'):  ?>
 		<button class="btn btn-success  hidden-print" type="button" id="btnAddPayment" data-fid="<?php echo secure_get('cid'); ?>" ><i class="fa fa-plus"></i> New Entry </button>
 		<?php endif; ?>

 		<a class="btn btn-warning  hidden-print" href="dashboard.php?page=viewdata&cid=<?php echo secure_get('cid'); ?>"><i class="fa fa-eye"></i> View Data</a>

 		<a class="btn btn-warning pull-right  hidden-print" id="btnPrintMe" ><i class="fa fa-print"></i> Print </a>
 		<hr class="hidden-print"/>

 		<div class="row hidden-print">
 			<div class="col-md-6">
 				<div class="col-md-4">
 					<div class="form-group">
 						<label>Date From:</label>
 						<input type="text" id="frmInpDstart" class="form-control">
 					</div>
 				</div>
 				<div class="col-md-4">
 					<div class="form-group">
 						<label>Date To:</label>
 						<input type="text" id="frmInpDend" class="form-control">
 					</div>
 				</div>
 				<div class="col-md-4">
 					<div class="spacer25"></div>
 					<button type="button" class="btn btn-success" id="btnLoadRange" data-id="<?php echo secure_get('cid'); ?>" ><i class="fa fa-refresh"></i> Load</button>
 				</div>
 			</div>
 			<div class="col-md-6">
 				<div class="spacer25"></div>
 				<button type="button" class="btn btn-success" id="btnReloadPage" data-id="<?php echo secure_get('cid'); ?>" ><i class="fa fa-refresh"></i> Reload Payments</button>
 			</div>
 		</div>
 		<hr class="hidden-print"/>
 		<table class="table table-striped" id="tblPaymentSheet" >
 			<thead>
		        <tr>
		            <th>Date</th>
		            <th>OR#</th>
		            <th>Interest Rate</th>
		            <th>Interest</th>
		            <th>Amount</th>
		        </tr>
 			</thead>
 			<!-- <tfoot>
 				<tr>
 					<td>Account Code</td>
 					<td>Account Name</td>
 					<td>Category</td>
 					<td>Action</td>
 				</tr>
 			</tfoot> -->
 			<tbody>
  				<?php
  					$axn = secure_get('axn');
  					if(empty($axn)){
 						$sql_query = "select * from `finance` where `client_id` = '".secure_get('cid')."' and `loan_type` like 'payment%' and
 									(`voucher_id` != '000000' and `voucher_id` != '0000000000') order by `date_approved` asc";  						
  					}else{
  						$date_start = secure_get('dstart');
  						$date_end = secure_get('dend');
						$sql_query = "select * from `finance` where `client_id` = '".secure_get('cid')."' and `loan_type` like 'payment%' and
 									(`voucher_id` != '000000' and `voucher_id` != '0000000000') and
 									(`date_approved` between '".$date_start." 00:00:00'  and '".$date_end." 23:59:59' ) order by `date_approved` asc";  						
  					}

  					// echo $sql_query;
 					$res = $conn->dbquery($sql_query);
 					// print_r($res);
 					if($res !== 'false'){
						$res = json_decode($res);
						foreach ($res->data as $key) {
							# code...
							$nres = json_decode($key);
							$extra = json_decode($nres->extra);
							if($extra->TypeRate == 'diminishing' || $extra->TypeRate == '-'){
								$type_rate = 'DR';
							}else{
								$type_rate = 'FR';
							}

							$totalPaidAmount += $nres->amount;
		 					echo '
				 				<tr>
				 					<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
				 					<td>'.$nres->voucher_id.'</td>
				 					<td>'.$nres->intrate.'%</td>
				 					<!--<td>'.$nres->intrate.'</td>-->
				 					<td>'.$type_rate.'</td>
				 					<td>'.number_format($nres->amount,2).'</td>
				 				</tr>
		 					';
						}
					}else{
	 					echo '
			 				<tr>
			 					<td colspan="5" align="center">No Payment Yet!</td>
			 				</tr>
	 					';
					}

  				// 	$sql = "select * from `finance` where `client_id` = '".secure_get('cid')."' and `status` = 'approved' and `loan_type` like 'payment%' order by fid asc";
  				// 	$res = $conn->dbquery($sql);
 					// if($res !== 'false'){
						// $res = json_decode($res);
						// foreach ($res->data as $key) {
						// 	# code...
						// 	$nres = json_decode($key);

						// 	$extra = json_decode($nres->extra);
						// 	$balance = '0';
						// 	$debit = $nres->amount;
						// 	$credit = '0';
						// 	$interest = '0';
						// 	$ORAmnt = '0';

						// 	if($nres->loan_type == 'new_loans'){
						// 		$balance = $nres->amount;
						// 		$debit = '0';
						// 	}else{
						// 		$balance = $debit;
						// 	}

						// 	$new_balance += $balance;

						// 	echo '
				 	// 			<tr>
				 	// 				<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
				 	// 				<td>'.valueToWords($nres->loan_type).' - CV#: '.$nres->voucher_id.'</td>
				 	// 				<td>'.$nres->intrate.'% - '.ucwords($extra->TypeRate).'</td>
				 	// 				<td>'.number_format($ORAmnt, 2).'</td>
				 	// 				<td>'.number_format($interest, 2).'</td>
				 	// 				<td>'.number_format($debit, 2).'</td>
				 	// 				<td>'.number_format($credit, 2).'</td>
				 	// 				<td>'.number_format($new_balance, 2).'</td>
				 	// 			</tr>
						// 	';	
						// }
 					// }else{
 					// 	echo '
			 		// 		<tr>
			 		// 			<td></td>
			 		// 			<td></td>
			 		// 			<td></td>
			 		// 			<td></td>
			 		// 			<td></td>
			 		// 			<td></td>
			 		// 			<td></td>
			 		// 			<td></td>
			 		// 		</tr>
 					// 	';
 					// }

 				?>
 			<tfoot>
 				<tr>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td style="font-weight: bold;">Total Paid Amount:</td>
 					<td style="font-weight: bold;"><?php echo number_format($totalPaidAmount, 2); ?></td>
 				</tr>
 			</tfoot>
 			</tbody>
 		</table>
 	
 	</div>

 </div>

<?php
	$cid = secure_get('cid');
?>


<!-- Modal -->
<div class="modal fade" role="dialog" id="modNewPayment">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">PAYMENT</h4>
      </div>
      <div class="modal-body">
      	
        <form id="frmPaymentSelection">
        	<div class="row">
        		<div class="col-md-12">
		        	<div class="form-group">
		        		<label>Payment Type</label>
		        		<select id="selPaymentType" class="form-control">
		        			<option value="or_payment">Manual Entry(OR)</option>
		        			<option value="orss_payment">System Generated(ORS)</option>
		        		</select>
		        	</div>
		        	<button type="button" class="btn btn-primary" id="btnPaymentMethodProceed" >Proceed <i class="fa fa-arrow-right"></i></button>
	        	</div>
        	</div>
        </form>
        <form id="frmPaymentForm">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="row">
        				<div class="col-md-12" id="divOR-warning">
        					<small style="color: red">ORS # may change / not, after processing. To avoid double ORS #.</small>
        				</div>
        			</div>
        			<div class="row">
	        			<div class="col-md-6">
				        	<div class="form-group" id="divOR-container">
				        		<label>OR # :</label>
				        		<input type="text" id="inpORNum" class="form-control">
				        	</div>
				        	<div class="form-group">
				        		<label>Client :</label>
				        		<input type="text" id="inpClientName" class="form-control" value="<?php echo getClientName($cid); ?>">
				        		<!-- <input type="hidden" id="inpClientID" class="form-control"> -->
				        		<input type="hidden" id="inpClientID" value="<?php echo secure_get('cid'); ?>">
				        	</div>
				        	<div class="form-group">
				        		<label>Balance :</label>
				        		<!-- <input type="text" id="inpBalance" class="form-control" value="<?php echo number_format($new_balance, 2); ?>"> -->
				        		<input type="text" id="inpBalance" class="form-control" value="<?php echo getClientBalance($cid); ?>" readonly>
				        	</div>
				        	<div class="form-group">
				        		<label>Amount :</label>
				        		<input type="text" id="inpPayAmnt" class="form-control">
				        	</div>
			        	</div>

			        	<!-- 
						getStartRate
						getStartTypeRate
			        	-->
			        	<div class="col-md-6">
				        	<div class="form-group">
				        		<label>Interest Rate :</label>
			        			<select id="selIntRate" class="form-control">
			        				<option value="4" <?php echo(getStartRate($cid) == "4")? "selected": ""; ?> >4%</option>
			        				<option value="3.5" <?php echo(getStartRate($cid) == "3.5")? "selected": ""; ?> >3.5%</option>
									<option value="3" <?php echo(getStartRate($cid) == "3")? "selected": ""; ?> >3%</option>
									<option value="2" <?php echo(getStartRate($cid) == "2")? "selected": ""; ?> >2%</option>
									<option value="2.5" <?php echo(getStartRate($cid) == "2.5")? "selected": ""; ?> >2.5%</option>
									<option value="1" <?php echo(getStartRate($cid) == "1")? "selected": ""; ?> >1%</option>
			        			</select>
				        	</div>
			        		<div class="form-group">
			        			<label>Type of Rate</label>
			        			<select id="selTypeRate" class="form-control">
			        				<option value="-">-select type-</option>
			        				<option value="diminishing" <?php echo(getStartTypeRate($cid) == "diminishing")? "selected": ""; ?> >Diminishing Rate</option>
									<option value="flat"  <?php echo(getStartTypeRate($cid) == "flat")? "selected": ""; ?> >Flat Rate</option>
			        			</select>
			        		</div>
				        	<div class="form-group hideme">
				        		<label>Apply Interest :</label>
				        		<input type="checkbox" id="inpApplyInt" class="form-control">
				        	</div>
				        	<div class="form-group">
				        		<label>Payment Date :</label>
				        		<input type="text" id="inpPaymentDate" class="form-control">
				        	</div>
			        	</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-md-12">
		        			<button type="button" class="btn btn-primary pull-left" id="btnPaymentMethodBack" ><i class="fa fa-arrow-left"></i> Back</button>
		        		</div>
		        	</div>
	        	</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnPayhMents" style="margin-top:-5px;"><i class="fa fa-save"></i> Save Payment</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div class="modal fade" role="dialog" id="modSetInterestStart">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Interest Rate Schedule</h4>
      </div>
      <div class="modal-body">
        <form id="frmPaymentSched">
        	<div class="row">
        		<div class="col-md-12">
		        	<div class="form-group">
		        		<label>Start Date</label>
		        		<input type="text" class="form-control" >
		        		<input type="hidden" id="inpPS_ClientID" value="<?php echo secure_get('cid'); ?>">
		        	</div>
	        	</div>
        	</div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnRateSchedule"><i class="fa fa-save"></i>Save Schedule</button>
      </div>
    </div>

  </div>
</div>
<style>
/*.modal-lg {
	width: 1200px;
}
*/
.hideme {
	display: none !important; 
}

.show-calendar {
	z-index: 99999;
}

.prev.available > .icon {
	background-image: none !important;
}

.next.available > .icon {
	background-image: none !important;
}

@media print {
	.col-md-12 {
		width: 100% !important;
	}
	#tblPaymentSheet {
		width: 100% !important;
	}
}
</style>
<?php
// applyZeroPayments(secure_get('cid'));
?>