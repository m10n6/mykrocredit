    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/js/moment/moment.min.js"></script>
    <script src="vendors/js/datepicker/daterangepicker.js"></script>

    <!-- bootstrap progress js -->
    <script src="includes/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="includes/js/nicescroll/jquery.nicescroll.min.js"></script>



    <!-- Datatables-->
    <script src="includes/js/datatables/jquery.dataTables.min.js"></script>
    <script src="includes/js/datatables/dataTables.bootstrap.js"></script>
    <script src="includes/js/datatables/dataTables.buttons.min.js"></script>
    <script src="includes/js/datatables/buttons.bootstrap.min.js"></script>
    <script src="includes/js/datatables/jszip.min.js"></script>
    <script src="includes/js/datatables/pdfmake.min.js"></script>
    <script src="includes/js/datatables/vfs_fonts.js"></script>
    <script src="includes/js/datatables/buttons.html5.min.js"></script>
    <script src="includes/js/datatables/buttons.print.min.js"></script>
    <script src="includes/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="includes/js/datatables/dataTables.keyTable.min.js"></script>
    <script src="includes/js/datatables/dataTables.responsive.min.js"></script>
    <script src="includes/js/datatables/responsive.bootstrap.min.js"></script>
    <script src="includes/js/datatables/dataTables.scroller.min.js"></script>

    <!-- Growl -->
    <script src="includes/js/jquery.growl/javascripts/jquery.growl.js"></script>
    <link href="includes/js/jquery.growl/stylesheets/jquery.growl.css" rel="stylesheet" type="text/css">

    <script src="includes/js/custom.js"></script>