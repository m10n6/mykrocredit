
<?php
$client_id = secure_get('cid');
$axn = secure_get('axn');

if(!empty($axn) && $axn == 'recalc'){
	$sql = "delete from `finance` where `client_id` = '".$client_id."' and `voucher_id` = '0000000000'";
	$conn->dbquery($sql);
}

applyZeroPayments($client_id, true);
sleep(5);
deleteExtraZeroPayments($client_id, false);
// exit();
$interest_psched = getClientSchedPayment($client_id);

switch($interest_psched){
	case '1':
		$sched = 15;
	break;

	case '2':
		$sched = 30;
	break;
}

$int_sched_start = getClientSchedPaymentStart($client_id)." 00:00:00";
?>
 <div class="row">
 	<div class="col-md-12">
 		<a class="btn btn-primary hidden-print" href="dashboard.php?page=clients_list" ><i class="fa fa-arrow-left"></i> BACK </a>
 		<a class="btn btn-success hidden-print" href="dashboard.php?page=client_payment&cid=<?php echo $client_id; ?>"><i class="fa fa-money"></i> Payments </a>
 		<a class="btn btn-warning pull-right hidden-print" id="btnReCalc"><i class="fa fa-calculator"></i> Re-Calculate </a>
 		<a class="btn btn-warning pull-right  hidden-print" id="btnPrintMe" ><i class="fa fa-print"></i> Print </a>
 		<!-- <button class="btn btn-primary" type="button" id="btnAddNew"><i class="fa fa-plus"></i> Add New Account </button> -->

 		<hr/>
 		<span style="display: none;"><strong>Date Start of Interest: </strong><?php echo date("m/d/Y", strtotime($int_sched_start)); ?><br/></span>
 		<strong>Client : </strong><?php echo getClientName($client_id); ?><br>
		<strong>Interest Applied every : </strong><?php echo $sched.'th of the month'; ?>
		<div class="spacer20"></div>

 		<table class="table table-striped" id="tblUserLogs">
 			<thead>
 				<tr>
 					<th>Date</th>
 					<th>Check Voucher #</th>
 					<th>Particulars</th>
 					<!-- <th>Loan Amount</th> -->
 					<!-- <th>Beg. Balance</th> -->
 					<th>OR#/ORS#</th>
 					<th>Payment Amount</th>
 					<!-- <th>Int. Rate</th> -->
 					<th>Interest</th>
 					<th>Debit</th>
 					<th>Credit</th>
 					<th>Balance</th>
 					<!-- <th class="hidden-print">Action</th> -->
 				</tr>
 			</thead>
 			<!-- <tfoot>
 				<tr>
 					<td>Account Code</td>
 					<td>Account Name</td>
 					<td>Category</td>
 					<td>Action</td>
 				</tr>
 			</tfoot> -->
 			<tbody>
 				<!-- <tr>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 				</tr> -->
  				<?php


  				$isActive = getIsInActive($client_id);
  				// echo $isActive;
  				//get is InActive to DISPLAY PAYMENTS ONLY
  				if(!empty($isActive) && $isActive == 'on'){ //is Active OFF or EMPTY


					$sqlBB = "select * from `client_meta` where `meta` = 'txtBeginningBal' and `client_id` = '".$client_id."' ";
					$rsBB = $conn->dbquery($sqlBB);
					// $rs1 = json_decode($rs1);
					// echo $rsBB;
					if($rsBB !== 'false'){
						
						$rsBB = json_decode($rsBB);
						$rsBB = json_decode($rsBB->data[0]);
						$beg_bal = $rsBB->value;	
						if(!empty($beg_bal)){
							$is_beg = true;
						}
					}

 					$sql = "select * from `finance` where `client_id` = '".$client_id."' and `status` = 'approved' order by `date_approved`, `fid` asc";
 					$res = $conn->dbquery($sql);
 					if($res !== 'false'){
						$res = json_decode($res);

						$int_ctr = 0; //counter for applied interest


						foreach ($res->data as $key) {
							# code...
							// echo $ctr;
							$nres = json_decode($key);
							$extra = json_decode($nres->extra);
							$loan_type = $nres->loan_type;
							$loan_amnt = $nres->amount;
							$loan_amount_disp = $loan_amnt;

							// print_r($end_bal);
							if($loan_type == 'payment' || $loan_type == 'payment_ors'){
								
								$loan_amnt = number_format(0,2);
								if($ctr == 0){
									$loan_start = $loan_amnt;
									if(!empty($beg_bal)){
									// if($is_beg == true){
										$loan_start = $beg_bal;		
										$loan_amnt = $beg_bal;
										$balance = $loan_amnt;
									}
								}

								$or_num = $nres->voucher_id;

								$payment = $nres->amount;

								$balance = end($end_bal) - $payment;
								$voucher_num = '-';

								$display_netamount = '
									<td>0.00</td>
									<td>'.number_format($loan_amount_disp,2).'</td>
								';
							}

							if($loan_type != 'payment' && $loan_type != 'payment_ors' && $loan_type != 'jv'){
								$display_netamount = '
									<td>'.number_format($loan_amount_disp,2).'</td>
									<td>0.00</td>
								';
							}


						// echo $n15th.'='.$n30th.'<br>';
							if($or_num == '0000000000'){
								$disp_particulars = 'No Payment';
							}else{
								$disp_particulars = ucwords(str_replace('_', ' ', str_replace('_ors','', $nres->loan_type)));
							}

							
							if($loan_type == 'beg_bal'){
								$balance = $nres->amount;
								echo '
					 				<tr>
					 					<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
					 					<td>Beg. Balance</td>
					 					<td>-</td>
					 					<td>-</td>
					 					<td>-</td>
					 					<td>-</td>
					 					<td>-</td>
					 					<td>-</td>
					 					<td>'.number_format($balance, 2).'</td>
					 					<!--<td class="hidden-print">-</td>-->
					 				</tr>
								';	
								
							}else{

								echo '
					 				<tr>
					 					<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
					 					<!--<td>'.$voucher_num.''.(($nres->loan_type == 'cash_advance')? '-CA':'').'</td>-->
					 					<td>'.(($voucher_num == '0000000000')? '-': $voucher_num).'</td>
					 					<td>'.$disp_particulars.'</td>
					 					<td>'.(($or_num == '0000000000')? '-': $or_num).'</td>
					 					<td>'.number_format($payment, 2).'</td>
					 					<td>'.number_format($int_amount,2).'</td>
					 					'.$display_netamount.'
					 					<td>'.number_format($balance, 2).'</td>
					 					<!--<td class="hidden-print">-</td>-->
					 				</tr>
								';	

							}
							$end_bal[] = $balance;
							$loan_amnt = $balance;
							
						}//END FOREACH
 					}else{
 						echo '
			 				<tr>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<!--<td class="hidden-print">-</td>-->
			 				</tr>
 						';	 						
 					}
  				}
  				else
  				{// IS ACTIVE START

  					// echo 'test';
					//txtBeginningBal
					$is_beg = false;
					$sqlBB = "select * from `client_meta` where `meta` = 'txtBeginningBal' and `client_id` = '".$client_id."' ";
					$rsBB = $conn->dbquery($sqlBB);
					// $rs1 = json_decode($rs1);
					// echo $rsBB;
					if($rsBB !== 'false'){
						
						$rsBB = json_decode($rsBB);
						$rsBB = json_decode($rsBB->data[0]);
						$beg_bal = $rsBB->value;	
						if(!empty($beg_bal)){
							$is_beg = true;
						}
					}
					// echo $beg_bal;

  					$is_interest_applied = false;
  					$is_interest_ctr = 0;
  					$ctr = 0;

  					$fixedRate_interest = 0;
  					$is_FR_set = false;

 					$sql = "select * from `finance` where `client_id` = '".$client_id."' and `status` = 'approved' order by `date_approved`, `fid` asc";
 					$res = $conn->dbquery($sql);
 					// print_r($res);
 					$n15th = 1;
 					$n30th = 1;
 					

 					if($res !== 'false'){
						$res = json_decode($res);

						$int_ctr = 0; //counter for applied interest


						foreach ($res->data as $key) {
							# code...
							// echo $ctr;
							$nres = json_decode($key);
							$extra = json_decode($nres->extra);

							if($extra->TypeRate == 'diminishing' || $extra->TypeRate == '-' || $extra->TypeRate == ''){
								$type_rate = 'DR';
							}else{
								$type_rate = 'FR';
							}

							if($type_rate == 'DR'){
								//DEMINISHING RATE

								$intrate = $nres->intrate;
								$intrate_val = $intrate / 100;

								$voucher_num = $nres->voucher_id;
								$or_num = '';
								$payment = number_format(0,2);


								$interest = $loan_amnt_temp * $intrate_val;

								// $loan_amnt = number_format($nres->amount, 2);
								$loan_amnt = $nres->amount;
								// echo $balance.'<-<br>';
								// echo $nres->amount;
								if($ctr == 0){
									$loan_start = $loan_amnt;
									if(!empty($beg_bal)){	
									// if($is_beg == true){
										$loan_start = $beg_bal;		
										$loan_amnt = $beg_bal;
									}
								}

								if($nres->loan_type == 'new_loans'){
									$balance = $loan_amnt;
									$int_amount = 0;
									$net_amount = 0;
									// $minus_ctr = 2;
								// }else{
								}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors' && $nres->loan_type != 'jv'){
									$int_amount = 0;
									$net_amount = 0;

									$balance = $balance + $loan_amnt;
									// $minus_ctr = 1;
								}


								

								// else{
								// 	$int_amount = 0;
								// 	$net_amount = 0;
								// 	$balance = $loan_amnt;
								// }
								// echo $loan_amnt; //exit();
								//cash_advance
								// special_account
								// additional_loan
								// renewal
								// excess
								// if($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
								// 	$int_amount = 0;
								// 	$net_amount = 0;
								// 	$balance = $balance + $loan_amnt;
								// }else{
								// 	$balance = $loan_amnt;
								// 	$int_amount = 0;
								// 	$net_amount = 0;
								// }
								// echo "[".$bal_array[$ctr-2]."]";

								if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors' && $nres->loan_type != 'jv'){
									$loan_amnt = number_format(0,2);
									if($ctr == 0){
										$loan_start = $loan_amnt;
										if(!empty($beg_bal)){
										// if($is_beg == true){
											$loan_start = $beg_bal;		
											$loan_amnt = $beg_bal;
											$balance = $loan_amnt;
										}
									}

									$voucher_num = '-';
									$or_num = $nres->voucher_id;
									$payment = $nres->amount;
									//date paid and start deductions
									$start_interest = getClientSchedPaymentStart($client_id);

									$d_approved = strtotime($nres->date_approved);
									$int_date_start = strtotime($start_interest);

									$p_day = explode(" ", $nres->date_approved);
									$p_day = explode("-", $p_day[0]);
									$p_day = $p_day[2];
									// echo 'd = '.$d_approved;
									// echo 'i = '.$int_date_start; 

									// if($d_approved == $int_date_start){
									// 	// $balance = $loan_start;
									// 	// $int_amount = ($balance * ($nres->intrate/100));
									// 	// $int_amount = ($loan_start * ($nres->intrate/100)); //original
									// 	// $int_amount = ($bal_array[0] * ($nres->intrate/100));
									// 	$int_amount = (end($last_month_balance) * ($nres->intrate/100));
									// 	$net_amount = $payment - $int_amount;	

									// 	// $loan_start = $				
									// 	$balance = $balance - $net_amount;
									// 	// $loan_start = $balance;
									// 	$int_ctr++;
									// }else{

										if($p_day >= 1 && $p_day <= 15){
											$sched_int = 1;
										}elseif($p_day >= 15 && $p_day <= 31){
											$sched_int = 2;
										}


										// if($interest_psched == $sched_int && $d_approved > $int_date_start){ //original
										if($interest_psched == $sched_int){
											// $int_amount = ($loan_start * ($nres->intrate/100)); //original
											// if($int_ctr == 0){
											// 	$int_amount = ($loan_start * ($nres->intrate/100)); //original
											// }else{
												// $int_amount = ($bal_array[$ctr-$minus_ctr] * ($nres->intrate/100));	
												// $int_amount = ($bal_array[$ctr-2] * ($nres->intrate/100));	
												$int_amount = (end($last_month_balance) * ($nres->intrate/100));	
											// }
											$net_amount = $payment - $int_amount;

											$balance = $balance - $net_amount;
											$loan_start = $balance;
											// $loan_amnt = $balance;
											$int_ctr++;
										}else{
											$int_amount = 0;
											// if($ctr == 0){
											// 	// $loan_start = $loan_amnt;
											// 	if($is_beg == true){
											// 		// $loan_start = $beg_bal;		
											// 		// $loan_amnt = $loan_start;
											// 		$balance = $beg_bal;
											// 	}
											// }
											$net_amount = $payment;			
											$balance = $balance - $net_amount;	

											// $loan_amnt = $balance;
											$loan_start = $balance;
										}

									// }

									// if($extra->ApplyInt == 'on'){
									// 	$int_amount = ($balance * ($nres->intrate/100));
									// 	$net_amount = $payment - $int_amount;					
									// }

									// if($extra->ApplyInt == 'off' || $extra->ApplyInt == ''){
									// 	$int_amount = 0;
									// 	$net_amount = $payment;
									// }
									// $loan_amnt = $balance;

								}
								$bal_array[$ctr] += $balance; //added 01-16-2018
								// echo $bal_array[$ctr].'='.$ctr;
								$ctr++;
							}else{
								//FIXED RATE

								$intrate = $nres->intrate;
								$intrate_val = $intrate / 100;

								$voucher_num = $nres->voucher_id;
								$or_num = '';
								$payment = number_format(0,2);

								$loan_amnt = $nres->amount;
								if($is_FR_set == false){
									$int_amount = ($loan_amnt * ($nres->intrate/100));
									$loan_start = $loan_amnt;
									$is_FR_set = true;
								}

								if($nres->loan_type == 'new_loans'){
									$balance = $loan_amnt;
									$int_amount = 0;
									$net_amount = 0;
								// }else{
								}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors' && $nres->loan_type != 'jv'){
									$int_amount = 0;
									$net_amount = 0;
									$balance = $balance + $loan_amnt;
									// $net_amount = $payment - $int_amount;	
								}

								if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors' && $nres->loan_type != 'jv'){
									$loan_amnt = number_format(0,2);
									$voucher_num = '-';
									$or_num = $nres->voucher_id;
									$payment = $nres->amount;
									//date paid and start deductions
									$start_interest = getClientSchedPaymentStart($client_id);

									$d_approved = strtotime($nres->date_approved);
									$int_date_start = strtotime($start_interest);

									$p_day = explode(" ", $nres->date_approved);
									$p_day = explode("-", $p_day[0]);
									$p_day = $p_day[2];


									if($d_approved == $int_date_start){
										// $balance = $loan_start;
										// $int_amount = ($balance * ($nres->intrate/100));
										// $int_amount = ($loan_start * ($nres->intrate/100));
										$int_amount = ($bal_array[0] * ($nres->intrate/100));
										$net_amount = $payment - $int_amount;	

										// $loan_start = $										
										$balance = $balance - $net_amount;
										$loan_start = $balance;
										// $loan_amnt = $balance;
									}else{

										if($p_day >= 1 && $p_day <= 15){
											$sched_int = 1;
										}elseif($p_day >= 15 && $p_day <= 31){
											$sched_int = 2;
										}

										// if($interest_psched == $sched_int && $d_approved > $int_date_start){
										if($interest_psched == $sched_int){
											// $int_amount = ($loan_start * ($nres->intrate/100));
											$int_amount = ($bal_array[0] * ($nres->intrate/100));
											
											$net_amount = $payment - $int_amount;											

											$balance = $balance - $net_amount;
											$loan_start = $balance;
											// $loan_amnt = $balance;
										}else{
											$int_amount = 0;
											$net_amount = $payment;			
											$balance = $balance - $net_amount;		
											$loan_start = $balance;	
											// $loan_amnt = $balance;													
										}



										
									}
								}							
								$bal_array[$ctr] += $balance; //added 01-16-2018
								// echo $bal_array[$ctr].'='.$ctr;
								$ctr++;

							}
							//END of ELSE

							// echo 'asdasd';
							$loan_amount_disp = $loan_amnt;

							//for JV
							if($net_amount < 0){
								$display_netamount = '
									<td>'.number_format(abs($net_amount),2).'</td>
					 				<td>0.00</td>
								';
							}else{
								$display_netamount = '
									<td>0.00</td>
									<td>'.number_format(abs($net_amount),2).'</td>
								';
							}
							
							if(empty($extra->TypeRate) && $nres->loan_type == 'jv'){
								if(!empty($extra->JVTypeID)){
									$chName = getChartAccountName($extra->JVTypeID);
								}

								if(stripos($chName, "A/R") !== false){

									if($nres->debit > 0){
										$balance = $balance + $nres->debit;	
										// $balance = end($bal_array) + number_format($nres->debit);	
										$display_netamount = '
											<td>'.number_format($loan_amnt,2).'</td>
							 				<td>0.00</td>
										';
									}

									if($nres->credit > 0){
										// echo $balance."<br>".$nres->credit;
										$balance = $balance - $nres->credit;	
										// echo end($bal_array);
										// $balance = end($bal_array) - number_format($nres->credit, 2);	

										$display_netamount = '
											<td>0.00</td>
											<td>'.number_format($loan_amnt,2).'</td>
										';
									}

									// print_r($bal_array);
									$int_amount = 0;
									$net_amount = 0;
									echo '
						 				<tr>
						 					<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
						 					<td> JV #'.$voucher_num.'</td>
						 					<td>-</td>
						 					<!--<td>'.number_format($loan_amount_disp, 2).'</td>-->
						 					<td>-</td>
						 					<td>-</td>
						 					<td>'.number_format($int_amount,2).'</td>
						 					'.$display_netamount.'
						 					<td>'.number_format($balance, 2).'</td>
						 					<!--<td class="hidden-print">-</td>-->
						 				</tr>
									';	
									$is_start_beg = false; // for display loan amount flag								
								}
							}else{

								// echo $loan_type;

								if($nres->loan_type == 'beg_bal'){
									echo '
						 				<tr>
						 					<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
						 					<td>Beg. Balance</td>
						 					<td>-</td>
						 					<td>-</td>
						 					<td>-</td>
						 					<td>-</td>
						 					<td>-</td>
						 					<td>-</td>
						 					<td>'.number_format($balance, 2).'</td>
						 					<!--<td class="hidden-print">-</td>-->
						 				</tr>
									';	
									$is_start_beg = true; // for display loan amount flag
								}else{
									// check display loan amount flag
									// if($is_start_beg){
									// 	$loan_amount_disp = 0;
									// 	$is_start_beg = false;
									// }

									//check for non-payment =====
									// if(count($current_paid_end_months) > 0){
									// 	echo checkEndMonthTransaction($client_id, end($current_paid_end_months));	
									// }
									//===========================
									// if((!empty($n15th) && $n15th == '1') && 
									// 	(!empty($n30th) && $n30th == '1')){
									// if($n15th == 0){
									// 	echo '
							 	// 			<tr>
							 	// 				<td>'.date('m/d/Y', strtotime($next_month_end_15)).'</td>
							 	// 				<td>-</td>
							 	// 				<td>NO PAYMENT</td>
							 	// 				<td>-</td>
							 	// 				<td>-</td>
							 	// 				<td>'.number_format($int_amount,2).'</td>
							 	// 				'.$display_netamount.'
							 	// 				<td>'.number_format($balance, 2).'</td>
							 	// 			</tr>
									// 	';	
									// }

									// if($n30th == 0){
									// 	// $int_amount = 0;
									// 	// $net_amount = 0;
									// 	echo '
							 	// 			<tr>
							 	// 				<td>'.date('m/d/Y', strtotime($next_month_end_30)).'</td>
							 	// 				<td>-</td>
							 	// 				<td>NO PAYMENT</td>
							 	// 				<td>-</td>
							 	// 				<td>-</td>
							 	// 				<td>'.number_format($int_amount,2).'</td>
							 	// 				'.$display_netamount.'
							 	// 				<td>'.number_format($balance, 2).'</td>
							 	// 			</tr>
									// 	';	
									// }

										if($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors' && $nres->loan_type != 'jv'){
											$display_netamount = '
												<td>'.number_format($loan_amount_disp,2).'</td>
												<td>0.00</td>
											';
											// echo 'hello';
										}
										// echo 'world';
									// echo $n15th.'='.$n30th.'<br>';
										if($or_num == '0000000000'){
											$disp_particulars = 'No Payment';
											$action_btn = '<button type="button" class="btn btn-danger delete_no_payment" data-id="'.$nres->fid.'"><i class="fa fa-trash"></i></button>';
										}else{
											$disp_particulars = ucwords(str_replace('_', ' ', str_replace('_ors','', $nres->loan_type)));
											$action_btn = '-';
										}

										/* if RENEWAL */
										if($nres->loan_type == 'renewal'){
											$balance = $loan_amount_disp;
										}
										/*===============*/
										echo '
							 				<tr>
							 					<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
							 					<td>'.(($voucher_num == '0000000000')? '-': $voucher_num).'</td>
							 					<td>'.$disp_particulars.'</td>
							 					<td>'.(($or_num == '0000000000')? '-': $or_num).'</td>
							 					<td>'.number_format($payment, 2).'</td>
							 					<td>'.number_format($int_amount,2).'</td>
							 					'.$display_netamount.'
							 					<td>'.number_format($balance, 2).'</td>
							 					<!--<td class="hidden-print">'.$action_btn.'</td>-->
							 				</tr>
										';	
									// }else{
									// 	echo '
							 	// 			<tr>
							 	// 				<td>-</td>
							 	// 				<td>-</td>
							 	// 				<td>NO PAYMENT</td>
							 	// 				<td>-</td>
							 	// 				<td>-</td>
							 	// 				<td>'.number_format($int_amount,2).'</td>
							 	// 				'.$display_netamount.'
							 	// 				<td>'.number_format($balance, 2).'</td>
							 	// 			</tr>
									// 	';	
									// }


								}

							}

							//get end of the month balance
							$temp_date_approved = explode(" ", $nres->date_approved);
							$temp_date_approved = explode("-", $temp_date_approved[0]);
							$temp_day_approved = $temp_date_approved[2];
							$temp_month_approved = $temp_date_approved[1];
							$temp_year_approved = $temp_date_approved[0];

							if(($temp_month_approved == '02' && ($temp_day_approved >= 28 && $temp_day_approved < 30)) 
								|| ($temp_day_approved >= 30 && $temp_day_approved <= 31)
								):
								// echo $balance;
								// echo $temp_day_approved;
								$last_month_balance[] = $balance;
								$current_paid_end_months[] = $temp_year_approved.'-'.$temp_month_approved.'-'.$temp_day_approved;

								//Next Month
								// if(($temp_month_approved+1) > 12){
								// 	// if(($temp_month_approved == '02' && ($temp_day_approved >= 28 && $temp_day_approved < 30))
								// 	$next_month_start = ($temp_year_approved + 1).'-01-01';
								// 	$next_month_end_15 = ($temp_year_approved + 1). '-01-15';
									
								// 	$next_month_end_30 = ($temp_year_approved + 1). '-01-'.$temp_day_approved;

								// }else{
								// 	$next_month_start = $temp_year_approved.'-'.str_pad(($temp_month_approved + 1), 2, '0', STR_PAD_LEFT).'-01';
								// 	$next_month_end_15 = $temp_year_approved.'-'.str_pad(($temp_month_approved + 1), 2, '0', STR_PAD_LEFT).'-15';
								// 	$next_month_end_16 = $temp_year_approved.'-'.str_pad(($temp_month_approved + 1), 2, '0', STR_PAD_LEFT).'-16';

								// 	// $next_month_end_30 = $temp_year_approved.'-'.str_pad(($temp_month_approved + 1), 2, '0', STR_PAD_LEFT).'-'.$temp_day_approved;	
								// 	$next_month_end_30 = $temp_year_approved.'-'.str_pad(($temp_month_approved + 1), 2, '0', STR_PAD_LEFT).'-01';	
								// 	// echo $next_month_end_30;
								// 	$next_month_end_30 = date('Y-m-t', strtotime($next_month_end_30));
								// 	// echo $next_month_end_30;
									
								// }

								// //get next month if it has 15th or End month payment
								// if(checkPaymentExist($client_id, $next_month_start, $next_month_end_15)){
								// 	$n15th = 1;
								// }else{
								// 	$n15th = 0;
								// }

								// if(checkPaymentExist($client_id, $next_month_end_16, $next_month_end_30)){ //END OF THE MONTH
								// 	$n30th = 1;
								// }else{
								// 	$n30th = 0;
								// }


							endif;

							//get next month ===========

							//==========================

							// Last LOAN AMOUNT
							$loan_amnt = $balance;


							//<td>'.number_format($balance, 2).'</td>
						}
 					}else{
 						echo '
			 				<tr>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<td>-</td>
			 					<!--<td class="hidden-print">-</td>-->
			 				</tr>
 						';	
 					}

  				}// IS ACTIVE END

 				?>
 			</tbody>
 		</table>
 	
 	</div>

 </div>

<script>
	$(document).ready(function(){
		$('#btnReCalc').click(function(){
			var q = confirm('Are you sure you want to Re-Calculate?');
			if(q){
				window.location = 'dashboard.php?page=viewdata&cid=<?php echo secure_get('cid')?>&axn=recalc';	
			}			
		});

	    $('#btnPrintMe').click(function(){
	        window.print();
	    });

	    $('.delete_no_payment').click(function(){
	    	var fid = $(this).data('id');
	    	var q = confirm('Are you sure you want to delete the data?');
	    	if(q){
	    		$.ajax({
	    			type : 'post',
	    			url  : 'lib/api/delete.nopayment.php',
	    			data : {
	    				"fid" : fid
	    			},
	    			success : function(result){
	    				window.location = 'dashboard.php?page=viewdata&cid=<?php echo secure_get('cid')?>&axn=recalc';
	    			}
	    		});
	    	}
	    });
	});
</script>

<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
	vertical-align: middle !important;
}
</style>