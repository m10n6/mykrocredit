     var ctx = document.getElementById("mybarChart12");
      var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
          datasets: [{
            label: 'Valid',
            backgroundColor: "#26B99A",
            data: [
            //51, 30, 40, 28, 92, 50, 45
            <?php
              include_once('config.php');
              include_once('lib/funcjax.php');
              
              $year_now = date('Y');
              $xt1 = '';
              for($x = 1; $x <= 12; $x++){
                  $sql = "SELECT * FROM `donation` where (`date_added` between '".date('Y-m-01', strtotime($year_now.'-'.$x.'-1'))." 00:00:00' and '".date('Y-m-t', strtotime($year_now.'-'.$x.'-1'))." 23:59:59') and `status` = 'valid'" ;

                  $res = $conn->dbquery($sql);
                  $res = json_decode($res);
                  $res = $res->data;
                  // echo $sql.'<br>';
                  if(count($res) > 0){
                     foreach($res as $rs){
                        $rs1 = json_decode($rs);

                        $val = $rs1->amount;
                        // if(empty($val)){
                        //   $val = 0;
                        // }
                        
                        // $xt .= '{';
                        // $xt .= 'title:\'Event: '.$rs1->event.'\nDetails: '.$rs1->details.'\nPriest: '.translateName($rs1->assigned_to, $conn).'\nStatus: '.$rs1->status.'\',';
                        // $xt .= 'start: new Date("'.$rs1->start_date.'"),';
                        // $xt .= 'end: new Date("'.$rs1->end_date.'")';
                        // $xt .= '},';
                     }
                     
                  }else{
                    $val = 0;
                  }
                  $xt1 .= $val.',';  
              }
              echo rtrim($xt1, ",");
            ?>
            ]
          }, {
            label: 'InValid',
            backgroundColor: "#ed6a6a",
            data: [
            //41, 56, 25, 48, 72, 34, 12
            <?php

              // $year_now = date('Y');
              $xt2 = '';
              for($x = 1; $x <= 12; $x++){
                  $sql = "SELECT * FROM `donation` where (`date_added` between '".date('Y-m-01', strtotime($year_now.'-'.$x.'-1'))." 00:00:00' and '".date('Y-m-t', strtotime($year_now.'-'.$x.'-1'))." 23:59:59') and `status` = 'invalid'" ;

                  $res = $conn->dbquery($sql);
                  $res = json_decode($res);
                  $res = $res->data;
                  // echo $sql.'<br>';
                  if(count($res) > 0){
                     foreach($res as $rs){
                        $rs1 = json_decode($rs);

                        $val = $rs1->amount;
                        // if(empty($val)){
                        //   $val = 0;
                        // }
                        
                        // $xt .= '{';
                        // $xt .= 'title:\'Event: '.$rs1->event.'\nDetails: '.$rs1->details.'\nPriest: '.translateName($rs1->assigned_to, $conn).'\nStatus: '.$rs1->status.'\',';
                        // $xt .= 'start: new Date("'.$rs1->start_date.'"),';
                        // $xt .= 'end: new Date("'.$rs1->end_date.'")';
                        // $xt .= '},';
                     }
                     
                  }else{
                    $val = 0;
                  }
                  $xt2 .= $val.',';  
              }
              echo rtrim($xt2, ",");
            ?>
            ]
          }]
        },

        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });