<?php

	// replace any non-ascii character with its hex code.
	function escape($value) {
	    $return = '';
	    for($i = 0; $i < strlen($value); ++$i) {
	        $char = $value[$i];
	        $ord = ord($char);
	        if($char !== "'" && $char !== "\"" && $char !== '\\' && $ord >= 32 && $ord <= 126)
	            $return .= $char;
	        else
	            $return .= '\\x' . dechex($ord);
	    }
	    return $return;
	}

	function mres($value)
	{
	    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
	    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

	    return str_replace($search, $replace, $value);
	}

	function secure_string($val)
 	{
 		$var= $val;
 		$var = trim($var);
 		$var = strip_tags($var); 
 		// if(function_exists('mysqli_connect')){
 		// 	// $var = mysqli_escape_string($var);			
 		// 	// $var = addslashes($var);
 		// 	// $var = mysql_real_escape_string($conn,$var);
 		// }else{
 		// $var = mysql_escape_string($var);
 		// }
		// if(function_exists('mysqli_connect')){
		// 	$var = mysqli_real_escape_string($var);
		// }else{
		// 	$var = mysql_escape_string($var);
		// }

 		return mres($var);
 	}
 	
 	function secure_post($val)
 	{
 		$string=$_POST[$val];
 		$var = secure_string($string);
 		return $var;
 	}
 	
 	function html_post($val)
 	{
 		$string=$_POST[$val];
 		$string =trim($string);
 		
 		return $string;
 	}
 	
 	function secure_get($val)
 	{
 		$string=$_GET[$val];
 		$var = secure_string($string);
 		return $var;
 	}
 	
 	function request($val)
 	{
 		$var = secure_post($val);
 		if(empty($var))
 			$var = secure_get($val);
 		
 		return $var;
 	}
 	
 	function email_check($email)
	{
		$email_regexp="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|";
    		$email_regexp.="(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
			
	    	if(!ereg($email_regexp, $email))
				return false;
			else
				return true;
	}
	
	function generateCode($length=6, $switch=1) {
		switch ($switch){
			case 1:
				$chars = "abcdefghijklmnopqrstuvwxyz0123456789";
				break;
			case 2: 
				$chars = "abcdefghijklmnopqrstuvwxyz";
				break;
			case 3:
				$chars = "0123456789";
				break;
			case 4:
				$chars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIKLMNOPQRSTUVWXYZ";
				break;
			default:
				$chars = "abcdefghijklmnopqrstuvwxyz0123456789";
				break;
		}
		$code = "";
		$clen = strlen($chars) - 1;  //a variable with the fixed length of chars correct for the fence post issue
		while (strlen($code) < $length) {
		    $code .= $chars[mt_rand(0,$clen)];  //mt_rand's range is inclusive - this is why we need 0 to n-1
		}
		return $code;
	}
	
	function isNaN( $var ) {
	     return ereg ("^[-]?[0-9]+([\.][0-9]+)?$", $var);
	}
	
	function unreadMsgCount(){
		$sql = "select * from messages where is_read = '0'";
		$rs  = mysql_query($sql);
		$count = mysql_num_rows($rs);
		if ($count > 0){
			$unread = "(".$count.")";
		}else;
		
		return $unread;
	}
	
	function FormatDate($date){
		//format Y-m-d H:i:s / 2008-01-25 00:00:00
		if ($date == "0000-00-00 00:00:00"){
			$dates = "Invalid Date";
		}else{
			$expld_dates = explode(" ",$date);
			$expld_date_2 = explode("-", $expld_dates[0]);
			if ($expld_date_2[1] == "01"){
				$text_month = "January";
			}elseif ($expld_date_2[1] == "02"){
				$text_month = "Februray";
			}elseif ($expld_date_2[1] == "03"){
				$text_month = "March";
			}elseif ($expld_date_2[1] == "04"){
				$text_month = "April";
			}elseif ($expld_date_2[1] == "05"){
				$text_month = "May";
			}elseif ($expld_date_2[1] == "06"){
				$text_month = "June";
			}elseif ($expld_date_2[1] == "07"){
				$text_month = "July";
			}elseif ($expld_date_2[1] == "08"){
				$text_month = "August";
			}elseif ($expld_date_2[1] == "09"){
				$text_month = "September";
			}elseif ($expld_date_2[1] == "10"){
				$text_month = "October";
			}elseif ($expld_date_2[1] == "11"){
				$text_month = "November";
			}elseif ($expld_date_2[1] == "12"){
				$text_month = "December";
			}else;
			
			
			$dates = $text_month." ".number_format($expld_date_2[2],0).", ".$expld_date_2[0];
		}
		return $dates;
	}
	
	function loadpaging($start, $limit, $total, $action){
		if($start > 0)
		{
			$newstart = $start - $limit;
			$x = $x - ($limit*2);
			$link .= "<a href=\"".$action."start=".$newstart."&nxt_x=".$x."\">&laquo;PREV</a>&nbsp;";
		}
		
		$totalnumpages = ceil($total / $limit);
		
		$y = 1;
		$startx = 0;
		for ($x=1; $x <= $totalnumpages; $x++){
			if($start == $startx){
				$z .= $y++." ";
			}else{
				$z .= "<a href=\"".$action."start=".$startx."\">".$y++."</a> ";
			}
			$startx = $startx + $limit;
		}
		if ($totalnumpages > 1){
			$link .= $z;
		}
		if ($total > $start)
		{
			$newstart = $start + $limit;
			$nxt_x = $newstart + 1;
			if ($newstart < $total)
			{
				$link .= "&nbsp;<a href=\"".$action."start=".$newstart."&nxt_x=".$nxt_x."\">NEXT&raquo;</a>";
			}
		}
		return $link;
	
	}

	function getFiles($path){
		//$npath = $path."/".$id;
		if ($handle = opendir($path)) {
		    /* This is the correct way to loop over the directory. */
		    $ctr=1;
		    echo '<table cellpadding="5px" cellspacing="0px">';
		    while (false !== ($file = readdir($handle))) {
		    	if ($file != "." && $file != "..") {
			    if ($file != "Thumbs.db"){
				    echo "<tr>";
				    echo "<td align=\"center\">".$ctr++."</td>";
				    echo "<td align=\"center\">../images/uploads/".$file."</td>";
				    echo "<td align=\"center\">";
				    echo "<a href=\"#\" onclick=\"deleteImg('../images/uploads/".urlencode($file)."'); return false;\">";
				    echo '<img src="images/cancel.png" border="0" align="absmiddle"/>';
				    echo "</a>";
				    echo "</td>";
				    echo "</tr>";
			    }
			}
		    }
		    echo '</table>';
		    closedir($handle);
		}
	}

	function uploadPhoto($uploaddir){
		$uploadfile = $uploaddir . basename($_FILES['file']['name']);
		$ex = explode(".", $_FILES['file']['name']);
		$extension = strtolower($ex[count($ex)-1]);
		if ($extension == "jpg" or $extension == "jpeg" or $extension == "png" or $extension == "bmp" or $extension == "gif"){
			if (!file_exists($uploadfile)){
				if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
				    $message = "<img src=\"../images/information.png\" align=\"absmiddle\" /> <strong>File is valid, and was successfully uploaded.</strong>";
				} else {
				    $message = "<img src=\"../images/error.png\" align=\"absmiddle\" /> <strong>Possible file upload attack!</strong>";
				}
			}else{
				$message = "<img src=\"../images/error.png\" align=\"absmiddle\" /> <strong>File Already Exists.</strong>";
			}
		}else{
			$message = "<img src=\"../images/error.png\" align=\"absmiddle\" /> <strong>Unable to upload file. Please check file format.</strong>";
		}
		return $message;
	}
	
	function getFullName($uid, $switch=1){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		switch($switch){
			case 1:
				$fullname = $fetch['lname'].", ".$fetch['fname'];
				break;
			default:
				$fullname = $fetch['fname']." ".$fetch['lname'];
				break;
		}
		return $fullname;
	}
	
	function getName($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$uname = $fetch['name'];
		return $uname;
	}

	function getUName($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$uname = $fetch['uname'];
		return $uname;
	}
	
	function getFName($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$fname = $fetch['fname'];
		return $fname;
	}

	function getLName($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$lname = $fetch['lname'];
		return $lname;
	}
	
	function getEmail($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$email = $fetch['email'];

		return $email;
	}
	
	function getMPhone($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$email = $fetch['mphone'];

		return $email;
	}
	
	function getHPhone($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$email = $fetch['hphone'];

		return $email;
	}
	
	function getPostal($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$postal = $fetch['address'];

		return $postal;
	}
	
	function getCity($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$city = $fetch['city'];

		return $city;
	}

	function getProv($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$prov = $fetch['province'];

		return $prov;
	}
	
	function getBdate($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$bdate = FormatDate($fetch['bdate']);

		return $bdate;
	}
	
	function getBdate2($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$bdate = $fetch['bdate'];

		return $bdate;
	}
	
	function getGender($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$gender = $fetch['gender'];
		if ($gender == "0"){
			$gender = "";
		}
		return $gender;
	}
	
	function getULevel($uid){
		$sql = "select * from users where user_id = '$uid'";
		$rs  = mysql_query($sql);
		$fetch = mysql_fetch_assoc($rs);
		$level = $fetch['ulevel'];
		switch($level){
			case "0":
				$level = "Super Admin";
				break;
			case "1":
				$level = "Admin";
				break;
			case "2":
				$level = "Operator";
				break;
			case "3":
				$level = "USER";
				break;
		}
		return $level;
	}
	
	function prefixZero($var, $prefix=10){
		$countDigit = count($var);
		$zeroDigits = $prefix - $countDigit;
		for($i = $prefix ; $i > 0; $i--){
			$addzero .= "0";
		}
		$prefixed = $addzero.$var;
		return $prefixed;
	}
	
	function carType($var){
		switch ($var){
			case "taxc":
				   $ctype = "Taxi (Car)";
				   break;
			case "taxv":
				   $ctype = "Taxi (Van)";
				   break;
			case "colc":
				   $ctype = "Colorum (Car)";
				   break;
			case "colv":
				   $ctype = "Colorum (Van)";
				   break;
		}
		return $ctype;
	}
	
	function numPass($var){
		switch ($var){
			case 1:
			      $num = "1";
			      break;
			case 2:
			      $num = "2";
			      break;
			case 3:
			      $num = "3";
			      break;
			case 4:
			      $num = "4";
			      break;
			case 5:
			      $num = "5";
			      break;
			case 6:
			      $num = "6";
			      break;
			case 7:
			      $num = "Over 6 Passengers (requires multiple taxi)";
			      break;
		}
		return $num;
	}
	
	function convBYear($date){
		//format Y-m-d H:i:s / 2008-01-25 00:00:00

		$expld_dates = explode(" ",$date);
		$expld_date_2 = explode("-", $expld_dates[0]);

		$year = $expld_date_2[0];

		return $year;
	}
	
	function convBMonth($date){
		//format Y-m-d H:i:s / 2008-01-25 00:00:00

		$expld_dates = explode(" ",$date);
		$expld_date_2 = explode("-", $expld_dates[0]);

		$month = $expld_date_2[1];

		return $month;
	}
	
	function convBDDay($date){
		//format Y-m-d H:i:s / 2008-01-25 00:00:00

		$expld_dates = explode(" ",$date);
		$expld_date_2 = explode("-", $expld_dates[0]);

		$day = $expld_date_2[2];

		return $day;
	}
	
	function DateConvertFormat($dateTime){
		//m/d/y
		$expld = explode("/",$dateTime);
		$newDate = $expld[2]."-".$expld[0]."-".$expld[1]." 00:00:00";
		return $newDate;
		
	}
	
	function TimeRemoveSec($dateTime){
		//H:i:s
		$expld = explode(":",$dateTime);
		$newTime = $expld[0].":".$expld[1];
		return $newTime;
		
	}
	
	
	function curPageURL() {
		$pageURL = 'http';
		
		$x = explode("/",$_SERVER['REQUEST_URI']);
		for($i = 0 ; $i < count($x)-1; $i++){
			$str .= $x[$i]."/";
		}
		
		
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
			$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$str;//$_SERVER["REQUEST_URI"]
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$str;
		}
		return $pageURL;
	}
	

	function SEO_link($link){
		$link = explode("?watch=", $link);
		
		return "http://videosk.in/watch/".$link[1];
	}	
	
	function HitCounter($vid_id){
		$sql = "select * from `stats` where `video_id` = '".$vid_id."'";
		$rs = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			return mysql_num_rows($rs);
		}else{
			return "0";
		}
		
	}
	
	function get_referralCode($fb_uid){
		//md5(`vidcode`) = md5('".$vidcode."')
		$sql = "select * from `users` where `fb_uid` = '".$fb_uid."' and `referral_code` != '' ";
		$rs  = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			while($userdata = mysql_fetch_assoc($rs)){
				$referral_code = $userdata['referral_code'];
			}
			
		}else{
			$referral_code = genrefCode($fb_uid);
		}
		return $referral_code;
	}
	
	function genrefCode($fb_uid){
		$refcode = generateCode(10, 4);
		$sql_code = "select * from `users` where md5(`referral_code`) = md5('".$refcode."')";
		$rs_code  = mysql_query($sql_code);
		if(mysql_num_rows($rs_code) > 0){
			genrefCode();
		}else{
			$sql_in = "update `users` set `referral_code` = '".$refcode."' where `fb_uid` = '".$fb_uid."'";
			mysql_query($sql_in);
			return $refcode;
		}
	}
	
	function getULevelText($ulid){
		$sql = "select * from `ulevels` where `level_id` = '".$ulid."'";
		$rs  = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			$d = mysql_fetch_assoc($rs);
			return $d['name'];
		}else{
			return false;
		}
	}
	
	function clonerelated($video_source, $video_clone){
		$sql = "select * from `related_vids` where `video_id` = '".$video_source."'";
		$rs  = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			while($d = mysql_fetch_assoc($rs)){
				$sql1 = "select * from `related_vids` where `video_id` = '".$video_clone."'";
				$rs1  = mysql_query($sql1);
				if(mysql_num_rows($rs1) > 0){
					while($d1 = mysql_fetch_assoc($rs1)){
						$sqlu = "update `related_vids` set 
								`active` = '".$d['active']."',
								`link` = '".$d['link']."',
								`img` = '".$d['img']."'
								where
								`video_id` = '".$video_clone."'
								and
								`rel_id` = '".$d1['rel_id']."'
						";
						mysql_query($sqlu);
					}
				}else{
					$sqlu = "insert into `related_vids` set 
							`active` = '".$d['active']."',
							`link` = '".$d['link']."',
							`img` = '".$d['img']."',
							`video_id` = '".$video_clone."'
					";
					mysql_query($sqlu);
				}

			}
		
		}else{
				$sql1 = "select * from `related_vids` where `video_id` = '".$video_clone."'";
				$rs1  = mysql_query($sql1);
				if(mysql_num_rows($rs1) > 0){
					$sqlu = "delete from `related_vids` where
							`video_id` = '".$video_clone."'
					";
					mysql_query($sqlu);
				}
		}


	}
	
	function updatesocial($userID, $video_id, $social, $value){
		$sqlu = "update `social_settings` set `".$social."` = '".$value."'
		            where `user_id` = '".$userID."' and `vid_id` = '".$video_id."'
		";
		mysql_query($sqlu);
		//echo $sqlu;
/*
		$sql = "select * from `social_settings` where `user_id` = '".$user_id."' and `vid_id` = '".$vid."'";
		$rs = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			
		}else{
		
		}
*/
	
	}
	
	function updatemetavalue($userID, $video_id, $meta, $value, $extraSet){
		$sql = "select * from `settings`
			where
			`user_id` = '".$userID."'
			and
			`meta` = '".$meta."'
			and
			`vid_id` = '".$video_id."'
			";
		$rs = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			mysql_query("update `settings` set `meta` = '".$meta."', `value` = '".$value."', `extra` = '".$extraSet."' where `user_id` = '".$userID."' and `vid_id` = '".$video_id."'");
		}else{
			mysql_query("insert into `settings` set `meta` = '".$meta."', `value` = '".$value."', `extra` = '".$extraSet."', `user_id` = '".$userID."', `vid_id` = '".$video_id."'");
		}
	}
	
	function getmetavalue($userID, $video_id, $meta, $extraSet = false){
		$sql = "select * from `settings`
			where
			`user_id` = '".$userID."'
			and
			`meta` = '".$meta."'
			and
			`vid_id` = '".$video_id."'
			";
		$rs = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			while($data = mysql_fetch_assoc($rs)){
				$value = $data['value'];
				$extra = $data['extra'];
			}
			if($extraSet == true){
				return array('value'=> $value, 'extra' => $extra);
			}else{
				return $value;
			}
		}else{
			return false;
		}
	}
	
	function generateXMLconfig($userid, $vid){
		$user_id = $userid;
		$sqlg = "select * from `video`
			where
			`user_id` = '".$userid."'
			and
			`vid_id`  = '".$vid."'
			";
			
		$rsg = mysql_query($sqlg);
		if(mysql_num_rows($rsg) > 0){
			while($vidata = mysql_fetch_assoc($rsg)){
				$vidcode = $vidata['vidcode'];
				$vidURL = $vidata['genvid_url'];
				$ytv = $vidata['vid_url'];
				$vidlogoURL = $vidata['logo_url'];
				$logoURL = $vidata['logo_linkurl'];
				$logopos = $vidata['logo_position'];
				$vidSkin = $vidata['skin'];
				$vidDomain = $vidata['domain'];
			}
		}
		
		//create a file
		$file = "uploads/".$user_id."/".$vidcode.".xml";
		$ctxtFile = fopen($file, "w+");
		
		$xmlContent = "
			<config>
			<file>".htmlspecialchars($ytv)."</file>
			<width>640</width>
			<height>460</height>
			<controls>true</controls>
			<autostart>true</autostart>
		";
		if(!empty($vidlogoURL)){
			$xmlContent .= "
				<logo.file>https://www.videosk.in/dashboard/".str_replace("../", "", $vidlogoURL)."</logo.file>
				<logo.hide>false</logo.hide>
				<logo.link>".$logoURL."</logo.link>
				<logo.position>".$logopos."</logo.position>		
			";		
		}
		
		//skin
		$xmlContent .= "<skin>https://www.videosk.in/vid/pro_skin/".$vidSkin."/".$vidSkin.".xml</skin>";
		
		//plugins settings
		//
		$TimeInterval = getmetavalue($user_id, $vid, 'timer');
		switch ($TimeInterval){
			case "on" :
				$timerOn = "on";
				break;
			case "off" :
				$timerOn = "off";
				break;
			default : 
				$timer_split_data = explode("|", $TimeInterval);
				$timerOn = $timer_split_data[0];
				$TimeIntervalS = $timer_split_data[1];
				if(strstr($TimeIntervalS, ":")){
					$splitTS = explode(":", $TimeIntervalS);
					$secsTS = ($splitTS[0] * 60) + $splitTS[1];
					$TimeIntervalS = $secsTS;
				}
				
				$TimeIntervalE = $timer_split_data[2];
				if(strstr($TimeIntervalE, ":")){
					$splitTE = explode(":", $TimeIntervalE);
					$secsTE = ($splitTE[0] * 60) + $splitTE[1];
					$TimeIntervalE = $secsTE;
				}
				if(empty($TimeIntervalE)){
					$TimeIntervalE = "99999";
				}
				break;
				
		}
		
//		if($timerOn == "on" || $timerOn == 'timed'){
			$s_arr = array("https://videosk.in/vid/player5plugin.swf");
			$customPlug1 = "1";
/*
		}else{
			$s_arr = array();
			$customPlug1 = "0";
		}
*/
		
		
		$sqlrel = "select * from `related_vids` where `video_id` = '".$vid."' and `active` = 'true' ";
		$rsrel  = mysql_query($sqlrel);
		if(mysql_num_rows($rsrel) > 0){
			array_push($s_arr, "https://www.videosk.in/vid/drelated-1.swf");
			//$s_arr = array("https://www.videosk.in/vid/fbit-1.swf", "https://www.videosk.in/vid/tweetit-1.swf", "https://www.videosk.in/vid/plusone-1.swf");
			$rel_vid = "1";
			$plgProto = "https";
		}else{
			$rel_vid = "0";
			$plgProto = "https";
		}
		
		$sql = "select * from `social_settings` where `user_id` = '".$user_id."' and `vid_id` = '".$vid."'";
		$rs  = mysql_query($sql);
		
		if(mysql_num_rows($rs) > 0){
		    while($social_setting = mysql_fetch_assoc($rs)){
			$twitter = $social_setting['twitter'];
			$fb = $social_setting['fb'];
			$gplus = $social_setting['gplus'];
		    }
		    
			if($twitter == "true"){
			  array_push($s_arr, "https://www.videosk.in/vid/tweetit-1.swf");
			}
			if($fb == "true"){
			  array_push($s_arr, "https://www.videosk.in/vid/fbit-1.swf");
			}
			if($gplus == "true"){
			  //array_push($s_arr, "https://www.videosk.in/vid/plusone-1.swf");
			  array_push($s_arr, "https://videosk.in/vid/VgPlus1.swf");	
			  //array_push($s_arr, "plusone-1");
			}
		}

	    $calltoaction_data = getmetavalue($user_id, $vid, 'calltoaction', true);
	    
	    if($calltoaction_data['extra'] == "true"){
		    $ctaLoc = $calltoaction_data['value'];
			array_push($s_arr, "https://videosk.in/vid/ButtonCaller.swf");
	    }
		
		//fb subscribe
	    $fbsubscribe_data = getmetavalue($user_id, $vid, 'fb_subscribe', true);
/*
		if(!empty($_SESSION['ulevel'])){
			if($_SESSION['ulevel']=="1"){
*/
			    if($fbsubscribe_data['extra'] == "true"){
				    $fbsLoc = $fbsubscribe_data['value'];
					array_push($s_arr, "https://videosk.in/vid/FbAuth.swf");
			    }	    
/*
		    }
		}
*/

	//pushlink
    $push_data = getmetavalue($user_id, $vid, 'push_link', true);
    $push_data_split = explode("*", $push_data['value']);
    $pushTimer = $push_data_split[0];
	if(strstr($pushTimer, ":")){
		$split_Ptime = explode(":", $pushTimer);
		$secs_Ptime = ($split_Ptime[0] * 60) + $split_Ptime[1];
		$pushTimer = $secs_Ptime;
	}
    $pushURL = $push_data_split[1];
    if(!strstr($pushURL, "http")){
    	$pushURL = "http://".$push_data_split[1];
    }
/*
    if(!strstr($pushURL, "https://")){
    	$pushURL = "https://".$push_data_split[1];
    }
*/
/*
	if(!empty($_SESSION['ulevel'])){
		if($_SESSION['ulevel']=="1"){
*/
			if($push_data['extra'] == "true"){
				array_push($s_arr, "https://videosk.in/vid/PushLink.swf");			
			}
/*
		}
	}
*/
    	
	//FBShareGate
    $sharegate_data = getmetavalue($user_id, $vid, 'sharegate', true);
    $sharegate_data_split = explode("*", $sharegate_data['value']);
    $sharegate_sel = $sharegate_data_split[0];
    $sharegate_stToggle = $sharegate_data_split[1];
    $sharegate_time = $sharegate_data_split[2];
	if(strstr($sharegate_time, ":")){
		$split_stime = explode(":", $sharegate_time);
		$secs_stime = ($split_stime[0] * 60) + $split_stime[1];
		$sharegate_time = $secs_stime;
	}
    $sharegate_img =  $sharegate_data_split[3];
	
	//if($_SESSION['ulevel'] == "1"){
		if($sharegate_data['extra'] == "true"){
			array_push($s_arr, "https://videosk.in/vid/FBShareGate.swf");
		}
	//}
    		
		foreach($s_arr as $sval){
		      $plugs .= ",".$sval;
		}
		  
		$plugins = rtrim($plugs, ",");
		$plugins = ltrim($plugins, ",");
		//if(count($s_arr) == 1){
		//	$plugins = str_replace(",", "", $plugs);
		//}
		/*
		<drelated.dxmlpath>'http://www.videosk.in/vid/related_vid.php?q=none&id=".urlencode($vid)."'</drelated.dxmlpath>
		//NOTE:
		HTTP
		protocol when implementing the custom plugin
		https://www.videosk.in/vid/player5plugin.swf
		,".$plugins."
		*/
		$relatedURL = 'https://www.videosk.in/vid/related_vid.php?q=none&amp;id='.$vid;
		$xmlContent .="
		<plugins>".$plugins."</plugins>
		";
		//<player5plugin.xml>https://www.videosk.in/vid/player5plugin.xml</player5plugin.xml>
		//<player5plugin.position>right</player5plugin.position>
		//$TimeInterval = getmetavalue($user_id, $vid, 'timer');
		if($customPlug1 == "1"){
			//$s_arr = array("https://videosk.in/vid/player5plugin.swf");
			

			
			
			$appearance_data = getmetavalue($user_id, $vid, 'appearance');
			$appearance_data_split = explode("&amp;", $appearance_data);
			
			$btnLoc = str_replace("location=", "", $appearance_data_split[0]);
			$opt_headline = str_replace("optHeader=", "", $appearance_data_split[1]);
			$emailbox_txt = str_replace("ebTxt=", "", $appearance_data_split[2]);
			$btnTxt = str_replace("btnTxt=", "", $appearance_data_split[3]);
			$btnColor = str_replace("btnColor=", "", $appearance_data_split[4]);
			//if($_SESSION['ulevel'] == "1"){
				$optFName = str_replace("optFName=", "", $appearance_data_split[5]);
				$player5plugin = '<player5plugin.namefield>'.$optFName.'</player5plugin.namefield>';
			//}
/*
<player5plugin.text></player5plugin.text>
*/
			$timerpause_data = getmetavalue($user_id, $vid, 'timer_pause');
			$timerForcepause_data = getmetavalue($user_id, $vid, 'timer_pause_force');
			$warningtext_data = stripslashes(getmetavalue($user_id, $vid, 'timer_pause_forcewarning'));
			//added 2014-07-25 by markramos
			//$tyURL_data = stripslashes(getmetavalue($user_id, $vid, 'timer_tyurl'));
			//<player5plugin.tyurl>".$tyURL_data."</player5plugin.tyurl>
			
			$xmlContent .= "				
				<player5plugin.ptimer>".$timerOn."</player5plugin.ptimer>
				<player5plugin.ptimerstart>".$TimeIntervalS."</player5plugin.ptimerstart>
				<player5plugin.ptimerend>".$TimeIntervalE."</player5plugin.ptimerend>
				<player5plugin.position>".$btnLoc."</player5plugin.position>
				<player5plugin.eboxtext>".stripslashes($emailbox_txt)."</player5plugin.eboxtext>
				<player5plugin.button>".$btnColor."</player5plugin.button>
				<player5plugin.btntext>".stripslashes($btnTxt)."</player5plugin.btntext>
				<player5plugin.headline>".stripslashes($opt_headline)."</player5plugin.headline>
				<player5plugin.video_id>".$vid."</player5plugin.video_id>
				<player5plugin.userid>".$user_id."</player5plugin.userid>
				<player5plugin.pausevid>".$timerpause_data."</player5plugin.pausevid>
				<player5plugin.forcecap>".$timerForcepause_data."</player5plugin.forcecap>
				<player5plugin.forcewarn>".$warningtext_data."</player5plugin.forcewarn>
				
				".$player5plugin."
			";
		}

		if(!empty($vidDomain)){
			$glink = "http://".$vidDomain."/watch/".$vidcode;
		}else{
			$glink = SEO_link($vidURL);
		}
		
		if($rel_vid == "1"){
			$xmlContent .= "
				<drelated.dxmlpath>".$relatedURL."</drelated.dxmlpath>
				<drelated.dposition>bottom</drelated.dposition>
				<drelated.dskin>https://www.videosk.in/vid/drelated/skins/grayskin.swf</drelated.dskin>
				<drelated.dtarget>_blank</drelated.dtarget>
			";	
		}	
		if($fb == "true"){
			$xmlContent .= "
				<fbit.link>".$glink."</fbit.link>
			";		
		}
		
		if($twitter == "true"){
			$xmlContent .= "
				<tweetit.link>".$glink."</tweetit.link>
			";		
		}
		
		if($gplus == "true"){
			//$xmlContent .= "
			//	<plusone.dock>false</plusone.dock>
			//";		
			$xmlContent .= "
				<vgplus1.link>".$glink."</vgplus1.link>
			";
		}
	    
	    if($calltoaction_data['extra'] == "true"){
		    $ctaLoc = $calltoaction_data['value'];
		
		    $calltoaction_img_data = getmetavalue($user_id, $vid, 'calltoaction_img', true);
		    
		    $img_prev = str_replace("../", "", $calltoaction_img_data['value']);
		    if(!empty($img_prev)){
		    	$img_prev = "https://videosk.in/dashboard/".$img_prev;
		    }
		    //$hpath_img = $calltoaction_img_data['value'];
		
		    $calltoaction_url_data = getmetavalue($user_id, $vid, 'calltoaction_url', true);
			    
		    $linkurl = $calltoaction_url_data['value'];
		    
		    $calltoaction_timer_data = getmetavalue($user_id, $vid, 'calltoaction_timer', true);
		    $timerInterv = explode("|",$calltoaction_timer_data['value']);
		    $timerInt = $timerInterv[0];
		    $timerIntervS = $timerInterv[1];
		    $timerIntervE = $timerInterv[2];
		    
			if(strstr($timerIntervS, ":")){
				$splitTcS = explode(":", $timerIntervS);
				$secsTcS = ($splitTcS[0] * 60) + $splitTcS[1];
				$timerIntervS = $secsTcS;
			}
			
			if(strstr($timerIntervE, ":")){
				$splitTcE = explode(":", $timerIntervE);
				$secscTE = ($splitTcE[0] * 60) + $splitTcE[1];
				$timerIntervE = $secsTcE;
			}
			
			if(empty($timerIntervE)){
				$timerIntervE = "99999";
			}
		    
			$xmlContent .= "
				<buttoncaller.position>".$ctaLoc."</buttoncaller.position>
				<buttoncaller.img>https://videosk.in/dashboard/imgresize.php?f=".$img_prev."</buttoncaller.img>
				<buttoncaller.link>".htmlspecialchars($linkurl)."</buttoncaller.link>
				<buttoncaller.timer>".$timerInt."</buttoncaller.timer>
				<buttoncaller.timestart>".$timerIntervS."</buttoncaller.timestart>
				<buttoncaller.timeend>".$timerIntervE."</buttoncaller.timeend>
			";	
	    }
	   
	//fb subscribe
/*
	if(!empty($_SESSION['ulevel'])){
		if($_SESSION['ulevel']=="1"){
*/
		    if($fbsubscribe_data['extra'] == "true"){
			    $fbsLoc = $fbsubscribe_data['value'];
			
			    $fbsubscribe_img_data = getmetavalue($user_id, $vid, 'fb_subscribe_img', true);
			    
			    $img_prevfb = str_replace("../", "", $fbsubscribe_img_data['value']);
			    if(!empty($img_prevfb)){
			    	$xs = strstr($img_prevfb, "http://videosk.in/vid/");
			    	if(!$xs){
			    		$img_prevfb = "https://videosk.in/dashboard/imgresize.php?f=https://videosk.in/dashboard/".$img_prevfb;
			    	}else{
			    		$img_prevfb = str_replace("_".$vid, "", $img_prevfb).".png";
			    	}
			    	
			    }
			    //$hpath_img = $calltoaction_img_data['value'];
			
			    $fbsubscribe_url_data = getmetavalue($user_id, $vid, 'fb_subscribe_url', true);
				    
			    $linkurlfb = $fbsubscribe_url_data['value'];
			    
			    $fbsubscribe_timer_data = getmetavalue($user_id, $vid, 'fb_subscribe_timer', true);
			    $timerIntervfb = explode("|",$fbsubscribe_timer_data['value']);
			    $timerIntfb = $timerIntervfb[0];
			    $timerIntervSfb = $timerIntervfb[1];
			    $timerIntervEfb = $timerIntervfb[2];
			    
				if(strstr($timerIntervSfb, ":")){
					$splitTcSfb = explode(":", $timerIntervSfb);
					$secsTcSfb = ($splitTcSfb[0] * 60) + $splitTcSfb[1];
					$timerIntervSfb = $secsTcSfb;
				}
				
				if(strstr($timerIntervEfb, ":")){
					$splitTcEfb = explode(":", $timerIntervEfb);
					$secscTEfb = ($splitTcEfb[0] * 60) + $splitTcEfb[1];
					$timerIntervEfb = $secsTcEfb;
				}
				
				if(empty($timerIntervEfb)){
					$timerIntervEfb = "99999";
				}
			      $pause_data = getmetavalue($user_id, $vid, 'fb_subscribe_pause');
			      if(empty($pause_data['value'])){
			      	$pauseOnFB = "0";
			      }else{
				    $pauseOnFB = $pause_data['value'];
			      }

			      
			      $force_data = getmetavalue($user_id, $vid, 'fb_subscribe_force');
			      if(empty($force_data['value'])){
			      	$forceOnFB = "0";
			      }else{
			      	$forceOnFB = $force_data['value'];
			      }
			      
				if(empty($vidDomain)){
					$fbdomain = 'videosk.in';
				}else{
					$fbdomain = $vidDomain;
				}
				$xmlContent .= "
					<fbauth.position>".$fbsLoc."</fbauth.position>
					<fbauth.img>".$img_prevfb."</fbauth.img>
					<fbauth.link>".$linkurlfb."</fbauth.link>
					<fbauth.timer>".$timerIntfb."</fbauth.timer>
					<fbauth.timestart>".$timerIntervSfb."</fbauth.timestart>
					<fbauth.timeend>".$timerIntervEfb."</fbauth.timeend>
					<fbauth.videoid>".$vid."</fbauth.videoid>
					<fbauth.ownerid>".$user_id."</fbauth.ownerid>
					<fbauth.pausevid>".$pauseOnFB."</fbauth.pausevid>
					<fbauth.forcecap>".$forceOnFB."</fbauth.forcecap>
					<fbauth.domain>".$fbdomain."</fbauth.domain>
				";	
		    }
/*
	    }
	 }
*/
	    
	//pushlink
/*
	if(!empty($_SESSION['ulevel'])){
		if($_SESSION['ulevel']=="1"){
*/
			if($push_data['extra'] == "true"){
				$xmlContent .= "
					<pushlink.link>".htmlspecialchars($pushURL)."</pushlink.link>
					<pushlink.xtime>".$pushTimer."</pushlink.xtime>
				";
			}
/*
		}
	}
*/
/*
	if($vidDomain == $_SERVER["SERVER_NAME"] && !empty($vidDomain)){
		$ogvidContent = "http://".$vidDomain."/watch/".$vidcode;
	}else{
		$ogvidContent = SEO_link($videoURL);
		$vidDomain = "VideoSk.in";
	}
*/
	
	//FBShareGate
	//if($_SESSION['ulevel']=="1"){
		if($sharegate_data['extra'] == "true"){
			$xmlContent .= "
				<fbsharegate.link>".$glink."</fbsharegate.link>
				<fbsharegate.img>".$sharegate_img."</fbsharegate.img>
				<fbsharegate.timer>".$sharegate_sel."</fbsharegate.timer>
				<fbsharegate.timestart>".$sharegate_time."</fbsharegate.timestart>
				<fbsharegate.timeend>9999999</fbsharegate.timeend>
			";
		}
	//}
	
/*
	//vgplus
	if($gplus == "true"){
		$xmlContent .= "
			<vgplus.link>".SEO_link($vidURL)."</vgplus.link>
		";
	}
*/
    		
		$uleveltxt = strtolower(ulevelTxt($_SESSION['ulevel']));
		
		$search_txt = "whitelabel";
		
		if(strstr($search_txt, $uleveltxt) || strstr(strtolower("administrator"), $uleveltxt)){
			    	$sqlb1 = "select * from `settings` where `user_id` = '".$user_id."' and `meta` = 'business_name'";
			    	$rsb1 = mysql_query($sqlb1);
			    	while($bus_data1 = mysql_fetch_assoc($rsb1)){
			    		$business_name = $bus_data1['value'];
			    	}
			    	$sqlb2 = "select * from `settings` where `user_id` = '".$user_id."' and `meta` = 'business_url'";
			    	$rsb2 = mysql_query($sqlb2);
			    	while($bus_data2 = mysql_fetch_assoc($rsb2)){
			    		$business_url = $bus_data2['value'];
			    	}
		}else{
			if(!empty($vidDomain)){
				$business_name = $vidDomain;
				$business_url = "http://".$vidDomain;
			}else{
				$business_name = "VideoSk.in";
				$business_url = "https://www.videosk.in";			
			}

		}
	
		$xmlContent .= "
				<abouttext>".$business_name."</abouttext>
				<aboutlink>".$business_url."</aboutlink>
			</config>	
		";
		
		fwrite($ctxtFile, $xmlContent);
		fclose($ctxtFile);
		
		
		
	}//end of generateXMLconfig
	
	function countEmails($uid){
		$sql = "select * from `mails` where `user_id` = '".$uid."'";
		$rs = mysql_query($sql);
		return mysql_num_rows($rs);
	}
	
	function getVideoTitle($video_id){
		$sql = "select * from `video` where `vid_id` = '".$video_id."'";
		$rs = mysql_query($sql);
		if(mysql_num_rows($rs) > 0) :
			while($d = mysql_fetch_assoc($rs)):
				$name = $d['title'];
			endwhile;
			return $name;
		endif;
		
		return false;
	}
	
	function checkUserStatus($uid){
	
		$sql = "select * from `users` where `user_id` = '".$uid."' or `fb_uid` = '".$uid."' ";
		$rs  = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			while($data = mysql_fetch_assoc($rs)){
				$status = $data['active'];
			}
			if($status == "1"){
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
	
	function countEmailByVideo($video_id){
		$sql = "select * from `mails` where `video_id` = '".$video_id."'";
		$rs = mysql_query($sql);
		return mysql_num_rows($rs);
	}
	
	function getNumOrphanedVideos(){
		$sql = "select * from `video` where `user_id`= ''";
		$rs  = mysql_query($sql);
		return mysql_num_rows($rs);
		
	}
	
	function getULevelNum($user_id){
		$sql = "select * from `users` where `user_id` = '".$user_id."' or `fb_uid` = '".$user_id."' ";
		$rs  = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			while($data = mysql_fetch_assoc($rs)){
				$ulevel = $data['ulevel'];
			}
			return $ulevel;
		}else{
			return false;
		}
	}
	
	function ulevelTxt($levelid){
		$sql = "select * from `ulevels` where `level_id` = '".$levelid."'";
		$rs  = mysql_query($sql);
		if(mysql_num_rows($rs) > 0 ){
			while($udata = mysql_fetch_assoc($rs)){
				$leveltxt = $udata['name'];
			}
			return $leveltxt;
		}else{
			return false;
		}
		
	}
	
	function getPID_fromMID($menuID){
		$sql = "select * from `custom_menu` = '".$menuID."'";
		$rs  = mysql_query($sql);
		if(mysq_num_rows($rs) > 0){
			while($data = mysql_fetch_assoc($rs)){
				$pid = $data['link'];
			}
			return $pid;
		}else{
			return false;
		}
	}
	
	function updateHistory($userID, $status, $levelID, $date_upgrade, $date_added){
		$level = ulevelTxt($levelID);
		$sql = "
			insert into `level_history` set
			`level`    = '".$level."',
			`level_id` = '".$levelID."',
			`user_id`  = '".$userID."',
			`status`   = '".$status."',
			`date_upgrade` = '".$date_upgrade."',
			`date_added` = '".$date_added."'
		";
		mysql_query($sql);
	
	}
	
	function getEmailByUID($userID){
		$sql = "select * from `users` where `user_id` = '".$userID."' or `fb_uid` = '".$userID."'";
		$rs  = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			while($d = mysql_fetch_assoc($rs)){
				$email = $d['email'];
			}
			return $email;
		}else{
			return false;
		}
	
	}
	
	function getFullNameByUID($userID){
		$sql = "select * from `users` where `user_id` = '".$userID."' or `fb_uid` = '".$userID."'";
		$rs  = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			while($d = mysql_fetch_assoc($rs)){
				$name = $d['name'];
			}
			return $name;
		}else{
			return false;
		}
	
	}
	
	function countRegister($reg_id){
		$sql = "select * from `track_rcodes` where code_id = '".$reg_id."'";
		$rs  = mysql_query($sql);
		return mysql_num_rows($rs);
	}

	function getCategoryName($cid){
		$sql = "select * from `category` where `cid` = '".$cid."'";
		$rs  = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			while($f = mysql_fetch_assoc($rs)){
				$name = $f['category'];
			}
			return $name;
		}else{
			return false;
		}
	}

	function countVideoByCategory($cid, $user_id){
		$sql = "select * from `video` where `user_id` = '".$user_id."' and `cat_id` = '".$cid."'";
		$rs  = mysql_query($sql);
		return mysql_num_rows($rs);
	}

	function video_count($user_id){
		$sql = "select * from `video` where `user_id` = '".$user_id."'";
		$rs  = mysql_query($sql);
		return mysql_num_rows($rs);
	}

	function relatedLinkCount($vid_id){
        $sql_r = "select * from `related_vids` where `video_id` = '".$vid_id."' order by `date_added` asc";
        $rs_r  = mysql_query($sql_r);
        return mysql_num_rows($rs_r);
	}

	function relatedlink_status($vid_id){
            $sql_r = "select * from `related_vids` where `video_id` = '".$vid_id."' order by `date_added` asc";
            $rs_r  = mysql_query($sql_r);
            if(mysql_num_rows($rs_r) > 0){
                $thumb_id = array();
                $thumb_link = array();
                $thumb_img = array();
                $newthumb_img = array();
                $thumb_onOff = array();
                while($thumb_data = mysql_fetch_assoc($rs_r)){
                    $thumb_id[] = $thumb_data['rel_id'];
                    $thumb_link[] = $thumb_data['link'];
                    $thumb_img[] = $thumb_data['img'];
                    $newthumb_img[] = str_replace("../", "", $thumb_data['img']);
                    $thumb_onOff[] = $thumb_data['active'];
                }
                $toggle_onOff = $thumb_onOff[0] ;
                if($toggle_onOff == "true"){
                    $toggle_checked = "checked";
                }else{
                    $toggle_checked = "";
                }
            }else{
                $thumb_id = array();
                $thumb_link = array();
                $thumb_img = array();
                $newthumb_img = array();
                $thumb_onOff = array();
                $toggle_checked = "";
            }
            return $toggle_checked;
	}

	function socialbutton_status($userID, $vid_id){
		  $sql = "select * from `social_settings` where `user_id` = '".$userID."' and `vid_id` = '".$vid_id."'";
		  $rs  = mysql_query($sql);
		  if(mysql_num_rows($rs) > 0){
		    while($social_setting = mysql_fetch_assoc($rs)){
		      $twitter = $social_setting['twitter'];
		      $fb = $social_setting['fb'];
		      $gplus = $social_setting['gplus'];
		    }
		    
		    if($twitter == "true"){
		      $tw_checked = "checked";
		    }else{
		      $tw_checked = "";
		    }
		    if($fb == "true"){
		      $fb_checked = "checked";
		    }else{
		      $fb_checked = "";
		    }
		    if($gplus == "true"){
		      $gp_checked = "checked";
		    }else{
		      $gp_checked = "";
		    }

		  }
		  return mysql_num_rows($rs);
	}

	function genURLCode(){
		$code = generateCode(10, 4);
		$sql = "select * from `tcards_url` where md5(`ccode`) = md5('".$code."') ";
		$rs = mysql_query($sql);
		if(mysql_num_rows($rs) > 0){
			genURLCode();
		}else{
			return $code;
		}
	}


	/* backup the db OR just a table */
	function backup_tables($host,$user,$pass,$name,$tables = '*')
	{
		
		$link = mysql_connect($host,$user,$pass);
		mysql_select_db($name,$link);
		
		//get all of the tables
		if($tables == '*')
		{
			$tables = array();
			$result = mysql_query('SHOW TABLES');
			while($row = mysql_fetch_row($result))
			{
				$tables[] = $row[0];
			}
		}
		else
		{
			$tables = is_array($tables) ? $tables : explode(',',$tables);
		}
		
		//cycle through
		foreach($tables as $table)
		{
			$result = mysql_query('SELECT * FROM '.$table);
			$num_fields = mysql_num_fields($result);
			
			$return.= 'DROP TABLE '.$table.';';
			$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
			$return.= "\n\n".$row2[1].";\n\n";
			
			for ($i = 0; $i < $num_fields; $i++) 
			{
				while($row = mysql_fetch_row($result))
				{
					$return.= 'INSERT INTO '.$table.' VALUES(';
					for($j=0; $j<$num_fields; $j++) 
					{
						$row[$j] = addslashes($row[$j]);
						$row[$j] = ereg_replace("\n","\\n",$row[$j]);
						if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
						if ($j<($num_fields-1)) { $return.= ','; }
					}
					$return.= ");\n";
				}
			}
			$return.="\n\n\n";
		}
	
		//save file
		$save_file = $host.'_db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql';
		$handle = fopen($save_file,'w+');
		fwrite($handle,$return);
		fclose($handle);
		return $save_file;
	}

	function unzip(){

		$zip = new ZipArchive;
		$res = $zip->open('file.zip');
		if ($res === TRUE) {
		  $zip->extractTo('/myzips/extract_path/');
		  $zip->close();
		  echo 'woot!';
		} else {
		  echo 'doh!';
		}

	}

	function getTCardName($card_id){
    	$sql = "select * from `tcards_url` where `tc_id` = '".$card_id."'";
    	$rs  = mysql_query($sql);
    	if(mysql_num_rows($rs) > 0){
    		while($f = mysql_fetch_assoc($rs)){
    			$name = $f['name'];
    		}
    		return $name;
    	}else{
    		return false;
    	} 
    }

    function getMySubscribers($uid){
    	$sql = 'select * from `subscribers` where `user_id` = \''.$uid.'\' ';
    	$rs = mysql_query($sql);
    	return mysql_num_rows($rs);
    }

    function get_option_meta($by, $by_value, $option_name){
    	$sql = "select * from `options` where `meta` = '".$option_name."' and `".$by."` = '".$by_value."' limit 1";
    	$rs  = mysql_query($sql);
    	if(mysql_num_rows($rs) > 0){
    		$data = mysql_fetch_assoc($rs);
    		return $data['meta_value'];
    	}else{
    		return false;
    	}
    }

    function getMyLeadsByID($cid, $uid){
    	$sql = 'select * from `subscribers` where `user_id` = \''.$uid.'\' and `tc_id` = \''.$cid.'\'';
    	$rs = mysql_query($sql);
    	return mysql_num_rows($rs);
    }

    function countURLByCategory($category_name, $u_id){
        $sql = "select * from `tcards_url` where `user_id` = '".$u_id."' and `tags` = '".$category_name."'";
        $rs  = mysql_query($sql);
        return mysql_num_rows($rs);
    }


    function checkemail_getresponse($email_user, $campaign_user){
		require_once('GetResponseAPI.class.php');
		$api = new GetResponse('14830f2fe0dc591cb598c16076c52b45');
		// $api = new GetResponse($apikey);

		// Connection Testing
		$ping = $api->ping();
		// var_dump($ping);

		// Account
		$details = $api->getAccountInfo();
		// var_dump($details);

		// Campaigns
		$campaigns 	 = (array)$api->getCampaigns();
		$campaignIDs = array_keys($campaigns);

		// Contacts
		$contacts 	= (array)$api->getContacts(null);
		$contactIDs	= array_keys($contacts);


		$ctr = 0;
		foreach($contacts as $contkey){
			if($contkey->email == $email_user && $contkey->campaign == $campaign_user){
				$ctr++;
			}
		}

		return $ctr;
    }

    function getOptionValue($meta, $uid){
    	$sql = "select * from `options` where `meta` = '$meta' and `user_id` = '$uid'";
    	$rs  = mysql_query($sql);
    	if(mysql_num_rows($rs) > 0){
    		$val = mysql_fetch_assoc($rs);
    		return $val['meta_value'];
    	}else{
    		return '';
    	}
    }

    function CheckIfAvailFeature($level_id, $level_name){
    	if($level_id > 1){
	    	$sql = "select * from `ulevels` where `level_id` = '$level_id' and `pages` like '%".$level_name."%'";
	    	$rs  = mysql_query($sql);
	    	if(mysql_num_rows($rs) > 0){
	    		return true;
	    	}else{
	    		return false;
	    	}
    	}else{
    		return true;
    	}
    }

    function getULevelName($level_id){
    	$sql = "select * from  `ulevels` where `level_id` = '$level_id'";
    	$rs  = mysql_query($sql);
    	if(mysql_num_rows($rs) > 0){
    		$val = mysql_fetch_assoc($rs);
    		return $val['name'];
    	}else{
    		return '';
    	}
    }

    function getULevelLimit($level_id){
    	$sql = "select * from  `ulevels` where `level_id` = '$level_id'";
    	$rs  = mysql_query($sql);
    	if(mysql_num_rows($rs) > 0){
    		$val = mysql_fetch_assoc($rs);
    		return $val['limit'];
    	}else{
    		return '';
    	}
    } 

    function getDomain($uid){
    	$sql = "select * from  `domains` where `user_id` = '$uid'";
    	$rs  = mysql_query($sql);
    	if(mysql_num_rows($rs) > 0){
    		$val = mysql_fetch_assoc($rs);
    		return $val['name'];
    	}else{
    		return '';
    	}
    }

function getDomStatus($domain){
    $result = dns_get_record($domain, DNS_CNAME);
    $cn = count($result);
    if($cn > 0){
        //echo "<img src=\"images/tick.png\"> ";
        $res['content'] = '
                <div class="alert alert-success">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <strong>Well done!</strong>
                Domain '.$_POST['d'].' was CNAME to '.$result[0]['target'].'
                </div>
        ';
        // if(!empty($dmdid)){
        //  echo '
        //      <script type="text/javascript">
        //          $(document).ready(function(){
        //              $("#divver'.$dmdid.'").html("<span class="label label-success">Verified</span>");
        //          });
        //      </script>
        //  ';
        // }
        $res['errcode'] = '100';
        $status = '<span style="color: #6add18;">Domain Verified</span>';
    }else{
        //echo "<img src=\"images/cross.png\"> Domain could not be verified!";
        $res['content'] ='
                <div class="alert alert-danger">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <strong>Oh Snap!</strong>
                Domain '.$_POST['d'].' was not set as CNAME
                </div>
        ';
        //return false;
        $res['errcode'] = '104';
        $status = '<span style="color: #ef3232;">Domain Not Verified</span>';
    }
    return $status;
}


//==============================================
// TRANSPORT MAILS 
// (POSTMARK MAILER FUNCTION)
//==============================================
function myTransportMailer($to, $to_name, $from, $from_name, $subject, $htmlmsg){
    require_once '../api/swiftmailer/lib/swift_required.php';
    require_once '../api/postmark-swiftmailer/postmark_swiftmailer.php';

    if(empty($to_name)){
    	$to_name = $to;
    }
    
    if(empty($from_name)){
    	$from_name = $from;
    }
    // 862e3b8d-5a3d-4d7a-858e-196309c77979
    // support@tweetlead.io
    $transport = Swift_PostmarkTransport::newInstance('862e3b8d-5a3d-4d7a-858e-196309c77979');

    $mailer = Swift_Mailer::newInstance($transport);
    $message = Swift_Message::newInstance($subject)
      ->setFrom(array($from => $from_name))
      ->setTo(array($to => $to_name))
      ->setBody($htmlmsg, 'text/html')
      ->addPart($htmlmsg, 'text/html');
    $mailer->send($message); 
}

//==============================================
// TRANSPORT PHP MAILS 
// (PHP MAILER FUNCTION)
//==============================================
function mySimpleMailer($to, $to_name, $from, $from_name, $subject, $htmlmsg){
    if(empty($to_name)){
    	$to_name = $to;
    }
    
    if(empty($from_name)){
    	$from_name = $from;
    }
	// To send HTML mail, the Content-type header must be set
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: '.$from_name.' <'.$from.'>' . "\r\n";
 
    $m = @mail($to, $subject, $htmlmsg, $headers);  
}

//==============================================
// LOGIN and PAGE VISIT LOGS
// 
//==============================================

function TheLogger($userid, $email, $page, $date){
	
}

//include_once("system/includes/connect_db.php");
//include_once("system/includes/classes/simple_html_dom.php");

function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
    $url = 'http://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}

function _page_title($page){
	$page = ucwords(str_replace("_", " ", $page));
	return (!empty($page)) ? $page : 'Dashboard';
}

function translateUlevel($id, $gconn){
	// switch($id){
	// 	case '1':
	// 		$level = 'Administrator';
	// 	break;
	// 	case '2':
	// 		$level = 'Priest';
	// 	break;
	// 	case '3':
	// 		$level = 'Reader';
	// 	break;
	// 	case '4':
	// 		$level = 'Collector';
	// 	break;
	// 	case '5':
	// 		$level = 'Lay Ministry';
	// 	break;
	// 	case '6':
	// 		$level = 'Acolytes';
	// 	break;
	// }
	$rs = $gconn->dbquery("SELECT * FROM `ulevels` WHERE `level_id` = '".$id."'" );
	$rs = json_decode($rs);
	$rs = $rs->data;
	// return $rs;
	if(count($rs) > 0){
		foreach($rs as $r){
			$ar = json_decode($r);
		}  	
	}else{
		return "Not Assigned";
	}
	return $ar->name;
}

function translateVenue($val){
	switch($val){
		case 'venue_1':
			$venue = 'Venue 1';
		break;
		case 'venue_2':
			$venue = 'Venue 2';
		break;
		case 'venue_3':
			$venue = 'Venue 3';
		break;
		case 'venue_4':
			$venue = 'Venue 4';
		break;
		case 'others':
			$venue = 'Others';
		break;
		default:
			$venue = 'None';
		break;
	}
	return $venue;
}


function translateName($id, $gconn){
  $rs = $gconn->dbquery("SELECT * FROM `users` WHERE `user_id` = '".$id."'" );
  $rs = json_decode($rs);
  $rs = $rs->data;
  if(count($rs) > 0){
	foreach($rs as $r){
		$ar = json_decode($r);
	}  	
  }else{
  	return "Not Assigned";
  }
  return $ar->name;
}

function getUser_levelid($id, $gconn){
  $rs = $gconn->dbquery("SELECT * FROM `users` WHERE `user_id` = '".$id."'" );
  $rs = json_decode($rs);
  $rs = $rs->data;
  if(count($rs) > 0){
	foreach($rs as $r){
		$ar = json_decode($r);
	}  	
  }else{
  	return "Not Assigned";
  }
  return $ar->ulevel;
}

function getUser_Email($id, $gconn){
  $rs = $gconn->dbquery("SELECT * FROM `users` WHERE `user_id` = '".$id."'" );
  $rs = json_decode($rs);
  $rs = $rs->data;
  if(count($rs) > 0){
	foreach($rs as $r){
		$ar = json_decode($r);
	}  	
  }else{
  	return "Not Assigned";
  }
  return $ar->email;
}


function getDonations($gconn){
  $rs = $gconn->dbquery("SELECT * FROM `donation` " );
  $rs = json_decode($rs);
  $rs = $rs->data;
  if(count($rs) > 0){
	foreach($rs as $r){
		$ar = json_decode($r);
	}  	
  }else{
  	return "0";
  }
  return count($rs);
}

function getInvalidDonations($gconn){
  $rs = $gconn->dbquery("SELECT * FROM `donation` where `status` = 'invalid'" );
  $rs = json_decode($rs);
  $rs = $rs->data;
  if(count($rs) > 0){
	foreach($rs as $r){
		$ar = json_decode($r);
	}  	
  }else{
  	return "0";
  }
  return count($rs);
}

function getBookings($gconn){
  $rs = $gconn->dbquery("SELECT * FROM `reservation`" );
  $rs = json_decode($rs);
  $rs = $rs->data;
  if(count($rs) > 0){
	foreach($rs as $r){
		$ar = json_decode($r);
	}  	
  }else{
  	return "0";
  }
  return count($rs);
}

function getPaidBookins($gconn){
  $rs = $gconn->dbquery("SELECT * FROM `reservation` where `status`  = 'approved'" );
  $rs = json_decode($rs);
  $rs = $rs->data;
  if(count($rs) > 0){
	foreach($rs as $r){
		$ar = json_decode($r);
		$amnt += $ar->amount;
	}  	
  }else{
  	return "0";
  }
  return $amnt;
}


function getNewBookins($gconn){
  $rs = $gconn->dbquery("SELECT * FROM `reservation` where `status` = 'new'" );
  $rs = json_decode($rs);
  $rs = $rs->data;
  if(count($rs) > 0){
	foreach($rs as $r){
		$ar = json_decode($r);
	}  	
  }else{
  	return "0";
  }
  return count($rs);
}

function getCollections($gconn){
  $rs = $gconn->dbquery("SELECT * FROM `collections`" );
  $rs = json_decode($rs);
  $rs = $rs->data;
  if(count($rs) > 0){
	foreach($rs as $r){
		$ar = json_decode($r);
		$amnt += $ar->amount;
	}  	
  }else{
  	return "0";
  }
  return $amnt;
}

function valueToWords($value){
	$v = str_replace("_", " ", $value);
	return ucwords($v);
}


function getClientName($cid){
	global $conn;
	$sql = "select * from `client_data` where `client_id` = '".$cid."' ";
	$rs  = $conn->dbquery($sql);
	$rs = json_decode($rs);
	$rs = $rs->data;
	if(count($rs) > 0){
		foreach ($rs as $r) {
			# code...
			$ar = json_decode($r);
			$full = $ar->name.' '.$ar->middle_name.' '.$ar->last_name;
		}

	}
	return $full;

}

function CheckORS($ors){
	global $conn;

	$sqlcheckORS = "select * from `finance` where `voucher_id` = '".$ors."' ";
	$res = $conn->dbquery($sqlcheckORS);
	// $res = json_decode($res);
	if($res === 'false'){
		// return $ors;
		$sqlprefix = "select * from `settings` where `meta` = 'orsprefix' ";
		$res = $conn->dbquery($sqlprefix);
		$res = json_decode($res);
		$res = json_decode($res->data[0]);
		$vprefix = $res->value;

		$sqldigit = "select * from `settings` where `meta` = 'orsdigit' ";
		$resd = $conn->dbquery($sqldigit);
		$resd = json_decode($resd);
		$resd = json_decode($resd->data[0]);
		$vstart = $resd->value; //digit

		//update 
		// `vid`, `prefix`, `number`, `date_added`

		$getLatest = "select * from `ors_numbers` where `prefix` = '".$vprefix."' order by `ors_id` desc  ";
		$res1 = $conn->dbquery($getLatest);
		$res1 = json_decode($res1);
		$res1 = json_decode($res1->data[0]);

		$vnum = $res1->code + 1;

		$newCode = $vprefix.''.str_pad($vnum, $vstart, '0', STR_PAD_LEFT);

		// echo $newCode;
		// $inpORNum = $newCode;

		return $newCode;
	}else{
		//get prefix
		// $sqlprefix = "select * from `settings` where `meta` = 'orsprefix' ";
		// $res = $conn->dbquery($sqlprefix);
		// $res = json_decode($res);
		// $res = json_decode($res->data[0]);
		// $vprefix = $res->value;

		// $sqldigit = "select * from `settings` where `meta` = 'orsdigit' ";
		// $resd = $conn->dbquery($sqldigit);
		// $resd = json_decode($resd);
		// $resd = json_decode($resd->data[0]);
		// $vstart = $resd->value; //digit

		// //update 
		// // `vid`, `prefix`, `number`, `date_added`

		// $getLatest = "select * from `ors_numbers` where `prefix` = '".$vprefix."' order by `ors_id` desc  ";
		// $res1 = $conn->dbquery($getLatest);
		// $res1 = json_decode($res1);
		// $res1 = json_decode($res1->data[0]);

		// $vnum = $res1->code + 1;

		// $newCode = $vprefix.''.str_pad($vnum, $vstart, '0', STR_PAD_LEFT);
		CheckORS($ors);	
	}
}

function getORSPrefix(){
	global $conn;

	$sqlprefix = "select * from `settings` where `meta` = 'orsprefix' ";
	$res = $conn->dbquery($sqlprefix);
	$res = json_decode($res);
	$res = json_decode($res->data[0]);
	$vprefix = $res->value;

	return $vprefix;
}

function getORSDigit(){
	global $conn;

	$sqldigit = "select * from `settings` where `meta` = 'orsdigit' ";
	$resd = $conn->dbquery($sqldigit);
	$resd = json_decode($resd);
	$resd = json_decode($resd->data[0]);
	$vstart = $resd->value; //digit

	return $vstart;
}


function getClientBalance($client_id){
	global $conn;

	//get beginning balance
	//txtBeginningBal
	$is_beg = false;
	$sqlBB = "select * from `client_meta` where `meta` = 'txtBeginningBal' and `client_id` = '".$client_id."' ";
	$rsBB = $conn->dbquery($sqlBB);
	// $rs1 = json_decode($rs1);
	if($rsBB !== 'false'){
		$is_beg = true;
		$rsBB = json_decode($rsBB);
		$nrsBB = json_decode($rsBB->data[0]);
		$beg_bal = $nrsBB->value;	
		// return $nrsBB->value.'hello';
		// exit();
	}

	// echo $beg_bal;
	//get schedule interest payment
	// $interest_psched = getClientSchedPayment($client_id);
	$sqlAB = "select * from `client_meta` where `client_id` = '".$client_id."' and `meta` = 'txtAdditionalBalance'";
	$rsAB = $conn->dbquery($sqlAB);
	if($rsAB !== 'false'){
		$rsAB = json_decode($rsAB);
		$rsAB = json_decode($rsAB->data[0]);
		$additionalBalance = $rsAB->value;		
	}else{
		$additionalBalance = 0;
	}

	$interest_psched = getClientSchedPayment($client_id);

	switch($interest_psched){
		case '1':
			$sched = 15;
		break;

		case '2':
			$sched = 30;
		break;
	}

	$int_sched_start = getClientSchedPaymentStart($client_id)." 00:00:00";

	$is_interest_applied = false;
	$is_interest_ctr = 0;
	$ctr = 0;

	$sql = "select * from `finance` where `client_id` = '".$client_id."' and `status` = 'approved' order by `date_added` asc";
	$res = $conn->dbquery($sql);

	if($res !== 'false'){
		$res = json_decode($res);

		$int_ctr = 0; //counter for applied interest
		foreach ($res->data as $key) {
			# code...
			// echo $ctr;
			$nres = json_decode($key);
			$extra = json_decode($nres->extra);
			if($extra->TypeRate == 'diminishing' || $extra->TypeRate == '-'){
				$type_rate = 'DR';
			}else{
				$type_rate = 'FR';
			}

			if($type_rate == 'DR'){
				//DEMINISHING RATE

				$intrate = $nres->intrate;
				$intrate_val = $intrate / 100;

				$voucher_num = $nres->voucher_id;
				$or_num = '';
				$payment = number_format(0,2);


				$interest = $loan_amnt_temp * $intrate_val;

				// $loan_amnt = number_format($nres->amount, 2);
				$loan_amnt = $nres->amount;

				// echo $nres->amount;
				if($ctr == 0){
					$loan_start = $loan_amnt;
					if(!empty($beg_bal)){	
					// if($is_beg == true){
						$loan_start = $beg_bal;		
						$loan_amnt = $beg_bal;
					}
				}

				if($nres->loan_type == 'new_loans'){
					$balance = $loan_amnt;
					$int_amount = 0;
					$net_amount = 0;
					// $minus_ctr = 2;
				// }else{
				}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
					$int_amount = 0;
					$net_amount = 0;

					$balance = $balance + $loan_amnt;
					// $minus_ctr = 1;
				}

				// else{
				// 	$int_amount = 0;
				// 	$net_amount = 0;
				// 	$balance = $loan_amnt;
				// }
				// echo $loan_amnt; //exit();
				//cash_advance
				// special_account
				// additional_loan
				// renewal
				// excess
				// if($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
				// 	$int_amount = 0;
				// 	$net_amount = 0;
				// 	$balance = $balance + $loan_amnt;
				// }else{
				// 	$balance = $loan_amnt;
				// 	$int_amount = 0;
				// 	$net_amount = 0;
				// }
				// echo "[".$bal_array[$ctr-2]."]";

				if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors'){
					$loan_amnt = number_format(0,2);
					if($ctr == 0){
						$loan_start = $loan_amnt;
						if(!empty($beg_bal)){
						// if($is_beg == true){
							$loan_start = $beg_bal;		
							$loan_amnt = $beg_bal;
							$balance = $loan_amnt;
						}
					}

					$voucher_num = '-';
					$or_num = $nres->voucher_id;
					$payment = $nres->amount;
					//date paid and start deductions
					$start_interest = getClientSchedPaymentStart($client_id);

					$d_approved = strtotime($nres->date_approved);
					$int_date_start = strtotime($start_interest);

					$p_day = explode(" ", $nres->date_approved);
					$p_day = explode("-", $p_day[0]);
					$p_day = $p_day[2];

					if($d_approved == $int_date_start){
						// $balance = $loan_start;
						// $int_amount = ($balance * ($nres->intrate/100));
						// $int_amount = ($loan_start * ($nres->intrate/100)); //original
						$int_amount = ($bal_array[0] * ($nres->intrate/100));
						$net_amount = $payment - $int_amount;	

						// $loan_start = $				
						$balance = $balance - $net_amount;
						// $loan_start = $balance;
						$int_ctr++;
					}else{

						if($p_day >= 1 && $p_day <= 15){
							$sched_int = 1;
						}elseif($p_day >= 15 && $p_day <= 31){
							$sched_int = 2;
						}


						if($interest_psched == $sched_int && $d_approved > $int_date_start){
							// $int_amount = ($loan_start * ($nres->intrate/100)); //original
							if($int_ctr == 0){
								$int_amount = ($loan_start * ($nres->intrate/100)); //original
							}else{
								// $int_amount = ($bal_array[$ctr-$minus_ctr] * ($nres->intrate/100));	
								$int_amount = ($bal_array[$ctr-2] * ($nres->intrate/100));	
							}
							$net_amount = $payment - $int_amount;

							$balance = $balance - $net_amount;
							$loan_start = $balance;
							// $loan_amnt = $balance;
							$int_ctr++;
						}else{
							$int_amount = 0;
							// if($ctr == 0){
							// 	// $loan_start = $loan_amnt;
							// 	if($is_beg == true){
							// 		// $loan_start = $beg_bal;		
							// 		// $loan_amnt = $loan_start;
							// 		$balance = $beg_bal;
							// 	}
							// }
							$net_amount = $payment;			
							$balance = $balance - $net_amount;	

							// $loan_amnt = $balance;
							$loan_start = $balance;
						}

					}

					// if($extra->ApplyInt == 'on'){
					// 	$int_amount = ($balance * ($nres->intrate/100));
					// 	$net_amount = $payment - $int_amount;					
					// }

					// if($extra->ApplyInt == 'off' || $extra->ApplyInt == ''){
					// 	$int_amount = 0;
					// 	$net_amount = $payment;
					// }
					// $loan_amnt = $balance;

				}
				$bal_array[$ctr] = $balance; //added 01-16-2018
				// echo $bal_array[$ctr].'='.$ctr;
				$ctr++;
			}else{
				//FIXED RATE

				$intrate = $nres->intrate;
				$intrate_val = $intrate / 100;

				$voucher_num = $nres->voucher_id;
				$or_num = '';
				$payment = number_format(0,2);

				$loan_amnt = $nres->amount;
				if($is_FR_set == false){
					$int_amount = ($loan_amnt * ($nres->intrate/100));
					$loan_start = $loan_amnt;
					$is_FR_set = true;
				}

				if($nres->loan_type == 'new_loans'){
					$balance = $loan_amnt;
					$int_amount = 0;
					$net_amount = 0;
				// }else{
				}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
					$int_amount = 0;
					$net_amount = 0;
					$balance = $balance + $loan_amnt;
					// $net_amount = $payment - $int_amount;	
				}


				if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors'){
					$loan_amnt = number_format(0,2);
					$voucher_num = '-';
					$or_num = $nres->voucher_id;
					$payment = $nres->amount;
					//date paid and start deductions
					$start_interest = getClientSchedPaymentStart($client_id);

					$d_approved = strtotime($nres->date_approved);
					$int_date_start = strtotime($start_interest);

					$p_day = explode(" ", $nres->date_approved);
					$p_day = explode("-", $p_day[0]);
					$p_day = $p_day[2];

					if($d_approved == $int_date_start){
						// $balance = $loan_start;
						// $int_amount = ($balance * ($nres->intrate/100));
						// $int_amount = ($loan_start * ($nres->intrate/100));
						$int_amount = ($bal_array[0] * ($nres->intrate/100));
						$net_amount = $payment - $int_amount;	

						// $loan_start = $										
						$balance = $balance - $net_amount;
						$loan_start = $balance;
						// $loan_amnt = $balance;
					}else{

						if($p_day >= 1 && $p_day <= 15){
							$sched_int = 1;
						}elseif($p_day >= 15 && $p_day <= 31){
							$sched_int = 2;
						}

						if($interest_psched == $sched_int && $d_approved > $int_date_start){
							// $int_amount = ($loan_start * ($nres->intrate/100));
							$int_amount = ($bal_array[0] * ($nres->intrate/100));
							
							$net_amount = $payment - $int_amount;											

							$balance = $balance - $net_amount;
							$loan_start = $balance;
							// $loan_amnt = $balance;
						}else{
							$int_amount = 0;
							$net_amount = $payment;			
							$balance = $balance - $net_amount;		
							$loan_start = $balance;	
							// $loan_amnt = $balance;													
						}

						
					}
				}							
				$bal_array[$ctr] = $balance; //added 01-16-2018
				// echo $bal_array[$ctr].'='.$ctr;
				$ctr++;

			}

			$loan_amount_disp = $loan_amnt;
			// echo $loan_type;
			if($nres->loan_type == 'beg_bal'){
				// echo '
	 		// 		<tr>
	 		// 			<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
	 		// 			<td>'.$voucher_num.'</td>
	 		// 			<td>-</td>
	 		// 			<td>'.number_format($loan_amount_disp, 2).'</td>
	 		// 			<td>'.$or_num.'</td>
	 		// 			<td>'.number_format($payment, 2).'</td>
	 		// 			<td>'.$intrate.'% '.$type_rate.'</td>
	 		// 			<td>'.number_format($int_amount,2).'</td>
	 		// 			<td>'.number_format($net_amount,2).'</td>
	 		// 			<td>'.number_format($balance, 2).'</td>
	 		// 		</tr>
				// ';	
				$is_start_beg = true; // for display loan amount flag
			}else{
				// check display loan amount flag
				if($is_start_beg){
					$loan_amount_disp = 0;
					$is_start_beg = false;
				}
				// echo '
	 		// 		<tr>
	 		// 			<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
	 		// 			<td>'.$voucher_num.'</td>
	 		// 			<td>'.number_format($loan_amount_disp, 2).'</td>
	 		// 			<td>-</td>
	 		// 			<td>'.$or_num.'</td>
	 		// 			<td>'.number_format($payment, 2).'</td>
	 		// 			<td>'.$intrate.'% '.$type_rate.'</td>
	 		// 			<td>'.number_format($int_amount,2).'</td>
	 		// 			<td>'.number_format($net_amount,2).'</td>
	 		// 			<td>'.number_format($balance, 2).'</td>
	 		// 		</tr>
				// ';	
			}

			$loan_amnt = $balance;

			//<td>'.number_format($balance, 2).'</td>
		}
		return number_format(($balance - $additionalBalance), 2);
	}else{
		return number_format(0, 2);	
	}


	if($res !== 'false'){
		$res = json_decode($res);

		$int_ctr = 1;
		foreach ($res->data as $key) {
			# code...
			// echo $ctr;
			$nres = json_decode($key);
			$extra = json_decode($nres->extra);
			if($extra->TypeRate == 'diminishing' || $extra->TypeRate == '-'){
				$type_rate = 'DR';
			}else{
				$type_rate = 'FR';
			}

			if($type_rate == 'DR'){
				//DEMINISHING RATE

				$intrate = $nres->intrate;
				$intrate_val = $intrate / 100;

				$voucher_num = $nres->voucher_id;
				$or_num = '';
				$payment = number_format(0,2);


				$interest = $loan_amnt_temp * $intrate_val;

				// $loan_amnt = number_format($nres->amount, 2);
				$loan_amnt = $nres->amount;

				// echo $nres->amount;
				if($ctr == 0){
					$loan_start = $loan_amnt;
					if(!empty($beg_bal)){	
					// if($is_beg == true){
						$loan_start = $beg_bal;		
						$loan_amnt = $beg_bal;
					}
				}

				if($nres->loan_type == 'new_loans'){
					$balance = $loan_amnt;
					$int_amount = 0;
					$net_amount = 0;
					$minus_ctr = 2;
				// }else{
				}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
					$int_amount = 0;
					$net_amount = 0;

					$balance = $balance + $loan_amnt;
					$minus_ctr = 1;
				}

				// else{
				// 	$int_amount = 0;
				// 	$net_amount = 0;
				// 	$balance = $loan_amnt;
				// }
				// echo $loan_amnt; //exit();
				//cash_advance
				// special_account
				// additional_loan
				// renewal
				// excess
				// if($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
				// 	$int_amount = 0;
				// 	$net_amount = 0;
				// 	$balance = $balance + $loan_amnt;
				// }else{
				// 	$balance = $loan_amnt;
				// 	$int_amount = 0;
				// 	$net_amount = 0;
				// }
				// echo "[".$bal_array[$ctr-2]."]";

				if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors'){
					$loan_amnt = number_format(0,2);
					if($ctr == 0){
						$loan_start = $loan_amnt;
						if(!empty($beg_bal)){
						// if($is_beg == true){
							$loan_start = $beg_bal;		
							$loan_amnt = $beg_bal;
							$balance = $loan_amnt;
						}
					}

					$voucher_num = '-';
					$or_num = $nres->voucher_id;
					$payment = $nres->amount;
					//date paid and start deductions
					$start_interest = getClientSchedPaymentStart($client_id);

					$d_approved = strtotime($nres->date_approved);
					$int_date_start = strtotime($start_interest);

					$p_day = explode(" ", $nres->date_approved);
					$p_day = explode("-", $p_day[0]);
					$p_day = $p_day[2];

					if($d_approved == $int_date_start){
						// $balance = $loan_start;
						// $int_amount = ($balance * ($nres->intrate/100));
						// $int_amount = ($loan_start * ($nres->intrate/100)); //original
						$int_amount = ($bal_array[$ctr-$minus_ctr] * ($nres->intrate/100));
						$net_amount = $payment - $int_amount;	

						// $loan_start = $				
						$balance = $balance - $net_amount;
						// $loan_start = $balance;

					}else{

						if($p_day >= 1 && $p_day <= 15){
							$sched_int = 1;
						}elseif($p_day >= 15 && $p_day <= 31){
							$sched_int = 2;
						}



						if($interest_psched == $sched_int && $d_approved > $int_date_start){
							// $int_amount = ($loan_start * ($nres->intrate/100)); //original
							$int_amount = ($bal_array[$ctr-$minus_ctr] * ($nres->intrate/100));
							$net_amount = $payment - $int_amount;

							$balance = $balance - $net_amount;
							$loan_start = $balance;
							// $loan_amnt = $balance;
						}else{
							$int_amount = 0;
							// if($ctr == 0){
							// 	// $loan_start = $loan_amnt;
							// 	if($is_beg == true){
							// 		// $loan_start = $beg_bal;		
							// 		// $loan_amnt = $loan_start;
							// 		$balance = $beg_bal;
							// 	}
							// }
							$net_amount = $payment;			
							$balance = $balance - $net_amount;	

							// $loan_amnt = $balance;
							$loan_start = $balance;
						}

					}

					// if($extra->ApplyInt == 'on'){
					// 	$int_amount = ($balance * ($nres->intrate/100));
					// 	$net_amount = $payment - $int_amount;					
					// }

					// if($extra->ApplyInt == 'off' || $extra->ApplyInt == ''){
					// 	$int_amount = 0;
					// 	$net_amount = $payment;
					// }
					// $loan_amnt = $balance;

				}
				$bal_array[$ctr] = $balance; //added 01-16-2018
				// echo $bal_array[$ctr].'='.$ctr;
				$ctr++;
			}else{
				//FIXED RATE

				$intrate = $nres->intrate;
				$intrate_val = $intrate / 100;

				$voucher_num = $nres->voucher_id;
				$or_num = '';
				$payment = number_format(0,2);

				$loan_amnt = $nres->amount;
				if($is_FR_set == false){
					$int_amount = ($loan_amnt * ($nres->intrate/100));
					$loan_start = $loan_amnt;
					$is_FR_set = true;
				}

				if($nres->loan_type == 'new_loans'){
					$balance = $loan_amnt;
					$int_amount = 0;
					$net_amount = 0;
				// }else{
				}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
					$int_amount = 0;
					$net_amount = 0;
					$balance = $balance + $loan_amnt;
					// $net_amount = $payment - $int_amount;	
				}


				if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors'){
					$loan_amnt = number_format(0,2);
					$voucher_num = '-';
					$or_num = $nres->voucher_id;
					$payment = $nres->amount;
					//date paid and start deductions
					$start_interest = getClientSchedPaymentStart($client_id);

					$d_approved = strtotime($nres->date_approved);
					$int_date_start = strtotime($start_interest);

					$p_day = explode(" ", $nres->date_approved);
					$p_day = explode("-", $p_day[0]);
					$p_day = $p_day[2];

					if($d_approved == $int_date_start){
						// $balance = $loan_start;
						// $int_amount = ($balance * ($nres->intrate/100));
						$int_amount = ($loan_start * ($nres->intrate/100));
						$net_amount = $payment - $int_amount;	

						// $loan_start = $										
						$balance = $balance - $net_amount;
						$loan_start = $balance;
						// $loan_amnt = $balance;
					}else{

						if($p_day >= 1 && $p_day <= 15){
							$sched_int = 1;
						}elseif($p_day >= 15 && $p_day <= 31){
							$sched_int = 2;
						}

						if($interest_psched == $sched_int && $d_approved > $int_date_start){
							$int_amount = ($loan_start * ($nres->intrate/100));
							$net_amount = $payment - $int_amount;											

							$balance = $balance - $net_amount;
							$loan_start = $balance;
							// $loan_amnt = $balance;
						}else{
							$int_amount = 0;
							$net_amount = $payment;			
							$balance = $balance - $net_amount;		
							$loan_start = $balance;	
							// $loan_amnt = $balance;													
						}

						
					}
				}							
				
			}

			$loan_amnt = $balance;

			//<td>'.number_format($balance, 2).'</td>
		}
		return number_format(($balance - $additionalBalance), 2);
	}else{
		if(!empty($beg_bal)){
			return number_format($beg_bal, 2);		
		}else{
			return number_format(0, 2);	
		}
	}

	
 					// if($res !== 'false'){
						// $res = json_decode($res);

						// $int_ctr = 1;
						// foreach ($res->data as $key) {
						// 	# code...
						// 	$nres = json_decode($key);
						// 	$extra = json_decode($nres->extra);
						// 	if($extra->TypeRate == 'diminishing' || $extra->TypeRate == '-'){
						// 		$type_rate = 'DR';
						// 	}else{
						// 		$type_rate = 'FR';
						// 	}

						// 	if($type_rate == 'DR'){
						// 		//DEMINISHING RATE

						// 		$intrate = $nres->intrate;
						// 		$intrate_val = $intrate / 100;

						// 		$voucher_num = $nres->voucher_id;
						// 		$or_num = '';
						// 		$payment = number_format(0,2);


						// 		$interest = $loan_amnt_temp * $intrate_val;

						// 		// $loan_amnt = number_format($nres->amount, 2);
						// 		$loan_amnt = $nres->amount;

						// 		// echo $nres->amount;
						// 		if($ctr == 0){
						// 			$loan_start = $loan_amnt;
						// 			if(!empty($beg_bal)){	
						// 			// if($is_beg == true){
						// 				$loan_start = $beg_bal;		
						// 				$loan_amnt = $beg_bal;
						// 			}
						// 		}

						// 		if($nres->loan_type == 'new_loans'){
						// 			$balance = $loan_amnt;
						// 			$int_amount = 0;
						// 			$net_amount = 0;
						// 		// }else{
						// 		}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
						// 			$int_amount = 0;
						// 			$net_amount = 0;

						// 			$balance = $balance + $loan_amnt;
						// 			// echo 'hello mark';
						// 		}

						// 		// else{
						// 		// 	$int_amount = 0;
						// 		// 	$net_amount = 0;
						// 		// 	$balance = $loan_amnt;
						// 		// }
						// 		// echo $loan_amnt; //exit();
						// 		//cash_advance
						// 		// special_account
						// 		// additional_loan
						// 		// renewal
						// 		// excess
						// 		// if($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
						// 		// 	$int_amount = 0;
						// 		// 	$net_amount = 0;
						// 		// 	$balance = $balance + $loan_amnt;
						// 		// }else{
						// 		// 	$balance = $loan_amnt;
						// 		// 	$int_amount = 0;
						// 		// 	$net_amount = 0;
						// 		// }

						// 		if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors'){
						// 			$loan_amnt = number_format(0,2);
						// 			if($ctr == 0){
						// 				$loan_start = $loan_amnt;
						// 				if($is_beg == true){
						// 					$loan_start = $beg_bal;		
						// 					$loan_amnt = $beg_bal;
						// 					$balance = $loan_amnt;
						// 				}
						// 			}

						// 			$voucher_num = '-';
						// 			$or_num = $nres->voucher_id;
						// 			$payment = $nres->amount;
						// 			//date paid and start deductions
						// 			$start_interest = getClientSchedPaymentStart($client_id);

						// 			$d_approved = strtotime($nres->date_approved);
						// 			$int_date_start = strtotime($start_interest);

						// 			$p_day = explode(" ", $nres->date_approved);
						// 			$p_day = explode("-", $p_day[0]);
						// 			$p_day = $p_day[2];

						// 			if($d_approved == $int_date_start){
						// 				// $balance = $loan_start;
						// 				// $int_amount = ($balance * ($nres->intrate/100));
						// 				$int_amount = ($loan_start * ($nres->intrate/100));
						// 				$net_amount = $payment - $int_amount;	

						// 				// $loan_start = $										
						// 				$balance = $balance - $net_amount;
						// 				$loan_start = $balance;

						// 			}else{

						// 				if($p_day >= 1 && $p_day <= 15){
						// 					$sched_int = 1;
						// 				}elseif($p_day >= 15 && $p_day <= 31){
						// 					$sched_int = 2;
						// 				}

						// 				if($interest_psched == $sched_int && $d_approved > $int_date_start){
						// 					$int_amount = ($loan_start * ($nres->intrate/100));
						// 					$net_amount = $payment - $int_amount;											

						// 					$balance = $balance - $net_amount;
						// 					$loan_start = $balance;
						// 					// $loan_amnt = $balance;
						// 				}else{
						// 					$int_amount = 0;
						// 					// if($ctr == 0){
						// 					// 	// $loan_start = $loan_amnt;
						// 					// 	if($is_beg == true){
						// 					// 		// $loan_start = $beg_bal;		
						// 					// 		// $loan_amnt = $loan_start;
						// 					// 		$balance = $beg_bal;
						// 					// 	}
						// 					// }
						// 					$net_amount = $payment;			
						// 					$balance = $balance - $net_amount;	
						// 					// $loan_amnt = $balance;
						// 					$loan_start = $balance;
						// 				}

										
						// 			}

						// 			// if($extra->ApplyInt == 'on'){
						// 			// 	$int_amount = ($balance * ($nres->intrate/100));
						// 			// 	$net_amount = $payment - $int_amount;					
						// 			// }

						// 			// if($extra->ApplyInt == 'off' || $extra->ApplyInt == ''){
						// 			// 	$int_amount = 0;
						// 			// 	$net_amount = $payment;
						// 			// }
						// 			// $loan_amnt = $balance;
						// 		}
						// 		$ctr++;
						// 	}else{
						// 		//FIXED RATE

						// 		$intrate = $nres->intrate;
						// 		$intrate_val = $intrate / 100;

						// 		$voucher_num = $nres->voucher_id;
						// 		$or_num = '';
						// 		$payment = number_format(0,2);

						// 		$loan_amnt = $nres->amount;
						// 		if($is_FR_set == false){
						// 			$int_amount = ($loan_amnt * ($nres->intrate/100));
						// 			$loan_start = $loan_amnt;
						// 			$is_FR_set = true;
						// 		}

						// 		if($nres->loan_type == 'new_loans'){
						// 			$balance = $loan_amnt;
						// 			$int_amount = 0;
						// 			$net_amount = 0;
						// 		// }else{
						// 		}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
						// 			$int_amount = 0;
						// 			$net_amount = 0;
						// 			$balance = $balance + $loan_amnt;
						// 			// $net_amount = $payment - $int_amount;	
						// 		}


						// 		if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors'){
						// 			$loan_amnt = number_format(0,2);
						// 			$voucher_num = '-';
						// 			$or_num = $nres->voucher_id;
						// 			$payment = $nres->amount;
						// 			//date paid and start deductions
						// 			$start_interest = getClientSchedPaymentStart($client_id);

						// 			$d_approved = strtotime($nres->date_approved);
						// 			$int_date_start = strtotime($start_interest);

						// 			$p_day = explode(" ", $nres->date_approved);
						// 			$p_day = explode("-", $p_day[0]);
						// 			$p_day = $p_day[2];

						// 			if($d_approved == $int_date_start){
						// 				// $balance = $loan_start;
						// 				// $int_amount = ($balance * ($nres->intrate/100));
						// 				$int_amount = ($loan_start * ($nres->intrate/100));
						// 				$net_amount = $payment - $int_amount;	

						// 				// $loan_start = $										
						// 				$balance = $balance - $net_amount;
						// 				$loan_start = $balance;
						// 				$loan_amnt = $balance;
						// 			}else{

						// 				if($p_day >= 1 && $p_day <= 15){
						// 					$sched_int = 1;
						// 				}elseif($p_day >= 15 && $p_day <= 31){
						// 					$sched_int = 2;
						// 				}

						// 				if($interest_psched == $sched_int && $d_approved > $int_date_start){
						// 					$int_amount = ($loan_start * ($nres->intrate/100));
						// 					$net_amount = $payment - $int_amount;											

						// 					$balance = $balance - $net_amount;
						// 					$loan_start = $balance;
						// 					$loan_amnt = $balance;
						// 				}else{
						// 					$int_amount = 0;
						// 					$net_amount = $payment;			
						// 					$balance = $balance - $net_amount;		
						// 					$loan_start = $balance;	
						// 					$loan_amnt = $balance;													
						// 				}

										
						// 			}
						// 		}							
								
						// 	}

						// 	// echo '
				 	// 	// 		<tr>
				 	// 	// 			<td>'.date('m/d/Y', strtotime($nres->date_approved)).'</td>
				 	// 	// 			<td>'.$voucher_num.'</td>
				 	// 	// 			<td>'.number_format($loan_amnt, 2).'</td>
				 	// 	// 			<td>'.$or_num.'</td>
				 	// 	// 			<td>'.number_format($payment, 2).'</td>
				 	// 	// 			<td>'.$intrate.'% '.$type_rate.'</td>
				 	// 	// 			<td>'.number_format($int_amount,2).'</td>
				 	// 	// 			<td>'.number_format($net_amount,2).'</td>
				 	// 	// 			<td>'.number_format($balance, 2).'</td>
				 	// 	// 		</tr>
						// 	// ';	
							
						// 	$loan_amnt = $balance;
						// 	//<td>'.number_format($balance, 2).'</td>
						// }
						// return number_format(($balance - $additionalBalance), 2);
 					// }else{

 					// 	// echo '
			 		// 	// 	<tr>
			 		// 	// 		<td>-</td>
			 		// 	// 		<td>-</td>
			 		// 	// 		<td>-</td>
			 		// 	// 		<td>-</td>
			 		// 	// 		<td>-</td>
			 		// 	// 		<td>-</td>
			 		// 	// 		<td>-</td>
			 		// 	// 	</tr>
 					// 	// ';	

						// if(!empty($beg_bal)){
						// 	return number_format($beg_bal, 2);		
						// }else{
						// 	return number_format(0, 2);	
						// }
 					// }

	// if($res !== 'false'){
	// 	$res = json_decode($res);

	// 	$int_ctr = 1;
	// 	foreach ($res->data as $key) {
	// 		# code...
	// 		$nres = json_decode($key);
	// 		$extra = json_decode($nres->extra);
	// 		if($extra->TypeRate == 'diminishing'){
	// 			$type_rate = 'DR';
	// 		}else{
	// 			$type_rate = 'FR';
	// 		}
			
	// 		if($type_rate == 'DR'){
	// 			//DEMINISHING RATE

	// 			$intrate = $nres->intrate;
	// 			$intrate_val = $intrate / 100;

	// 			$voucher_num = $nres->voucher_id;
	// 			$or_num = '';
	// 			$payment = number_format(0,2);


	// 			$interest = $loan_amnt_temp * $intrate_val;

	// 			// $loan_amnt = number_format($nres->amount, 2);
	// 			$loan_amnt = $nres->amount;

	// 			if($ctr == 0){
	// 				$loan_start = $loan_amnt;
	// 				if($is_beg == true){
	// 					$loan_start = $beg_bal;	
	// 					$loan_amnt = $beg_bal;	
	// 				}
	// 			}



	// 			if($nres->loan_type == 'new_loans'){
	// 				$balance = $loan_amnt;
	// 				$int_amount = 0;
	// 				$net_amount = 0;
	// 			// }else{
	// 			}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
	// 				$int_amount = 0;
	// 				$net_amount = 0;
	// 				$balance = $balance + $loan_amnt;
	// 			}else{
	// 				$balance = $loan_amnt;
	// 			}
	// 			//cash_advance
	// 			// special_account
	// 			// additional_loan
	// 			// renewal
	// 			// excess
	// 			// if($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
	// 			// 	$int_amount = 0;
	// 			// 	$net_amount = 0;
	// 			// 	$balance = $balance + $loan_amnt;
	// 			// }else{
	// 			// 	$balance = $loan_amnt;
	// 			// 	$int_amount = 0;
	// 			// 	$net_amount = 0;
	// 			// }

	// 			if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors'){
	// 				$loan_amnt = number_format(0,2);
	// 				if($ctr == 0){
	// 					$loan_start = $loan_amnt;
	// 					if($is_beg == true){
	// 						$loan_start = $beg_bal;		
	// 						$loan_amnt = $beg_bal;
	// 					}
	// 				}
	// 				$voucher_num = '-';
	// 				$or_num = $nres->voucher_id;
	// 				$payment = $nres->amount;
	// 				//date paid and start deductions
	// 				$start_interest = getClientSchedPaymentStart($client_id);

	// 				$d_approved = strtotime($nres->date_approved);
	// 				$int_date_start = strtotime($start_interest);

	// 				$p_day = explode(" ", $nres->date_approved);
	// 				$p_day = explode("-", $p_day[0]);
	// 				$p_day = $p_day[2];

	// 				if($d_approved == $int_date_start){
	// 					// $balance = $loan_start;
	// 					// $int_amount = ($balance * ($nres->intrate/100));
	// 					$int_amount = ($loan_start * ($nres->intrate/100));
	// 					$net_amount = $payment - $int_amount;	

	// 					// $loan_start = $										
	// 					$balance = $balance - $net_amount;
	// 					$loan_start = $balance;
	// 				}else{

	// 					if($p_day >= 1 && $p_day <= 15){
	// 						$sched_int = 1;
	// 					}elseif($p_day >= 15 && $p_day <= 31){
	// 						$sched_int = 2;
	// 					}

	// 					if($interest_psched == $sched_int && $d_approved > $int_date_start){
	// 						$int_amount = ($loan_start * ($nres->intrate/100));
	// 						$net_amount = $payment - $int_amount;											

	// 						$balance = $balance - $net_amount;
	// 						$loan_start = $balance;
	// 					}else{
	// 						$int_amount = 0;
	// 						$net_amount = $payment;			
	// 						$balance = $balance - $net_amount;	
	// 						// $loan_start = $balance;															
	// 					}

						
	// 				}

	// 				// if($extra->ApplyInt == 'on'){
	// 				// 	$int_amount = ($balance * ($nres->intrate/100));
	// 				// 	$net_amount = $payment - $int_amount;					
	// 				// }

	// 				// if($extra->ApplyInt == 'off' || $extra->ApplyInt == ''){
	// 				// 	$int_amount = 0;
	// 				// 	$net_amount = $payment;
	// 				// }
					
	// 			}
	// 			$ctr++;
	// 		}else{
	// 			//FIXED RATE

	// 			$intrate = $nres->intrate;
	// 			$intrate_val = $intrate / 100;

	// 			$voucher_num = $nres->voucher_id;
	// 			$or_num = '';
	// 			$payment = number_format(0,2);

	// 			$loan_amnt = $nres->amount;
	// 			if($is_FR_set == false){
	// 				$int_amount = ($loan_amnt * ($nres->intrate/100));
	// 				$loan_start = $loan_amnt;
	// 				$is_FR_set = true;
	// 			}

	// 			if($nres->loan_type == 'new_loans'){
	// 				$balance = $loan_amnt;
	// 				$int_amount = 0;
	// 				$net_amount = 0;
	// 			// }else{
	// 			}elseif($nres->loan_type != 'payment' && $nres->loan_type != 'payment_ors'){
	// 				// $int_amount = 0;
	// 				// $net_amount = 0;
	// 				$balance = $balance + $loan_amnt;
	// 				$net_amount = $payment - $int_amount;	
	// 			}


	// 			if($nres->loan_type == 'payment' || $nres->loan_type == 'payment_ors'){
	// 				$loan_amnt = number_format(0,2);
	// 				$voucher_num = '-';
	// 				$or_num = $nres->voucher_id;
	// 				$payment = $nres->amount;
	// 				//date paid and start deductions
	// 				$start_interest = getClientSchedPaymentStart($client_id);

	// 				$d_approved = strtotime($nres->date_approved);
	// 				$int_date_start = strtotime($start_interest);

	// 				$p_day = explode(" ", $nres->date_approved);
	// 				$p_day = explode("-", $p_day[0]);
	// 				$p_day = $p_day[2];

	// 				if($d_approved == $int_date_start){
	// 					// $balance = $loan_start;
	// 					// $int_amount = ($balance * ($nres->intrate/100));
	// 					$int_amount = ($loan_start * ($nres->intrate/100));
	// 					$net_amount = $payment - $int_amount;	

	// 					// $loan_start = $										
	// 					$balance = $balance - $net_amount;
	// 					// $loan_start = $balance;
	// 				}else{

	// 					if($p_day >= 1 && $p_day <= 15){
	// 						$sched_int = 1;
	// 					}elseif($p_day >= 15 && $p_day <= 31){
	// 						$sched_int = 2;
	// 					}

	// 					if($interest_psched == $sched_int && $d_approved > $int_date_start){
	// 						$int_amount = ($loan_start * ($nres->intrate/100));
	// 						$net_amount = $payment - $int_amount;											

	// 						$balance = $balance - $net_amount;
	// 						// $loan_start = $balance;
	// 					}else{
	// 						$int_amount = 0;
	// 						$net_amount = $payment;			
	// 						$balance = $balance - $net_amount;																
	// 					}

						
	// 				}
	// 			}	
	// 		}
	// 		$ctr++;
	// 	}

	// 	return number_format(($balance - $additionalBalance), 2);
	// }else{
	// 	// echo '
	// 	// 	<tr>
	// 	// 		<td>-</td>
	// 	// 		<td>-</td>
	// 	// 		<td>-</td>
	// 	// 		<td>-</td>
	// 	// 		<td>-</td>
	// 	// 		<td>-</td>
	// 	// 		<td>-</td>
	// 	// 	</tr>
	// 	// ';	
	// 	if($is_beg == true){
	// 		return number_format($beg_bal, 2);		
	// 	}else{
	// 		return number_format(0, 2);	
	// 	}
		

	// }

}

function getClientSchedPayment($client_id){
	global $conn;

	$sql = "select * from `client_meta` where `client_id` = '".$client_id."' and `meta` = 'txtPaymentSched'";
	$rs2 = $conn->dbquery($sql);
	$rs2 = json_decode($rs2);
	$rs2 = json_decode($rs2->data[0]);
	return $rs2->value;
}

function getCreditLimit($client_id){
	global $conn;
	
	$sql = "select * from `client_meta` where `meta` = 'txtCreditLimit' and `client_id` = '".$client_id."'";
	$res = $conn->dbquery($sql);
	$res = json_decode($res);
	$res = json_decode($res->data[0]);
	$creditLimit = $res->value;

	$sql1 = "select * from `client_meta` where `meta` = 'txtAdditionalBalance' and `client_id` = '".$client_id."'";
	$res1 = $conn->dbquery($sql1);
	$res1 = json_decode($res1);
	$res1 = json_decode($res1->data[0]);
	$txtAdditionalBalance = $res1->value;

	return number_format(($creditLimit + $txtAdditionalBalance), 2);
}


function getClientSchedPaymentStart($client_id){
	global $conn;

	$sql = "select * from `client_meta` where `client_id` = '".$client_id."' and `meta` = 'txtInterestSchedStart'";
	$rs2 = $conn->dbquery($sql);
	$rs2 = json_decode($rs2);
	$rs2 = json_decode($rs2->data[0]);
	return $rs2->value;
}

function getStartRate($client_id){
	global $conn;

	$sql = "select * from `finance` where `client_id` = '".$client_id."' and `status` = 'approved' order by `date_added` asc";
	$res = $conn->dbquery($sql);
	// print_r($res);
	if($res !== 'false'){
		$res = json_decode($res);
		$nres = json_decode($res->data[0]);
		$extra = json_decode($nres->extra);
		// $int_ctr = 1;
		// foreach ($res->data as $key) {
		// 	# code...
		// 	$nres = json_decode($key);
		// 	$extra = json_decode($nres->extra);
		// 	if($extra->TypeRate == 'diminishing'){
		// 		$type_rate = 'DR';
		// 	}else{
		// 		$type_rate = 'FR';
		// 	}
		// }
		$value = $nres->intrate;
	}else{
		$value = '';
	}

	return $value;

}

function getStartTypeRate($client_id){
	global $conn;

	$sql = "select * from `finance` where `client_id` = '".$client_id."' and `status` = 'approved' order by `date_added` asc";
	$res = $conn->dbquery($sql);
	// print_r($res);
	if($res !== 'false'){
		$res = json_decode($res);
		$nres = json_decode($res->data[0]);
		$extra = json_decode($nres->extra);
		// $int_ctr = 1;
		// foreach ($res->data as $key) {
		// 	# code...
		// 	$nres = json_decode($key);
		// 	$extra = json_decode($nres->extra);
		// 	if($extra->TypeRate == 'diminishing'){
		// 		$type_rate = 'DR';
		// 	}else{
		// 		$type_rate = 'FR';
		// 	}
		// }
		$value = $extra->TypeRate;
	}else{
		$value = '';
	}

	return $value;
}

function jvDebitTotal($jvnid){
	global $conn;

	$sql = "select `debit` from `journal_voucher` where `jvnid` = '".$jvnid."'";
	$res = $conn->dbquery($sql);
	if($res !== 'false'){
		$res = json_decode($res);
		foreach ($res->data as $r) {
			# code...
			$nr = json_decode($r);
			$debit += $nr->debit;
		}
	}else{
		$debit = 0;
	}
	return $debit;
}

function jvCreditTotal($jvnid){
	global $conn;

	$sql = "select `credit` from `journal_voucher` where `jvnid` = '".$jvnid."'";
	$res = $conn->dbquery($sql);
	if($res !== 'false'){
		$res = json_decode($res);
		foreach ($res->data as $r) {
			# code...
			$nr = json_decode($r);
			$credit += $nr->credit;
		}
	}else{
		$credit = 0;
	}
	return $credit;
}

function jvNotes($jvnid){
	global $conn;

	$sql = "select `notes` from `jv_notes` where `jvnid` = '".$jvnid."'";
	$res = $conn->dbquery($sql);
	if($res !== 'false'){
		$res = json_decode($res);
		foreach ($res->data as $r) {
			# code...
			$nr = json_decode($r);
			$notes = $nr->notes;
		}
	}else{
		$notes = '';
	}
	return $notes;
}


function getLoanTypeTotal($loan_type){
	global $conn;

	$sql = "select * from `finance` where `loan_type` = '".$loan_type."' and `status` = 'approved' ";
	$rs = $conn->dbquery($sql);
	if($rs !== 'false'){
		$rs = json_decode($rs);
		foreach($rs->data as $r){
			$nr = json_decode($r);
			$total += $nr->amount;
		}
	}else{
		$total = 0;
	}

	return $total;
}

function getProcFeeTotal(){
	global $conn;

	$sql = "select `inProcFee` from `finance` where `status` = 'approved' ";
	$rs = $conn->dbquery($sql);
	if($rs !== 'false'){
		$rs = json_decode($rs);
		foreach($rs->data as $r){
			$nr = json_decode($r);
			// if(empty($nr->inpProcFee)){
			// 	$nr->inpProcFee = 0;
			// }
			$total += $nr->inProcFee;
		}
	}else{
		$total = 0;
	}

	return $total;
}

function getNotFeeTotal(){
	global $conn;

	$sql = "select `inpNotarial` from `finance` where `status` = 'approved' ";
	$rs = $conn->dbquery($sql);
	if($rs !== 'false'){
		$rs = json_decode($rs);
		foreach($rs->data as $r){
			$nr = json_decode($r);
			// if(empty($nr->inpNotarial)){
			// 	$nr->inpNotarial = 0;
			// }
			$total += $nr->inpNotarial;
		}
	}else{
		$total = 0;
	}

	return $total;
}


function getFindersFeeTotal(){
	global $conn;

	$sql = "select `inpFindersFee` from `finance` where `status` = 'approved' ";
	$rs = $conn->dbquery($sql);
	if($rs !== 'false'){
		$rs = json_decode($rs);
		foreach($rs->data as $r){
			$nr = json_decode($r);
			// if(empty($nr->inpNotarial)){
			// 	$nr->inpNotarial = 0;
			// }
			$total += $nr->inpFindersFee;
		}
	}else{
		$total = 0;
	}

	return $total;
}

function getMiscFeeTotal(){
	global $conn;

	$sql = "select `inpMisc` from `finance` where `status` = 'approved' ";
	$rs = $conn->dbquery($sql);
	if($rs !== 'false'){
		$rs = json_decode($rs);
		foreach($rs->data as $r){
			$nr = json_decode($r);
			// if(empty($nr->inpNotarial)){
			// 	$nr->inpNotarial = 0;
			// }
			$total += $nr->inpMisc;
		}
	}else{
		$total = 0;
	}

	return $total;
}

function getChartAccountCode($id){
	global $conn;

	$sql = "select `code` from `chart_accounts` where `caid` = '".$id."' ";
	$rs = $conn->dbquery($sql);
	if($rs !== 'false'){
		$rs = json_decode($rs);
		foreach($rs->data as $r){
			$nr = json_decode($r);
			// if(empty($nr->inpNotarial)){
			// 	$nr->inpNotarial = 0;
			// }
			$code = $nr->code;
		}
	}else{
		$code = '';
	}

	return $code;
}

function getChartAccountName($id){
	global $conn;

	$sql = "select `name` from `chart_accounts` where `caid` = '".$id."' ";
	$rs = $conn->dbquery($sql);
	if($rs !== 'false'){
		$rs = json_decode($rs);
		foreach($rs->data as $r){
			$nr = json_decode($r);
			// if(empty($nr->inpNotarial)){
			// 	$nr->inpNotarial = 0;
			// }
			$name = $nr->name;
		}
	}else{
		$name = '';
	}

	return $name;
}


function getChartAccountIDByCode($code){
	global $conn;

	$sql = "select `caid` from `chart_accounts` where `code` = '".$id."' ";
	$rs = $conn->dbquery($sql);
	if($rs !== 'false'){
		$rs = json_decode($rs);
		foreach($rs->data as $r){
			$nr = json_decode($r);
			// if(empty($nr->inpNotarial)){
			// 	$nr->inpNotarial = 0;
			// }
			$caid = $nr->caid;
		}
	}else{
		$caid = '';
	}

	return $caid;
}
?>