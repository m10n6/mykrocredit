-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 06, 2018 at 05:13 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 5.6.33-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mykrocredit`
--

-- --------------------------------------------------------

--
-- Table structure for table `chart_accounts`
--

CREATE TABLE `chart_accounts` (
  `caid` bigint(20) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `category` varchar(255) NOT NULL,
  `is_display` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chart_accounts`
--

INSERT INTO `chart_accounts` (`caid`, `code`, `name`, `category`, `is_display`, `date_added`) VALUES
(1, '100-001', 'Cash in Bank (BDO Hilado)', 'Cash', '1', '2017-07-31 02:28:08'),
(2, '101-001', 'A/R Trade', 'Loans and Other Receivables', '1', '2017-07-31 02:32:19'),
(4, '100-003', 'Cash in Bank (China Bank)', 'Cash', '1', '2017-07-31 02:28:00'),
(5, '101-001-01', 'A/R Loans', 'Loans and Other Receivables', '1', '2017-07-31 02:33:01'),
(6, '101-001-02', 'A/R Cash Advance', 'Loans and Other Receivables', '1', '2017-07-31 02:33:38'),
(7, '101-001-03', 'A/R Employee', 'Loans and Other Receivables', '1', '2017-07-31 02:34:03'),
(8, '101-001-04', 'A/R Blacklist', 'Loans and Other Receivables', '1', '2017-07-31 02:36:21'),
(9, '101-001-05', 'A/R Special Accounts', 'Loans and Other Receivables', '1', '2017-07-31 02:36:51'),
(10, '101-001-06', 'A/R Excess', 'Loans and Other Receivables', '1', '2017-07-31 02:37:14'),
(11, '400-003', 'Service Fee', 'Revenue', '1', '2017-07-31 02:38:04'),
(12, '400-003-01', 'Processing Fee', 'Revenue', '1', '2017-07-31 02:38:37'),
(13, '400-003-02', 'Notarial Fee', 'Revenue', '1', '2017-07-31 02:39:06'),
(14, '400-003-03', 'Miscellaneous', 'Revenue', '1', '2017-07-31 02:39:30'),
(15, '400-003-04', 'Finders Fee', 'Revenue', '1', '2017-07-31 02:39:51'),
(16, '400-005', 'Rebates/Discounts', 'Revenue', '1', '2017-07-31 02:40:22'),
(17, '100-002', 'Cash in Bank (BDO East)', 'Cash', '1', '2017-07-31 03:10:43'),
(18, '102-000', 'DUE FROM-MARC ACCOUNT', 'Loans and Other Receivables', '1', '2018-02-06 04:54:13'),
(19, '105-000', 'Bonuses', 'Others', '1', '2018-01-30 12:21:43'),
(20, '101-001-008', 'A/R Inactive Account', 'Loans and Other Receivables', '1', '2018-02-06 04:51:54'),
(21, '102-001', 'DUE FROM-MARC JOB', 'Loans and Other Receivables', '1', '2018-02-06 04:54:56'),
(22, '102-002', 'DUE FROM-RGR', 'Loans and Other Receivables', '1', '2018-02-06 04:55:17'),
(23, '102-003', 'DUE FROM-JDR', 'Loans and Other Receivables', '1', '2018-02-06 04:55:38'),
(24, '102-004', 'DUE FROM-MYKRO', 'Loans and Other Receivables', '1', '2018-02-06 04:56:04'),
(25, '103-000', 'ADVANCES FROM OFFICERS & EMPLOYEES', 'Loans and Other Receivables', '1', '2018-02-06 04:56:32'),
(26, '104-000', 'LAND & BLGD. IMPROVEMENTS', 'Assets', '1', '2018-02-06 04:57:12'),
(27, '106-000', 'SERVICE VEHICLE', 'Assets', '1', '2018-02-06 04:58:24'),
(28, '200-001', 'ACCOUNTS PAYABLE', 'Payable', '1', '2018-02-06 04:58:43'),
(29, '200-002', 'VAT PAYABLE', 'Payable', '1', '2018-02-06 04:59:04'),
(30, '200-003', 'INCOME TAX PAYABLE', 'Payable', '1', '2018-02-06 04:59:24'),
(31, '200-004', 'WITHHOLDING TAX PAYABLE', 'Payable', '1', '2018-02-06 04:59:47'),
(32, '200-005', 'SSS LOAN PAYABLE', 'Payable', '1', '2018-02-06 05:00:10'),
(33, '200-006', 'PHILHEALTH PAYABLE', 'Payable', '1', '2018-02-06 05:00:36'),
(34, '200-007', 'PAG-IBIG PAYABLE', 'Payable', '1', '2018-02-06 05:06:39'),
(35, '200-008', 'SALARIES & WAGES PAYABLE', 'Payable', '1', '2018-02-06 05:06:56'),
(36, '200-009', 'DUE TO - MARC ACCOUNT', 'Payable', '1', '2018-02-06 05:07:14'),
(37, '200-010', 'DUE TO - MARC JOB', 'Payable', '1', '2018-02-06 05:07:30'),
(38, '200-011', 'DUE TO - RGR', 'Payable', '1', '2018-02-06 05:07:44'),
(39, '200-012', 'DUE TO -JDR', 'Payable', '1', '2018-02-06 05:07:57'),
(40, '200-013', 'DUE TO - MYKRO', 'Payable', '1', '2018-02-06 05:08:10'),
(41, '500-000', 'EXPENSES', 'Expenses', '1', '2018-02-06 05:08:27'),
(42, '500-001', 'ADVERTISING & PROMOTION EXPENSE', 'Expenses', '1', '2018-02-06 05:08:41'),
(43, '500-002', 'DONATION EXPENSE', 'Expenses', '1', '2018-02-06 05:08:56'),
(44, '500-003', 'FUEL & OIL EXPENSE', 'Expenses', '1', '2018-02-06 05:09:16'),
(45, '500-004', 'INTEREST EXPENSE', 'Expenses', '1', '2018-02-06 05:09:31'),
(46, '500-005', 'LIGHT & WATER EXPENSE', 'Expenses', '1', '2018-02-06 05:09:47'),
(47, '500-006', 'ALLOWANCE EXPENSE', 'Expenses', '1', '2018-02-06 05:10:01'),
(48, '500-007', 'OFFICE EXPENSE', 'Expenses', '1', '2018-02-06 05:10:14'),
(49, '500-008', 'OTHER EXPENSE', 'Expenses', '1', '2018-02-06 05:10:30'),
(50, '500-009', 'PROFESSIONAL FEES', 'Expenses', '1', '2018-02-06 05:10:47'),
(51, '500-010', 'RENT EXPENSE', 'Expenses', '1', '2018-02-06 05:11:00'),
(52, '500-011', 'REPAIRS & MAINTENANCE', 'Expenses', '1', '2018-02-06 05:11:16'),
(53, '500-012', 'REPRESENTATION', 'Expenses', '1', '2018-02-06 05:11:30'),
(54, '500-013', 'SALARIES & WAGES EXPENSE', 'Expenses', '1', '2018-02-06 05:11:45'),
(55, '500-014', 'TRANSPORTATION EXPENSE', 'Expenses', '1', '2018-02-06 05:12:01'),
(56, '500-015', 'TELECOM & POSTAGE', 'Expenses', '1', '2018-02-06 05:12:20'),
(57, '500-016', 'TAXES & LICENSES EXPENSE', 'Expenses', '1', '2018-02-06 05:12:39');

-- --------------------------------------------------------

--
-- Table structure for table `client_begloanbal`
--

CREATE TABLE `client_begloanbal` (
  `cbb_id` bigint(20) NOT NULL,
  `cid` varchar(255) DEFAULT NULL,
  `balance` varchar(255) DEFAULT NULL,
  `irate` varchar(255) DEFAULT NULL,
  `irateType` varchar(255) DEFAULT NULL,
  `terms` varchar(255) DEFAULT NULL,
  `entryDate` datetime DEFAULT NULL,
  `entryStatus` varchar(255) DEFAULT NULL,
  `acctType` varchar(255) DEFAULT NULL,
  `paidStat` varchar(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_data`
--

CREATE TABLE `client_data` (
  `client_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address` longtext NOT NULL,
  `birthdate` date NOT NULL,
  `civil_status` varchar(255) NOT NULL,
  `contact_num` varchar(255) NOT NULL,
  `sss_num` varchar(255) NOT NULL,
  `company` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_data`
--

INSERT INTO `client_data` (`client_id`, `name`, `middle_name`, `last_name`, `address`, `birthdate`, `civil_status`, `contact_num`, `sss_num`, `company`) VALUES
(3, 'Janet', 'P', 'Alde', 'Bago City', '1972-01-12', 'married', '09208166612', '07-12354-23', 'RTC'),
(4, 'Mark', 'B', 'Ramos', '', '1983-01-31', 'married', '', '', ''),
(5, 'Erlando', 'A', 'Guancia', 'Talisay City', '1960-03-28', 'married', '', '', 'City Hall'),
(6, 'John', 'D', 'Doe', '', '1930-01-22', 'single', '', '', ''),
(7, 'Test', 'T', 'Tester', '', '1966-02-15', 'married', '', '', ''),
(8, 'Inactive', 'T', 'T', '', '1972-01-12', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `client_meta`
--

CREATE TABLE `client_meta` (
  `c_metaid` bigint(20) NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `meta` text NOT NULL,
  `value` longtext NOT NULL,
  `extra` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_meta`
--

INSERT INTO `client_meta` (`c_metaid`, `client_id`, `meta`, `value`, `extra`) VALUES
(47, 3, 'txtCompany', 'RTC', ''),
(48, 3, 'txtCompanyAddress', 'Bacolod City', ''),
(49, 3, 'txtS_name', 'John Lo', ''),
(50, 3, 'txtS_contact', '4359623', ''),
(51, 3, 'txtG_name', 'Faanny sion', ''),
(52, 3, 'txtG_address', 'Manila', ''),
(53, 3, 'txtG_contactnum', '4332336', ''),
(54, 3, 'txtG_SSS', '07-12-12-45', ''),
(55, 3, 'txtCard1', 'BDO- 232323232', ''),
(56, 3, 'txtPin1', '4236', ''),
(57, 3, 'txtCard2', '', ''),
(58, 3, 'txtPin2', '', ''),
(59, 3, 'txtCard3', '', ''),
(60, 3, 'txtPin3', '', ''),
(61, 3, 'txtCreditLimit', '25000', ''),
(62, 3, 'txtP_name', 'Jose & Maria Alde', ''),
(63, 3, 'txtP_address', 'Naga City', ''),
(64, 3, 'txtClientType', 'regular', ''),
(65, 3, 'txtPaymentSched', '1', ''),
(66, 3, 'txtAdditionalBalance', '', ''),
(67, 3, 'txtInterestSchedStart', '2017-12-15', ''),
(68, 3, 'txtActualPaymentAmount', '2000', ''),
(69, 3, 'txtBeginningBal', '', ''),
(70, 4, 'txtCompany', '', ''),
(71, 4, 'txtCompanyAddress', '', ''),
(72, 4, 'txtS_name', '', ''),
(73, 4, 'txtS_contact', '', ''),
(74, 4, 'txtG_name', '', ''),
(75, 4, 'txtG_address', '', ''),
(76, 4, 'txtG_contactnum', '', ''),
(77, 4, 'txtG_SSS', '', ''),
(78, 4, 'txtCard1', '', ''),
(79, 4, 'txtPin1', '', ''),
(80, 4, 'txtCard2', '', ''),
(81, 4, 'txtPin2', '', ''),
(82, 4, 'txtCard3', '', ''),
(83, 4, 'txtPin3', '', ''),
(84, 4, 'txtCreditLimit', '25000', ''),
(85, 4, 'txtP_name', '', ''),
(86, 4, 'txtP_address', '', ''),
(87, 4, 'txtClientType', 'regular', ''),
(88, 4, 'txtPaymentSched', '2', ''),
(89, 4, 'txtAdditionalBalance', '', ''),
(90, 4, 'txtInterestSchedStart', '2018-02-28', ''),
(91, 4, 'txtActualPaymentAmount', '2000', ''),
(92, 4, 'txtBeginningBal', '', ''),
(93, 5, 'txtCompany', 'City Hall', ''),
(94, 5, 'txtCompanyAddress', 'Bacolod City', ''),
(95, 5, 'txtS_name', '', ''),
(96, 5, 'txtS_contact', '', ''),
(97, 5, 'txtG_name', '', ''),
(98, 5, 'txtG_address', '', ''),
(99, 5, 'txtG_contactnum', '', ''),
(100, 5, 'txtG_SSS', '', ''),
(101, 5, 'txtCard1', '', ''),
(102, 5, 'txtPin1', '', ''),
(103, 5, 'txtCard2', '', ''),
(104, 5, 'txtPin2', '', ''),
(105, 5, 'txtCard3', '', ''),
(106, 5, 'txtPin3', '', ''),
(107, 5, 'txtCreditLimit', '35000', ''),
(108, 5, 'txtP_name', '', ''),
(109, 5, 'txtP_address', '', ''),
(110, 5, 'txtClientType', 'regular', ''),
(111, 5, 'txtPaymentSched', '2', ''),
(112, 5, 'txtAdditionalBalance', '', ''),
(113, 5, 'txtInterestSchedStart', '2018-01-31', ''),
(114, 5, 'txtActualPaymentAmount', '1500', ''),
(115, 5, 'txtBeginningBal', '31334', ''),
(116, 5, 'txtBegBalDate', '2017-12-31', ''),
(117, 6, 'txtCompany', '', ''),
(118, 6, 'txtCompanyAddress', '', ''),
(119, 6, 'txtS_name', '', ''),
(120, 6, 'txtS_contact', '', ''),
(121, 6, 'txtG_name', '', ''),
(122, 6, 'txtG_address', '', ''),
(123, 6, 'txtG_contactnum', '', ''),
(124, 6, 'txtG_SSS', '', ''),
(125, 6, 'txtCard1', '', ''),
(126, 6, 'txtPin1', '', ''),
(127, 6, 'txtCard2', '', ''),
(128, 6, 'txtPin2', '', ''),
(129, 6, 'txtCard3', '', ''),
(130, 6, 'txtPin3', '', ''),
(131, 6, 'txtCreditLimit', '20000', ''),
(132, 6, 'txtP_name', '', ''),
(133, 6, 'txtP_address', '', ''),
(134, 6, 'txtClientType', 'regular', ''),
(135, 6, 'txtPaymentSched', '2', ''),
(136, 6, 'txtAdditionalBalance', '', ''),
(137, 6, 'txtInterestSchedStart', '2018-01-15', ''),
(138, 6, 'txtActualPaymentAmount', '2000', ''),
(139, 6, 'txtBeginningBal', '18000', ''),
(140, 6, 'txtBegBalDate', '2017-12-15', ''),
(141, 7, 'txtCompany', '', ''),
(142, 7, 'txtCompanyAddress', '', ''),
(143, 7, 'txtS_name', '', ''),
(144, 7, 'txtS_contact', '', ''),
(145, 7, 'txtG_name', '', ''),
(146, 7, 'txtG_address', '', ''),
(147, 7, 'txtG_contactnum', '', ''),
(148, 7, 'txtG_SSS', '', ''),
(149, 7, 'txtCard1', '', ''),
(150, 7, 'txtPin1', '', ''),
(151, 7, 'txtCard2', '', ''),
(152, 7, 'txtPin2', '', ''),
(153, 7, 'txtCard3', '', ''),
(154, 7, 'txtPin3', '', ''),
(155, 7, 'txtCreditLimit', '20000', ''),
(156, 7, 'txtP_name', '', ''),
(157, 7, 'txtP_address', '', ''),
(158, 7, 'txtClientType', 'regular', ''),
(159, 7, 'txtPaymentSched', '1', ''),
(160, 7, 'txtAdditionalBalance', '', ''),
(161, 7, 'txtInterestSchedStart', '2018-02-05', ''),
(162, 7, 'txtActualPaymentAmount', '2000', ''),
(163, 7, 'txtBeginningBal', '', ''),
(164, 7, 'txtBegBalDate', '2018-02-05', ''),
(165, 4, 'txtBegBalDate', '', ''),
(166, 8, 'txtCompany', '', ''),
(167, 8, 'txtCompanyAddress', '', ''),
(168, 8, 'txtS_name', '', ''),
(169, 8, 'txtS_contact', '', ''),
(170, 8, 'txtG_name', '', ''),
(171, 8, 'txtG_address', '', ''),
(172, 8, 'txtG_contactnum', '', ''),
(173, 8, 'txtG_SSS', '', ''),
(174, 8, 'txtCard1', '', ''),
(175, 8, 'txtPin1', '', ''),
(176, 8, 'txtCard2', '', ''),
(177, 8, 'txtPin2', '', ''),
(178, 8, 'txtCard3', '', ''),
(179, 8, 'txtPin3', '', ''),
(180, 8, 'txtCreditLimit', '20000', ''),
(181, 8, 'txtP_name', '', ''),
(182, 8, 'txtP_address', '', ''),
(183, 8, 'txtClientType', 'regular', ''),
(184, 8, 'txtPaymentSched', '1', ''),
(185, 8, 'txtAdditionalBalance', '', ''),
(186, 8, 'txtInterestSchedStart', '2017-12-15', ''),
(187, 8, 'txtActualPaymentAmount', '1000', ''),
(188, 8, 'txtBeginningBal', '', ''),
(189, 8, 'txtBegBalDate', '', ''),
(190, 8, 'txtIsInActive', 'on', '');

-- --------------------------------------------------------

--
-- Table structure for table `finance`
--

CREATE TABLE `finance` (
  `fid` bigint(20) NOT NULL,
  `client_id` text NOT NULL,
  `intrate` text NOT NULL,
  `debit` text NOT NULL,
  `credit` text NOT NULL,
  `balance` text NOT NULL,
  `extra` text NOT NULL,
  `status` text NOT NULL,
  `loan_type` text NOT NULL,
  `voucher_id` text NOT NULL,
  `term` text NOT NULL,
  `amount` text NOT NULL,
  `date_added` datetime NOT NULL,
  `loan_date` datetime NOT NULL,
  `processedby` text NOT NULL,
  `cancelledby` text NOT NULL,
  `addedby` text NOT NULL,
  `date_approved` datetime NOT NULL,
  `inProcFee` varchar(255) DEFAULT NULL,
  `inpFindersFee` varchar(255) DEFAULT NULL,
  `inpNotarial` varchar(255) DEFAULT NULL,
  `inpMisc` varchar(255) DEFAULT NULL,
  `inpInsurance` varchar(255) DEFAULT NULL,
  `inpOthers` varchar(255) DEFAULT NULL,
  `inpAdjustment` varchar(255) DEFAULT NULL,
  `inpBonus` varchar(255) DEFAULT NULL,
  `approvedby` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `finance`
--

INSERT INTO `finance` (`fid`, `client_id`, `intrate`, `debit`, `credit`, `balance`, `extra`, `status`, `loan_type`, `voucher_id`, `term`, `amount`, `date_added`, `loan_date`, `processedby`, `cancelledby`, `addedby`, `date_approved`, `inProcFee`, `inpFindersFee`, `inpNotarial`, `inpMisc`, `inpInsurance`, `inpOthers`, `inpAdjustment`, `inpBonus`, `approvedby`) VALUES
(19, '3', '4', '0', '25000', '0', '{ "guarantor": "fANNY", "TypeRate" : "diminishing" }', 'approved', 'new_loans', 'MYK00005', '12', '25000', '2018-01-23 13:26:40', '2017-11-23 00:00:00', '', '', '3393', '2017-11-23 00:00:00', '100.00', '100.00', '100.00', '100.00', '100.00', '0.00', '0.00', '0.00', '3393'),
(20, '3', '4', '', '0', '25,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00004', '', '2000.00', '2018-01-23 13:27:42', '2018-01-23 13:27:42', '3393', '', '3393', '2017-11-30 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '3', '4', '', '0', '23,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00005', '', '2000.00', '2018-01-23 13:29:16', '2018-01-23 13:29:16', '3393', '', '3393', '2017-12-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '3', '4', '', '0', '22,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment', '123456', '', '2000.00', '2018-01-23 13:30:48', '2018-01-23 13:30:48', '3393', '', '3393', '2017-12-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '3', '4', '0', '1000', '24000', '{ "guarantor": "", "TypeRate" : "diminishing" }', 'approved', 'cash_advance', 'MYK00006', '1', '1000', '2018-01-23 13:31:34', '2018-01-05 00:00:00', '', '', '3393', '2018-01-05 00:00:00', '0.00', '0.00', '0.00', '30.00', '0.00', '0.00', '0.00', '0.00', '3393'),
(25, '3', '4', '', '0', '21,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00006', '', '2000.00', '2018-01-23 13:32:17', '2018-01-23 13:32:17', '3393', '', '3393', '2018-01-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, '3', '', '1000.00', '0.00', '0', '{ "guarantor": "", "TypeRate" : "", "ApplyInt" : "", "JVTypeID": "1" }', 'approved', 'jv', '8', '', '1000', '2018-01-30 05:21:28', '2018-01-30 05:21:28', '3393', '', '3393', '2018-01-16 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, '3', '', '0.00', '1000.00', '0', '{ "guarantor": "", "TypeRate" : "", "ApplyInt" : "", "JVTypeID": "2" }', 'approved', 'jv', '8', '', '1000', '2018-01-30 05:21:28', '2018-01-30 05:21:28', '3393', '', '3393', '2018-01-16 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '3', '4', '', '0', '21,800.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00007', '', '1000.00', '2018-01-30 05:36:03', '2018-01-30 05:36:03', '3393', '', '3393', '2018-01-30 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, '3', '4', '', '0', '20,800.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00008', '', '2000.00', '2018-01-30 05:36:54', '2018-01-30 05:36:54', '3393', '', '3393', '2018-02-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, '3', '4', '0', '2000', '23000', '{ "guarantor": "", "TypeRate" : "diminishing" }', 'pending', 'renewal', 'MYK00007', '12', '2000', '2018-01-30 12:19:45', '2018-01-30 00:00:00', '', '', '3393', '2018-01-30 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, '4', '4', '0', '25000', '0', '{ "guarantor": "Fanny", "TypeRate" : "diminishing" }', 'approved', 'new_loans', 'MYK00008', '12', '25000', '2018-01-31 12:34:38', '2018-01-31 00:00:00', '', '', '3393', '2018-01-31 00:00:00', '500.00', '200.00', '100.00', '450.00', '0.00', '0.00', '0.00', '0.00', '3393'),
(38, '4', '4', '', '0', '25,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00009', '', '2000.00', '2018-01-31 12:38:59', '2018-01-31 12:38:59', '3393', '', '3393', '2018-02-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, '4', '4', '0', '1000', '24000', '{ "guarantor": "", "TypeRate" : "diminishing" }', 'approved', 'cash_advance', 'MYK00009', '1', '1000', '2018-01-31 12:39:47', '2018-02-15 00:00:00', '', '', '3393', '2018-02-15 00:00:00', '0.00', '0.00', '0.00', '30.00', '0.00', '0.00', '0.00', '0.00', '3393'),
(40, '4', '4', '', '0', '25,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00010', '', '2000.00', '2018-01-31 12:40:41', '2018-01-31 12:40:41', '3393', '', '3393', '2018-02-28 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '4', '4', '', '0', '23,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00011', '', '2000.00', '2018-01-31 13:28:47', '2018-01-31 13:28:47', '3393', '', '3393', '2018-03-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, '4', '', '1000.00', '0.00', '0', '{ "guarantor": "", "TypeRate" : "", "ApplyInt" : "", "JVTypeID": "1" }', 'approved', 'jv', '10', '', '1000', '2018-01-31 13:30:26', '2018-01-31 13:30:26', '3393', '', '3393', '2018-03-16 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, '4', '', '0.00', '1000.00', '0', '{ "guarantor": "", "TypeRate" : "", "ApplyInt" : "", "JVTypeID": "6" }', 'approved', 'jv', '10', '', '1000', '2018-01-31 13:30:26', '2018-01-31 13:30:26', '3393', '', '3393', '2018-03-16 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, '4', '4', '', '0', '20,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00012', '', '2000.00', '2018-01-31 13:31:32', '2018-01-31 13:31:32', '3393', '', '3393', '2018-03-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, '5', '0', '0', '31334', '31334', '{ "guarantor": "", "TypeRate" : "diminishing" }', 'approved', 'beg_bal', '', '', '31334', '2018-01-31 14:52:31', '2018-01-31 14:52:31', '', '', '3393', '2017-12-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, '5', '4', '', '0', '31,334.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00013', '', '1500.00', '2018-01-31 14:55:43', '2018-01-31 14:55:43', '3393', '', '3393', '2018-01-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, '5', '4', '', '0', '29,834.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00014', '', '1000.00', '2018-01-31 14:56:15', '2018-01-31 14:56:15', '3393', '', '3393', '2018-01-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, '5', '4', '', '0', '30,087.36', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00015', '', '1500.00', '2018-01-31 15:09:30', '2018-01-31 15:09:30', '3393', '', '3393', '2018-02-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, '5', '4', '', '0', '28,587.36', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00016', '', '1000.00', '2018-01-31 15:10:03', '2018-01-31 15:10:03', '3393', '', '3393', '2018-02-28 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, '6', '', '0', '18000', '18000', '{ "guarantor": "", "TypeRate" : "" }', 'approved', 'beg_bal', '', '', '18000', '2018-01-31 15:30:14', '2018-01-31 15:30:14', '', '', '3393', '2017-12-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, '6', '4', '', '0', '18,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00017', '', '2000.00', '2018-01-31 15:30:59', '2018-01-31 15:30:59', '3393', '', '3393', '2017-12-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, '6', '4', '', '0', '16,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00018', '', '2000.00', '2018-01-31 15:31:36', '2018-01-31 15:31:36', '3393', '', '3393', '2018-01-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, '6', '4', '', '0', '14,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00019', '', '2000.00', '2018-01-31 15:59:17', '2018-01-31 15:59:17', '3393', '', '3393', '2018-02-28 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, '6', '4', '', '0', '12,640.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment', '000000', '', '0.00', '2018-01-31 16:06:53', '2018-01-31 16:06:53', '3393', '', '3393', '2018-01-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, '5', '4', '', '0', '28,790.85', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00020', '', '1500.00', '2018-02-04 22:26:14', '2018-02-04 22:26:14', '3393', '', '3393', '2018-05-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '6', '4', '', '0', '13,225.60', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00021', '', '2000.00', '2018-02-04 22:29:32', '2018-02-04 22:29:32', '3393', '', '3393', '2018-07-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, '6', '4', '0', '0', '14000', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:25:20', '2018-02-05 02:25:20', '3393', '', '3393', '2018-02-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, '6', '4', '0', '0', '12000', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:25:20', '2018-02-05 02:25:20', '3393', '', '3393', '2018-03-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, '6', '4', '0', '0', '12000', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:25:21', '2018-02-05 02:25:21', '3393', '', '3393', '2018-03-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, '6', '4', '0', '0', '12000', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:25:21', '2018-02-05 02:25:21', '3393', '', '3393', '2018-04-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, '6', '4', '0', '0', '12000', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:25:21', '2018-02-05 02:25:21', '3393', '', '3393', '2018-04-30 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, '6', '4', '0', '0', '12000', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:25:21', '2018-02-05 02:25:21', '3393', '', '3393', '2018-05-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, '6', '4', '0', '0', '12000', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:25:21', '2018-02-05 02:25:21', '3393', '', '3393', '2018-05-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, '6', '4', '0', '0', '12000', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:25:22', '2018-02-05 02:25:22', '3393', '', '3393', '2018-06-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, '6', '4', '0', '0', '12000', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:25:22', '2018-02-05 02:25:22', '3393', '', '3393', '2018-06-30 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, '5', '4', '0', '0', '26334', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:29:40', '2018-02-05 02:29:40', '3393', '', '3393', '2018-03-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, '5', '4', '0', '0', '26334', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:29:40', '2018-02-05 02:29:40', '3393', '', '3393', '2018-03-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, '5', '4', '0', '0', '26334', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:29:40', '2018-02-05 02:29:40', '3393', '', '3393', '2018-04-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, '5', '4', '0', '0', '26334', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "" }', 'approved', 'payment_ors', '0000000000', '', '0', '2018-02-05 02:29:40', '2018-02-05 02:29:40', '3393', '', '3393', '2018-04-30 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `journal_voucher`
--

CREATE TABLE `journal_voucher` (
  `jvid` bigint(20) NOT NULL,
  `jvnid` text,
  `client_id` text,
  `chart_account_id` text,
  `debit` float DEFAULT NULL,
  `credit` float DEFAULT NULL,
  `date_trans` datetime DEFAULT NULL,
  `addby` text,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `journal_voucher`
--

INSERT INTO `journal_voucher` (`jvid`, `jvnid`, `client_id`, `chart_account_id`, `debit`, `credit`, `date_trans`, `addby`, `date_added`) VALUES
(14, '8', '3', '1', 1000, 0, '2018-01-16 00:00:00', '3393', '2018-01-30 05:21:28'),
(15, '8', '3', '6', 0, 1000, '2018-01-16 00:00:00', '3393', '2018-01-30 05:21:28'),
(18, '10', '4', '1', 1000, 0, '2018-03-16 00:00:00', '3393', '2018-01-31 13:30:26'),
(19, '10', '4', '6', 0, 1000, '2018-03-16 00:00:00', '3393', '2018-01-31 13:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `jv_notes`
--

CREATE TABLE `jv_notes` (
  `jvntid` bigint(20) NOT NULL,
  `jvnid` text,
  `notes` text CHARACTER SET utf8,
  `dateadded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jv_notes`
--

INSERT INTO `jv_notes` (`jvntid`, `jvnid`, `notes`, `dateadded`) VALUES
(8, '8', 'Adjustment', '2018-01-30 05:21:28'),
(10, '10', 'Erroneous data', '2018-01-31 13:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `jv_numbers`
--

CREATE TABLE `jv_numbers` (
  `jvnid` bigint(20) NOT NULL,
  `prefix` text,
  `number` text,
  `dateadded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jv_numbers`
--

INSERT INTO `jv_numbers` (`jvnid`, `prefix`, `number`, `dateadded`) VALUES
(8, '', '', '2018-01-30 05:21:28'),
(10, '', '', '2018-01-31 13:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `log_id` bigint(20) NOT NULL,
  `transaction` longtext NOT NULL,
  `addedby` bigint(20) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ors_numbers`
--

CREATE TABLE `ors_numbers` (
  `ors_id` bigint(20) NOT NULL,
  `fid` text,
  `ors_num` text NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `dateadded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ors_numbers`
--

INSERT INTO `ors_numbers` (`ors_id`, `fid`, `ors_num`, `prefix`, `code`, `dateadded`) VALUES
(9, NULL, 'MYK-ORS00001', 'MYK-ORS', '00001', '2018-01-23 13:01:41'),
(10, NULL, 'MYK-ORS00002', 'MYK-ORS', '00002', '2018-01-23 13:03:31'),
(11, NULL, 'MYK-ORS00003', 'MYK-ORS', '00003', '2018-01-23 13:04:02'),
(12, NULL, 'MYK-ORS00004', 'MYK-ORS', '00004', '2018-01-23 13:27:42'),
(13, NULL, 'MYK-ORS00005', 'MYK-ORS', '00005', '2018-01-23 13:29:16'),
(14, NULL, 'MYK-ORS00006', 'MYK-ORS', '00006', '2018-01-23 13:32:17'),
(15, NULL, 'MYK-ORS00007', 'MYK-ORS', '00007', '2018-01-30 05:36:03'),
(16, NULL, 'MYK-ORS00008', 'MYK-ORS', '00008', '2018-01-30 05:36:54'),
(17, NULL, 'MYK-ORS00009', 'MYK-ORS', '00009', '2018-01-31 12:38:59'),
(18, NULL, 'MYK-ORS00010', 'MYK-ORS', '00010', '2018-01-31 12:40:41'),
(19, NULL, 'MYK-ORS00011', 'MYK-ORS', '00011', '2018-01-31 13:28:47'),
(20, NULL, 'MYK-ORS00012', 'MYK-ORS', '00012', '2018-01-31 13:31:32'),
(21, NULL, 'MYK-ORS00013', 'MYK-ORS', '00013', '2018-01-31 14:55:43'),
(22, NULL, 'MYK-ORS00014', 'MYK-ORS', '00014', '2018-01-31 14:56:15'),
(23, NULL, 'MYK-ORS00015', 'MYK-ORS', '00015', '2018-01-31 15:09:30'),
(24, NULL, 'MYK-ORS00016', 'MYK-ORS', '00016', '2018-01-31 15:10:03'),
(25, NULL, 'MYK-ORS00017', 'MYK-ORS', '00017', '2018-01-31 15:30:59'),
(26, NULL, 'MYK-ORS00018', 'MYK-ORS', '00018', '2018-01-31 15:31:36'),
(27, NULL, 'MYK-ORS00019', 'MYK-ORS', '00019', '2018-01-31 15:59:17'),
(28, NULL, 'MYK-ORS00020', 'MYK-ORS', '00020', '2018-02-04 22:26:14'),
(29, NULL, 'MYK-ORS00021', 'MYK-ORS', '00021', '2018-02-04 22:29:32');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `pid` bigint(20) NOT NULL,
  `or_num` varchar(255) DEFAULT NULL,
  `ors_num` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `irate` varchar(255) DEFAULT NULL,
  `iratetype` varchar(255) DEFAULT NULL,
  `cid` varchar(255) DEFAULT NULL,
  `paymentType` varchar(255) DEFAULT NULL,
  `apply_int` varchar(255) DEFAULT NULL,
  `payDate` datetime NOT NULL,
  `entryDate` datetime NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `processed_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `cancelled_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `s_id` int(11) NOT NULL,
  `meta` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `extra` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`s_id`, `meta`, `value`, `extra`) VALUES
(1, 'from_email', 'info@markramosonline.com', '-'),
(2, 'vprefix', 'MYK', ''),
(3, 'vstart', '5', ''),
(4, 'orsprefix', 'MYK-ORS', ''),
(5, 'orsdigit', '5', '');

-- --------------------------------------------------------

--
-- Table structure for table `trial_balance`
--

CREATE TABLE `trial_balance` (
  `tb_id` bigint(20) NOT NULL,
  `month_year` text,
  `chart_account_id` text,
  `prev_debit` text,
  `prev_credit` text,
  `trans_debit` text,
  `trans_credit` text,
  `ud_debit` text,
  `ud_credit` text,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ulevels`
--

CREATE TABLE `ulevels` (
  `level_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `pages` text NOT NULL,
  `limit` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ulevels`
--

INSERT INTO `ulevels` (`level_id`, `name`, `desc`, `pages`, `limit`) VALUES
(1, 'Administrator', 'Administrator Account', 'all', ''),
(2, 'Loans', 'User Level', 'dashboard,statistics,subscribers,help', ''),
(3, 'Clerk', '', '', ''),
(4, 'Collector', 'Collector', '', '10'),
(5, 'CI', 'Credit Investigator', '', ''),
(6, 'Auditor', 'Auditor', '', ''),
(7, 'Cashier', 'Cashier', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `email` text CHARACTER SET utf8 NOT NULL,
  `password` text CHARACTER SET utf8 NOT NULL,
  `name` text CHARACTER SET utf8 NOT NULL,
  `gender` text CHARACTER SET utf8,
  `profile_url` text CHARACTER SET utf8,
  `username` text CHARACTER SET utf8 NOT NULL,
  `birthday` text CHARACTER SET utf8,
  `location` text CHARACTER SET utf8,
  `rel_status` text CHARACTER SET utf8,
  `image_url` text CHARACTER SET utf8,
  `fb_uid` text CHARACTER SET utf8,
  `ulevel` int(11) NOT NULL,
  `referral_code` text,
  `num_users` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `active` text,
  `options` text,
  `parent` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `password`, `name`, `gender`, `profile_url`, `username`, `birthday`, `location`, `rel_status`, `image_url`, `fb_uid`, `ulevel`, `referral_code`, `num_users`, `date_added`, `active`, `options`, `parent`) VALUES
(3393, 'mykrocredit@yahoo.com', 'f5d1278e8109edd94e1e4197e04873b9', 'Admin', '', '', '', '', '', '', '', '', 1, '', '', '2014-11-05 19:04:36', '1', '', NULL),
(3431, 'test1@test.com', 'f5d1278e8109edd94e1e4197e04873b9', 'John Doe', NULL, NULL, 'test1@test.com', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2017-10-13 03:11:04', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `vid` int(11) NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `is_used` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`vid`, `prefix`, `code`, `is_used`, `date_added`) VALUES
(178, 'MYK', '00001', 0, '2018-01-23 12:56:57'),
(179, 'MYK', '00002', 0, '2018-01-23 13:02:50'),
(180, 'MYK', '00003', 0, '2018-01-23 13:14:17'),
(181, 'MYK', '00004', 0, '2018-01-23 13:21:31'),
(182, 'MYK', '00005', 0, '2018-01-23 13:26:48'),
(183, 'MYK', '00006', 0, '2018-01-23 13:31:41'),
(184, 'MYK', '00007', 0, '2018-01-30 12:19:53'),
(185, 'MYK', '00008', 0, '2018-01-31 12:34:45'),
(186, 'MYK', '00009', 0, '2018-01-31 12:39:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chart_accounts`
--
ALTER TABLE `chart_accounts`
  ADD PRIMARY KEY (`caid`);

--
-- Indexes for table `client_begloanbal`
--
ALTER TABLE `client_begloanbal`
  ADD PRIMARY KEY (`cbb_id`);

--
-- Indexes for table `client_data`
--
ALTER TABLE `client_data`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `client_meta`
--
ALTER TABLE `client_meta`
  ADD PRIMARY KEY (`c_metaid`);

--
-- Indexes for table `finance`
--
ALTER TABLE `finance`
  ADD PRIMARY KEY (`fid`);

--
-- Indexes for table `journal_voucher`
--
ALTER TABLE `journal_voucher`
  ADD PRIMARY KEY (`jvid`);

--
-- Indexes for table `jv_notes`
--
ALTER TABLE `jv_notes`
  ADD PRIMARY KEY (`jvntid`);

--
-- Indexes for table `jv_numbers`
--
ALTER TABLE `jv_numbers`
  ADD PRIMARY KEY (`jvnid`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `ors_numbers`
--
ALTER TABLE `ors_numbers`
  ADD PRIMARY KEY (`ors_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `trial_balance`
--
ALTER TABLE `trial_balance`
  ADD PRIMARY KEY (`tb_id`);

--
-- Indexes for table `ulevels`
--
ALTER TABLE `ulevels`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`vid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chart_accounts`
--
ALTER TABLE `chart_accounts`
  MODIFY `caid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `client_begloanbal`
--
ALTER TABLE `client_begloanbal`
  MODIFY `cbb_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_data`
--
ALTER TABLE `client_data`
  MODIFY `client_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `client_meta`
--
ALTER TABLE `client_meta`
  MODIFY `c_metaid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;
--
-- AUTO_INCREMENT for table `finance`
--
ALTER TABLE `finance`
  MODIFY `fid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `journal_voucher`
--
ALTER TABLE `journal_voucher`
  MODIFY `jvid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `jv_notes`
--
ALTER TABLE `jv_notes`
  MODIFY `jvntid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `jv_numbers`
--
ALTER TABLE `jv_numbers`
  MODIFY `jvnid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ors_numbers`
--
ALTER TABLE `ors_numbers`
  MODIFY `ors_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `pid` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `trial_balance`
--
ALTER TABLE `trial_balance`
  MODIFY `tb_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ulevels`
--
ALTER TABLE `ulevels`
  MODIFY `level_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3432;
--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `vid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
