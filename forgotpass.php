<?php
session_start();

// include_once('db.php');
//   $hostname = "localhost";
//   $database = "desktools";
//   $username_r = "root";
//   $password_r = 'root';
// $conn = new dbconHelper($database, $username_r, $password_r, $hostname);
// $conn->connect();
// $res = $conn->dbquery("select * from `users`" );
// $res = json_decode($res);
// foreach ($res->data as $key) {
//   # code...
//   $nres = json_decode($key);
//   echo $nres->email.'<br>';
// }

// // $res = $conn->dbquery("select * from `users` where `email` = 'ml13ramos@yahoo.com'" );
// // $res = json_decode($res);
// // $res = json_decode($res->data[0]);
// // echo $res->email;

// $conn->display_result($res);

if($_SESSION['authenticated']){
   header('location: dashboard.php');
   exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Financial System | Mykro Credit Service Corporation</title>

  <!-- Bootstrap core CSS -->

  <link href="includes/css/bootstrap.min.css" rel="stylesheet">

  <link href="includes/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="includes/css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="includes/css/custom.css" rel="stylesheet">
  <link href="includes/css/icheck/flat/green.css" rel="stylesheet">


  <script src="includes/js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">

  <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
      <div id="login" class="animate form">
        <section class="login_content">
          <img src="images/logo.png">
          <form>
            <h1>Financial System</h1>
            <div>
              <input type="text" class="form-control" placeholder="Email" required="" id="txtEmail"/>
            </div>
            <div>
              <a class="btn btn-default submit" href="javascript:void(0);" id="btnReqNewPass">Submit</a>
              <a class="reset_pass" href="index.php">Did you remember you login?</a>
            </div>
            <div class="clearfix"></div>
            <div class="separator">

              <!-- <p class="change_link">New to site?
                <a href="#toregister" class="to_register"> Create Account </a>
              </p> -->
              <div class="clearfix"></div>
              <br />
              <div>
                <!-- <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1> -->

                <p>&copy;<?php echo date('Y'); ?> All Rights Reserved.<br>Mykro Credit Service Corporation</p>
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>
      <div id="register" class="animate form">
        <section class="login_content">
          <form>
            <h1>Create Account</h1>
            <div>
              <input type="text" class="form-control" placeholder="Username" required="" />
            </div>
            <div>
              <input type="email" class="form-control" placeholder="Email" required="" />
            </div>
            <div>
              <input type="password" class="form-control" placeholder="Password" required="" />
            </div>
            <div>
              <a class="btn btn-default submit" href="index.html">Submit</a>
            </div>
            <div class="clearfix"></div>
            <div class="separator">

              <p class="change_link">Already a member ?
                <a href="#tologin" class="to_register"> Log in </a>
              </p>
              <div class="clearfix"></div>
              <br />
              <div>
                <!-- <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1> -->

                <p>©2015 All Rights Reserved. Lupit</p>
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>

    </div>
  </div>

<!-- footer codes -->
<div class="cssload-thecube">
  <div class="cssload-cube cssload-c1"></div>
  <div class="cssload-cube cssload-c2"></div>
  <div class="cssload-cube cssload-c4"></div>
  <div class="cssload-cube cssload-c3"></div>
</div>
<script type="text/javascript" src="includes/js/custom-login.js"></script>
<link href="includes/css/custom-loader.css" rel="stylesheet">
</body>

</html>
