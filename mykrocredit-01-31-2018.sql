-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 31, 2018 at 11:37 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 5.6.33-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mykrocredit`
--

-- --------------------------------------------------------

--
-- Table structure for table `chart_accounts`
--

CREATE TABLE `chart_accounts` (
  `caid` bigint(20) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `category` varchar(255) NOT NULL,
  `is_display` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chart_accounts`
--

INSERT INTO `chart_accounts` (`caid`, `code`, `name`, `category`, `is_display`, `date_added`) VALUES
(1, '100-001', 'Cash in Bank (BDO Hilado)', 'Cash', '1', '2017-07-31 02:28:08'),
(2, '101-001', 'A/R Trade', 'Loans and Other Receivables', '1', '2017-07-31 02:32:19'),
(4, '100-003', 'Cash in Bank (China Bank)', 'Cash', '1', '2017-07-31 02:28:00'),
(5, '101-001-01', 'A/R Loans', 'Loans and Other Receivables', '1', '2017-07-31 02:33:01'),
(6, '101-001-02', 'A/R Cash Advance', 'Loans and Other Receivables', '1', '2017-07-31 02:33:38'),
(7, '101-001-03', 'A/R Employee', 'Loans and Other Receivables', '1', '2017-07-31 02:34:03'),
(8, '101-001-04', 'A/R Blacklist', 'Loans and Other Receivables', '1', '2017-07-31 02:36:21'),
(9, '101-001-05', 'A/R Special Accounts', 'Loans and Other Receivables', '1', '2017-07-31 02:36:51'),
(10, '101-001-06', 'A/R Excess', 'Loans and Other Receivables', '1', '2017-07-31 02:37:14'),
(11, '400-003', 'Service Fee', 'Revenue', '1', '2017-07-31 02:38:04'),
(12, '400-003-01', 'Processing Fee', 'Revenue', '1', '2017-07-31 02:38:37'),
(13, '400-003-02', 'Notarial Fee', 'Revenue', '1', '2017-07-31 02:39:06'),
(14, '400-003-03', 'Miscellaneous', 'Revenue', '1', '2017-07-31 02:39:30'),
(15, '400-003-04', 'Finders Fee', 'Revenue', '1', '2017-07-31 02:39:51'),
(16, '400-005', 'Rebates/Discounts', 'Revenue', '1', '2017-07-31 02:40:22'),
(17, '100-002', 'Cash in Bank (BDO East)', 'Cash', '1', '2017-07-31 03:10:43'),
(18, '102-000', 'Due From ...', 'Loans and Other Receivables', '1', '2017-07-31 15:37:52'),
(19, '105-000', 'Bonuses', 'Others', '1', '2018-01-30 12:21:43');

-- --------------------------------------------------------

--
-- Table structure for table `client_begloanbal`
--

CREATE TABLE `client_begloanbal` (
  `cbb_id` bigint(20) NOT NULL,
  `cid` varchar(255) DEFAULT NULL,
  `balance` varchar(255) DEFAULT NULL,
  `irate` varchar(255) DEFAULT NULL,
  `irateType` varchar(255) DEFAULT NULL,
  `terms` varchar(255) DEFAULT NULL,
  `entryDate` datetime DEFAULT NULL,
  `entryStatus` varchar(255) DEFAULT NULL,
  `acctType` varchar(255) DEFAULT NULL,
  `paidStat` varchar(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_data`
--

CREATE TABLE `client_data` (
  `client_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address` longtext NOT NULL,
  `birthdate` date NOT NULL,
  `civil_status` varchar(255) NOT NULL,
  `contact_num` varchar(255) NOT NULL,
  `sss_num` varchar(255) NOT NULL,
  `company` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client_meta`
--

CREATE TABLE `client_meta` (
  `c_metaid` bigint(20) NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `meta` text NOT NULL,
  `value` longtext NOT NULL,
  `extra` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `finance`
--

CREATE TABLE `finance` (
  `fid` bigint(20) NOT NULL,
  `client_id` text NOT NULL,
  `intrate` text NOT NULL,
  `debit` text NOT NULL,
  `credit` text NOT NULL,
  `balance` text NOT NULL,
  `extra` text NOT NULL,
  `status` text NOT NULL,
  `loan_type` text NOT NULL,
  `voucher_id` text NOT NULL,
  `term` text NOT NULL,
  `amount` text NOT NULL,
  `date_added` datetime NOT NULL,
  `loan_date` datetime NOT NULL,
  `processedby` text NOT NULL,
  `cancelledby` text NOT NULL,
  `addedby` text NOT NULL,
  `date_approved` datetime NOT NULL,
  `inProcFee` varchar(255) DEFAULT NULL,
  `inpFindersFee` varchar(255) DEFAULT NULL,
  `inpNotarial` varchar(255) DEFAULT NULL,
  `inpMisc` varchar(255) DEFAULT NULL,
  `inpInsurance` varchar(255) DEFAULT NULL,
  `inpOthers` varchar(255) DEFAULT NULL,
  `inpAdjustment` varchar(255) DEFAULT NULL,
  `inpBonus` varchar(255) DEFAULT NULL,
  `approvedby` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `journal_voucher`
--

CREATE TABLE `journal_voucher` (
  `jvid` bigint(20) NOT NULL,
  `jvnid` text,
  `client_id` text,
  `chart_account_id` text,
  `debit` float DEFAULT NULL,
  `credit` float DEFAULT NULL,
  `date_trans` datetime DEFAULT NULL,
  `addby` text,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jv_notes`
--

CREATE TABLE `jv_notes` (
  `jvntid` bigint(20) NOT NULL,
  `jvnid` text,
  `notes` text CHARACTER SET utf8,
  `dateadded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jv_numbers`
--

CREATE TABLE `jv_numbers` (
  `jvnid` bigint(20) NOT NULL,
  `prefix` text,
  `number` text,
  `dateadded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `log_id` bigint(20) NOT NULL,
  `transaction` longtext NOT NULL,
  `addedby` bigint(20) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ors_numbers`
--

CREATE TABLE `ors_numbers` (
  `ors_id` bigint(20) NOT NULL,
  `fid` text,
  `ors_num` text NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `dateadded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `pid` bigint(20) NOT NULL,
  `or_num` varchar(255) DEFAULT NULL,
  `ors_num` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `irate` varchar(255) DEFAULT NULL,
  `iratetype` varchar(255) DEFAULT NULL,
  `cid` varchar(255) DEFAULT NULL,
  `paymentType` varchar(255) DEFAULT NULL,
  `apply_int` varchar(255) DEFAULT NULL,
  `payDate` datetime NOT NULL,
  `entryDate` datetime NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `processed_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `cancelled_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `s_id` int(11) NOT NULL,
  `meta` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `extra` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`s_id`, `meta`, `value`, `extra`) VALUES
(1, 'from_email', 'info@markramosonline.com', '-'),
(2, 'vprefix', 'MYK', ''),
(3, 'vstart', '5', ''),
(4, 'orsprefix', 'MYK-ORS', ''),
(5, 'orsdigit', '5', '');

-- --------------------------------------------------------

--
-- Table structure for table `trial_balance`
--

CREATE TABLE `trial_balance` (
  `tb_id` bigint(20) NOT NULL,
  `month_year` text,
  `chart_account_id` text,
  `prev_debit` text,
  `prev_credit` text,
  `trans_debit` text,
  `trans_credit` text,
  `ud_debit` text,
  `ud_credit` text,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ulevels`
--

CREATE TABLE `ulevels` (
  `level_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `pages` text NOT NULL,
  `limit` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ulevels`
--

INSERT INTO `ulevels` (`level_id`, `name`, `desc`, `pages`, `limit`) VALUES
(1, 'Administrator', 'Administrator Account', 'all', ''),
(2, 'Loans', 'User Level', 'dashboard,statistics,subscribers,help', ''),
(3, 'Clerk', '', '', ''),
(4, 'Collector', 'Collector', '', '10'),
(5, 'CI', 'Credit Investigator', '', ''),
(6, 'Auditor', 'Auditor', '', ''),
(7, 'Cashier', 'Cashier', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `email` text CHARACTER SET utf8 NOT NULL,
  `password` text CHARACTER SET utf8 NOT NULL,
  `name` text CHARACTER SET utf8 NOT NULL,
  `gender` text CHARACTER SET utf8,
  `profile_url` text CHARACTER SET utf8,
  `username` text CHARACTER SET utf8 NOT NULL,
  `birthday` text CHARACTER SET utf8,
  `location` text CHARACTER SET utf8,
  `rel_status` text CHARACTER SET utf8,
  `image_url` text CHARACTER SET utf8,
  `fb_uid` text CHARACTER SET utf8,
  `ulevel` int(11) NOT NULL,
  `referral_code` text,
  `num_users` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `active` text,
  `options` text,
  `parent` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `password`, `name`, `gender`, `profile_url`, `username`, `birthday`, `location`, `rel_status`, `image_url`, `fb_uid`, `ulevel`, `referral_code`, `num_users`, `date_added`, `active`, `options`, `parent`) VALUES
(3393, 'mykrocredit@yahoo.com', 'f5d1278e8109edd94e1e4197e04873b9', 'Admin', '', '', '', '', '', '', '', '', 1, '', '', '2014-11-05 19:04:36', '1', '', NULL),
(3431, 'test1@test.com', 'f5d1278e8109edd94e1e4197e04873b9', 'John Doe', NULL, NULL, 'test1@test.com', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2017-10-13 03:11:04', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `vid` int(11) NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `is_used` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chart_accounts`
--
ALTER TABLE `chart_accounts`
  ADD PRIMARY KEY (`caid`);

--
-- Indexes for table `client_begloanbal`
--
ALTER TABLE `client_begloanbal`
  ADD PRIMARY KEY (`cbb_id`);

--
-- Indexes for table `client_data`
--
ALTER TABLE `client_data`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `client_meta`
--
ALTER TABLE `client_meta`
  ADD PRIMARY KEY (`c_metaid`);

--
-- Indexes for table `finance`
--
ALTER TABLE `finance`
  ADD PRIMARY KEY (`fid`);

--
-- Indexes for table `journal_voucher`
--
ALTER TABLE `journal_voucher`
  ADD PRIMARY KEY (`jvid`);

--
-- Indexes for table `jv_notes`
--
ALTER TABLE `jv_notes`
  ADD PRIMARY KEY (`jvntid`);

--
-- Indexes for table `jv_numbers`
--
ALTER TABLE `jv_numbers`
  ADD PRIMARY KEY (`jvnid`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `ors_numbers`
--
ALTER TABLE `ors_numbers`
  ADD PRIMARY KEY (`ors_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `trial_balance`
--
ALTER TABLE `trial_balance`
  ADD PRIMARY KEY (`tb_id`);

--
-- Indexes for table `ulevels`
--
ALTER TABLE `ulevels`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`vid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chart_accounts`
--
ALTER TABLE `chart_accounts`
  MODIFY `caid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `client_begloanbal`
--
ALTER TABLE `client_begloanbal`
  MODIFY `cbb_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_data`
--
ALTER TABLE `client_data`
  MODIFY `client_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `client_meta`
--
ALTER TABLE `client_meta`
  MODIFY `c_metaid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `finance`
--
ALTER TABLE `finance`
  MODIFY `fid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `journal_voucher`
--
ALTER TABLE `journal_voucher`
  MODIFY `jvid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `jv_notes`
--
ALTER TABLE `jv_notes`
  MODIFY `jvntid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `jv_numbers`
--
ALTER TABLE `jv_numbers`
  MODIFY `jvnid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ors_numbers`
--
ALTER TABLE `ors_numbers`
  MODIFY `ors_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `pid` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `trial_balance`
--
ALTER TABLE `trial_balance`
  MODIFY `tb_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ulevels`
--
ALTER TABLE `ulevels`
  MODIFY `level_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3432;
--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `vid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
