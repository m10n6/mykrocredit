mykrocredit

ToDo:
- Generate ORS
- Generate Reports
- User Level assignments
- 2nd Payment Apply Interest [changed to payment schedule DONE]

Low Priority:
- popup balance checker for next loans [DONE]
- no user intervention for popup 
- messaging
- settings


After going live
- Trial Balance


A/R = Debit
Fee = Credit

Display Due Date

===========================================
cleaner

TRUNCATE TABLE client_meta;
TRUNCATE TABLE client_data;
TRUNCATE TABLE finance;
TRUNCATE TABLE journal_voucher;
TRUNCATE TABLE jv_notes;
TRUNCATE TABLE jv_numbers;
TRUNCATE TABLE log;
TRUNCATE TABLE ors_numbers;
TRUNCATE TABLE trial_balance;
TRUNCATE TABLE voucher;




Patches
===========================================
http://localhost/mykrocredit/patches/main-admin-userfix.php


Fixers
===========================================
ALTER TABLE my_table AUTO_INCREMENT=200;
