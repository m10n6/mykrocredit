function loginMe(){
	var user = $('#txtUser').val().trim();
	var pass = $('#txtPass').val().trim();

	$.ajax({
		type : 'post',
		url  : 'lib/login.php',
		data : {
			un : user,
			pw : pass
		},
		beforeSend: function(xhr){
			$('.cssload-thecube').toggle();
		},
		success: function(xhr){
			// console.log(xhr);
			$('.cssload-thecube').toggle();
			if(xhr == 'Success'){
				window.location = 'dashboard.php';
			}else{
				alert(xhr);
			}
		},
		error: function(error){
			$('.cssload-thecube').toggle();
			alert('Error: '+ error.status + ' ' + error.statusText );
			console.log(error);
		}
	});

}


$(document).ready(function(){
	$('#btnLogin').click(function(){
		loginMe();
	});

	$('#txtPass').keypress(function(e){
		if(e.which == 13) {
			loginMe();
		}
	});

	$('#btnReqNewPass').click(function(){
		var txtEmail = $('#txtEmail').val();
		if(txtEmail !== ''){
			$.ajax({
				type : 'post',
				url : 'lib/request.password.php',
				data : {
					'email' : txtEmail
				},
				beforeSend: function(){
					$('.cssload-thecube').toggle();
				},
				success : function(res){
					$('.cssload-thecube').toggle();
					alert('Please check your email address!\nIf you have received any, please check your spam folder.\nOtherwise, call Developer!');
				}
			});			
		}else{
			alert('Please enter your email!');
		}

	});
});