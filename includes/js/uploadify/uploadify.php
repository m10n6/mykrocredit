<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
// include_once('config.php');
// include_once('lib/funcjax.php');

// include('../../../../db.php');
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
include_once('../../../config.php');

include_once('../../../lib/funcjax.php');

// Define a destination
$targetFolder = '../../../uploads'; // Relative to the root

$verifyToken = md5('unique_salt' . $_POST['timestamp']);

// if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	// $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
	$targetPath = $targetFolder;
	$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];
	
	// Validate the file type
	// $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
	$fileTypes = array('mp3', 'ogg', 'wav'); // File extensions
	$fileParts = pathinfo($_FILES['Filedata']['name']);

	$fname = $_FILES['Filedata']['name'];
	
	if (in_array($fileParts['extension'],$fileTypes)) {
		move_uploaded_file($tempFile,$targetFile);
		//save to database
		// $sql = "insert into `files` set 
		// 	`name` = '".$fname."', 
		// 	`created_date` = '".date('Y-m-d H:i:s')."'
		// ";	
		// $conn->dbquery($sql);
		// print_r($conn);
		// mysql_query($sql);
		// echo FormatDate(date('Y-m-d H:i:s'));
		echo '1';
	} else {
		echo 'Invalid file type.';
	}
}

?>