<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

	ini_set('memory_limit','2048M');
	ini_set('max_execution_time', 0);
	date_default_timezone_set("Asia/Taipei");

	define('DBCONN','');
	/* LOCALHOST */
	// $hostname = "localhost";
	$database = "mykrocredit";
	$username_r = "root";
	$password_r = 'pa55w0rd!';
	// $password_r = ''; // live


	/* LIVE */
	$hostname = "localhost";
	// $database = "geekbaco_mykro";
	// $username_r = "geekbaco_lupit";
	// $password_r = 'TXUpJF5svmRo';

	//legacy
	// $connect  = mysql_pconnect($hostname, $username_r, $password_r) or die(mysql_error());
	// mysql_select_db($database, $connect) or die(mysql_error());

	// //latest
	// $connect  = new mysqli($hostname, $username_r, $password_r, $database);
	// // Check connection
	// if ($connect->connect_error) {
	//     die("Connection failed: " . $connect->connect_error);
	// }
	include_once(dirname(__FILE__) .'/db.php');

	$conn = new dbconHelper($database, $username_r, $password_r, $hostname);
	$conn->connect();
	// define('DBCONN',$conn);
?>
