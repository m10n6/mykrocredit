<?php
// error_reporting(-1);
session_start();
include_once('config.php');
include_once('lib/funcjax.php');
if(!$_SESSION['authenticated']){
   header('location: index.php');
   exit();
}else{
  $page = secure_get('page');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title> <?php echo _page_title($page); ?> | Mykro Credit </title>

  <?php
    include_once('header.php');
    //addition headers here
  ?>
  <script src="includes/js/printthis/printThis.js"></script>
  <link href="includes/js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="includes/js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="includes/js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="includes/js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="includes/js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <?php
      include_once('sidebar.php');
      ?>

      <!-- top navigation -->
      <?php include_once('top_nav.php'); ?>
      <!-- /top navigation -->


      <!-- page content -->
      <div class="right_col" role="main">
        <?php
          switch ($page) {
            case 'dashboard':
              # code...
              include_once('lib/contents/dashboard.php');
              break;

            case 'user_list':
              include_once('lib/contents/userlist.php');
              break;  

            case 'userlevel_list':
              include_once('lib/contents/user.manager.php');
              break;  

            case 'clients_list':
              # code...
              include_once('lib/contents/clients.php');
              break;
            
            case 'artrade_list':
            // case 'voucher_list':
              include_once('lib/contents/ar_table.php');
              break;

            case 'voucher_list':
            // case 'artrade_list':
              include_once('lib/contents/voucher.php');
              break;

            case 'chart_of_accounts':
              include_once('lib/contents/chart_accounts.php');
              break;

            case 'trial_balance':
              include_once('lib/contents/trialbalance.php');
              break;

            case 'viewdata':
              include_once('lib/contents/viewdata.php');
              break;

            case 'journal_list':
              include_once('lib/contents/journalvoucher.php');
              break;           

            case 'google_analytics':
              include_once('lib/contents/googleanalytics.php');
              break; 

            case 'schedules':
              include_once('lib/contents/schedules.php');
              break; 

            case 'genreport':
              include_once('lib/contents/genreport.php');
              break;   

            case 'payment':
              include_once('lib/contents/payments.php');
              break;  

            case 'under_payment':
              include_once('lib/contents/underpayment.php');
              break;  

            case 'no_payment':
              include_once('lib/contents/nopayment.php');
              break; 

            case 'no_paymenti':
              include_once('lib/contents/nopayment_inactive.php');
              break; 

            case 'client_payment':
              include_once('lib/contents/client.payments.php');
              break;  

            case 'myprofile':
              include_once('lib/contents/myprofile.php');
              break;  

            case 'myprocessed':
              include_once('lib/contents/myprocessed.php');
              break;

            case 'calendar':
              include_once('lib/contents/calendar.php');
              break;  
            case 'sched_page':
              include_once('lib/contents/schedpages.php');
              break;
            case 'schedules_add':
              include_once('lib/contents/schedules.add.php');
              break;   
            case 'gospels':
              include_once('lib/contents/gospels.php');
              break;  
            case 'gospels_add':
              include_once('lib/contents/gospels.add.php');
              break;   
            case 'announce':
              include_once('lib/contents/announce.php');
              break;  
            case 'announce_add':
              include_once('lib/contents/announce.add.php');
              break;   
            case 'audios':
              include_once('lib/contents/audios.php');
              break;
            case 'donation':
              include_once('lib/contents/donation.php');
              break;
            case 'inbox':
              include_once('lib/contents/inbox.php');
              break;
            case 'msg_compose':
              include_once('lib/contents/compose.php');
              break;

            case 'statistics':
              # code...
              include_once('lib/contents/statistics.php');
              break;
            case 'basic_settings':
              include_once('lib/contents/basic_settings.php');
              break;

            case 'collections':
              include_once('lib/contents/collection.php');
              break;
            case 'schedlist':
              include_once('lib/contents/schedlist.php');
            break; 

            case 'report_dailyca':
              include_once('lib/contents/report.daily.ca.php');
            break;

            case 'report_orpayments' :
              include_once('lib/contents/report.payment.or.php');
            break;

            case 'report_orspayments' :
              include_once('lib/contents/report.payment.ors.php');
            break;

            default:
              # code...
              include_once('lib/contents/dashboard.php');
              break;
          }
        ?>

        <!-- footer content -->

        <footer class="hidden-print">
          <div class="copyright-info">
            <p class="pull-right">Design and Built for Mykro Credit Services Corporation  
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

<div class="modal fade" role="dialog" id="modNotifications">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Notifications</h4>

      </div>
      <div class="modal-body">
        

      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="btnProcessLoans" disabled>Submit</button> -->
        <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 5px;">Close</button>
      </div>
    </div>
  </div>
</div>

  <?php
    switch ($page) {
      case 'dashboard':
        # code...
        include_once('lib/contents/dashboard.footer.php');
        break;

      case 'user_list':
        # code...
        include_once('lib/contents/userlist.footer.php');
        break;

      case 'userlevel_list':
        # code...
        include_once('lib/contents/user.manager.footer.php');
        break;

      case 'clients_list':
        # code...
        include_once('lib/contents/clients.footer.php');
        break;

      case 'artrade_list':
      // case 'voucher_list':
        include_once('lib/contents/ar_table.footer.php');
        break;

      // case 'artrade_list':
      case 'voucher_list':
        include_once('lib/contents/voucher.footer.php');
        break;

      case 'chart_of_accounts':
        include_once('lib/contents/chart_accounts.footer.php');
        break;

      // case 'trial_balance':
      //   include_once('lib/contents/chart_accounts.footer.php');
      //   break;

      case 'myprocessed':
        include_once('lib/contents/myprocessed.footer.php');
        break;

      case 'sched_page':
        include_once('lib/contents/schedpages.footer.php');
        break;

      case 'stats_report':
        include_once('lib/contents/genreport.footer.php');
        break;
      case 'schedules':
        include_once('lib/contents/schedules.footer.php');
        break; 
      case 'schedules_add':
        include_once('lib/contents/schedules.footer.php');
        break; 

      case 'gospels':
        include_once('lib/contents/gospels.footer.php');
        break; 
      case 'gospels_add':
        include_once('lib/contents/gospels.footer.php');
        break; 

      case 'announce':
        include_once('lib/contents/announce.footer.php');
        break; 
      case 'announce_add':
        include_once('lib/contents/announce.footer.php');
        break; 

      case 'audios':
        include_once('lib/contents/audios.footer.php');
      break;   

      case 'donation':
        include_once('lib/contents/donation.footer.php');
        break;

      case 'collections':
        include_once('lib/contents/collection.footer.php');
        break;
      case 'inbox':
        include_once('lib/contents/inbox.footer.php');
        break;
      case 'statistics':
        # code...
        include_once('lib/contents/statistics.footer.php');
        break;
      case 'calendar':
        # code...
        include_once('lib/contents/calendar.footer.php');
        break;
      case 'payment':
        include_once('lib/contents/payments.footer.php');
        break; 

      case 'client_payment':
        include_once('lib/contents/client.payments.footer.php');
      break;

      case 'schedlist':
        include_once('lib/contents/schedlist.footer.php');
      break; 

      case 'report_dailyca':
        include_once('lib/contents/report.daily.ca.footer.php');
      break;

      case 'report_orpayments' :
        include_once('lib/contents/report.payment.or.footer.php');
      break;

      case 'report_orspayments' :
        include_once('lib/contents/report.payment.ors.footer.php');
      break;

      default:
        # code...
        include_once('lib/contents/dashboard.footer.php');
        break;
    }
  ?>  


<div class="modal fade" role="dialog" id="modNeedHelp">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Need Help? <small>(This will email the developer. Make sure you have internet)</small></h4>

      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
              <textarea style="width: 100%; height: 300px;" id="txtNeedHelpBox"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnSendHelp" ><i class="fa fa-send"></i>Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 5px;">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- bootstrap-wysiwyg -->
<script src="vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
<script src="vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="vendors/google-code-prettify/src/prettify.js"></script>
<script>
  $(document).ready(function(){
    $('#txtNeedHelpBox').wysiwyg();
    $('#btnNeedHelp').click(function(e){
      e.preventDefault();
      $('#modNeedHelp').modal('show');
    });
    $('#btnSendHelp').click(function(){
      var txtHelp = $('#txtNeedHelpBox').val();
      if(txtHelp !== ''){
        $.ajax({
          type : 'post',
          url : 'lib/sendhelp.php',
          data : {
            'msghtml' : txtHelp
          },
          beforeSend: function(){
            $('#btnSendHelp').html('<i class="fa fa-gear fa-spin"></i> Sending...');
          },
          success : function(result){
            $('#btnSendHelp').html('<i class="fa fa-send"></i> Send');
          }
        });
      }else{
        alert('Unable to send help! You need to put your message. :) ');
      }
    });
  });
</script>
<style>
.picker_4 {
  z-index: 99999;
}

/* added by markramos 02-22-2018 */
/*.sidebar-footer {
  display: none !important;
}*/

.profile_info h2 {
  font-size: 12px !important;
}

</style>
<script>
  var HW_config = {
    selector: ".latest_updates", // CSS selector where to inject the badge
    account:  "JnGKqy"
  }
</script>
<!--<script async src="//cdn.headwayapp.co/widget.js"></script>-->
</body>

</html>
