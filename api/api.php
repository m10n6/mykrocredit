<?php

include_once('../config.php');
include_once('../lib/funcjax.php');

$action = request('action');

switch ($action) {
	case 'reservation':
		# code...
		// $sql = "";
		//`res_id`, `name`, `phone`, `email`, `details`, `start_date`, `end_date`, `created_date`, `by`, `assigned_to`, `status`
		// name  : nn,
		// email : ne,
		// phone : np,
		// ddate : nd,
		// ntime : nt

		$name = secure_post('name');
		$phone = secure_post('phone');
		$email = secure_post('email');
		$date = secure_post('ndate');
		$time = secure_post('ntime');
		$newdate = $date." ".$time; 
		$insert_by = secure_post('ins_by');
		$assigned_to = secure_post('ass_to');
		$status = secure_post('status');
		$details = secure_post('details');
		$ax = secure_post('ax');
		$event = secure_post('event_');
		$venue = secure_post('venue_');
		$amount = secure_post('amount_');
		if(empty($amount)){
			$amount = '0';
		}
		$payment_type = secure_post('ptype');

		$transaction = secure_post('trans');

		$res_id = secure_post('res_id');
		if(empty($res_id)){
			if($transaction == 'front'){
				$sql = "insert into `reservation` set 
					`name` = '".$name."', 
					`phone` = '".$phone."', 
					`email` = '".$email."', 
					`event` = '".$event."',
					`venue` = '".$venue."',
					`details` = '', 
					`start_date` = '".$newdate."', 
					`end_date` = '".$newdate."', 
					`created_date` = '".date("Y-m-d H:i:s")."',
					`status` = '".$status."',
					`insert_by` = '".$insert_by."',
					`assigned_to` = '".$assigned_to."',
					`action` = '".$ax."',
					`amount` = '".$amount."'
				";
				$res = $conn->dbquery($sql);

				$htmlmsg = 'Hi '. $name. ",
						<strong>Name : </strong> ".$name."<br>
						<strong>Event : </strong> ".$event."<br>
						<strong>Venue : </strong> ".$venue."<br>
						<strong>Date : </strong> ".$newdate."<br>
						Your reservation is being processed. Please confirm your reservation 3days before the event or the reservation will be automatically Cancelled.
				";
				mySimpleMailer($email, $email, 'lupit@geekbacolod.com','lupit@geekbacolod.com', 'Reservation', $htmlmsg);
				echo 'success';				
			}else{
				$sqlc1 = "select * from `reservation` where `start_date` = '".$newdate."' and `venue` = '".$venue."'  and (`status` != 'cancelled' or `status` = 'approved')";
	            $res2 = $conn->dbquery($sqlc1);
	            $res2 = json_decode($res2);
	            $res2 = $res2->data;
	            if(count($res2) > 0){
	            	echo 'Please select different venue. Venue is already booked';
	            }else{

					$sqlc = "select * from `reservation` where `start_date` = '".$newdate."' and `assigned_to` = '".$assigned_to."' and (`status` != 'cancelled'  or `status` = 'approved') ";
		            $res1 = $conn->dbquery($sqlc);
		            $res1 = json_decode($res1);
		            $res1 = $res1->data;
		            if(count($res1) > 0){
		            	// echo $sqlc;
		            	echo 'Priest already assigned!';
		            }else{
						$sql = "insert into `reservation` set 
							`name` = '".$name."', 
							`phone` = '".$phone."', 
							`email` = '".$email."', 
							`event` = '".$event."',
							`venue` = '".$venue."',
							`details` = '".$details."', 
							`start_date` = '".$newdate."', 
							`end_date` = '".$newdate."', 
							`created_date` = '".date("Y-m-d H:i:s")."',
							`status` = '".$status."',
							`insert_by` = '".$insert_by."',
							`assigned_to` = '".$assigned_to."',
							`action` = '".$ax."',
							`amount` = '".$amount."',
							`payment_type` = '".$payment_type."'
						";
						$res = $conn->dbquery($sql);
						// echo $sql;

						$htmlmsg = 'Hi '. $name. ",
								<strong>Name : </strong> ".$name."<br>
								<strong>Event : </strong> ".$event."<br>
								<strong>Venue : </strong> ".$venue."<br>
								<strong>Date : </strong> ".$newdate."<br>
								Your reservation is being processed. Please confirm your reservation 3days before the event or the reservation will be automatically Cancelled.
						";
						mySimpleMailer($email, $email, 'lupit@geekbacolod.com','lupit@geekbacolod.com', 'Reservation', $htmlmsg);
						echo 'success';
		            }            	
	            }
			}


		}else{ 
			$sqlc1 = "select * from `reservation` where `start_date` = '".$newdate."' and `venue` = '".$venue."' and `res_id` != '".$res_id."'  and (`status` != 'cancelled' or `status` = 'approved')";
            $res2 = $conn->dbquery($sqlc1);
            $res2 = json_decode($res2);
            $res2 = $res2->data;
            if(count($res2) > 0){
            	echo 'Please select different venue. Venue is already booked';
            }else{

				$sqlc = "select * from `reservation` where `start_date` = '".$newdate."' and `assigned_to` = '".$assigned_to."'  and `res_id` != '".$res_id."'  and (`status` != 'cancelled' or `status` = 'approved')";
	            $res1 = $conn->dbquery($sqlc);
	            $res1 = json_decode($res1);
	            $res1 = $res1->data;
	            if(count($res1) > 0){
	            	echo 'Priest already assigned!';
	            }else{
					$sql = "update `reservation` set 
						`name` = '".$name."', 
						`phone` = '".$phone."', 
						`email` = '".$email."', 
						`event` = '".$event."',
						`venue` = '".$venue."',
						`details` = '".$details."', 
						`start_date` = '".$newdate."', 
						`end_date` = '".$newdate."', 
						`status` = '".$status."',
						`insert_by` = '".$insert_by."',
						`assigned_to` = '".$assigned_to."',
						`action` = '".$ax."',
						`amount` = '".$amount."',
						`payment_type` = '".$payment_type."'
						where `res_id` = '".$res_id."'
					";
					$res = $conn->dbquery($sql);
					echo 'success';
	            }            	
            }
		}
		// echo $sql;
		// $res = $conn->dbquery("select * from `users` where `email` = 'ml13ramos@yahoo.com'" );
		// $res = json_decode($res);
		// $res = json_decode($res->data[0]);
		// echo $res->email;

	break;

	case 'fullPayment':
		$res_id = secure_post('res_id');
		$amount = secure_post('amount');
		$payment_type = secure_post('ptype');
		$sqlc = "select * from `reservation` where `res_id` = '".$res_id."' and  `assigned_to` != '0'";
        $res1 = $conn->dbquery($sqlc);
        $res1 = json_decode($res1);
        $res1 = $res1->data;
        if(count($res1) > 0){

        	foreach($res1 as $r){
        		$s = json_decode($r);
        	}

			$htmlmsg = "Hi ,
					Your reservation has been approved.
			";
			mySimpleMailer($s->email, $s->email, 'lupit@geekbacolod.com','lupit@geekbacolod.com', 'Reservation', $htmlmsg);

			$sql = "update `reservation` set 
				`status` = 'approved',
				`amount` = '".$amount."',
				`payment_type` = '".$payment_type."'
				where `res_id` = '".$res_id."'
			";

			// echo $sql;
			$res = $conn->dbquery($sql);
			echo 'success';
		}else{
			echo 'Please assign Priest and update before approving.';
		}
	break;

	case 'pending_reservation':
		$res_id = secure_post('res_id');

		$sqlc = "select * from `reservation` where `res_id` = '".$res_id."' and  `assigned_to` != '0'";
        $res1 = $conn->dbquery($sqlc);
        $res1 = json_decode($res1);
        $res1 = $res1->data;
        if(count($res1) > 0){

        	foreach($res1 as $r){
        		$s = json_decode($r);
        	}

			$htmlmsg = "Hi ,
					Your reservation is now pending.
			";
			mySimpleMailer($s->email, $s->email, 'lupit@geekbacolod.com','lupit@geekbacolod.com', 'Reservation', $htmlmsg);
		}

		$sql = "update `reservation` set 
			`status` = 'pending'
			where `res_id` = '".$res_id."'
		";

		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'approved_reservation':
		$name = secure_post('name');
		$phone = secure_post('phone');
		$email = secure_post('email');
		$date = secure_post('ndate');
		$time = secure_post('ntime');
		$newdate = $date." ".$time; 
		$insert_by = secure_post('ins_by');
		$assigned_to = secure_post('ass_to');
		$status = secure_post('status');
		$details = secure_post('details');
		$ax = secure_post('ax');
		$event = secure_post('event_');
		$venue = secure_post('venue_');

		$res_id = secure_post('res_id');
		$amount = secure_post('amount');
		$payment_type = secure_post('ptype');
		$sqlc = "select * from `reservation` where `res_id` = '".$res_id."' and  `assigned_to` != '0'";
        $res1 = $conn->dbquery($sqlc);
        $res1 = json_decode($res1);
        $res1 = $res1->data;
  //       if(count($res1) > 0){
		if($assigned_to != '0'){

        	foreach($res1 as $r){
        		$s = json_decode($r);
        	}


			$sqlc1 = "select * from `reservation` where `start_date` = '".$newdate."' and `venue` = '".$venue."' and `res_id` != '".$res_id."'  and (`status` != 'cancelled' or `status` = 'approved')";
            $res2 = $conn->dbquery($sqlc1);
            $res2 = json_decode($res2);
            $res2 = $res2->data;
            if(count($res2) > 0){
            	echo 'Please select different venue. Venue is already booked';
            }else{

				$sqlc = "select * from `reservation` where `start_date` = '".$newdate."' and `assigned_to` = '".$assigned_to."'  and `res_id` != '".$res_id."'  and (`status` != 'cancelled' or `status` = 'approved')";
	            $res1 = $conn->dbquery($sqlc);
	            $res1 = json_decode($res1);
	            $res1 = $res1->data;
	            if(count($res1) > 0){
	            	echo 'Priest already assigned!';
	            }else{

					$htmlmsg = "Hi ,
							Your reservation has been approved.<br/>
							<br/>
							<strong>Name : </strong> ".$name."<br>
							<strong>Event : </strong> ".$event."<br>
							<strong>Venue : </strong> ".$venue."<br>
							<strong>Date : </strong> ".$newdate."<br>
					";
					mySimpleMailer($s->email, $s->email, 'lupit@geekbacolod.com','lupit@geekbacolod.com', 'Reservation', $htmlmsg);

					$sql = "update `reservation` set 
						`name` = '".$name."', 
						`phone` = '".$phone."', 
						`email` = '".$email."', 
						`event` = '".$event."',
						`venue` = '".$venue."',
						`details` = '".$details."', 
						`start_date` = '".$newdate."', 
						`end_date` = '".$newdate."', 
						`status` = '".$status."',
						`insert_by` = '".$insert_by."',
						`assigned_to` = '".$assigned_to."',
						`action` = '".$ax."',
						`status` = 'approved',
						`amount` = '".$amount."',
						`payment_type` = '".$payment_type."'
						where `res_id` = '".$res_id."'
					";

					// echo $sql;
					$res = $conn->dbquery($sql);
					echo 'success';
				}
			}
		}else{
			echo 'Please assign Priest approving.';
		}
	break;

	case 'updateAmount':
		$res_id = secure_post('res_id');
		$amount = secure_post('amount_');

		$sql = "update `reservation` set 
			`amount` = '".$amount."'
			where `res_id` = '".$res_id."'
		";

		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'cancelled_reservation':
		$res_id = secure_post('res_id');

		$sqlc = "select * from `reservation` where `res_id` = '".$res_id."' and  `assigned_to` != '0'";
        $res1 = $conn->dbquery($sqlc);
        $res1 = json_decode($res1);
        $res1 = $res1->data;
        if(count($res1) > 0){

        	foreach($res1 as $r){
        		$s = json_decode($r);
        	}

			$htmlmsg = "Hi ,
					Your reservation is now cancelled.
			";
			mySimpleMailer($s->email, $s->email, 'lupit@geekbacolod.com','lupit@geekbacolod.com', 'Reservation', $htmlmsg);
		}


		$sql = "update `reservation` set 
			`status` = 'cancelled'
			where `res_id` = '".$res_id."'
		";

		$res = $conn->dbquery($sql);
		echo 'success';
	break;


	case 'getDailyGospel':

		$startdate = date("Y-m-d")." "."00:00:00";
		$enddate = date("Y-m-d")." "."23:59:59";
		$res = $conn->dbquery("SELECT * FROM `daily_gospel` WHERE `created_date` between '".$startdate."' and '".$enddate."' " );
		$res = json_decode($res);
		$res = $res->data;
		echo json_encode($res);
	break;

	case 'saveDailyGospel':
		$gospel = secure_post('gospel');
		$gos_id = secure_post('gos_id');
		$date = date('Y-m-d H:i:s');

		$newdate = $date." ".$time; 

		if(empty($gos_id)){
			$sql = "insert into `daily_gospel` set 
				`gospel` = '".$gospel."',
				`created_date` = '".$date."',
				`by` = '1'
			";			
		}else{
			$sql = "update `daily_gospel` set 
				`gospel` = '".$gospel."' where
				`gos_id` = '".$gos_id."'
			";			
		}


		$res = $conn->dbquery($sql);
		// echo $sql;
		echo 'success';
	break;

	case 'deleteDailyGospel':
		$gos_id = secure_post('gos_id');

		$newdate = $date." ".$time; 

		$sql = "delete from `daily_gospel` where
			`gos_id` = '".$gos_id."'
		";

		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'getDetailGospel':
		$gos_id = secure_post('gos_id');
		$res = $conn->dbquery("SELECT * FROM `daily_gospel` WHERE `gos_id` = '".$gos_id."' " );
		$res = json_decode($res);
		$res = $res->data;
		echo json_encode($res);
	break;


	case 'deleteSchedule':
		$res_id = secure_post('res_id');

		$newdate = $date." ".$time; 

		$sql = "delete from `reservation` where
			`res_id` = '".$res_id."'
		";

		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'removeSchedule':
		$sched_id = secure_post('sid');

		$newdate = $date." ".$time; 

		$sql = "delete from `schedule` where
			`sched_id` = '".$sched_id."'
		";

		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'saveSchedule':
		//`sched_id`, `user_id`, `title`, `desc`, `start_date`, `end_date`, `created_date`, `by`

		$date = date('Y-m-d H:i:s');

		
		$uid =secure_post('user_id');
		$ulevel =secure_post('ul');
		$ddate = secure_post('ddate');
		$dtime = secure_post('dtime');
		$newdate = $ddate." ".$dtime; 
		$hid = secure_post('hid_id');
		$title = secure_post('titl');

		$err = 0;

		//`title` = '".$title."' and
		$sqlc = "select * from `schedule` where  `start_date`='".$newdate."' and `user_id` = '".$uid."'";
	    $res1 = $conn->dbquery($sqlc);
	    $res1 = json_decode($res1);
	    $res1 = $res1->data;
	    $errmsg = '';

	    if(count($res1) > 0){
	    	$err++;
	    	$errmsg .= 'Please select different  Date and Time!';
	    }

	    if($ulevel != '3' and count($res1) > 4){
	    	$err++;
	    	$errmsg .= translateUlevel($ulevel).' is already full for this schedule!';
	    }

	    if($ulevel == '3' and count($res1) > 2){
	    	$err++;
	    	$errmsg .= translateUlevel($ulevel).' is already full for this schedule!';
	    }

		if(empty($hid)){

			if($err == 0){
				$sql = "insert into `schedule` set 
					`user_id` = '".$uid."', 
					`ulevel` = '".$ulevel."', 
					`start_date` = '".$newdate."', 
					`end_date` = '".$newdate."', 
					`title` = '".$title."', 
					`created_date` = '".$date."'
				";					
				$res = $conn->dbquery($sql);
				echo 'success';
			}else{
				echo $errmsg;
			}		
		}else{
			$sql = "update `schedule` set 
				`user_id` = '".$uid."',  
				`ulevel` = '".$ulevel."', 
				`start_date` = '".$newdate."', 
				`end_date` = '".$newdate."',
				`title` = '".$title."'
				where 
				`sched_id` = '".$hid."'
			";						
			$res = $conn->dbquery($sql);
			echo 'success';
		}

		// echo $sql;
	break;

	case 'removeCollection':
		$cid = secure_post('cid_');

		$newdate = $date." ".$time; 

		$sql = "delete from `collections` where
			`c_id` = '".$c_id."'
		";

		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'addCollection':
		$date = date('Y-m-d H:i:s');

		$newdate = $date." ".$time; 
		$uid =secure_post('user_id');
		$amount = secure_post('amount_');

		$hid = secure_post('hid_id');

		if(empty($hid)){
			$sql = "insert into `collections` set 
				`user_id` = '".$uid."',
				`amount` = '".$amount."',
				`date_added` = '".$date."'
			";						
		}else{
			$sql = "update `collections` set 
				`user_id` = '".$uid."',
				`amount` = '".$amount."'
				where 
				`c_id` = '".$hid."'
			";						
		}



		// echo $sql;
		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'saveUser':
		// $uid = secure_post('uid');
		$name = secure_post('nn');
		$email = secure_post('ne');
		// $uname = secure_post('nu');
		$password = secure_post('np');
		$ulevel = secure_post('nps');
		$uid = secure_post('ni');
		$uname = secure_post('un');
		/*
		`user_id`, `email`, `password`, `name`, `gender`, `profile_url`, `username`, `birthday`, `location`, `rel_status`, `image_url`, `fb_uid`, `ulevel`, `referral_code`, `num_users`, `date_added`, `active`, `options`, `parent`
		*/
		$res_check = $conn->dbquery("SELECT * FROM `users` WHERE `email` = '".$email."' or `username` = '".$uname."' " );
		$res_check = json_decode($res_check);
		$res_check = $res_check->data;


		// if(empty($res_id)){
		if(empty($uid)){
			if(count($res_check) > 0){
				$result = 'Username/Email already exists!';
			}else{
				$sql = "insert into `users` set 
					`name` = '".$name."', 
					`username` = '".$uname."', 
					`ulevel` = '".$ulevel."',
					`email` = '".$email."',
					`password` = '".md5($password)."',
					`active` = '1',
					`date_added` = '".date('Y-m-d H:i:s')."'
				";	
				$conn->dbquery($sql);
				$result = 'User successfully added!';
			}
		
		}else{
			if(!empty($password)){
				$sql = "update `users` set 
					`name` = '".$name."', 
					`username` = '".$uname."', 
					`ulevel` = '".$ulevel."',
					`email` = '".$email."',
					`password` = '".md5($password)."'
					where `user_id` = '".$uid."'
				";				
			}else{
				$sql = "update `users` set 
					`name` = '".$name."', 
					`username` = '".$uname."', 
					`ulevel` = '".$ulevel."',
					`email` = '".$email."'
					where `user_id` = '".$uid."'
				";				
			}

			// echo $sql;
			$conn->dbquery($sql);
			$result = 'Data successfully updated!';
		}

		echo $result;
	break;
	case 'deleteUser':
		$user_id = secure_post('user_id');
		$sql = "delete from `users` where
			`user_id` = '".$user_id."'
		";
		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'getUser':
		$user_id = secure_post('user_id');
		$res = $conn->dbquery("SELECT * FROM `users` WHERE `user_id` = '".$user_id."' " );
		$res = json_decode($res);
		$res = $res->data;
		echo json_encode($res);
	break;

	case 'donate':
	    $name = secure_post('name');
	    $email = secure_post('email');
	    $phone = secure_post('phone');
	    $amount = secure_post('amount');
	    $accnt = secure_post('accnt');

        $sql = "insert into `donation` set 
            `name` = '".$name."', 
            `email` = '".$email."', 
            `phone` = '".$phone."', 
            `amount` = '".$amount."', 
            `status` = 'new', 
            `account_num` = '".$accnt."', 
            `date_added`  = '".date('Y-m-d H:i:s')."',
            `confirmed_date` = '".date('Y-m-d H:i:s')."'
        ";  
        // echo $sql;
        $conn->dbquery($sql);
        echo 'success';
	break;

	case 'contact':
	    $name = secure_post('name');
	    $email = secure_post('email');
	    $subject = secure_post('subj');
	    $message = secure_post('msg');
//`name`, `email`, `subject`, `message`, `date_added`
        $sql = "insert into `messages` set 
            `name` = '".$name."', 
            `email` = '".$email."', 
			`subject`= '".$subject."', 
			`message` = '".$message."',
			`status` = '1',
            `date_added`  = '".date('Y-m-d H:i:s')."'
        ";  
        // echo $sql;
        $conn->dbquery($sql);

		$res_check = $conn->dbquery("SELECT * FROM `settings` WHERE `meta` = 'from_email' " );
		$res_check = json_decode($res_check);
		$res_check = $res_check->data;
		if(count($res_check) > 0){
			foreach($res_check as $r){
				$ar = json_decode($r);
			}  	
			$to = $ar->value;
			$to_name = $to;
		}else{
			$to = 'webmaster@lupitchurch.com';
			$to_name = $to;
		}

		$from = $email;
		$from_name = $from;

		$htmlmsg = $message;

        mySimpleMailer($to, $to_name, $from, $from_name, $subject, $htmlmsg);
        echo 'success';
	break;

	case 'sendMail':
	    $email = secure_post('email');
	    $subject = secure_post('subj');
	    $message = secure_post('msg');

		$res_check = $conn->dbquery("SELECT * FROM `settings` WHERE `meta` = 'from_email' " );
		$res_check = json_decode($res_check);
		$res_check = $res_check->data;
		if(count($res_check) > 0){
			foreach($res_check as $r){
				$ar = json_decode($r);
			}  	
			$from = $ar->value;
			$from_name = $from;
		}else{
			$from = 'webmaster@lupitchurch.com';
			$from_name = $from;
		}

		$to = $email;
		$to_name = $to;

		$htmlmsg = $message;

		mySimpleMailer($to, $to_name, $from, $from_name, $subject, $htmlmsg);
		echo 'success';
	break;

	case 'saveSettings':
	    $meta = secure_post('meta');
	    $value = secure_post('value');
	    $extra = secure_post('extra');
	    if(empty($extra)){
	    	$extra = '-';
	    }


		$res_check = $conn->dbquery("SELECT * FROM `settings` WHERE `meta` = '".$meta."' " );
		$res_check = json_decode($res_check);
		$res_check = $res_check->data;
		if(count($res_check) > 0){
	        $sql = "update `settings` set  
	            `value` = '".$value."', 
				`extra`= '".$extra."'
				where
				`meta` = '".$meta."'
	        ";  
		}else{
	        $sql = "insert into `settings` set 
	            `meta` = '".$meta."', 
	            `value` = '".$value."', 
				`extra`= '".$extra."'
	        ";  			
		}

		// echo $sql;
        $conn->dbquery($sql);
        echo 'success';
	break;

	case 'deleteMsg':
		$mid = secure_post('mid');
		$sql = "delete from `messages` where `m_id` = '".$mid."'";
		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'deleteDonate':
		$mid = secure_post('did');
		$sql = "delete from `donation` where `don_id` = '".$mid."'";
		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'validDonate':
		$mid = secure_post('did');
		$sql = "update `donation` set `status` = 'valid' where `don_id` = '".$mid."'";
		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'invalidDonate':
		$mid = secure_post('did');
		$sql = "update `donation` set `status` = 'invalid' where `don_id` = '".$mid."'";
		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'getMsg':
		$mid = secure_post('mid');

		$res = $conn->dbquery("SELECT * FROM `messages` WHERE `m_id` = '".$mid."' " );
		$res = json_decode($res);
		$res = $res->data;
		echo json_encode($res);
	break;


	case 'saveAnnounce':
		$gospel = secure_post('announce');
		$gos_id = secure_post('a_id');
		$date = date('Y-m-d H:i:s');

		$newdate = $date." ".$time; 

		if(empty($gos_id)){
			$sql = "insert into `announcements` set 
				`announcements` = '".$gospel."',
				`created_date` = '".$date."',
				`by` = '1'
			";			
		}else{
			$sql = "update `daily_gospel` set 
				`gospel` = '".$gospel."' where
				`gos_id` = '".$gos_id."'
			";			
		}


		$res = $conn->dbquery($sql);
		// echo $sql;
		echo 'success';
	break;

	case 'deleteAnnounce':
		$gos_id = secure_post('a_id');

		$newdate = $date." ".$time; 

		$sql = "delete from `announcements` where
			`a_id` = '".$gos_id."'
		";

		$res = $conn->dbquery($sql);
		echo 'success';
	break;

	case 'getPriestVenue':
		$priestid = secure_post('pid');
		$event = secure_post('ev');
		$sql = "select * from `reservation` 
			where 
			`assigned_to` = '".$priestid."' and 
			`event` = '".$event."'
			";
			//			`status` = 'approved' and 
		$res = $conn->dbquery($sql);
		echo $res;
		// $res = json_decode($res);
		// $res = $res->data;
		// echo json_encode($res);
	break;

	case 'getPriestDateSched':
		$priestid = secure_post('pid');
		$event = secure_post('ev');
		$venue = secure_post('ve');
		$sql = "select * from `reservation` 
			where 
			`assigned_to` = '".$priestid."' and 
			`event` = '".$event."' and
			`venue` = '".$venue."'
			";
			//			`status` = 'approved' and 
		$res = $conn->dbquery($sql);
		echo $res;
	break;


	case 'saveSchedule2':
		//`sched_id`, `user_id`, `title`, `desc`, `start_date`, `end_date`, `created_date`, `by`

		$date = date('Y-m-d H:i:s');

		
		$uid =secure_post('user_id');
		$ulevel =secure_post('ul');
		$ddate = secure_post('ddtm');
		
		$newdate = $ddate; 
		$hid = secure_post('hid_id');
		$title = secure_post('ev');
		$event = $title;
		$venue = secure_post('ve');
		$priest_id = secure_post('pi');

		$err = 0;

		//`title` = '".$title."' and
		// $sqlc = "select * from `schedule` where 
		//  `title` = '".$event."' and 
		//  `desc` = '".$venue."' and 
		//  `start_date`='".$newdate."' and 
		//  `user_id` = '".$uid."'";

		// $sqlc = "select * from `schedule` where 
		//  `title` = '".$event."' and 
		//  `start_date`='".$newdate."' and 
		//  `user_id` = '".$uid."'";

		$sqlc = "select * from `schedule` where 
		 `start_date`='".$newdate."' and 
		 `user_id` = '".$uid."'";

	    $res1 = $conn->dbquery($sqlc);
	    $res1 = json_decode($res1);
	    $res1 = $res1->data;
	    $errmsg = '';


		$sqlc1 = "select * from `schedule` where 
		 `title` = '".$event."' and 
		 `desc` = '".$venue."' and 
		 `start_date`='".$newdate."'";

	    $res2 = $conn->dbquery($sqlc1);
	    $res2 = json_decode($res2);
	    $res2 = $res2->data;

	    if(count($res1) > 0){
	    	$err++;
	    	$errmsg .= translateUlevel($ulevel).' has already a schedule!';
	    }

	    if($ulevel != '3' and count($res2) > 4){
	    	$err++;
	    	$errmsg .= translateUlevel($ulevel).' is already full for this schedule!';
	    }

	    if($ulevel == '3' and count($res2) > 2){
	    	$err++;
	    	$errmsg .= translateUlevel($ulevel).' is already full for this schedule!';
	    }

		if(empty($hid)){

			if($err == 0){
				$sql = "insert into `schedule` set 
					`user_id` = '".$uid."', 
					`ulevel` = '".$ulevel."', 
					`start_date` = '".$newdate."', 
					`end_date` = '".$newdate."', 
					`title` = '".$title."', 
					`desc` = '".$venue."', 
					`created_date` = '".$date."'
				";					
				$res = $conn->dbquery($sql);
				echo 'success';
			}else{
				echo $errmsg;
			}		
		}else{
			$sql = "update `schedule` set 
				`user_id` = '".$uid."',  
				`ulevel` = '".$ulevel."', 
				`start_date` = '".$newdate."', 
				`end_date` = '".$newdate."',
				`title` = '".$title."',
				`desc` = '".$venue."',
				where 
				`sched_id` = '".$hid."'
			";						
			$res = $conn->dbquery($sql);
			echo 'success';
		}

		// echo $sql;
	break;

	default:
		# code...

	break;
}
?>