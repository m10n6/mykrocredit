Form on the Process Voucher (For Printer also)
========================================
fields: (less to principal amount)
- Processing Fee
- Finder's Fee
- Notarial
- Miscellaneous
- Insurance
- Others

fields:
- Bonus (add to principal amount)



======================================
Vouchers Table Columns
======================================
- Issuance Date
- Voucher No
- Client
- Amount
- Processing Fee
- Finders Fee
- Notarial
- Miscellaneous
- Insurance
- Others
- Adjustment
- Net Proceeds
- Action (Edit / Approved)


===========================================
Customer/Client [View Data] Table Columns
===========================================
- Date
- particulars (OR # and Voucher #) [prefix type of loan] - CV # XXXX
- Interest Rate (4.0% D or FR)
- OR Amount (Payment)
- Interest
- Debit
- Credit
- Balance
- Action (remove)



