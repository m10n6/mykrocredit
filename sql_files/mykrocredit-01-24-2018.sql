-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 24, 2018 at 03:09 AM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 5.6.33-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mykrocredit`
--

-- --------------------------------------------------------

--
-- Table structure for table `chart_accounts`
--

CREATE TABLE `chart_accounts` (
  `caid` bigint(20) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `category` varchar(255) NOT NULL,
  `is_display` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chart_accounts`
--

INSERT INTO `chart_accounts` (`caid`, `code`, `name`, `category`, `is_display`, `date_added`) VALUES
(1, '100-001', 'Cash in Bank (BDO Hilado)', 'Cash', '1', '2017-07-31 02:28:08'),
(2, '101-001', 'A/R Trade', 'Loans and Other Receivables', '1', '2017-07-31 02:32:19'),
(4, '100-003', 'Cash in Bank (China Bank)', 'Cash', '1', '2017-07-31 02:28:00'),
(5, '101-001-01', 'A/R Loans', 'Loans and Other Receivables', '1', '2017-07-31 02:33:01'),
(6, '101-001-02', 'A/R Cash Advance', 'Loans and Other Receivables', '1', '2017-07-31 02:33:38'),
(7, '101-001-03', 'A/R Employee', 'Loans and Other Receivables', '1', '2017-07-31 02:34:03'),
(8, '101-001-04', 'A/R Blacklist', 'Loans and Other Receivables', '1', '2017-07-31 02:36:21'),
(9, '101-001-05', 'A/R Special Accounts', 'Loans and Other Receivables', '1', '2017-07-31 02:36:51'),
(10, '101-001-06', 'A/R Excess', 'Loans and Other Receivables', '1', '2017-07-31 02:37:14'),
(11, '400-003', 'Service Fee', 'Revenue', '1', '2017-07-31 02:38:04'),
(12, '400-003-01', 'Processing Fee', 'Revenue', '1', '2017-07-31 02:38:37'),
(13, '400-003-02', 'Notarial Fee', 'Revenue', '1', '2017-07-31 02:39:06'),
(14, '400-003-03', 'Miscellaneous', 'Revenue', '1', '2017-07-31 02:39:30'),
(15, '400-003-04', 'Finders Fee', 'Revenue', '1', '2017-07-31 02:39:51'),
(16, '400-005', 'Rebates/Discounts', 'Revenue', '1', '2017-07-31 02:40:22'),
(17, '100-002', 'Cash in Bank (BDO East)', 'Cash', '1', '2017-07-31 03:10:43'),
(18, '102-000', 'Due From ...', 'Loans and Other Receivables', '1', '2017-07-31 15:37:52'),
(19, '400-002', 'Interest Income', 'Revenue', '1', '2018-01-22 15:28:45');

-- --------------------------------------------------------

--
-- Table structure for table `client_begloanbal`
--

CREATE TABLE `client_begloanbal` (
  `cbb_id` bigint(20) NOT NULL,
  `cid` varchar(255) DEFAULT NULL,
  `balance` varchar(255) DEFAULT NULL,
  `irate` varchar(255) DEFAULT NULL,
  `irateType` varchar(255) DEFAULT NULL,
  `terms` varchar(255) DEFAULT NULL,
  `entryDate` datetime DEFAULT NULL,
  `entryStatus` varchar(255) DEFAULT NULL,
  `acctType` varchar(255) DEFAULT NULL,
  `paidStat` varchar(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_data`
--

CREATE TABLE `client_data` (
  `client_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address` longtext NOT NULL,
  `birthdate` date NOT NULL,
  `civil_status` varchar(255) NOT NULL,
  `contact_num` varchar(255) NOT NULL,
  `sss_num` varchar(255) NOT NULL,
  `company` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_data`
--

INSERT INTO `client_data` (`client_id`, `name`, `middle_name`, `last_name`, `address`, `birthdate`, `civil_status`, `contact_num`, `sss_num`, `company`) VALUES
(1, 'Fernandez', 'X', 'Realyn', 'Brgy. Dos Hermanas', '1986-01-05', 'married', '', '', 'Marc Job Services Inc.'),
(2, 'Mark', 'B', 'Ramos', '', '1983-12-21', 'married', '', '', 'Marc Job Services Inc.'),
(3, 'Tarah', 'L', 'Ramos', '', '1983-07-27', 'married', '', '', ''),
(5, 'John', 'S', 'Doe', '', '2018-01-17', 'single', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `client_meta`
--

CREATE TABLE `client_meta` (
  `c_metaid` bigint(20) NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `meta` text NOT NULL,
  `value` longtext NOT NULL,
  `extra` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_meta`
--

INSERT INTO `client_meta` (`c_metaid`, `client_id`, `meta`, `value`, `extra`) VALUES
(1, 1, 'txtCompany', 'Marc Job Services Inc.', ''),
(2, 1, 'txtCompanyAddress', 'Talisay City', ''),
(3, 1, 'txtS_name', '', ''),
(4, 1, 'txtS_contact', '', ''),
(5, 1, 'txtG_name', '', ''),
(6, 1, 'txtG_address', '', ''),
(7, 1, 'txtG_contactnum', '', ''),
(8, 1, 'txtG_SSS', '', ''),
(9, 1, 'txtCard1', '', ''),
(10, 1, 'txtPin1', '', ''),
(11, 1, 'txtCard2', '', ''),
(12, 1, 'txtPin2', '', ''),
(13, 1, 'txtCard3', '', ''),
(14, 1, 'txtPin3', '', ''),
(15, 1, 'txtCreditLimit', '20000', ''),
(16, 1, 'txtP_name', '', ''),
(17, 1, 'txtP_address', '', ''),
(18, 1, 'txtClientType', 'regular', ''),
(19, 1, 'txtPaymentSched', '2', ''),
(20, 1, 'txtAdditionalBalance', '', ''),
(21, 1, 'txtInterestSchedStart', '2018-01-30', ''),
(22, 1, 'txtActualPaymentAmount', '1000', ''),
(23, 1, 'txtBeginningBal', '14500', ''),
(24, 2, 'txtCompany', 'Marc Job Services Inc.', ''),
(25, 2, 'txtCompanyAddress', '', ''),
(26, 2, 'txtS_name', '', ''),
(27, 2, 'txtS_contact', '', ''),
(28, 2, 'txtG_name', '', ''),
(29, 2, 'txtG_address', '', ''),
(30, 2, 'txtG_contactnum', '', ''),
(31, 2, 'txtG_SSS', '', ''),
(32, 2, 'txtCard1', '', ''),
(33, 2, 'txtPin1', '', ''),
(34, 2, 'txtCard2', '', ''),
(35, 2, 'txtPin2', '', ''),
(36, 2, 'txtCard3', '', ''),
(37, 2, 'txtPin3', '', ''),
(38, 2, 'txtCreditLimit', '10000', ''),
(39, 2, 'txtP_name', '', ''),
(40, 2, 'txtP_address', '', ''),
(41, 2, 'txtClientType', 'regular', ''),
(42, 2, 'txtPaymentSched', '1', ''),
(43, 2, 'txtAdditionalBalance', '', ''),
(44, 2, 'txtInterestSchedStart', '2018-02-15', ''),
(45, 2, 'txtActualPaymentAmount', '', ''),
(46, 2, 'txtBeginningBal', '', ''),
(47, 3, 'txtCompany', '', ''),
(48, 3, 'txtCompanyAddress', '', ''),
(49, 3, 'txtS_name', '', ''),
(50, 3, 'txtS_contact', '', ''),
(51, 3, 'txtG_name', '', ''),
(52, 3, 'txtG_address', '', ''),
(53, 3, 'txtG_contactnum', '', ''),
(54, 3, 'txtG_SSS', '', ''),
(55, 3, 'txtCard1', '', ''),
(56, 3, 'txtPin1', '', ''),
(57, 3, 'txtCard2', '', ''),
(58, 3, 'txtPin2', '', ''),
(59, 3, 'txtCard3', '', ''),
(60, 3, 'txtPin3', '', ''),
(61, 3, 'txtCreditLimit', '20000', ''),
(62, 3, 'txtP_name', '', ''),
(63, 3, 'txtP_address', '', ''),
(64, 3, 'txtClientType', 'regular', ''),
(65, 3, 'txtPaymentSched', '2', ''),
(66, 3, 'txtAdditionalBalance', '', ''),
(67, 3, 'txtInterestSchedStart', '2018-01-30', ''),
(68, 3, 'txtActualPaymentAmount', '2000', ''),
(69, 3, 'txtBeginningBal', '14500', ''),
(93, 5, 'txtCompany', '', ''),
(94, 5, 'txtCompanyAddress', '', ''),
(95, 5, 'txtS_name', '', ''),
(96, 5, 'txtS_contact', '', ''),
(97, 5, 'txtG_name', '', ''),
(98, 5, 'txtG_address', '', ''),
(99, 5, 'txtG_contactnum', '', ''),
(100, 5, 'txtG_SSS', '', ''),
(101, 5, 'txtCard1', '', ''),
(102, 5, 'txtPin1', '', ''),
(103, 5, 'txtCard2', '', ''),
(104, 5, 'txtPin2', '', ''),
(105, 5, 'txtCard3', '', ''),
(106, 5, 'txtPin3', '', ''),
(107, 5, 'txtCreditLimit', '10000', ''),
(108, 5, 'txtP_name', '', ''),
(109, 5, 'txtP_address', '', ''),
(110, 5, 'txtClientType', 'regular', ''),
(111, 5, 'txtPaymentSched', '1', ''),
(112, 5, 'txtAdditionalBalance', '', ''),
(113, 5, 'txtInterestSchedStart', '2018-02-15', ''),
(114, 5, 'txtActualPaymentAmount', '1000', ''),
(115, 5, 'txtBeginningBal', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `finance`
--

CREATE TABLE `finance` (
  `fid` bigint(20) NOT NULL,
  `client_id` text NOT NULL,
  `intrate` text NOT NULL,
  `debit` text NOT NULL,
  `credit` text NOT NULL,
  `balance` text NOT NULL,
  `extra` text NOT NULL,
  `status` text NOT NULL,
  `loan_type` text NOT NULL,
  `voucher_id` text NOT NULL,
  `term` text NOT NULL,
  `amount` text NOT NULL,
  `date_added` datetime NOT NULL,
  `loan_date` datetime NOT NULL,
  `processedby` text NOT NULL,
  `cancelledby` text NOT NULL,
  `addedby` text NOT NULL,
  `date_approved` datetime NOT NULL,
  `inProcFee` varchar(255) DEFAULT NULL,
  `inpFindersFee` varchar(255) DEFAULT NULL,
  `inpNotarial` varchar(255) DEFAULT NULL,
  `inpMisc` varchar(255) DEFAULT NULL,
  `inpInsurance` varchar(255) DEFAULT NULL,
  `inpOthers` varchar(255) DEFAULT NULL,
  `inpAdjustment` varchar(255) DEFAULT NULL,
  `inpBonus` varchar(255) DEFAULT NULL,
  `approvedby` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `finance`
--

INSERT INTO `finance` (`fid`, `client_id`, `intrate`, `debit`, `credit`, `balance`, `extra`, `status`, `loan_type`, `voucher_id`, `term`, `amount`, `date_added`, `loan_date`, `processedby`, `cancelledby`, `addedby`, `date_approved`, `inProcFee`, `inpFindersFee`, `inpNotarial`, `inpMisc`, `inpInsurance`, `inpOthers`, `inpAdjustment`, `inpBonus`, `approvedby`) VALUES
(1, '1', '1', '', '0', '14,500.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00002', '', '1000.00', '2018-01-05 16:09:25', '2018-01-05 16:09:25', '3393', '', '3393', '2018-01-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '1', '1', '', '0', '13,500.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00003', '', '1000.00', '2018-01-05 16:10:10', '2018-01-05 16:10:10', '3393', '', '3393', '2018-01-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '1', '4', '0', '10000', '10000', '{ "guarantor": "", "TypeRate" : "diminishing" }', 'pending', 'additional_loan', '', '12', '10000', '2018-01-05 16:52:25', '2018-01-05 00:00:00', '', '', '3393', '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '2', '4', '0', '10000', '0', '{ "guarantor": "Test", "TypeRate" : "diminishing" }', 'approved', 'new_loans', 'MYK00009', '12', '10000', '2018-01-09 16:02:53', '2018-01-09 00:00:00', '', '', '3393', '2018-01-09 16:03:10', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '3393'),
(6, '2', '4', '', '0', '10,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00004', '', '2000.00', '2018-01-09 17:29:36', '2018-01-09 17:29:36', '3393', '', '3393', '2018-01-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '2', '4', '', '0', '8,000.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00005', '', '2000.00', '2018-01-09 17:30:06', '2018-01-09 17:30:06', '3393', '', '3393', '2018-02-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '2', '4', '', '0', '6,320.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00006', '', '2000.00', '2018-01-09 17:30:45', '2018-01-09 17:30:45', '3393', '', '3393', '2018-02-28 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '2', '4', '0', '2000', '8000', '{ "guarantor": "", "TypeRate" : "diminishing" }', 'approved', 'cash_advance', 'MYK00010', '1', '2000', '2018-01-09 17:31:23', '2018-03-05 00:00:00', '', '', '3393', '2018-03-05 17:31:41', '0.00', '0.00', '0.00', '60.00', '0.00', '0.00', '0.00', '0.00', '3393'),
(10, '2', '4', '', '0', '6,320.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00007', '', '2000.00', '2018-01-09 17:34:12', '2018-01-09 17:34:12', '3393', '', '3393', '2018-03-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '2', '4', '', '0', '4,656.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00008', '', '2000.00', '2018-01-16 22:34:26', '2018-01-16 22:34:26', '3393', '', '3393', '2018-03-30 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '2', '4', '', '0', '2,656.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00009', '', '1000.00', '2018-01-16 22:37:03', '2018-01-16 22:37:03', '3393', '', '3393', '2018-04-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '2', '4', '0', '5000', '5000', '{ "guarantor": "", "TypeRate" : "diminishing" }', 'approved', 'cash_advance', 'MYK00011', '1', '5000', '2018-01-16 22:38:28', '2018-04-18 00:00:00', '', '', '3393', '2018-04-18 22:38:45', '0.00', '0.00', '0.00', '250.00', '0.00', '0.00', '0.00', '0.00', '3393'),
(14, '2', '4', '', '0', '6,762.24', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00010', '', '2000.00', '2018-01-16 22:40:51', '2018-01-16 22:40:51', '3393', '', '3393', '2018-04-30 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '2', '4', '', '0', '4,762.24', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00011', '', '2000.00', '2018-01-16 22:41:34', '2018-01-16 22:41:34', '3393', '', '3393', '2018-05-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '3', '0', '0', '14500', '14500', '{ "guarantor": "", "TypeRate" : "" }', 'approved', 'beg_bal', '', '', '14500', '2018-01-17 01:20:10', '2018-01-17 01:20:10', '', '', '3393', '2018-01-17 01:20:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '3', '4', '', '0', '14,500.00', '{ "guarantor": "", "TypeRate" : "diminishing", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00012', '', '1000.00', '2018-01-17 01:31:08', '2018-01-17 01:31:08', '3393', '', '3393', '2018-01-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '5', '4', '0', '10000', '0', '{ "guarantor": "Mark", "TypeRate" : "flat" }', 'approved', 'new_loans', 'MYK00013', '12', '10000', '2018-01-17 02:37:23', '2018-01-17 00:00:00', '', '', '3393', '2018-01-17 02:37:33', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '3393'),
(21, '5', '4', '', '0', '10,000.00', '{ "guarantor": "", "TypeRate" : "flat", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00013', '', '1000.00', '2018-01-17 02:43:32', '2018-01-17 02:43:32', '3393', '', '3393', '2018-01-17 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '5', '4', '', '0', '9,000.00', '{ "guarantor": "", "TypeRate" : "flat", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00014', '', '1000.00', '2018-01-17 02:43:59', '2018-01-17 02:43:59', '3393', '', '3393', '2018-01-31 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '5', '4', '', '0', '8,000.00', '{ "guarantor": "", "TypeRate" : "flat", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00015', '', '1000.00', '2018-01-17 02:44:32', '2018-01-17 02:44:32', '3393', '', '3393', '2018-02-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '5', '4', '', '0', '7,320.00', '{ "guarantor": "", "TypeRate" : "flat", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00016', '', '1000.00', '2018-01-17 02:56:38', '2018-01-17 02:56:38', '3393', '', '3393', '2018-02-28 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '5', '4', '', '0', '6,320.00', '{ "guarantor": "", "TypeRate" : "flat", "ApplyInt" : "off" }', 'approved', 'payment_ors', 'MYK-ORS00017', '', '1000.00', '2018-01-17 02:57:08', '2018-01-17 02:57:08', '3393', '', '3393', '2018-03-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `journal_voucher`
--

CREATE TABLE `journal_voucher` (
  `jvid` bigint(20) NOT NULL,
  `jvnid` text,
  `client_id` text,
  `chart_account_id` text,
  `debit` float DEFAULT NULL,
  `credit` float DEFAULT NULL,
  `date_trans` datetime DEFAULT NULL,
  `addby` text,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `journal_voucher`
--

INSERT INTO `journal_voucher` (`jvid`, `jvnid`, `client_id`, `chart_account_id`, `debit`, `credit`, `date_trans`, `addby`, `date_added`) VALUES
(2, '2', '1', '1', 1000, 0, '2018-01-23 00:00:00', '3393', '2018-01-23 00:08:26'),
(3, '2', '1', '2', 0, 1000, '2018-01-23 00:00:00', '3393', '2018-01-23 00:08:26'),
(4, '3', '2', '2', 0, 2000, '2018-01-23 00:00:00', '3393', '2018-01-23 00:10:27'),
(5, '3', '2', '1', 2000, 0, '2018-01-23 00:00:00', '3393', '2018-01-23 00:10:27');

-- --------------------------------------------------------

--
-- Table structure for table `jv_notes`
--

CREATE TABLE `jv_notes` (
  `jvntid` bigint(20) NOT NULL,
  `jvnid` text,
  `notes` text CHARACTER SET utf8,
  `dateadded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jv_notes`
--

INSERT INTO `jv_notes` (`jvntid`, `jvnid`, `notes`, `dateadded`) VALUES
(2, '2', '', '2018-01-23 00:08:26'),
(3, '3', 'Over Payment', '2018-01-23 00:10:27');

-- --------------------------------------------------------

--
-- Table structure for table `jv_numbers`
--

CREATE TABLE `jv_numbers` (
  `jvnid` bigint(20) NOT NULL,
  `prefix` text,
  `number` text,
  `dateadded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jv_numbers`
--

INSERT INTO `jv_numbers` (`jvnid`, `prefix`, `number`, `dateadded`) VALUES
(2, '', '', '2018-01-23 00:08:26'),
(3, '', '', '2018-01-23 00:10:27');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `log_id` bigint(20) NOT NULL,
  `transaction` longtext NOT NULL,
  `addedby` bigint(20) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ors_numbers`
--

CREATE TABLE `ors_numbers` (
  `ors_id` bigint(20) NOT NULL,
  `fid` text,
  `ors_num` text NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `dateadded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ors_numbers`
--

INSERT INTO `ors_numbers` (`ors_id`, `fid`, `ors_num`, `prefix`, `code`, `dateadded`) VALUES
(1, NULL, 'MYK-ORS00001', 'MYK-ORS', '00001', '2018-01-05 15:32:09'),
(2, NULL, 'MYK-ORS00002', 'MYK-ORS', '00002', '2018-01-05 16:09:25'),
(3, NULL, 'MYK-ORS00003', 'MYK-ORS', '00003', '2018-01-05 16:10:10'),
(4, NULL, 'MYK-ORS00004', 'MYK-ORS', '00004', '2018-01-09 17:29:36'),
(5, NULL, 'MYK-ORS00005', 'MYK-ORS', '00005', '2018-01-09 17:30:06'),
(6, NULL, 'MYK-ORS00006', 'MYK-ORS', '00006', '2018-01-09 17:30:45'),
(7, NULL, 'MYK-ORS00007', 'MYK-ORS', '00007', '2018-01-09 17:34:12'),
(8, NULL, 'MYK-ORS00008', 'MYK-ORS', '00008', '2018-01-16 22:34:26'),
(9, NULL, 'MYK-ORS00009', 'MYK-ORS', '00009', '2018-01-16 22:37:03'),
(10, NULL, 'MYK-ORS00010', 'MYK-ORS', '00010', '2018-01-16 22:40:51'),
(11, NULL, 'MYK-ORS00011', 'MYK-ORS', '00011', '2018-01-16 22:41:34'),
(12, NULL, 'MYK-ORS00012', 'MYK-ORS', '00012', '2018-01-17 01:31:08'),
(13, NULL, 'MYK-ORS00013', 'MYK-ORS', '00013', '2018-01-17 02:43:32'),
(14, NULL, 'MYK-ORS00014', 'MYK-ORS', '00014', '2018-01-17 02:43:59'),
(15, NULL, 'MYK-ORS00015', 'MYK-ORS', '00015', '2018-01-17 02:44:32'),
(16, NULL, 'MYK-ORS00016', 'MYK-ORS', '00016', '2018-01-17 02:56:39'),
(17, NULL, 'MYK-ORS00017', 'MYK-ORS', '00017', '2018-01-17 02:57:09');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `pid` bigint(20) NOT NULL,
  `or_num` varchar(255) DEFAULT NULL,
  `ors_num` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `irate` varchar(255) DEFAULT NULL,
  `iratetype` varchar(255) DEFAULT NULL,
  `cid` varchar(255) DEFAULT NULL,
  `paymentType` varchar(255) DEFAULT NULL,
  `apply_int` varchar(255) DEFAULT NULL,
  `payDate` datetime NOT NULL,
  `entryDate` datetime NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `processed_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `cancelled_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `s_id` int(11) NOT NULL,
  `meta` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `extra` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`s_id`, `meta`, `value`, `extra`) VALUES
(1, 'from_email', 'info@markramosonline.com', '-'),
(2, 'vprefix', 'MYK', ''),
(3, 'vstart', '5', ''),
(4, 'orsprefix', 'MYK-ORS', ''),
(5, 'orsdigit', '5', '');

-- --------------------------------------------------------

--
-- Table structure for table `ulevels`
--

CREATE TABLE `ulevels` (
  `level_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `pages` text NOT NULL,
  `limit` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ulevels`
--

INSERT INTO `ulevels` (`level_id`, `name`, `desc`, `pages`, `limit`) VALUES
(1, 'Administrator', 'Administrator Account', 'all', ''),
(2, 'Loans', 'User Level', 'dashboard,statistics,subscribers,help', ''),
(3, 'Clerk', '', '', ''),
(4, 'Collector', 'Collector', '', '10'),
(5, 'CI', 'Credit Investigator', '', ''),
(6, 'Auditor', 'Auditor', '', ''),
(7, 'Cashier', 'Cashier', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `email` text CHARACTER SET utf8 NOT NULL,
  `password` text CHARACTER SET utf8 NOT NULL,
  `name` text CHARACTER SET utf8 NOT NULL,
  `gender` text CHARACTER SET utf8,
  `profile_url` text CHARACTER SET utf8,
  `username` text CHARACTER SET utf8 NOT NULL,
  `birthday` text CHARACTER SET utf8,
  `location` text CHARACTER SET utf8,
  `rel_status` text CHARACTER SET utf8,
  `image_url` text CHARACTER SET utf8,
  `fb_uid` text CHARACTER SET utf8,
  `ulevel` int(11) NOT NULL,
  `referral_code` text,
  `num_users` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `active` text,
  `options` text,
  `parent` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `password`, `name`, `gender`, `profile_url`, `username`, `birthday`, `location`, `rel_status`, `image_url`, `fb_uid`, `ulevel`, `referral_code`, `num_users`, `date_added`, `active`, `options`, `parent`) VALUES
(3393, 'mykrocredit@yahoo.com', 'f5d1278e8109edd94e1e4197e04873b9', 'Admin', '', '', '', '', '', '', '', '', 1, '', '', '2014-11-05 19:04:36', '1', '', NULL),
(3431, 'test1@test.com', 'f5d1278e8109edd94e1e4197e04873b9', 'John Doe', NULL, NULL, 'test1@test.com', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2017-10-13 03:11:04', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `vid` int(11) NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `is_used` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`vid`, `prefix`, `code`, `is_used`, `date_added`) VALUES
(1, 'ABC', '00003', 0, '2017-07-31 00:00:00'),
(6, 'ABC', '00004', 0, '0000-00-00 00:00:00'),
(7, 'ABC', '00005', 0, '0000-00-00 00:00:00'),
(8, 'ABC', '00006', 0, '0000-00-00 00:00:00'),
(9, 'ABC', '00007', 0, '0000-00-00 00:00:00'),
(10, 'ABC', '00008', 0, '0000-00-00 00:00:00'),
(11, 'ABC', '00009', 0, '0000-00-00 00:00:00'),
(12, 'ABC', '00010', 0, '0000-00-00 00:00:00'),
(13, 'ABC', '00011', 0, '0000-00-00 00:00:00'),
(14, 'ABC', '00012', 0, '0000-00-00 00:00:00'),
(15, 'ABC', '00013', 0, '0000-00-00 00:00:00'),
(16, 'ABC', '00014', 0, '0000-00-00 00:00:00'),
(17, 'ABC', '00015', 0, '0000-00-00 00:00:00'),
(18, 'ABC', '00016', 0, '0000-00-00 00:00:00'),
(19, 'ABC', '00017', 0, '0000-00-00 00:00:00'),
(20, 'ABC', '00018', 0, '0000-00-00 00:00:00'),
(21, 'ABC', '00019', 0, '0000-00-00 00:00:00'),
(22, 'ABC', '00020', 0, '2017-08-07 09:20:38'),
(23, 'ABC', '00021', 0, '2017-08-07 09:24:25'),
(24, 'ABC', '00022', 0, '2017-08-07 09:25:18'),
(25, 'ABC', '00023', 0, '2017-08-07 09:26:17'),
(26, 'ABC', '00024', 0, '2017-08-07 09:26:59'),
(27, 'ABC', '00025', 0, '2017-08-07 09:33:56'),
(28, 'ABC', '00026', 0, '2017-08-07 09:38:06'),
(29, 'ABC', '00027', 0, '2017-08-07 09:41:03'),
(30, 'ABC', '00028', 0, '2017-08-07 09:48:46'),
(31, 'ABC', '00029', 0, '2017-08-07 09:49:10'),
(32, 'ABC', '00030', 0, '2017-08-07 09:53:08'),
(33, 'ABC', '00031', 0, '2017-08-07 09:56:16'),
(34, 'ABC', '00032', 0, '2017-08-07 09:56:26'),
(35, 'ABC', '00033', 0, '2017-08-07 10:20:19'),
(36, 'ABC', '00034', 0, '2017-08-07 11:06:44'),
(37, 'ABC', '00035', 0, '2017-08-07 11:07:47'),
(38, 'ABC', '00036', 0, '2017-08-07 11:10:28'),
(39, 'ABC', '00037', 0, '2017-08-07 11:11:09'),
(40, 'ABC', '00038', 0, '2017-08-07 11:11:42'),
(41, 'ABC', '00039', 0, '2017-08-07 11:20:59'),
(42, 'ABC', '00040', 0, '2017-08-07 11:21:32'),
(43, 'ABC', '00041', 0, '2017-08-07 11:22:08'),
(44, 'ABC', '00042', 0, '2017-08-07 11:22:50'),
(45, 'ABC', '00043', 0, '2017-08-07 11:24:01'),
(46, 'ABC', '00044', 0, '2017-08-07 11:24:46'),
(47, 'ABC', '00045', 0, '2017-08-07 11:26:08'),
(48, 'ABC', '00046', 0, '2017-08-07 11:27:04'),
(49, 'ABC', '00047', 0, '2017-08-07 11:28:14'),
(50, 'ABC', '00048', 0, '2017-08-07 11:29:02'),
(51, 'ABC', '00049', 0, '2017-08-07 11:29:51'),
(52, 'ABC', '00050', 0, '2017-08-07 11:30:43'),
(53, 'ABC', '00051', 0, '2017-08-07 11:31:02'),
(54, 'ABC', '00052', 0, '2017-08-07 11:31:45'),
(55, 'ABC', '00053', 0, '2017-08-07 11:32:22'),
(56, 'ABC', '00054', 0, '2017-08-07 12:13:12'),
(57, 'ABC', '00055', 0, '2017-08-07 12:18:14'),
(58, 'ABC', '00056', 0, '2017-08-07 12:18:53'),
(59, 'ABC', '00057', 0, '2017-08-07 12:20:03'),
(60, 'ABC', '00058', 0, '2017-08-07 12:20:57'),
(61, 'ABC', '00059', 0, '2017-08-07 12:21:57'),
(62, 'ABC', '00060', 0, '2017-08-07 12:23:30'),
(63, 'ABC', '00061', 0, '2017-08-07 12:24:40'),
(64, 'ABC', '00062', 0, '2017-08-07 12:25:37'),
(65, 'ABC', '00063', 0, '2017-08-07 12:26:33'),
(66, 'ABC', '00064', 0, '2017-08-07 12:28:39'),
(67, 'ABC', '00065', 0, '2017-08-07 12:29:46'),
(68, 'ABC', '00066', 0, '2017-08-07 12:30:59'),
(69, 'ABC', '00067', 0, '2017-08-07 12:31:48'),
(70, 'ABC', '00068', 0, '2017-08-07 12:32:26'),
(71, 'ABC', '00069', 0, '2017-08-07 12:33:23'),
(72, 'ABC', '00070', 0, '2017-08-07 12:34:40'),
(73, 'ABC', '00071', 0, '2017-08-07 12:36:34'),
(74, 'ABC', '00072', 0, '2017-08-07 12:39:21'),
(75, 'ABC', '00073', 0, '2017-08-07 12:40:19'),
(76, 'ABC', '00074', 0, '2017-08-07 12:41:21'),
(77, 'ABC', '00075', 0, '2017-08-07 12:41:45'),
(78, 'ABC', '00076', 0, '2017-08-07 12:42:41'),
(79, 'ABC', '00077', 0, '2017-08-07 12:45:28'),
(80, 'ABC', '00078', 0, '2017-08-07 12:46:24'),
(81, 'ABC', '00079', 0, '2017-08-07 12:47:41'),
(82, 'ABC', '00080', 0, '2017-08-07 12:51:57'),
(83, 'ABC', '00081', 0, '2017-08-07 12:53:31'),
(84, 'ABC', '00082', 0, '2017-08-07 12:56:21'),
(85, 'ABC', '00083', 0, '2017-08-07 12:58:56'),
(86, 'ABC', '00084', 0, '2017-08-07 12:59:42'),
(87, 'ABC', '00085', 0, '2017-08-07 13:00:32'),
(88, 'ABC', '00086', 0, '2017-08-07 13:03:07'),
(89, 'ABC', '00087', 0, '2017-08-07 13:03:48'),
(90, 'ABC', '00088', 0, '2017-08-07 13:04:36'),
(91, 'ABC', '00089', 0, '2017-08-07 13:18:41'),
(92, 'ABC', '00090', 0, '2017-08-07 13:22:52'),
(93, 'ABC', '00091', 0, '2017-08-07 13:24:53'),
(94, 'ABC', '00092', 0, '2017-08-07 13:27:12'),
(95, 'ABC', '00093', 0, '2017-08-07 13:28:35'),
(96, 'ABC', '00094', 0, '2017-08-07 14:24:47'),
(97, 'ABC', '00095', 0, '2017-08-07 14:26:22'),
(98, 'ABC', '00096', 0, '2017-08-07 14:32:34'),
(99, 'ABC', '00097', 0, '2017-08-07 14:47:04'),
(100, 'ABC', '00098', 0, '2017-08-07 14:51:06'),
(101, 'ABC', '00099', 0, '2017-08-07 14:53:57'),
(102, 'ABC', '00100', 0, '2017-08-07 14:55:56'),
(103, 'ABC', '00101', 0, '2017-08-07 14:56:02'),
(104, 'ABC', '00102', 0, '2017-08-07 15:00:26'),
(105, 'ABC', '00103', 0, '2017-08-08 08:35:48'),
(106, 'ABC', '00104', 0, '2017-08-08 08:44:34'),
(107, 'ABC', '00105', 0, '2017-08-08 08:45:12'),
(108, 'ABC', '00106', 0, '2017-08-08 09:06:42'),
(109, 'ABC', '00107', 0, '2017-08-08 09:08:00'),
(110, 'ABC', '00108', 0, '2017-08-08 09:33:16'),
(111, 'ABC', '00109', 0, '2017-08-08 09:44:28'),
(112, 'ABC', '00110', 0, '2017-08-08 09:52:24'),
(113, 'ABC', '00111', 0, '2017-08-08 09:54:15'),
(114, 'ABC', '00112', 0, '2017-08-08 09:57:56'),
(115, 'ABC', '00113', 0, '2017-08-08 09:58:03'),
(116, 'ABC', '00114', 0, '2017-08-08 09:59:42'),
(117, 'ABC', '00115', 0, '2017-08-08 10:00:33'),
(118, 'ABC', '00116', 0, '2017-08-08 10:23:21'),
(119, 'ABC', '00117', 0, '2017-08-08 10:26:52'),
(120, 'ABC', '00118', 0, '2017-08-08 10:30:09'),
(121, 'ABC', '00119', 0, '2017-08-08 10:33:37'),
(122, 'ABC', '00120', 0, '2017-08-08 10:33:56'),
(123, 'ABC', '00121', 0, '2017-08-08 10:35:36'),
(124, 'ABC', '00122', 0, '2017-08-08 10:36:05'),
(125, 'ABC', '00123', 0, '2017-08-08 10:36:39'),
(126, 'ABC', '00124', 0, '2017-08-08 10:37:43'),
(127, 'ABC', '00125', 0, '2017-08-08 10:39:13'),
(128, 'ABC', '00126', 0, '2017-08-08 10:40:55'),
(129, 'ABC', '00127', 0, '2017-08-08 10:41:37'),
(130, 'ABC', '00128', 0, '2017-08-08 10:44:53'),
(131, 'ABC', '00129', 0, '2017-08-08 10:50:46'),
(132, 'ABC', '00130', 0, '2017-08-08 10:55:18'),
(133, 'ABC', '00131', 0, '2017-08-08 11:00:39'),
(134, 'ABC', '00132', 0, '2017-08-08 11:04:15'),
(135, 'ABC', '00133', 0, '2017-08-08 11:04:32'),
(136, 'ABC', '00134', 0, '2017-08-08 11:13:16'),
(137, 'ABC', '00135', 0, '2017-08-08 11:14:02'),
(138, 'ABC', '00136', 0, '2017-08-08 11:26:24'),
(139, 'ABC', '00137', 0, '2017-08-08 11:37:50'),
(140, 'ABC', '00138', 0, '2017-08-08 11:40:42'),
(141, 'ABC', '00139', 0, '2017-08-08 12:39:53'),
(142, 'ABC', '00140', 0, '2017-08-08 12:49:53'),
(143, 'ABC', '00141', 0, '2017-08-08 12:52:21'),
(144, 'ABC', '00142', 0, '2017-08-08 12:54:25'),
(145, 'ABC', '00143', 0, '2017-08-08 12:55:09'),
(146, 'ABC', '00144', 0, '2017-08-08 13:06:09'),
(147, 'ABC', '00145', 0, '2017-08-08 13:31:02'),
(148, 'ABC', '00146', 0, '2017-08-08 13:32:46'),
(149, 'ABC', '00147', 0, '2017-08-08 13:33:28'),
(150, 'ABC', '00148', 0, '2017-08-08 13:35:41'),
(151, 'ABC', '00149', 0, '2017-08-08 13:37:34'),
(152, 'ABC', '00150', 0, '2017-08-08 13:39:14'),
(153, 'ABC', '00151', 0, '2017-08-08 13:39:40'),
(154, 'ABC', '00152', 0, '2017-08-08 14:17:38'),
(155, 'ABC', '00153', 0, '2017-08-09 08:48:36'),
(156, 'ABC', '00154', 0, '2017-08-09 08:49:12'),
(157, 'ABC', '00155', 0, '2017-08-09 08:49:32'),
(158, 'ABC', '00156', 0, '2017-08-09 08:50:01'),
(159, 'ABC', '00157', 0, '2017-08-09 08:51:53'),
(160, 'ABC', '00158', 0, '2017-08-09 08:58:31'),
(161, 'ABC', '00159', 0, '2017-08-09 09:13:06'),
(162, 'ABC', '00160', 0, '2017-08-09 15:08:48'),
(163, 'ABC', '00161', 0, '2017-08-09 15:22:53'),
(164, 'ABC', '00162', 0, '2017-08-09 15:34:04'),
(165, 'XYZ', '00001', 0, '2017-10-11 16:39:12'),
(166, '', '00001', 0, '2017-10-13 17:23:33'),
(167, '', '00002', 0, '2017-10-13 17:33:04'),
(168, 'MYK', '00001', 0, '2017-11-02 03:27:16'),
(169, 'MYK', '00002', 0, '2017-11-27 11:23:35'),
(170, 'MYK', '00003', 0, '2017-11-27 11:25:51'),
(171, 'MYK', '00004', 0, '2017-12-04 04:57:44'),
(172, 'MYK', '00005', 0, '2017-12-04 11:21:25'),
(173, 'MYK', '00006', 0, '2017-12-04 12:30:23'),
(174, 'MYK', '00007', 0, '2018-01-05 14:00:22'),
(175, 'MYK', '00008', 0, '2018-01-05 16:54:04'),
(176, 'MYK', '00009', 0, '2018-01-09 16:03:06'),
(177, 'MYK', '00010', 0, '2018-01-09 17:31:32'),
(178, 'MYK', '00011', 0, '2018-01-16 22:38:41'),
(179, 'MYK', '00012', 0, '2018-01-17 02:31:02'),
(180, 'MYK', '00013', 0, '2018-01-17 02:37:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chart_accounts`
--
ALTER TABLE `chart_accounts`
  ADD PRIMARY KEY (`caid`);

--
-- Indexes for table `client_begloanbal`
--
ALTER TABLE `client_begloanbal`
  ADD PRIMARY KEY (`cbb_id`);

--
-- Indexes for table `client_data`
--
ALTER TABLE `client_data`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `client_meta`
--
ALTER TABLE `client_meta`
  ADD PRIMARY KEY (`c_metaid`);

--
-- Indexes for table `finance`
--
ALTER TABLE `finance`
  ADD PRIMARY KEY (`fid`);

--
-- Indexes for table `journal_voucher`
--
ALTER TABLE `journal_voucher`
  ADD PRIMARY KEY (`jvid`);

--
-- Indexes for table `jv_notes`
--
ALTER TABLE `jv_notes`
  ADD PRIMARY KEY (`jvntid`);

--
-- Indexes for table `jv_numbers`
--
ALTER TABLE `jv_numbers`
  ADD PRIMARY KEY (`jvnid`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `ors_numbers`
--
ALTER TABLE `ors_numbers`
  ADD PRIMARY KEY (`ors_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `ulevels`
--
ALTER TABLE `ulevels`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`vid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chart_accounts`
--
ALTER TABLE `chart_accounts`
  MODIFY `caid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `client_begloanbal`
--
ALTER TABLE `client_begloanbal`
  MODIFY `cbb_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_data`
--
ALTER TABLE `client_data`
  MODIFY `client_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `client_meta`
--
ALTER TABLE `client_meta`
  MODIFY `c_metaid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT for table `finance`
--
ALTER TABLE `finance`
  MODIFY `fid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `journal_voucher`
--
ALTER TABLE `journal_voucher`
  MODIFY `jvid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jv_notes`
--
ALTER TABLE `jv_notes`
  MODIFY `jvntid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jv_numbers`
--
ALTER TABLE `jv_numbers`
  MODIFY `jvnid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ors_numbers`
--
ALTER TABLE `ors_numbers`
  MODIFY `ors_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `pid` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ulevels`
--
ALTER TABLE `ulevels`
  MODIFY `level_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3432;
--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `vid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
